package app.omegasoftware.wifitransferpro.date;

import java.sql.Time;
import java.util.Calendar;


import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.listener.HourClickListener;

public class ContainerClickListenerHour {
	
	public Integer __minuto=null;
	public Integer __hora=null;
	private Button __button;
	HourClickListener __listener;
	
	public ContainerClickListenerHour(
			Button pButton, 
			HourClickListener pListener){

        final Calendar c = Calendar.getInstance();
        __hora = c.get(Calendar.HOUR);
    
        __minuto = c.get(Calendar.MINUTE);
        __button = pButton;
        __listener = pListener;
	}
	
	public Time getTime(){
		
		return new Time(__hora, __minuto, 0);
	}
	
	public TextView getButton(){
		return __button;
	}
	public void setTextView(){
		__button.setText(new StringBuilder()
        // Month is 0 based so add 1
        .append(pad(__hora)).append(":")
        .append(pad(__minuto)));
	}
	private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }
	public Integer getHora(){
		return __hora;
	}
		
	public Integer getMinuto(){
		return __minuto;
	}
	
	public void setHora(Integer pHora){
		__hora = pHora;
	}
	
	public void setOfTimeView(Integer pHour, Integer pMinute){
		if(pHour != null && pMinute != null){
			__hora = pHour;
			__minuto = pMinute;
			__listener.setTimeOfView(pHour, pMinute);
		}
	}
	
	public void setMinuto(Integer pMinuto){
		__minuto = pMinuto;
	}
}
