package app.omegasoftware.wifitransferpro.http.imports;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class Mensagens extends InterfaceHTML{
	static String __ultimaMensagem = "";
	static String __ultimoReferer = "";
	public Mensagens(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		String mensagem = "";
		if(__helper.GET("msgSucesso") != null){

			mensagem = __helper.GET("msgSucesso");

		}
		else if(__helper.GET("msgErro") != null){

			mensagem = __helper.GET("msgErro");

		}


		boolean esconder = false;
		if(mensagem != null && mensagem.trim().length() > 0){
			if( __ultimaMensagem.compareTo(mensagem) == 0 &&  
					__ultimoReferer.compareTo(__ultimoReferer) == 0){
				esconder = true;
			}
			else{
				__ultimaMensagem = mensagem;
				__ultimoReferer = __helper.uri;
			}
		}
		else{
			__ultimaMensagem = "";
			esconder = true;
		}
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("<div id=\"caixaDeMensagemSucesso\" class=\"centerDivMensagem okDiv\" style=\"display: none;\">");
		vBuilder.append("<table width=\"100%\" class=\"okTabela\" id=\"tabelaMensagem\">");
		vBuilder.append("<tr>");
		vBuilder.append("<td height=\"70px\" width=\"13%\">");
		vBuilder.append("<img src=\"/adm/imgs/padrao/imgSucesso.png\" />" );
		vBuilder.append("</td>") ;
		vBuilder.append("<td width=\"87%\">" );
		vBuilder.append("<span class=\"textoDivMensagem\" id=\"textoMensagemSucesso\">" + mensagem + "</span>" );
		vBuilder.append("</td>" );
		vBuilder.append("</tr>" );
		vBuilder.append("<tr>" );
		vBuilder.append("<td height=\"30px\" colspan=\"2\" align=\"center\" valign=\"middle\">" );
		vBuilder.append("<input type=\"button\" onclick=\"javascript:document.getElementById('caixaDeMensagemSucesso').style.display='none'\"" );
		vBuilder.append("id=\"botao_sucesso_ok\" value=\"" + __helper.context.getResources().getString(R.string.ok) + "\" class=\"botoes_form \" />" );
		vBuilder.append("</td>" );
		vBuilder.append("</tr>" );
		vBuilder.append("</table>" );
		vBuilder.append("</div>" );

		vBuilder.append("<div id=\"caixaDeMensagemErro\" class=\"centerDivMensagem erroDiv\" style=\"display: none;\">" );
		vBuilder.append("<table width=\"100%\" class=\"erroTabela\" id=\"tabelaMensagem\">" );
		vBuilder.append("<tr>" );
		vBuilder.append("<td height=\"70px\" width=\"13%\">" );
		vBuilder.append("<img src=\"/adm/imgs/padrao/imgErro.png\" />" );
		vBuilder.append("</td>" );
		vBuilder.append("<td width=\"87%\">" );
		vBuilder.append("<span class=\"textoDivMensagem\" id=\"textoMensagemErro\">" + mensagem + "</span>" );
		vBuilder.append("</td>" );
		vBuilder.append("</tr>" );
		vBuilder.append("<tr>" );
		vBuilder.append("<td height=\"30px\" colspan=\"2\" align=\"center\" valign=\"middle\">" );
		vBuilder.append("<input type=\"button\" onclick=\"javascript:document.getElementById('caixaDeMensagemErro').style.display='none'\"" );
		vBuilder.append("id=\"botao_erro_ok\" value=\"" + __helper.context.getResources().getString(R.string.ok) + "\" class=\"botoes_form \" />" );
		vBuilder.append("</td>" );
		vBuilder.append("</tr>" );
		vBuilder.append("</table>" );
		vBuilder.append("</div>" );

		vBuilder.append("<div id=\"caixaDeMensagemDialogo\" class=\"centerDivMensagem dialogo\" style=\"display: none;\">" );
		vBuilder.append("<table width=\"100%\" class=\"okTabela\" id=\"tabelaMensagem\">" );
		vBuilder.append("<tr>" );
		vBuilder.append("<td height=\"70px\" width=\"13%\">" );
		vBuilder.append("<img src=\"/adm/imgs/padrao/imgInfo.png\" />" );
		vBuilder.append("</td>" );
		vBuilder.append("<td width=\"87%\">" );
		vBuilder.append("<span class=\"textoDivMensagem\" id=\"textoMensagemDialogo\">" + mensagem + "</span>" );
		vBuilder.append("</td>" );
		vBuilder.append("</tr>" );
		vBuilder.append("<tr>" );
		vBuilder.append("<td height=\"30px\" colspan=\"2\" align=\"center\" valign=\"middle\">" );
		vBuilder.append("<input type=\"button\"" );
		vBuilder.append("id=\"botao_dialogo_sim\" value=\" " + __helper.context.getResources().getString(R.string.yes)+ "\" class=\"botoes_form \" />" );
		vBuilder.append("<input type=\"button\" onclick=\"javascript:document.getElementById('caixaDeMensagemDialogo').style.display='none'\"" );
		vBuilder.append("id=\"botao_dialogo_nao\" value=\"" + __helper.context.getResources().getString(R.string.no) + "\" class=\"botoes_form \" />" );
		vBuilder.append("</td>" );
		vBuilder.append("</tr>" );
		vBuilder.append("</table>" );
		vBuilder.append("</div>" );

		vBuilder.append("<script language=\"javascript\">" );

		vBuilder.append("jQuery(function() {" );
		vBuilder.append("jQuery( \"#caixaDeMensagemSucesso\" ).draggable();" );
		vBuilder.append("});" );

		vBuilder.append("jQuery(function() {" );
		vBuilder.append("jQuery( \"#caixaDeMensagemErro\" ).draggable();" );
		vBuilder.append("});" );

		vBuilder.append("jQuery(function() {" );
		vBuilder.append("jQuery( \"#caixaDeMensagemDialogo\" ).draggable();" );
		vBuilder.append("});" );

		vBuilder.append("</script>" );

		if(__helper.GET("msgSucesso") != null &&  __helper.GET("msgSucesso").trim().length() > 0 && !esconder){

			vBuilder.append("<script language=\"javascript\">" );

			vBuilder.append("document.getElementById(\"caixaDeMensagemSucesso\").style.display = \"block\";" );
			vBuilder.append("document.getElementById(\"botao_sucesso_ok\").focus();" );

			vBuilder.append("</script>" );

		}

		if(__helper.GET("msgErro") != null && __helper.GET("msgErro").length() > 0 && !esconder){

			vBuilder.append("<script language=\"javascript\">" );

			vBuilder.append("document.getElementById(\"caixaDeMensagemErro\").style.display = \"block\";" );
			vBuilder.append("document.getElementById(\"botao_erro_ok\").focus();" );

			vBuilder.append("</script>" );

		} 
		return new ContainerResponse(__helper, vBuilder);
	}

}
