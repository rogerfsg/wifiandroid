package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Javascript;

public class PopupPlayerList extends InterfaceHTML{

		public PopupPlayerList(HelperHttp pHelper) {
			super(pHelper);
			// TODO Auto-generated constructor stub
		}

		@Override
		public ContainerResponse getContainerResponse() {
			
			
			String vPlayList = __helper.GET("playlist");
			
			StringBuilder vBuilder = new StringBuilder();
			
			if(!(__helper.GET("popup") != null  && __helper.GET("popup").compareTo("false") == 0 ))
				vBuilder.append("<img onclick=\"javascript:window.open('popup.php5?tipo=pages&page=PopupPlayerList&popup=false&playlist=" + vPlayList + "', '12345')\" src=\"" + OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + "new_window.png\" class=\"icones_list\">");
			
			vBuilder.append(Javascript.importarBibliotecaJavascriptPlaylistPlayer(vPlayList));
					
			ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder(vBuilder));
			
			return vContainer;
		}


}
