package app.omegasoftware.wifitransferpro.file;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;
import app.omegasoftware.wifitransferpro.common.FileDateListComparator;
import app.omegasoftware.wifitransferpro.date.HelperDate;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class HelperFile {

	    private HelperFile() {
	        super();
	    }
	    
	    
	    public static String getNomeSemBarra(String pPath){
	    	if(pPath == null || pPath.length() == 0 ) return "";
	    	else{
	    		
	    		int vLastIndex = pPath.lastIndexOf("/");
	    		if(vLastIndex + 1 == pPath.length()){
	    			pPath = pPath.substring(0, pPath.length() - 1);
	    		}
	    		return pPath;
	    	} 
	    }
	    
	    public static String getNameOfFileInPath(String pPath){
	    	if(pPath == null || pPath.length() == 0 ) return "";
	    	else{
	    		
	    		int vLastIndex = pPath.lastIndexOf("/");
	    		if(vLastIndex == pPath.length() - 1)
	    			return "";
	    		else return pPath.substring(vLastIndex + 1, pPath.length() );
	    	}
	    }
	    
	    public static String getLastNameOfPath(String pPath){
	    	if(pPath == null || pPath.length() == 0 ) return "";
	    	else{
	    		
	    		int vLastIndex = pPath.lastIndexOf("/");
	    		if(vLastIndex + 1 == pPath.length()){
	    			pPath = pPath.substring(0, pPath.length() - 1);
	    			vLastIndex = pPath.lastIndexOf("/");
	    		}
	    		if(vLastIndex + 1 < pPath.length() && vLastIndex == 0){
	    			return pPath.substring(vLastIndex, pPath.length() - 1);
	    		} else return pPath ;
	    		
	    		
	    	}
	    }
	    
	    public static String getPathOfFile(String pPath){
	    	if(pPath == null || pPath.length() == 0 ) return "";
	    	else{
	    		
	    		int vLastIndex = pPath.lastIndexOf("/");
	    		return pPath.substring(0, vLastIndex);
	    	}
	    }
	    
		public static boolean deleteDirectory(File pDirectory){
	    	for (File vFileInterno : pDirectory.listFiles()) {
				if(vFileInterno.isDirectory()){
					deleteDirectory(vFileInterno);
				}else vFileInterno.delete();
			}
	    	pDirectory.delete();
	    	return true;
	    }
	    
	    public static String getFileWithUniqueName(){
			HelperDate vDateHelper = new HelperDate();
			String vData = vDateHelper.getDateAndTimeForFileName();
			
			Random vRandom = new Random();
			Integer vRandomNumber = vRandom.nextInt();
			if(vRandomNumber < 0 ) vRandomNumber = -vRandomNumber;
			
			String vFileName = vData + "_" + vRandomNumber.toString(); 
			
			return vFileName;
		}
		public static boolean StoreByteImage(Context mContext, byte[] imageData,
				int quality, String expName) {

	        File sdImageMainDirectory = new File("/sdcard");
			FileOutputStream fileOutputStream = null;
			String nameFile;
			try {

				BitmapFactory.Options options=new BitmapFactory.Options();
				options.inSampleSize = 5;
				
				Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,
						imageData.length,options);

				
				fileOutputStream = new FileOutputStream(
						sdImageMainDirectory.toString() +"/image.jpg");
								
	  
				BufferedOutputStream bos = new BufferedOutputStream(
						fileOutputStream);

				myImage.compress(CompressFormat.JPEG, quality, bos);

				bos.flush();
				bos.close();

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		}
		
		public static String generateRandomFileName(String pExtension)
		{
			if(pExtension == null) return HelperString.generateRandomString();
			else return HelperString.generateRandomString() + pExtension;
		}	
		
		public static long getFilesSize(String pPath)
		{
			long fileSize = 0;

			File baseDirectory = new File(pPath);
			if(baseDirectory.isDirectory())
			{
				for(File file : baseDirectory.listFiles())
				{
					fileSize += file.length();
				}
			}
			return fileSize;
		}

		public static boolean createDirectory(File f, boolean pIsToCreateSubFolders){
			if (!f.exists()) {
				if(pIsToCreateSubFolders){
				String vPath = f.getAbsolutePath();
				
					if(vPath != null && vPath.length() > 0 ){
						String vVetorPath[] = vPath.split("[/]");
						String vSubPath = "";
						for(int i = 0 ; i < vVetorPath.length ; i ++){
							if(vVetorPath[i] != null && vVetorPath.length > 0 ){
								if(vSubPath.length() > 0 )
								vSubPath += "/" + vVetorPath[i];
								else vSubPath += vVetorPath[i];
								HelperFile.createDirectory(new File(vSubPath), false);
							}
						}
					}
				}
				boolean vValidade = f.mkdir();
				return vValidade;
			}
			return false;
		}
		
		public static boolean createDirectory(String p_strPath){
			File f = new File(p_strPath);
			
			if (!f.exists()) {
				boolean vValidade = f.mkdir();
				return vValidade;
			}
			return false;
		}
		public static boolean isDirectoryInAssets(Context pContext, String pPath){
			boolean vIsDirectory = true;
			try{
				
				pContext.getAssets().open(pPath);
				vIsDirectory = false;
			} catch(Exception ex){
				vIsDirectory = true;
			}
			return vIsDirectory;
		}
	// assets/ is root
		public static String[] getListOfFileInDirectoryAssets(String p_strPath, Context p_context){
			
			try{
				AssetManager v_assetManager = p_context.getAssets();
				return v_assetManager.list( p_strPath);	
			}catch (Exception e) {
				// TODO: handle exception
				//Log.e("FileSystemHelper::getListOfFileInDirectoryAssets", "Error when try read path '" + p_strPath + "': " + e.getMessage());
				return null;
			}
		}
		
		public static double getSizeBytes(File[] pVetorFile){
			if(pVetorFile == null || pVetorFile.length == 0 ) return 0;
			else {
				long vTotalSize = 0;
				for (File file : pVetorFile) {
					long vSizeFile = file.length();
					vTotalSize += vSizeFile;
					
				}
				return vTotalSize;
			}
		}
		
		public static String getStrLength(File pFile){
			long len = pFile.length();
			StringBuilder vMsgBuilder = new StringBuilder();
			if ( len < 1024 )
				vMsgBuilder.append(len + " B");
			else if ( len < 1024 * 1024 )
				vMsgBuilder.append(len/1024 + "." + (len%1024/10%100) + " KB");
			else
				vMsgBuilder.append(len/(1024*1024) + "." + len%(1024*1024)/10%100 + " MB");
			return vMsgBuilder.toString();
		}
		
		public static int getTotalFile(File[] pVetorFile, boolean pIsDirectory){
			if(pVetorFile == null || pVetorFile.length == 0 ) return 0;
			else {
				int vTotal = 0;
				for (File file : pVetorFile) {
					if(!file.isDirectory() && !pIsDirectory)
						vTotal += 1;
					else if(file.isDirectory() && pIsDirectory) 
						vTotal += 1; 
				}
				return vTotal;
			}
		}
		
		public static void copyAllDirectory(File pDirectoryOrigin, File pDirectoryDestino){
			File vVetorFileOrigin[] = pDirectoryOrigin.listFiles();
			
			HelperFile.createDirectory(pDirectoryDestino, true);
			for (File vFileOrigin : vVetorFileOrigin) {
				try {
					String vNomeArquivo = HelperFile.getNameOfFileInPath(vFileOrigin.getName());
					if(!vFileOrigin.isDirectory()){
						File vFileOut = new File(HelperHttp.getPathComBarra( pDirectoryDestino.getAbsolutePath()), vNomeArquivo);
						if(!vFileOut.exists())
							HelperFile.copyFile(vFileOrigin, vFileOut);	
					} else {
						copyAllDirectory(vFileOrigin, new File( HelperHttp.getPathComBarra( pDirectoryDestino.getAbsolutePath()), vNomeArquivo));
					}
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
//		
//		public static void copyAllDirectoryFromAssets(File pPathDirectoryOrigin, String pPathDirectoryDestino){
//			HelperFile.createDirectory(pPathDirectoryDestino);
//			
//			File vVetorFileOrigin[] = pPathDirectoryOrigin.listFiles();
//			for (File vFile : vVetorFileOrigin) {
//				try {
//					
//					boolean vIsDirectory = vFile.isDirectory();
//					if(!vIsDirectory){
//						File vFileOut = new File(pPathDirectoryDestino, HelperFile.getLastNameOfPath(vFile.getName()));
//						if(!vFileOut.exists())
//							HelperFile.copyFile(vFile , vFileOut);	
//					} else {
//						copyAllDirectoryFromAssets(pContext, pPathDirectoryAssets + "/"+ vNomeArquivo, HelperHttp.getPathComBarra( pPathDirectoryDestino) + vNomeArquivo);
//					}
//					
//					
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//		
		public static double getFreeSpace(){
			StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
			double sdAvailSize = (double)stat.getAvailableBlocks()
			                   * (double)stat.getBlockSize();
			//One binary gigabyte equals 1,073,741,824 bytes.
			double gigaAvailable = sdAvailSize / 1073741824;
			return gigaAvailable;
		}
		public static void copyAllDirectoryFromAssets(Context pContext, String pPathDirectoryAssets, String pPathDirectoryDestino){
			String vVetorNomeArquivo[] = HelperFile.getListOfFileInDirectoryAssets(pPathDirectoryAssets, pContext);
			
			HelperFile.createDirectory(pPathDirectoryDestino);
			for (String vNomeArquivo : vVetorNomeArquivo) {
				try {
					
					boolean vIsDirectory = HelperFile.isDirectoryInAssets(pContext, pPathDirectoryAssets + "/" + vNomeArquivo);
					if(!vIsDirectory){
						File vFileOut = new File(pPathDirectoryDestino, vNomeArquivo);
						if(!vFileOut.exists())
							HelperFile.copyFileFromAssets(pContext, pPathDirectoryAssets + "/"+ vNomeArquivo, vFileOut);	
					} else {
						copyAllDirectoryFromAssets(pContext, pPathDirectoryAssets + "/"+ vNomeArquivo, HelperHttp.getPathComBarra( pPathDirectoryDestino) + vNomeArquivo);
					}
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		public static byte[] readBytesOfFile(File vFile){

			try
			{	
				
				InputStream inputFile = new FileInputStream(vFile);
				byte vVetorConteudo[] = new byte[(int)vFile.length()];
				
				int TotalLength = 0;
				try
				{
					TotalLength = inputFile.available();
				}
				catch ( IOException e)
				{
					//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "File don't is available: " + v_pathFile + ". Error: " + e.getMessage() );
					return null;
				}

				// Reading and writing the file Method 1 :
				byte[] buffer = new byte[256];
				int len = 0;
				int cont = 0 ;
				while(len != -1){
					try
					{
						len = inputFile.read(buffer);
						for(int i = 0 ; i < len; i++, cont++){
							vVetorConteudo[cont] = buffer[i];
						}
					}
					catch ( IOException e)
					{
						//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "Can't read file: " + v_pathFile + ". Error: " + e.getMessage() );					
						return null;
					}
					if(len > 0 ){
							
					}
						
				}
				inputFile.close();
				    
				return vVetorConteudo;	
			}
			catch( IOException e)
			{
				return null;
			}
			
		}
		
		public static boolean copyFile(File vFileOrigin, File destino){

			try
			{
				FileOutputStream Fos = new FileOutputStream( destino);

				InputStream inputFile = new FileInputStream(vFileOrigin);

				int TotalLength = 0;
				try
				{
					TotalLength = inputFile.available();
				}
				catch ( IOException e)
				{
					//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "File don't is available: " + v_pathFile + ". Error: " + e.getMessage() );
					return false;
				}

				// Reading and writing the file Method 1 :
				byte[] buffer = new byte[256];
				int len = 0;
				while(len != -1){
					try
					{
						len = inputFile.read(buffer);
					}
					catch ( IOException e)
					{
						//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "Can't read file: " + v_pathFile + ". Error: " + e.getMessage() );					
						return false;
					}
					if(len > 0 ){
						Fos.write( buffer, 0 , len );	
					}
						
				}
				inputFile.close();
				    
				Fos.close();
			}
			catch( IOException e)
			{
				return false;
			}
			return true;
		}
		
		public static boolean copyFileFromAssets(Context pContext, String pPathAssets, File destino){

			try
			{
				FileOutputStream Fos = new FileOutputStream( destino);

				InputStream inputFile = pContext.getAssets().open( pPathAssets);

				int TotalLength = 0;
				try
				{
					TotalLength = inputFile.available();
				}
				catch ( IOException e)
				{
					//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "File don't is available: " + v_pathFile + ". Error: " + e.getMessage() );
					return false;
				}

				// Reading and writing the file Method 1 :
				byte[] buffer = new byte[256];
				int len = 0;
				while(len != -1){
					try
					{
						len = inputFile.read(buffer);
					}
					catch ( IOException e)
					{
						//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "Can't read file: " + v_pathFile + ". Error: " + e.getMessage() );					
						return false;
					}
					if(len > 0 ){
						Fos.write( buffer, 0 , len );	
					}
						
				}
				inputFile.close();
				    
				Fos.close();
			}
			catch( IOException e)
			{
				return false;
			}
			return true;
		}
		
		public static boolean copyFile(Context pContext, String pPathAssets, File destino){

			try
			{
				FileOutputStream Fos = new FileOutputStream( destino);
				
				InputStream inputFile = new FileInputStream(new File(pPathAssets));

				int TotalLength = 0;
				try
				{
					TotalLength = inputFile.available();
				}
				catch ( IOException e)
				{
					//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "File don't is available: " + v_pathFile + ". Error: " + e.getMessage() );
					return false;
				}

				// Reading and writing the file Method 1 :
				byte[] buffer = new byte[256];
				int len = 0;
				while(len != -1){
					try
					{
						len = inputFile.read(buffer);
					}
					catch ( IOException e)
					{
						//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "Can't read file: " + v_pathFile + ". Error: " + e.getMessage() );					
						return false;
					}
					if(len > 0 ){
						Fos.write( buffer, 0 , len );	
					}
						
				}
				inputFile.close();
				    
				Fos.close();
			}
			catch( IOException e)
			{
				return false;
			}
			return true;
		}
		
		//p_vetorFilePath - the path have to be considering 'assets/' as root.
		public static boolean mergeListFileInAssetsInFile(String p_vetorFilePath[], 
				String p_strPathOutFile, 
				Context p_context){

			try
			{
				FileOutputStream Fos = new FileOutputStream( p_strPathOutFile );
				for (String  v_pathFile : p_vetorFilePath) {

					InputStream inputFile = p_context.getAssets().open( v_pathFile);

					int TotalLength = 0;
					try
					{
						TotalLength = inputFile.available();
					}
					catch ( IOException e)
					{
						//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "File don't is available: " + v_pathFile + ". Error: " + e.getMessage() );
						return false;
					}

					// Reading and writing the file Method 1 :
					byte[] buffer = new byte[256];
					int len = 0;
					while(len != -1){
						try
						{
							len = inputFile.read(buffer);
						}
						catch ( IOException e)
						{
							//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "Can't read file: " + v_pathFile + ". Error: " + e.getMessage() );					
							return false;
						}
						if(len > 0 ){
							Fos.write( buffer, 0 , len );	
						}
							
					}
					inputFile.close();
				}    
				Fos.close();
			}
			catch( IOException e)
			{
				return false;
			}
			return true;
		}


		public static boolean CutFilesInSizeParts(String InputFileName, String OutputFileName, int MaxPartSize)
		{
			try
			{
				File f = new File(InputFileName);
				FileInputStream fis = new FileInputStream(f);
				int TotalLength = fis.available();
				byte[] buffer = new byte[ TotalLength + 1 ];
				int len = fis.read( buffer );

				int nbPart = len / MaxPartSize + 1;
				int CurPos = 0;

				for ( int i = 0; i < nbPart; i++ )    
				{
					int PartLen = MaxPartSize;
					if ( CurPos + PartLen >= len )
						PartLen = len - CurPos;
					String outRealFileName = OutputFileName + (i+1);
					FileOutputStream fos = new FileOutputStream(outRealFileName);
					fos.write(buffer, CurPos, PartLen);
					CurPos += PartLen;
				}
			}
			catch( IOException e)
			{
				return false;
			}
			return true;
		}

		public static File getFileModified(String pPath , boolean pIsFirstFileModifie, FileFilter pFileFilter){
			
			
			File[] vListFileSort = HelperFile.getFilesOrderByLastDateModified(pPath, pIsFirstFileModifie, pFileFilter);
			if(vListFileSort == null) return null;
			else if(vListFileSort.length == 0 ) return null;
			if(pIsFirstFileModifie)
			return vListFileSort[0];
			else return vListFileSort[vListFileSort.length - 1];
			
		}
		
		public static ArrayList<File> filter(ArrayList<File> pListRetorno, File pDiretorioCorrente, FilenameFilterLikeAndExtension pFilterFile, int pLimiteBusca){

			if(pDiretorioCorrente.isDirectory()){
				File vVetorFileAccept[] = pDiretorioCorrente.listFiles(pFilterFile);
				if(vVetorFileAccept != null && vVetorFileAccept.length > 0 )
					for (File file : vVetorFileAccept) {
						pListRetorno.add(file);
						if(pListRetorno.size() >=  pLimiteBusca && pLimiteBusca > 0) return pListRetorno;
					}
				
				for (File vDiretorio : pDiretorioCorrente.listFiles(new FilterFileIsDirectory(true))) {
					pListRetorno = filter(pListRetorno, vDiretorio, pFilterFile, pLimiteBusca);
					if(pListRetorno.size() >=  pLimiteBusca && pLimiteBusca > 0) return pListRetorno;
				}	
			}
			return pListRetorno;
		}

		public static ArrayList<File> searchFile(File pDiretorioCorrente, String pVetorNome[], String pVetorExtension[], int pLimiteBusca){

			FilenameFilterLikeAndExtension vFilterFile = null;
			if(pVetorExtension.length > 0 || pVetorNome.length > 0 ){
				if(pVetorExtension.length == 0 )
					vFilterFile = new FilenameFilterLikeAndExtension(pVetorNome, pVetorExtension, true);
				else if(pVetorExtension.length > 0 )
					vFilterFile = new FilenameFilterLikeAndExtension(pVetorNome, pVetorExtension, false);
			}
			ArrayList<File> vListRetorno = new ArrayList<File>();
			vListRetorno = filter(vListRetorno, pDiretorioCorrente, vFilterFile, pLimiteBusca);

			return vListRetorno;
		}

		
		public static File[] getFilesOrderByLastDateModified( String pPath, boolean pIsAsc, FileFilter pFileFilter){
			
			File vDirectory = new File(pPath);
			ArrayList<File> vList = new ArrayList<File>(); 
			if(vDirectory.isDirectory()){
				File vVetorFile[] = vDirectory.listFiles(new FilterFileIsDirectory(false));
				
				for (File vFile : vVetorFile) {
					vList.add(vFile);
				}
				
				Collections.sort(vList, new FileDateListComparator(pIsAsc));
			}
			
			File[] vNewVetorFile = new File[vList.size()];
			vList.toArray(vNewVetorFile);
			
			return vNewVetorFile;
		}
		
	    public static String getFileContents(final File file) throws IOException {
	        final InputStream inputStream = new FileInputStream(file);
	        final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	        
	        final StringBuilder stringBuilder = new StringBuilder();
	        
	        boolean done = false;
	        
	        while (!done) {
	            final String line = reader.readLine();
	            done = (line == null);
	            
	            if (line != null) {
	                stringBuilder.append(line);
	            }
	        }
	        
	        reader.close();
	        inputStream.close();
	        
	        return stringBuilder.toString();
	    }

}
