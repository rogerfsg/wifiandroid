package app.omegasoftware.wifitransferpro.http.lists;

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOImagemArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.ContainerTypeOrderFile;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.file.ContainerTypeOrderFile.TYPE_ORDER;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.pages.InformacaoTelefonePagina;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Link;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class ListsImagem extends InterfaceHTML {

	private enum TYPE{
		IMAGE,
		CAMERA,
		ALL
	}
	private static int LIMIT_TUPLA = 30;
	private TYPE getType(){
		String vStrType = __helper.GET("type");
		if(vStrType == null || vStrType.length() == 0 )
			return TYPE.ALL;
		else{
			if(vStrType.compareTo("photo") == 0) return TYPE.IMAGE;
			else if(vStrType.compareTo("camera") == 0) return TYPE.CAMERA;
			else return TYPE.ALL; 
		}
	}
	
	public String getStrType(TYPE pType){
		switch (pType) {
		case IMAGE:
			return "photo";
			
		case CAMERA:
			
			return "camera";
		case ALL:
			
			return "all";

		default:
			return "all";
		}
	}

	public ListsImagem(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}

	

	
	private Integer getCount(){
//		Se a contagem ja tiver sido realizada, entao nao ha a necessidade de realiza-la novamente
		if(__helper.GET("numero_paginas") != null && __helper.GET("numero_paginas").length() > 0 ){
			return HelperInteger.parserInt( __helper.GET("numero_paginas"));
		}else{
			TYPE vType = getType();
			Database db = new DatabaseWifiTransfer(__helper.context);
			EXTDAOImagemArquivo vObjImagemArquivo = new EXTDAOImagemArquivo(db);

			String vStrLimitInferior = __helper.GET("pagina");
			Integer vLimiteInferior = HelperInteger.parserInt(vStrLimitInferior);
			Integer vCount = 0;
			try{
				switch (vType) {
				case IMAGE:
//					ArrayList<File> vList = vObjImagemArquivo.getListFileImagem(null, new String[]{"jpg", "jpeg"});	
					vCount = vObjImagemArquivo.getCountListFileImagemFast(null, new String[]{"jpg", "jpeg"}, new String[]{"."});
					break;
				case CAMERA:
					ContainerTypeFile vContainerTypeFileImagem = ContainerTypeFile.getContainerTypeFile(TYPE_FILE.IMAGE);
					vCount = vObjImagemArquivo.getCountListFileImagemFast(new String[]{"/sdcard/camera/"}, vContainerTypeFileImagem.getVetorExtensao(), null);
					break;
				case ALL:
//					ArrayList<File> vList3 = vObjImagemArquivo.getListFileImagem();

					vCount = vObjImagemArquivo.getCountListFileImagemFast(null, null, null);
					break;
				default:
					return null;
				}
				vCount = (int) Math.ceil(vCount / HelperHttp.BOTOES_DE_PAGINACAO_POR_LINHA);
				return vCount;
			}catch(Exception ex){
				return null;
			}finally{
				db.close();
			}	
		}
		
	}
	
	private ArrayList<File> getListFile(){
		TYPE vType = getType();
		Database db = new DatabaseWifiTransfer(__helper.context);
		EXTDAOImagemArquivo vObjImagemArquivo = new EXTDAOImagemArquivo(db);

		String vStrPagina = __helper.GET("pagina");
		Integer vPagina = HelperInteger.parserInteger(vStrPagina);
		
		Integer vLimiteInferior = (vPagina == null ) ? 0 : (vPagina - 1) * LIMIT_TUPLA ;
		try{
			switch (vType) {
			case IMAGE:
//				ArrayList<File> vList = vObjImagemArquivo.getListFileImagem(null, new String[]{"jpg", "jpeg"});	
				ArrayList<File> vList = vObjImagemArquivo.getListFileImagemFast(null, new String[]{"jpg", "jpeg"}, new String[]{"."}, vLimiteInferior, LIMIT_TUPLA);
				
				return vList;
			case CAMERA:
				ContainerTypeFile vContainerTypeFileImagem = ContainerTypeFile.getContainerTypeFile(TYPE_FILE.IMAGE);
				ArrayList<File> vList2 = vObjImagemArquivo.getListFileImagemFast(new String[]{"/sdcard/DCIM/Camera/"}, vContainerTypeFileImagem.getVetorExtensao(), null, vLimiteInferior, LIMIT_TUPLA);
				return vList2;		

			case ALL:
//				ArrayList<File> vList3 = vObjImagemArquivo.getListFileImagem();
				ArrayList<File> vList3 = vObjImagemArquivo.getListFileImagemFast(null, null, null, vLimiteInferior, LIMIT_TUPLA);
				return vList3;


			default:
				return null;
			}
		}catch(Exception ex){
			return null;
		}finally{
			db.close();
		}
	}

	@Override
	public ContainerResponse getContainerResponse() {
		String vStrPath = __helper.GET("path");
		if (vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		vStrPath = HelperFile.getNomeSemBarra(vStrPath);
		vStrPath += "/";
		StringBuilder vBuilder = new StringBuilder();
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		if (vStrPath != null && vStrPath.length() > 0) {
			File vDirectory = new File(vStrPath);
			if (vDirectory.isDirectory()) {

				ContainerTypeOrderFile vContainerOrder = new ContainerTypeOrderFile();
				String vLinkOrder = vContainerOrder.getLinkHTML(null, null);

				vContainerOrder.setByGet(__helper);
				vBuilder.append("<td id=\"td_left\" >");
				vBuilder.append("      <form name=\"form_arquivo\" id=\"form_arquivo\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
				vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");
				
				vBuilder.append("           <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
				vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
				vBuilder.append("           <input type=\"hidden\" name=\"page\" id=\"page\" value=\"page\">");
				vBuilder.append("           <input type=\"hidden\" name=\"tipo\" id=\"tipo\" value=\"lists\">");


				vBuilder.append("            <input type=\"hidden\" name=\""
						+ EXTDAOArquivo.NOME + "1\" id=\"" + EXTDAOArquivo.NOME
						+ "1\" value=\"" + __helper.GET(EXTDAOArquivo.NOME)
						+ "\">");
				vBuilder.append("            <input type=\"hidden\" name=\""
						+ EXTDAOArquivo.PATH + "1\" id=\"" + EXTDAOArquivo.PATH
						+ "1\" value=\"" + vStrPath + "\">");


				//				TABELA CABECALHO =================================================================================================================
				vBuilder.append("	<table width=\"350\" align=\"top\" class=\"tabela_list\">");

				vBuilder.append("		<colgroup>");
				vBuilder.append("			<col width=\"33%\" />");
				vBuilder.append("			<col width=\"33%\" />");
				vBuilder.append("			<col width=\"33%\" />");
				vBuilder.append("		</colgroup>");
				vBuilder.append("		<thead>");
				vBuilder.append("		<tr class=\"tr_list_titulos\">");
				vBuilder.append("			<td class=\"td_list_titulos_arredondados\">");

				vBuilder.append(" <img class=\"icones_list\" src=\""
						+ OmegaFileConfiguration
						.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
						+ "icon_camera_phone_48.png\" "
						+ "onmouseover=\"javascript:tip('"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_DIRETORIO)
						+ "')\" "
						+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=imagem&"
						+ "type=camera"
						+ "'\""
						+ "onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(__helper.context.getResources().getString(R.string.camera));

				vBuilder.append("    		</td>");
				vBuilder.append("			<td class=\"td_list_titulos_arredondados\">");
				
				vBuilder.append(" <img class=\"icones_list\" src=\""
						+ OmegaFileConfiguration
						.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
						+ "icon_photo_galery_48.png\" "
						+ "onmouseover=\"javascript:tip('"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_DIRETORIO)
						+ "')\" "
						+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=imagem&"
						+ "type=photo"
						+ "'\""
						+ "onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(__helper.context.getResources().getString(R.string.fotos));
				
				vBuilder.append("    		</td>");
				vBuilder.append("			<td class=\"td_list_titulos_arredondados\">");
				
				vBuilder.append(" <img class=\"icones_list\" src=\""
						+ OmegaFileConfiguration
						.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
						+ "icon_galery_48.png\" "
						+ "onmouseover=\"javascript:tip('"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_DIRETORIO)
						+ "')\" "
						+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=imagem&"
						+ "type=all"
						+ "'\""
						+ "onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(__helper.context.getResources().getString(R.string.todas_as_imagens));
				
				vBuilder.append("    		</td>");
				vBuilder.append("    	</tr>");
				vBuilder.append("		</thead>");
				vBuilder.append("  	</table>");



				//		TABELA CABECALHO =================================================================================================================
				vBuilder.append("    		<table width=\"350\" align=\"top\" class=\"tabela_list\">");


				vBuilder.append("<colgroup>");
				vBuilder.append("<col width=\"33%\" />");
				vBuilder.append("<col width=\"33%\" />");
				vBuilder.append("<col width=\"33%\" />");
				vBuilder.append("</colgroup>");
				vBuilder.append("<thead>");

				vBuilder.append("<tr class=\"tr_list_titulos\">");

				//		NOME ======================================================================================

				vBuilder.append("<td class=\"td_list_titulos\">");
				vBuilder.append(vContainerOrder.getButtonTitulo(
						__helper.context,
						HelperString.ucFirst(__helper.context.getResources().getString(
								R.string.arquivo_nome)), "lists", "imagem",
								EXTDAOArquivo.PATH + "=" + vStrPath, TYPE_ORDER.NAME));
				vBuilder.append(vContainerOrder.getImageHTML(TYPE_ORDER.NAME, "lists", "imagem", EXTDAOArquivo.PATH + "=" + vStrPath));
				vBuilder.append("</td>");
				//		DATA =========================================================================================
				vBuilder.append("<td class=\"td_list_titulos\">");
				vBuilder.append(vContainerOrder.getButtonTitulo(
						__helper.context,
						HelperString.ucFirst( __helper.context.getResources().getString(
								R.string.arquivo_data)), "lists", "imagem",
								EXTDAOArquivo.PATH + "=" + vStrPath, TYPE_ORDER.DATE));
				vBuilder.append(vContainerOrder.getImageHTML(TYPE_ORDER.DATE, "lists", "imagem", EXTDAOArquivo.PATH + "=" + vStrPath));

				vBuilder.append("</td>");
				//		TAMANHO =========================================================================================
				vBuilder.append("<td class=\"td_list_titulos\">");
				vBuilder.append(vContainerOrder.getButtonTitulo(
						__helper.context,
						HelperString.ucFirst(__helper.context.getResources().getString(
								R.string.arquivo_tamanho)), "lists", "imagem",
								EXTDAOArquivo.PATH + "=" + vStrPath, TYPE_ORDER.SIZE));
				vBuilder.append(vContainerOrder.getImageHTML(TYPE_ORDER.SIZE, "lists", "imagem", EXTDAOArquivo.PATH + "=" + vStrPath));
				vBuilder.append("</td>");

				vBuilder.append("</tr>");

				vBuilder.append("</thead>");
				vBuilder.append("    		</table>");


				//		TABELA ==================================================================================================================================
				vBuilder.append("<table class=\"tabela_list\">");


				vBuilder.append("<tbody>");


				ArrayList<File> vListFile = getListFile();

				if (vListFile == null || vListFile.size() == 0) {
					vBuilder.append("<tr class=\"tr_list_conteudo_impar\">");
					vBuilder.append("<td >");
					vBuilder.append(HelperHttp.imprimirMensagem(
							HelperString.ucFirst(context.getResources().getString(
									R.string.imagens_nao_carregadas)),
									HelperHttp.TYPE_MENSAGEM.MENSAGEM_OK, null));
					vBuilder.append("</td>");
					vBuilder.append("</tr>");
				} else {

					vBuilder.append("<tr class=\"tr_list_conteudo_impar\">");
					vBuilder.append("<td  class=\"td_list_conteudo\">");
					vBuilder.append("<div  class=\"div_galeria\">");
					for (int i = 0; i < vListFile.size(); i++) {
						try{
							File vFile = vListFile.get(i);
							String vFilePath = vFile.getAbsolutePath();

							String vPath = HelperFile.getPathOfFile(vFilePath);
							vPath = HelperFile.getNomeSemBarra(vPath);
							String vFileName = HelperFile.getNameOfFileInPath(vFilePath);
							//						String vFileName = HelperFile.getNomeArquivo(vFile.getAbsolutePath());
							vBuilder.append("<div  class=\"div_imagem_amostra\">");

							Properties vLinkImage = new Properties();
							vLinkImage
							.put("onmouseover",
									"tip(\'"
											+ __helper
											.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_VISUALIZAR_IMAGEM)
											+ "\', this);");
							vLinkImage.put("onmouseout", "notip();");
							Link vObjLinkImage = new Link(
									600,
									600,
									context.getResources().getString(
											R.string.imagem),
											"image.php5?tipo=pages&page=ImageArquivo&"
													+ "complete_path=" + vFilePath,
													vLinkImage,
													"<img class=\"imagem_amostra\" src=\""
															+ "image.php5?tipo=pages&page=ImageArquivo&"
															+ "complete_path=" + vFilePath
															+ "\" onmouseover=\"javascript:tip('"
															+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_VISUALIZAR_IMAGEM)
															+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
															true);
							vBuilder.append(vObjLinkImage.montarLink());
							vBuilder.append("</br>");
							vBuilder.append(vFileName);
							
							vBuilder.append("</br>");
							vBuilder.append(" <img class=\"icones_list\" src=\""
									+ OmegaFileConfiguration
									.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
									+ "icon_folder_link.png\" "
									+ "onmouseover=\"javascript:tip('"
									+ __helper
									.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_DIRETORIO)
									+ "')\" "
									+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&"
									+ EXTDAOArquivo.PATH + "="
									+ vPath
									+ "'\""
									+ "onmouseout=\"javascript:notip()\">&nbsp;");
							
							vBuilder.append("</br>");
							vBuilder.append("<img class=\"icones_list\" " +
									"src=\"" +  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
									+ "icone_download.png\""
									+ " onclick=\""
									+ "javascript:document.location.href='"
									+"download.php5?tipo=pages&page=DownloadArquivo&"
									+ "path_completo="
									+ vPath + "/" + vFileName + "'\">&nbsp;");
							
							vBuilder.append("</div>");		
						}catch(Exception ex){

						}
					}

					vBuilder.append("</div>");
					vBuilder.append("</td>");
					vBuilder.append("</tr>");	
					vBuilder.append("</tbody>");
					vBuilder.append("</table>");

					vBuilder.append("<br/>");
					vBuilder.append("<br/>");
					TYPE vType = getType();
					String vURL = "index.php5?tipo=lists&page=imagem&" + "type=" + getStrType(vType);
					String vStrPagina = __helper.GET("pagina");
					vStrPagina = (vStrPagina == null || vStrPagina.length() == 0 ) ? "" : vStrPagina;
					Integer vPagina = HelperInteger.parserInteger(vStrPagina);
					StringBuilder vBuilderHTMLPaginacao = HelperHttp.getHTMLPaginacao(getCount(), vPagina, vURL, "");
					if(vBuilderHTMLPaginacao != null)
						vBuilder.append(vBuilderHTMLPaginacao);
					
					vBuilder.append("</form>");
					vBuilder.append("</td>");

					//				Direita Arquivo ============================================================
					vBuilder.append("<td id=\"td_right\">");

					//				Informacao Telefone Arquivo ============================================================				
					vContainer.appendData(vBuilder);
					ContainerResponse vContainerInformacaoTelefonePagina = new InformacaoTelefonePagina(__helper).getContainerResponse();
					vContainer.copyContainerResponse(vContainerInformacaoTelefonePagina);
					vBuilder = new StringBuilder();

					vBuilder.append("</td>");

					

				}
				vContainer.appendData(vBuilder);

			}
		}

		return vContainer;
	}

}
