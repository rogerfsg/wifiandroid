package app.omegasoftware.wifitransferpro.mapa;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.google.android.maps.GeoPoint;

import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;



import android.location.Address;
import android.location.Geocoder;
import android.view.MotionEvent;
import android.widget.Toast;

public class AdressTouchOverlay extends Overlay{
	private MapView m_mapView;
	
	
	
	
	public AdressTouchOverlay(MapView p_mapView) {
		
		this.m_mapView = p_mapView;
	
	}
	
	public boolean onTouchEvent(MotionEvent event, MapView mapView) 
	{   
		//---when user lifts his finger---
		if (event.getAction() == 1) {                
			GeoPoint p = mapView.getProjection().fromPixels(
					(int) event.getX(),
					(int) event.getY());

			Geocoder geoCoder = new Geocoder(
					m_mapView.getContext(), Locale.getDefault());
			try {
				List<Address> addresses = geoCoder.getFromLocation(
						p.getLatitudeE6()  / 1E6, 
						p.getLongitudeE6() / 1E6, 1);

				String add = "";
				if (addresses.size() > 0) 
				{
					for (int i=0; i<addresses.get(0).getMaxAddressLineIndex(); 
					i++)
						add += addresses.get(0).getAddressLine(i) + "\n";
				}

				Toast.makeText(m_mapView.getContext(), add, Toast.LENGTH_SHORT).show();
			}
			catch (IOException e) {                
				e.printStackTrace();
			}   
			return true;
		}
		else                
			return false;
	}        
}

