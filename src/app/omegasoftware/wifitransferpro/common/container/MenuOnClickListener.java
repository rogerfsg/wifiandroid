package app.omegasoftware.wifitransferpro.common.container;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.wifitransferpro.TabletActivities.FactoryMenuActivityMobile;

public class MenuOnClickListener  implements OnClickListener {
		
		FactoryMenuActivityMobile __activity; 
		Class<?> __class;
		String __tag;
		Bundle __bundle;
		
		public MenuOnClickListener(
				FactoryMenuActivityMobile pActivity, 
				Class<?> pClass,
				Bundle pBundle){
			__class = pClass;
			__activity = pActivity;
			__bundle = pBundle;
			 
		}	
		
		public MenuOnClickListener(
				FactoryMenuActivityMobile pActivity, 
				Class<?> pClass){
			__class = pClass;
			__activity = pActivity;
			 
		}	
		public MenuOnClickListener(
				FactoryMenuActivityMobile pActivity, 
				Class<?> pClass,
				String pTag){
			__class = pClass;
			__activity = pActivity;
			__tag = pTag;
		}	
		
		public void onClick(View v) {
			
			new LoaderTask(__activity, __class, __tag, __bundle).execute();
			
		}
		
	
}
