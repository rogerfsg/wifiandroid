package app.omegasoftware.wifitransferpro.file;

import java.io.File;

import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;

public class ContainerTypeFile{
	public static enum TYPE_FILE {
		DOC,
		EXCEL,
		EXE,
		HTML,
		IMAGE,
		MUSIC,
		VIDEO,
		XML,
		PDF,
		ZIP,
		FOLDER,
		FILE
	}
	
	public TYPE_FILE typeFile;
	
	public static ContainerTypeFile __vetorContainerTypeFile[] = new ContainerTypeFile[]{
		new ContainerTypeFile(TYPE_FILE.FILE,"file", "icon_file.png"),
		new ContainerTypeFile(TYPE_FILE.FOLDER,"folder", "icon_folder.png"),
    	new ContainerTypeFile(TYPE_FILE.DOC, "doc", "icon_word.png", "doc"),
    	new ContainerTypeFile(TYPE_FILE.EXCEL, "excel", "icon_excel.png", "xls"),
    	new ContainerTypeFile(TYPE_FILE.EXE, "exe", "icon_exe.png", "exe"),
    	new ContainerTypeFile(TYPE_FILE.HTML, "html", "icon_html.png", new String[]{"html", "htm"}),
    	new ContainerTypeFile(TYPE_FILE.IMAGE,"image", "icon_image.png", new String[]{"png", "jpg", "icon", "mpg", "gif", "jpeg", "bmp", "dib", "tif", "tiff", "jfif"}),
    	new ContainerTypeFile(TYPE_FILE.MUSIC,"music", "icon_music.png", new String[]{"wma", "mp3"}), 
    	new ContainerTypeFile(TYPE_FILE.VIDEO,"video", "icon_video.png", new String[]{"3gp", "avi", "mp4", "rmvb", "mkv", "fla", "wmv"}),
    	new ContainerTypeFile(TYPE_FILE.XML, "xml", "icon_xml.png", new String[]{"xml"}),
    	new ContainerTypeFile(TYPE_FILE.PDF,"pdf", "icon_pdf.png", new String[]{"pdf"}),
    	new ContainerTypeFile(TYPE_FILE.ZIP, "zip", "icon_zip.png", new String[]{"zip", "gzip", "7zip"}),
    	};
	
	private String vetorExtensao[];
	private String image;
	private String label;

	public static String getExtensao(String pNomeArquivo){
		int vLastIndex = pNomeArquivo.lastIndexOf(".");
		if(vLastIndex < 0 ) return null;
		else{
    		String vExtensao = pNomeArquivo.substring(vLastIndex + 1, pNomeArquivo.length() ); 
    		return vExtensao;
		}	
	}
	public static String getPathImagemHTML(File pFile){
    	ContainerTypeFile vContainer = ContainerTypeFile.getContainerTypeFile(pFile);
    	if(vContainer != null)
    		return vContainer.getImage();
    	
		return null;
    }
	public static ContainerTypeFile getContainerTypeFile(TYPE_FILE pTypeFile){
		for (ContainerTypeFile vContainerTypeFile : __vetorContainerTypeFile) {
			if(vContainerTypeFile.typeFile == pTypeFile) return vContainerTypeFile;
		}
		return null;
	}
	public static ContainerTypeFile getContainerTypeFile(File pFile){
		if(pFile.isDirectory()) return getContainerTypeFile(TYPE_FILE.FOLDER);
		
		String vNomeArquivo = pFile.getName();
		if(vNomeArquivo == null || vNomeArquivo.length() == 0 ){
			return getContainerTypeFile(TYPE_FILE.FILE);
    	} else {
    		String vExtensao = getExtensao(vNomeArquivo);
    		for (ContainerTypeFile vContainer : __vetorContainerTypeFile) {
    			if(vContainer.isExtensao(vExtensao))
    				return vContainer;
			}
    	}
		return getContainerTypeFile(TYPE_FILE.FILE);
	}
	
	public ContainerTypeFile(TYPE_FILE pTypeFile, String pLabel, String pImagem){
		typeFile = pTypeFile;
		vetorExtensao = null;
		image = OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + pImagem;;
		label = pLabel;
	}
	
	public ContainerTypeFile(TYPE_FILE pTypeFile, String pLabel, String pImagem, String pVetorExtensao[]){
		typeFile = pTypeFile;
		vetorExtensao = pVetorExtensao;
		image = OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + pImagem;;
		label = pLabel;
	}
	
	
	public ContainerTypeFile(TYPE_FILE pTypeFile,String pLabel, String pImagem, String pExtensao){
		typeFile = pTypeFile;
		vetorExtensao = new String[]{pExtensao};
		image = OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + pImagem;
		label = pLabel;
	}
	
	public String getLabel(){
		return label;
	}
	public String getImage(){
		return image;
	}
	public String[] getVetorExtensao(){
		return vetorExtensao;
	}
	
	public static ContainerTypeFile[] getOrderVetorContainerTypeFile(boolean pIsAsc){
		ContainerTypeFile vVetor [] = new ContainerTypeFile[__vetorContainerTypeFile.length];
		for(int i = 0 ; i < __vetorContainerTypeFile.length; i++){
			String vLabel = __vetorContainerTypeFile[i].getLabel();
			ContainerTypeFile vRef = __vetorContainerTypeFile[i];
			for(int j = 0 ; j < __vetorContainerTypeFile.length; j++){
				if(__vetorContainerTypeFile[j].getLabel().compareTo(vLabel) < 0 && pIsAsc)
					vRef = __vetorContainerTypeFile[j];
				else if (__vetorContainerTypeFile[j].getLabel().compareTo(vLabel) > 0 && !pIsAsc)
					vRef = __vetorContainerTypeFile[j];
					
			}
			vVetor[i] = vRef;
		}
		return vVetor;
	}
	
	public boolean isExtensao(String pExtensao){
		if(pExtensao == null || pExtensao.length() == 0 ) return false;
		else if(vetorExtensao == null) return false;
		else {
			for (String vExtensao : vetorExtensao) {
				if(vExtensao.compareTo(pExtensao) == 0 ){
					return true;
				}
			}
			return false;
		}
		
	}
}