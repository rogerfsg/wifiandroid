package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class PaginaInicial extends InterfaceHTML{

	public PaginaInicial(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		String vStr = ""; 
		vStr += "<style type=\"text/css\">";

		vStr += "   .header{";

		vStr += "       border-bottom: 3px solid #009AD9;";
		vStr += "       line-height: normal;";
		vStr += "       vertical-align: middle;";
		vStr += "       padding-top: 40px;";
		vStr += "       padding-bottom: 40px;";
		vStr += "       font-size: 30px;";
		vStr += "       font-weight: bold;";
		vStr += "       letter-spacing: -2px;";
		vStr += "       line-height: 38px;";
		vStr += "       font-family: arial,Helvetica,sans-serif;";


		vStr += "   }";

		vStr += "   .footer{";

		vStr += "       height: 150px;";
		vStr += "       border-top: 3px solid #009AD9;";
		vStr += "       line-height: normal;";
		vStr += "       margin-left: auto;";
		vStr += "       margin-right: auto;";
		vStr += "       width: 100%;";
		vStr += "       font-family: arial,Helvetica,sans-serif;";
		vStr += "       font-size: 14px;";
		vStr += "       font-weight: bold;";


		vStr += "   }";

		vStr += "   .footer table td{";

		vStr += "       text-align: center;";
		vStr += "       width: 500px;";

		vStr += "   }";

		vStr += "   .footer img{";

		vStr += "       height: 130px;";
		vStr += "       width: 130px;";

		vStr += "   }";

		vStr += "   .container {";
		vStr += "       height: 650px;";
		vStr += "       width: 900px;";
		vStr += "       position: static;";
		vStr += "       margin-left: auto;";
		vStr += "       margin-right: auto;";
		vStr += "       text-align: center;";
		vStr += "   }";
		vStr += "   ul.thumb {";
		vStr += "       float: left;";
		vStr += "       list-style: none;";
		vStr += "       padding: 10px;                    ";
		vStr += "       width: 900px;";

		vStr += "   }";

		vStr += "   ul.thumb li > a{";

		vStr += "       display: block;";
		vStr += "       margin-bottom: 20px;";
		vStr += "       min-height: 200px;";

		vStr += "   }";

		vStr += "   ul.thumb li {";
		vStr += "       margin: 15px;";
		vStr += "       margin-left: 0px;";
		vStr += "       padding: 0px;";
		vStr += "       padding-bottom: 5px;";
		vStr += "       float: left;";
		vStr += "       position: relative;";
		vStr += "       width: 210px;";
		vStr += "       height: 210px;";
		vStr += "       margin-bottom: 50px;";
		vStr += "   }";
		vStr += "   ul.thumb li img {";
		vStr += "       width: 200px; height: 200px;";
		vStr += "       border: 1px solid #ddd;";
		vStr += "       padding: 5px;";
		vStr += "       background: #f0f0f0;";
		vStr += "       position: absolute;";
		vStr += "       left: 0; top: 0;";
		vStr += "       -ms-interpolation-mode: bicubic; ";
		vStr += "   }";
		vStr += "   ul.thumb li img.hover {";
		vStr += "       background:url(thumb_bg.png) no-repeat center center;";
		vStr += "       border: none;";
		vStr += "   }";
		vStr += "   #main_view {";
		vStr += "       float: left;";
		vStr += "       padding: 9px 0;";
		vStr += "       margin-left: -10px;";
		vStr += "   }";

		vStr += "   ul.thumb li h2 {";
		vStr += "       font-size: 1em;";
		vStr += "       font: normal 10px Verdana, Arial, Helvetica, sans-serif;";
		vStr += "       font-weight: normal;";
		vStr += "       text-transform: uppercase;";
		vStr += "       padding: 10px;";
		vStr += "       margin-top: 20px;";
		vStr += "       background: #f0f0f0;";
		vStr += "       width: 190px;";
		vStr += "       min-width: 190px;";
		vStr += "       border: 1px solid #ddd;";
		vStr += "       text-align: center;";
		vStr += "       height: 100px;";
		vStr += "       min-height: 100px;";
		vStr += "       vertical-align: middle;";
		vStr += "       display: table-cell;";

		vStr += "   }";

		vStr += "   ul.thumb li h2 a ";
		vStr += "   {";
		vStr += "       height: 100%; ";
		vStr += "       text-decoration: none; ";
		vStr += "       color: #777; ";
		vStr += "       vertical-align: middle;";
		vStr += "       font-size: 13px;";
		vStr += "       font-weight: bold;";
		vStr += "   }";

		vStr += "   ul.thumb li span { /*--Used to crop image--*/";
		vStr += "       width: 202px;";
		vStr += "       height: 202px;";
		vStr += "       overflow: hidden;";
		vStr += "       display: block;";
		vStr += "   }";

		vStr += "</style>";

		vStr += "<script type=\"text/javascript\"> ";

		vStr += "   jQuery(document).ready(function(){";

		vStr += "       //Larger thumbnail preview ";

		vStr += "       jQuery(\"ul.thumb li\").hover(function() {";
		vStr += "           jQuery(this).css({'z-index' : '10'});";
		vStr += "           jQuery(this).find('img').addClass(\"hover\").stop()";
		vStr += "           .animate({";
		vStr += "               marginTop: '-150px', ";
		vStr += "               marginLeft: '-150px', ";
		vStr += "               top: '50%', ";
		vStr += "               left: '50%', ";
		vStr += "               width: '256px', ";
		vStr += "               height: '256px',";
		vStr += "               padding: '20px' ";
		vStr += "           }, 200);";

		vStr += "       } , function() {";
		vStr += "           jQuery(this).css({'z-index' : '0'});";
		vStr += "           jQuery(this).find('img').removeClass(\"hover\").stop()";
		vStr += "           .animate({";
		vStr += "               marginTop: '0', ";
		vStr += "               marginLeft: '0',";
		vStr += "               top: '0', ";
		vStr += "               left: '0', ";
		vStr += "               width: '200px', ";
		vStr += "               height: '200px', ";
		vStr += "               padding: '5px'";
		vStr += "           }, 400);";
		vStr += "       });";

		vStr += "       //Swap Image on Click";
		vStr += "       jQuery(\"ul.thumb li a\").click(function() {";

		vStr += "           //var mainImage = jQuery(this).attr(\"href\"); //Find Image Name";
		vStr += "           //jQuery(\"#main_view img\").attr({ src: mainImage });";
		vStr += "           //return false;		";

		vStr += "       });";

		vStr += "   });";



		vStr += "</script> ";		

		vStr += "<div class=\"container\">";

		vStr += "   <ul class=\"thumb\">";
		vStr += "       <li style=\"z-index: 0;\">";
		vStr += "           <a href=\"index.php5?tipo=lists&page=arquivo\"><span><img style=\"margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px; overflow: hidden;\" class=\"\" src=\"adm/imgs/especifico/topo_folders.png\" alt=\"\"></span></a>";
		vStr += "           <h2><a href=\"index.php5?tipo=forms&page=empresa\">" + context.getString(R.string.topo_folders)+ "</a></h2>";
		vStr += "       </li>";
//
//		vStr += "       <li style=\"z-index: 0;\">";
//		vStr += "           <a href=\"index.php5?tipo=lists&page=empresa\"><span><img style=\"margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px;\" class=\"\" src=\"adm/imgs/especifico/lista_de_empresas.png\" alt=\"\"></span></a>";
//		vStr += "           <h2><a href=\"index.php5?tipo=lists&page=empresa\">Visualizar Lista de Empresas</a></h2>";
//		vStr += "       </li>";
//		vStr += "       <li style=\"z-index: 0;\">";
//		vStr += "           <a href=\"index.php5?tipo=forms&page=pessoa\"><span><img style=\"margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px; overflow: hidden;\" class=\"\" src=\"imgs/especifico/pessoa.png\" alt=\"\"></span></a>";
//		vStr += "           <h2><a href=\"index.php5?tipo=forms&page=empresa\">Cadastrar Pessoa</a></h2>";
//		vStr += "       </li>";
//		vStr += "       <li style=\"z-index: 0;\">";
//		vStr += "           <a href=\"index.php5?tipo=lists&page=pessoa\"><span><img style=\"overflow: hidden; margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px;\" class=\"\" src=\"imgs/especifico/lista_de_pessoas.png\" alt=\"\"></span></a>";
//		vStr += "           <h2><a href=\"index.php5?tipo=lists&page=pessoa\">Visualizar Lista de Pessoas</a></h2>";
//		vStr += "       </li>";

		vStr += "   </ul>";
		vStr += "</div>";
		
		return new ContainerResponse(__helper, new StringBuilder(vStr));
		
	}

}
