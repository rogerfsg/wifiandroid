package app.omegasoftware.wifitransferpro.mapa;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.container.ContainerRouteInformation;
import app.omegasoftware.wifitransferpro.primitivetype.HelperFloat;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

public class HelperRoutePath {
	/** Called when the activity is first created. */


	public static ArrayList<RotaOverlay> getListOverlayItemOfRoute(GeoPoint src,GeoPoint dest, int color, Document doc)
	{
		try
		{	
			if(doc == null) return null;
			else if(doc.getElementsByTagName("GeometryCollection").getLength()>0)
			{
				//String path = doc.getElementsByTagName("GeometryCollection").item(0).getFirstChild().getFirstChild().getNodeName();
				String path = doc.getElementsByTagName("GeometryCollection").item(0).getFirstChild().getFirstChild().getFirstChild().getNodeValue() ;
				//Log.d("xxx","path="+ path);
				String [] pairs = path.split(" ");
				String[] lngLat = pairs[0].split(","); // lngLat[0]=longitude lngLat[1]=latitude lngLat[2]=height
				// src
				GeoPoint startGP = new GeoPoint((int)(Double.parseDouble(lngLat[1])*1E6),
						(int)(Double.parseDouble(lngLat[0])*1E6));
				ArrayList<RotaOverlay> vList = new ArrayList<RotaOverlay>(); 
				vList.add(new RotaOverlay(startGP,startGP,1));
				GeoPoint gp1;
				GeoPoint gp2 = startGP;
				for(int i=1;i<pairs.length;i++) // the last one would be crash
				{
					lngLat = pairs[i].split(",");
					gp1 = gp2;
					// watch out! For GeoPoint, first:latitude, second:longitude
					gp2 = new GeoPoint((int)(Double.parseDouble(lngLat[1])*1E6),(int)(Double.parseDouble(lngLat[0])*1E6));
					vList.add(new RotaOverlay(gp1,gp2,2,color));
					//Log.d("xxx","pair:" + pairs[i]);
				}
				vList.add(new RotaOverlay(dest,dest, 3)); // use the default color
				return vList;
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	
	public static ContainerRouteInformation getDistanceOfRoute(Document doc)
	{
		try
		{	
			if(doc == null) return null;
			else 
			{
				int countPlacemark = doc.getElementsByTagName("GeometryCollection").getLength();
				
				if(countPlacemark > 0){
					Node vNode =  doc.getElementsByTagName("GeometryCollection").item(0).getPreviousSibling().getFirstChild();
//					Distance: 1.7&#160;km (about 4 mins)<br/>Map data �2012 Google, MapLink
					String vComent = "";
					if (vNode instanceof CharacterData) {
				      CharacterData cd = (CharacterData) vNode;
				      vComent =  cd.getData();
				    }
					Float vDistancia = null;
					String vUnidadeDistancia = "";
					if(vComent != null)
						if(vComent.length() > 0){
							String vStrDistanceStart = "Distance:";
							
							if(vComent.startsWith(vStrDistanceStart)){
								int vIndexEComercial = vComent.indexOf("&");
								
								if(vIndexEComercial > 0 ){
									int vIndexPontoVirgula = vComent.indexOf(";");
									int vIndexAbreParenteses = vComent.indexOf("(");
									
									String vSubstringAbout = vComent.substring(vIndexAbreParenteses, vComent.length() - 1);
									int vIndexFechaParenteses = vSubstringAbout.indexOf(")");
									//about 8 hours 32 mins
									String vStrTempoStart = "(about ";
									if(vSubstringAbout.startsWith(vStrTempoStart))
										vSubstringAbout = vSubstringAbout.substring(vStrTempoStart.length(), vIndexFechaParenteses);
									
									String vVetorToken[] = HelperString.splitWithNoneEmptyToken(vSubstringAbout, "[ ]" );
									String vVetorTempo[] = new String [] {"day", "hour", "min"};
									LinkedHashMap<String, Integer> vHashDate = new LinkedHashMap<String, Integer>(); 
									int vIndexUnidadeTempo = vVetorTempo.length - 1;
									for(int j = vVetorToken.length - 1; j >= 0 ; j -= 2){
										
										String vTokenInput = vVetorToken[j];
										if(vTokenInput != null)
											if(vTokenInput.length() > 0 )
												vTokenInput = vTokenInput.trim();
										
										if(vIndexUnidadeTempo >= 0 ){
										String vTokenUnidadeTempo = vVetorTempo[vIndexUnidadeTempo];
										if(vTokenInput.contains(vTokenUnidadeTempo)){
											
											String vStrTempo = vVetorToken[j - 1];
											Integer vNumber = HelperInteger.parserInteger(vStrTempo);
											if(vNumber != null)
												vHashDate.put(vTokenUnidadeTempo, vNumber);
										}
										vIndexUnidadeTempo --;
										}
									}
									
									if(vIndexPontoVirgula > 0 && 
											vIndexAbreParenteses > 0 && 
											vIndexPontoVirgula < vIndexAbreParenteses){
										vUnidadeDistancia = vComent.substring(vIndexPontoVirgula + 1, vIndexAbreParenteses -1);
										String vStrDistancia = vComent.substring(vStrDistanceStart.length() + 1, vIndexEComercial);
										
										if(vStrDistancia != null && vStrDistancia.length() > 0 ){
											vDistancia = HelperFloat.parserFloat(vStrDistancia);
											ContainerRouteInformation vContainerDistancia = new ContainerRouteInformation(vDistancia, vUnidadeDistancia, vHashDate);
											return vContainerDistancia;
										}
									}
								}
								
							}
						}
					
					return null;
					
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public static void DrawPath(GeoPoint src,GeoPoint dest, int color, MapView mMapView01, Document doc)
	{
		try
		{	
			
			if(doc.getElementsByTagName("GeometryCollection").getLength()>0)
			{
				//String path = doc.getElementsByTagName("GeometryCollection").item(0).getFirstChild().getFirstChild().getNodeName();
				String path = doc.getElementsByTagName("GeometryCollection").item(0).getFirstChild().getFirstChild().getFirstChild().getNodeValue() ;
				//Log.d("xxx","path="+ path);
				String [] pairs = path.split(" ");
				String[] lngLat = pairs[0].split(","); // lngLat[0]=longitude lngLat[1]=latitude lngLat[2]=height
				// src
				GeoPoint startGP = new GeoPoint((int)(Double.parseDouble(lngLat[1])*1E6),(int)(Double.parseDouble(lngLat[0])*1E6));
				mMapView01.getOverlays().add(new RotaOverlay(startGP,startGP,1));
				GeoPoint gp1;
				GeoPoint gp2 = startGP;
				for(int i=1;i<pairs.length;i++) // the last one would be crash
				{
					lngLat = pairs[i].split(",");
					gp1 = gp2;
					// watch out! For GeoPoint, first:latitude, second:longitude
					gp2 = new GeoPoint((int)(Double.parseDouble(lngLat[1])*1E6),(int)(Double.parseDouble(lngLat[0])*1E6));
					mMapView01.getOverlays().add(new RotaOverlay(gp1,gp2,2,color));
					//Log.d("xxx","pair:" + pairs[i]);
				}
				mMapView01.getOverlays().add(new RotaOverlay(dest,dest, 3)); // use the default color
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public static Document getDocumentInformationRoute(GeoPoint src,GeoPoint dest, int color, int pTypeResultCodeTracarRota)
	{
		if(src == null || dest == null) return null;
		
		// connect to map web service
		StringBuilder urlString = new StringBuilder();
		urlString.append("http://maps.google.com/maps?f=d&hl=en");
		urlString.append("&saddr=");//from
		urlString.append( Double.toString((double)src.getLatitudeE6()/1.0E6 ));
		urlString.append(",");
		urlString.append( Double.toString((double)src.getLongitudeE6()/1.0E6 ));
		urlString.append("&daddr=");//to
		urlString.append( Double.toString((double)dest.getLatitudeE6()/1.0E6 ));
		urlString.append(",");
		urlString.append( Double.toString((double)dest.getLongitudeE6()/1.0E6 ));
		if(pTypeResultCodeTracarRota == OmegaConfiguration.RESULT_CODE_TRACA_ROTA_A_PE){
			urlString.append("&dirflg=w");	
		}
		urlString.append("&ie=UTF8&0&om=0&output=kml");
		//Log.d("xxx","URL="+urlString.toString());
		// get the kml (XML) doc. And parse it to get the coordinates(direction route).
		Document doc = null;
		HttpURLConnection urlConnection= null;
		URL url = null;
		try
		{
			url = new URL(urlString.toString());
			urlConnection=(HttpURLConnection)url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setDoOutput(true);
			urlConnection.setDoInput(true);
			urlConnection.connect();

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(urlConnection.getInputStream());

			return doc;
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ParserConfigurationException e)
		{
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			e.printStackTrace();
		}
		return null;
	}


}

