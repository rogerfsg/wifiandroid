package app.omegasoftware.wifitransferpro.database;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.Adapter.AppointmentPlaceAdapter;
import app.omegasoftware.wifitransferpro.common.Adapter.AppointmentPlaceMapAdapter;
import app.omegasoftware.wifitransferpro.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public abstract class  Table{
	public String __name;	   	   
	public static final String ID_UNIVERSAL = "id";
	public static final String NOME_UNIVERSAL = "nome";
	public static final String CPF_UNIVERSAL = "cpf";
	public static final String RG_UNIVERSAL = "rg";
	public static final String TELEFONE_UNIVERSAL = "telefone";
	public static final String TELEFONE_1_UNIVERSAL = "telefone1";
	public static final String TELEFONE_2_UNIVERSAL = "telefone2";
	public static final String FAX_UNIVERSAL = "fax";
	public static final String CELULAR_UNIVERSAL = "celular";
	public static final String CELULAR_SMS_UNIVERSAL = "celular_sms";
	
	public static final String LATITUDE_INT_UNIVERSAL = "latitude_INT";
	public static final String LONGITUDE_INT_UNIVERSAL = "longitude_INT";

	public static String CEP_UNIVERSAL = "cep";
	public static String LOGRADOURO_UNIVERSAL = "logradouro";
	public static String EMAIL_UNIVERSAL = "email";
	public static String SITE_UNIVERSAL = "site";
	public static String NUMERO_UNIVERSAL = "numero";
	public static String COMPLEMENTO_UNIVERSAL = "complemento";
	public static String ESTADO_ID_INT_UNIVERSAL = "uf_id_INT";
	public static String CIDADE_ID_INT_UNIVERSAL = "cidade_id_INT";
	public static String PAIS_ID_INT_UNIVERSAL = "pais_id_INT";
	public static String BAIRRO_ID_INT_UNIVERSAL = "bairro_id_INT";
	public static String SEXO_ID_INT_UNIVERSAL = "sexo_id_INT";

	public static String TAG = "Table";

	public ContainerUniqueKey __containerUniqueKey = null;
	public Hashtable<String, Attribute> __hashNameAttributeByAttribute;
	public Database __database; 
	public ArrayList<String> __listSortNameAttribute;

	public Table( String p_name, Database p_database ){
		__listSortNameAttribute = new ArrayList<String>();
		__name = p_name;			
		__database = p_database;
		__hashNameAttributeByAttribute = new Hashtable<String, Attribute>();
	}

	public void setUniqueKey(String pVetorAttributeName[]){

		if(pVetorAttributeName == null) return;
		else if (pVetorAttributeName.length == 0 ) return;
		else {
			__containerUniqueKey = new ContainerUniqueKey(pVetorAttributeName);	
		}

	}


	public String getStrListAttrValueWithoutCorporacaoESenhaAttr(){
		String vToken = "";

		for (String vNomeAttr : __listSortNameAttribute) {
			Attribute vAttr = getAttribute(vNomeAttr);
			if(vAttr == null) continue;
			if(vAttr.isAutoIncrement() && vAttr.isPrimaryKey())
				continue;
			String vAttrToLower = vNomeAttr.toLowerCase();
			if(vAttrToLower.compareTo("corporacao_id_int") == 0 ) continue;
			else if(vAttrToLower.compareTo("senha") == 0 ) continue;
			else{
				String vValue = "";
				if(vAttr.isEmpty())
					vValue = "null";
				else  vValue = vAttr.getStrValue();

				if(vToken.length() == 0 )
					vToken += vValue;
				else vToken += OmegaConfiguration.DELIMITER_QUERY_COLUNA + vValue;
			}
		}

		return vToken;
	}

	public String getStrListAttrWithoutCorporacaoAndSenhaIfExist(){
		String vToken = "";

		for (String vAttrNome : __listSortNameAttribute) {

			Attribute vAttr = getAttribute(vAttrNome);
			if(vAttr == null) continue;
			if(vAttr.isAutoIncrement() && vAttr.isPrimaryKey())
				continue;

			else {
				String vAttrToLower = vAttrNome.toLowerCase();
				if(vAttrToLower.compareTo("corporacao_id_int") == 0 )
					continue;
				else if(vAttrToLower.compareTo("senha") == 0 )
					continue;
				else if(vToken.length() == 0 )
					vToken += vAttrNome;
				else vToken += OmegaConfiguration.DELIMITER_QUERY_COLUNA + vAttrNome;
			}
		}

		return vToken;
	}


	public String getStrQueryWhereUniqueId(){

		if(__containerUniqueKey == null) return null;
		else {

			String vVetorNameAttribute[] = __containerUniqueKey.getVetorAttribute();
			String vQuery = "WHERE " ;
			String vQueryWhere = "";
			for (String vAttrName  : vVetorNameAttribute) {
				Attribute vAttr = getAttribute(vAttrName);
				String vLowerName = vAttrName.toLowerCase();
				if(vLowerName.compareTo("corporacao_id_int") == 0 ) continue;
				else if(!vAttr.isEmpty()){
//					if(vAttr.getSQLType() == SQL_TYPE.TEXT){
//						if(vQueryWhere.length() == 0 )
//							vQueryWhere += vAttr.getName() + " = '" + vAttr.getStrValue() + "' ";
//						else vQueryWhere += " AND " + vAttr.getName() + " = '" + vAttr.getStrValue() + "' ";
//					}
//					else{
						if(vQueryWhere.length() == 0 )
							vQueryWhere += vAttr.getName() + " = " + vAttr.getStrValue();
						else vQueryWhere += " AND " + vAttr.getName() + " = '" + vAttr.getStrValue() + "' ";
//					}	 
				} else return null;
			}
			return vQuery + vQueryWhere;
		}

	}

	public abstract void populate() ;

	public boolean isEqual(String p_nomeTabela){
		String v_nomeTabela = "";
		if(p_nomeTabela == null){
			return false;
		} else if(p_nomeTabela.length() == 0 ) return false;
		v_nomeTabela = p_nomeTabela.toLowerCase();
		String v_myName = __name.toString().toLowerCase();

		if(v_nomeTabela.compareTo(v_myName) == 0 ){
			return true;
		} else return false;
	}

	public ArrayList<Attribute> getListAttributeFKOfTable(String pStrName){
		ArrayList<Attribute> vListAttribute = new ArrayList<Attribute>();
		if(pStrName == null) return null;
		else if(pStrName.length() == 0 ) return null;
		pStrName =  pStrName.toLowerCase();
		for (String vAttrName : this.__listSortNameAttribute) {
			Attribute vAttr = this.getAttribute(vAttrName);
			if(vAttr.isFK()){
				String vTable = vAttr.getStrTableFK();
				if(vTable.compareTo(pStrName) == 0)
					vListAttribute.add(vAttr);
			}
		}
		return vListAttribute;
	}

	public boolean isTableDependent(String pStrName){

		if(pStrName == null) return false;
		else if(pStrName.length() == 0 ) return false;
		pStrName =  pStrName.toLowerCase();
		for (String vAttrName : this.__listSortNameAttribute) {
			Attribute vAttr = this.getAttribute(vAttrName);
			if(vAttr.isFK()){
				String vTable = vAttr.getStrTableFK();
				if(vTable.compareTo(pStrName) == 0)
					return true;
			}
		}
		return false;
	}

	public ArrayList<Attribute> getListAttributeFK(){
		ArrayList<Attribute> vListAttribute = new ArrayList<Attribute>(); 
		for (String vAttrName : this.__listSortNameAttribute) {
			Attribute vAttr = this.getAttribute(vAttrName);
			if(vAttr.isFK()) vListAttribute.add(vAttr);
		}
		return vListAttribute;
	}

	private class ContainerOperationUpdate{
		String __newId;
		ArrayList<Table> __listTable;
		Attribute __attribute ;
		public ContainerOperationUpdate(
				String pNewId,
				ArrayList<Table> pListTable, 
				Attribute pAttribute){
			__newId = pNewId;
			__listTable = pListTable;
			__attribute = pAttribute;
		}

		public boolean execute(){
			boolean vValidade = true;
			for (Table vTableDependent : __listTable) {
				vTableDependent.setAttrStrValue(__attribute.getName(), __newId);
				boolean vValidadeUpdate = vTableDependent.update();
				if(! vValidadeUpdate) vValidade = false;
				vTableDependent.clearData();
			}
			return vValidade;
		}
	}

	public ArrayList<ContainerOperationUpdate> getListContainerOperationUpdateOfTableDependent(
			String pOldId,
			String pNewId, 
			Table pTableDependent){

		ArrayList<ContainerOperationUpdate> vListContainerOperationUpdate = new ArrayList<ContainerOperationUpdate>();

		ArrayList<Attribute> vListAttribute = pTableDependent.getListAttributeFKOfTable(getName());

		//vListAttribute = {fucionario_id_INT}
		for (Attribute vAttr: vListAttribute) {
			pTableDependent.clearData();

			pTableDependent.setAttrStrValue(vAttr.getName(), pOldId);
			ArrayList<Table> vListTableLinked = pTableDependent.getListTable(new String[]{ID_UNIVERSAL}, true);
			vListContainerOperationUpdate.add(
					new ContainerOperationUpdate(
							pNewId,
							vListTableLinked,
							vAttr));

		}
		pTableDependent.clearData();
		return vListContainerOperationUpdate;
	}

	public boolean updateListIdToNewId(String[] pListOldId, String[] pListNewId){
		boolean vValidade = true;
		for (int i = pListNewId.length - 1 ; i >= 0 ; i--) {
			try{
				String pOldId = pListOldId[i];
				String pNewId = pListNewId[i];
				if(pOldId == null || pNewId == null) continue;
				else if(pOldId.length() == 0 || pNewId.length() == 0) continue;
				//Se os id's forem iguais, entao nao a necessidade de atualiza-lo
				else if (pOldId.compareTo(pNewId) == 0 ) continue;
				else{
					Integer vIndexCheck = HelperInteger.parserInteger(pNewId);
					if(vIndexCheck == null) continue;
					else if (vIndexCheck < 0) continue;
					else{
						this.clearData();
						this.setAttrStrValue(ID_UNIVERSAL, pOldId);
						boolean vValidadeUpdate = this.updateId(pNewId, false);
						if( ! vValidadeUpdate) vValidade = false;	
					}
				}
			} catch(Exception ex){
				//Log.d(TAG, ex.getMessage());
			}
		}
		return vValidade;

	}




	public boolean updateListIdInListTableDependent(String[] pListOldId, String[] pListNewId){
		ArrayList<Table> vListTableDependent = __database.getListTableDependent(getName());
		boolean vValidade= true;
		for (Table vTableDependent : vListTableDependent) {
			ArrayList<ContainerOperationUpdate> vTotalListContainerOperationUpdate = new ArrayList<ContainerOperationUpdate>();

			for (int i = 0 ; i < pListNewId.length; i++) {
				String pOldId = pListOldId[i];
				String pNewId = pListNewId[i];
				if(pNewId.compareTo("null") == 0 ){
					//Log.d(TAG , "Id nulo");
					continue;
				} else if (pNewId.compareTo(pOldId) == 0 ) continue;
				//todo atributo da tabela que faz referencia para a tabela em questao
				//this: Funcionario
				//vListTableDependent = {EmpresaFuncionario}
				//tablea = EmpresaFuncionario
				ArrayList<ContainerOperationUpdate> vListContainerOperationOfTable  = getListContainerOperationUpdateOfTableDependent(
						pOldId, 
						pNewId, 
						vTableDependent); 
				//Armazena todas as tuplas q irao sofrer modificacao
				for (ContainerOperationUpdate containerOperationUpdate : vListContainerOperationOfTable) {
					vTotalListContainerOperationUpdate.add(containerOperationUpdate);
				}
			}
			//uma vez identificada executa as operacoes
			for (ContainerOperationUpdate containerOperationUpdate : vTotalListContainerOperationUpdate) {
				containerOperationUpdate.execute();
			}
		}
		return vValidade;
	}

	public AppointmentPlaceMapAdapter getAppointmentPlacesMapAdapter(Activity pActivity)
	{
		
		String vStrId = this.getStrValueOfAttribute(ID_UNIVERSAL);
		int vId = -1;
		if(vStrId != null)
			vId = Integer.valueOf(vStrId);
		
		AppointmentPlaceItemList place = 
				new AppointmentPlaceItemList(
						pActivity, 
						vId,
						this.getStrValueOfAttribute(LOGRADOURO_UNIVERSAL),
						this.getStrValueOfAttribute(NUMERO_UNIVERSAL),  
						this.getStrValueOfAttribute(COMPLEMENTO_UNIVERSAL),
						getNomeDaChaveExtrangeira(BAIRRO_ID_INT_UNIVERSAL),
						null, 
						null,
						null,
						this.getStrValueOfAttribute(EXTDAOArquivo.CEP_UNIVERSAL), 
						this.getPhones(),  
						this.getStrValueOfAttribute(EXTDAOArquivo.EMAIL_UNIVERSAL),
						this.getStrValueOfAttribute(EXTDAOArquivo.SITE_UNIVERSAL));

		AppointmentPlaceMapAdapter adapter = new AppointmentPlaceMapAdapter(pActivity, place, vStrId);

		return adapter;

	}
	public AppointmentPlaceAdapter getAppointmentPlacesAdapter(Context p_context)
	{
		
		String vStrId = this.getStrValueOfAttribute(ID_UNIVERSAL);
		int vId = -1;
		if(vStrId != null)
			vId = Integer.valueOf(vStrId);
		ArrayList<AppointmentPlaceItemList> result = new ArrayList<AppointmentPlaceItemList>();
		AppointmentPlaceItemList place = 
				new AppointmentPlaceItemList(
						p_context, 
						vId,
						this.getStrValueOfAttribute(LOGRADOURO_UNIVERSAL),
						this.getStrValueOfAttribute(NUMERO_UNIVERSAL),  
						this.getStrValueOfAttribute(COMPLEMENTO_UNIVERSAL),
						getNomeDaChaveExtrangeira(BAIRRO_ID_INT_UNIVERSAL),
						null, 
						null,
						null,
						this.getStrValueOfAttribute(EXTDAOArquivo.CEP_UNIVERSAL), 
						this.getPhones(),  
						this.getStrValueOfAttribute(EXTDAOArquivo.EMAIL_UNIVERSAL),
						this.getStrValueOfAttribute(EXTDAOArquivo.SITE_UNIVERSAL));

		result.add(place);



		AppointmentPlaceAdapter adapter = new AppointmentPlaceAdapter(p_context, result);

		return adapter;

	}

	public String getStrValueOfAttribute(String pAttrName){
		Attribute vAttr = this.getAttribute(pAttrName);
		if(vAttr == null) return null;
		else return vAttr.getStrValue();

	}

	public LinkedHashMap<String, String> getHashMapIdByDefinition(boolean p_isToUpperCase){
		return getHashMapIdByDefinition(p_isToUpperCase, "nome");
	}

	public LinkedHashMap<String, String> getHashMapIdByDefinition(boolean p_isToUpperCase, String pNomeAttr)
	{

		this.clearData();
		//		setAttrCorporacaoIdIntIfExist();

		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		ArrayList<Table> vList =  this.getListTable(new String[]{"id"}, true);
		if(vList != null && vList.size() > 0)
			for(Table iEntry : vList)
			{
				Table obj =  iEntry;
				String v_strId = obj.getAttribute("id").getStrValue();
				String v_strNome = obj.getAttribute(pNomeAttr).getStrValue();

				if(v_strNome != null){
					String v_strNomeModificado = v_strNome;
					if(p_isToUpperCase){
						v_strNomeModificado = v_strNomeModificado.toUpperCase();
					} else
						v_strNomeModificado = v_strNomeModificado.toLowerCase();
					hashTable.put(v_strId, v_strNomeModificado);
				}

			}

		return hashTable;

	}

	public boolean containsAttribute(String pStrAttribute){

		if(getAttribute(pStrAttribute) == null) return false;
		else return true;
	}

	public LinkedHashMap<String, String> getHashMapIdByAttributeValue(boolean p_isToUpperCase, String pStrNameAttribute)
	{

		//		this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		try{

			if(!containsAttribute(pStrNameAttribute)) return null;
			ArrayList<Table> vListTupla = this.getListTable(new String[]{"id"}, null, true);
			if(vListTupla != null){
				for(Table iEntry : vListTupla){
				
					Table obj =  iEntry;
					String v_strId = obj.getAttribute("id").getStrValue();
					String v_strNome = obj.getAttribute(pStrNameAttribute).getStrValue();
	
					if(v_strNome != null){
						String v_strNomeModificado = v_strNome;
						if(p_isToUpperCase){
							v_strNomeModificado = v_strNomeModificado.toUpperCase();
						}
						hashTable.put(v_strId, v_strNomeModificado);
					}
	
				}
			}
		}
		catch(Exception ex){
			//Log.e(TAG, ex.getMessage());
		}
		return hashTable;
	}


	public String[] getVetorStringDefinition(boolean p_isToUpperCase)
	{

		this.clearData();

		ArrayList<Table> vListTable =  this.getListTable(new String[]{"id"}, true);
		ArrayList<String> vListToken = new ArrayList<String>();

		int i = 0 ;
		for(Table iEntry : vListTable)
		{
			Table obj =  iEntry;

			String v_strNome = obj.getAttribute("nome").getStrValue();
			if(v_strNome != null){
				String v_strNomeModificado = v_strNome.toUpperCase();
				if(p_isToUpperCase){
					v_strNomeModificado = v_strNomeModificado.toUpperCase();
				}
				vListToken.add(v_strNomeModificado);
			}


		}

		String vVetorTokenRetorno[]  = new String[vListTable.size()];
		vListToken.toArray(vVetorTokenRetorno);
		return vVetorTokenRetorno;

	}

	public String getNomeDaChaveExtrangeira(String p_nomeChave){
		Attribute v_attr = this.getAttribute(p_nomeChave);
		if(v_attr == null) return null;
		String p_id  = v_attr.getStrValue();
		if(p_id == null) return null;
		else if (p_id.length() == 0 )return null;

		String v_nameTableFK = v_attr.getStrTableFK();
		Database v_database = this.getDatabase();

		Table v_tableFK  = v_database.factoryTable(v_nameTableFK); 
		v_tableFK.setAttrStrValue(ID_UNIVERSAL, p_id);
		v_tableFK.select();

		Attribute v_attrNomeTabelaFK = v_tableFK.getAttribute(NOME_UNIVERSAL);
		if(v_attrNomeTabelaFK != null){
			return v_attrNomeTabelaFK.getStrValue();
		} else return null;
	}

	public String getValorDoAtributoDaChaveExtrangeira(String p_nomeChave, String pNomeDoAtributoNaTabelaExtrangeira){
		Attribute v_attr = this.getAttribute(p_nomeChave);
		if(v_attr == null) return null;
		String p_id  = v_attr.getStrValue();
		if(p_id == null) return null;
		else if (p_id.length() == 0 )return null;

		String v_nameTableFK = v_attr.getStrTableFK();
		Database v_database = this.getDatabase();

		Table v_tableFK  = v_database.factoryTable(v_nameTableFK); 
		v_tableFK.setAttrStrValue(ID_UNIVERSAL, p_id);
		if(v_tableFK.select()){

			Attribute v_attrNomeTabelaFK = v_tableFK.getAttribute(pNomeDoAtributoNaTabelaExtrangeira);
			if(v_attrNomeTabelaFK != null){
				return v_attrNomeTabelaFK.getStrValue();
			} else return null;
		} else return null;
	}

	public String getNomeDoIdentificadorDaTabela(String p_id, Table p_table){	

		p_table.setAttrStrValue(ID_UNIVERSAL, p_id);
		p_table.select();
		return p_table.getAttribute(NOME_UNIVERSAL).getStrValue();
	}

	public void setDatabase(Database p_database){
		__database = p_database;
	}

	public ArrayList<String> getPhones()
	{
		ArrayList<String> phones = new ArrayList<String>();
		String v_phone = getStrValueOfAttribute(TELEFONE_UNIVERSAL);
		String v_phone1 = getStrValueOfAttribute(TELEFONE_1_UNIVERSAL);
		String v_phone2 = getStrValueOfAttribute(TELEFONE_2_UNIVERSAL);
		String v_celular = getStrValueOfAttribute(CELULAR_UNIVERSAL);

		String[] v_telefones = {v_phone, v_phone1,  v_phone2, v_celular};

		for (String v_telefone : v_telefones) {
			if(v_telefone != null)
				phones.add(v_telefone);

		}

		return phones;

	}


	public String getName(){
		return this.__name;	
	}

	public Database getDatabase(){
		return this.__database;
	}

	public void addAttribute(Attribute p_attribute){
		if(p_attribute == null) return ;
		else{
			String v_strNameAttribute = p_attribute.getName();
			if( ! __hashNameAttributeByAttribute.containsKey(v_strNameAttribute)){
				__hashNameAttributeByAttribute.put(v_strNameAttribute, p_attribute)  ;
				__listSortNameAttribute.add(v_strNameAttribute);
			}
		}
	}

	public ArrayList<Attribute> getListAttributeKey(){

		Enumeration<String> v_listNameStrAttr = __hashNameAttributeByAttribute.keys();
		ArrayList<Attribute> v_list = new ArrayList<Attribute>() ; 

		while(v_listNameStrAttr.hasMoreElements()){
			String v_strNameElement =v_listNameStrAttr.nextElement();
			Attribute v_attr = __hashNameAttributeByAttribute.get(v_strNameElement);
			if(v_attr.isPrimaryKey()){
				v_list.add(v_attr);
			}
		}

		return v_list;
	}

	public Attribute getAttribute(String p_strNameAttribute){
		if(  __hashNameAttributeByAttribute.containsKey(p_strNameAttribute)){
			return __hashNameAttributeByAttribute.get(p_strNameAttribute);		
		} else{
			String v_strNameAttr = p_strNameAttribute.toLowerCase().trim();
			Enumeration<String> v_listNameStrAttr = __hashNameAttributeByAttribute.keys();


			while(v_listNameStrAttr.hasMoreElements()){
				String v_strNameElement =v_listNameStrAttr.nextElement();
				String v_strNameCompare =  v_strNameElement.toLowerCase().trim(); 
				if(v_strNameCompare == v_strNameAttr){
					return __hashNameAttributeByAttribute.get(v_strNameElement);
				} 
			}

			return null;
		}

	}


	public List<Tuple> selectV2(Tuple p_tupleKey){

		List<Tuple> list = new ArrayList<Tuple>();

		String[] v_vetorChave = p_tupleKey.getVectorStrKeyNameAttr();
		String[] v_vetorArg = new String[v_vetorChave.length];
		int v_index = 0;
		for (String v_strKey : v_vetorChave) {

			v_vetorArg[v_index] = v_vetorChave[v_index] + "=" + p_tupleKey.getStrValue(v_strKey);  
			v_index += 1;
		}
		SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
		Cursor cursor = v_SQLiteDatabase.query(getName(), v_vetorChave, null, v_vetorArg, null,  "", "" );
		try{
			if (cursor.moveToFirst()) {

				do {
					Tuple v_tuple = new Tuple( );
					v_index = 0;
					for (String v_strNameAttr : v_vetorChave) {
						String v_strValue = cursor.getString(v_index);
						v_tuple.addAttrValue(v_strNameAttr, v_strValue);
						v_index += 1;
					}
					list.add(v_tuple); 

				} while (cursor.moveToNext());
			}
		} catch(Exception ex){}

		if (cursor != null && !cursor.isClosed()) {

			cursor.close();
		}

		return list;
	}

	public void setAttrStrValue(String p_strName, String p_strValue){
		Attribute v_attr = this.getAttribute(p_strName);
		if(! (v_attr == null)){
			v_attr.setStrValue(p_strValue);
		}

	}
	public boolean remove(boolean pIsToInsertInSynchronizeTable){
		String vId = this.getStrValueOfAttribute(ID_UNIVERSAL);
		return remove(vId, pIsToInsertInSynchronizeTable);
	}

	public boolean remove(String pId, boolean pIsToInsertInSynchronizeTable){
		try{
			if(pId == null) return false;
			else if (pId.length() == 0 ) return false;
			else{
				SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
				int vNumberRowsAffected = v_SQLiteDatabase.delete(this.getName(), "id = ?" , new String[] {pId});
				if(vNumberRowsAffected > 0 ) {

					return true;
				}
				else return false;
			}
		} catch(Exception ex){
			return false;
		}
	}

	public boolean isEmpty(){
		SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
		Cursor cursor = v_SQLiteDatabase.query(getName(), new String[]{"id"}, null, null,  "", "", null, "1" );
		boolean v_validade = true;
		try{

			if (cursor.moveToFirst()) {

				do {
					v_validade = false;
					break;

				} while (cursor.moveToNext());
			}
		} catch(Exception ex){

		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return v_validade;
	}

	// Select first tuple of result, and load all data into this object
	public boolean select(){

		ArrayList<Attribute> v_vetorAttributeWithStrValue = this.getListAttributeWithStrValue();
		ArrayList<String> vListArg = new ArrayList<String>(); 

		int v_index = 0;
		for (Attribute v_attr : v_vetorAttributeWithStrValue) {
			String vStrValue = v_attr.getStrValue();
			if(HelperString.checkIfIsNull(vStrValue) != null){
				vListArg.add(vStrValue);
				v_index += 1;
			}
		}
		String[] v_vetorArg = new String[vListArg.size()];
		vListArg.toArray(v_vetorArg);
		String v_querSelect = this.getStrQueryWhereDoSelect();
		SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
		Cursor cursor = v_SQLiteDatabase.query(getName(), null, v_querSelect, v_vetorArg, null,  "", "",    "1" );

		boolean v_validade = false;
		try{
			if (cursor.moveToFirst()) {
				clearData();
				do {
					v_validade = true;
					v_index = 0;
					for (String v_strNameAttr : __listSortNameAttribute) {
						String v_strValue = cursor.getString(v_index);

						setAttrStrValue(v_strNameAttr, v_strValue);
						v_index += 1;
					}
				} while (cursor.moveToNext());
			}
		} catch(Exception ex){

		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return v_validade;
	}

	public String[] getVetorValueOfAttributeWithValue(){
		ArrayList<String> vListValue = getListValueOfAttributeWithValue();
		if(vListValue.size() == 0 ) return null;
		String vVetor[] = new String[vListValue.size()];
		return vListValue.toArray(vVetor);
	}
	public ArrayList<String> getListValueOfAttributeWithValue(){
		Enumeration<String> v_listNameStrAttr = __hashNameAttributeByAttribute.keys();
		ArrayList<Attribute> v_listReturn = new ArrayList<Attribute>() ; 
		ArrayList<String> vListContent = new ArrayList<String>(); 
		while(v_listNameStrAttr.hasMoreElements()){
			String v_strNameElement =v_listNameStrAttr.nextElement();
			Attribute v_attr = __hashNameAttributeByAttribute.get(v_strNameElement);
			if(! v_attr.isEmpty()){
				vListContent.add(v_attr.getStrValue());

			}
		}

		return vListContent;
	}

	public ArrayList<Attribute> getListAttributeWithStrValue(){
		
		Enumeration<String> v_listNameStrAttr = __hashNameAttributeByAttribute.keys();
		ArrayList<Attribute> v_listReturn = new ArrayList<Attribute>() ; 

		while(v_listNameStrAttr.hasMoreElements()){
			String v_strNameElement =v_listNameStrAttr.nextElement();
			Attribute v_attr = __hashNameAttributeByAttribute.get(v_strNameElement);
			if(! v_attr.isEmpty()){
				v_listReturn.add(v_attr);
			}
		}
		return v_listReturn;
	}

	public Table getFirstTupla(){
		return getFirstTupla(null, true);
	}
	
	public Table getFirstTupla(String p_vectorOrderAttrNameToOrderBy[], boolean pIsToSetIdCorporacao){
		ArrayList<Table> vListTupla = getListTable(p_vectorOrderAttrNameToOrderBy, " 0,1 ", pIsToSetIdCorporacao);
		if(vListTupla != null && vListTupla.size() > 0){
			Table vTupla = vListTupla.get(0);
			return vTupla;
		} 
		return null;
	}

	private String getStrOrderBy(String p_vectorOrderAttrNameToOrderBy[]){
		if(p_vectorOrderAttrNameToOrderBy == null){
			return "";
		}
		else if(p_vectorOrderAttrNameToOrderBy.length == 0){
			return "";
		}
		else{
			String v_clause = "";
			for (String v_attr : p_vectorOrderAttrNameToOrderBy) {
				if(v_clause.length() == 0 ){
					v_clause += v_attr + " ";   
				}else{
					v_clause += ", " + v_attr + " ";
				}
			}
			return v_clause;
		}
	}



	public String getStrQueryRemoveSQL()
	{
		String id = getStrValueOfAttribute(ID_UNIVERSAL);
		if(id == null) return null;

		String sql = " DELETE FROM " + this.__name + " WHERE id = '" + id + "' ";

		return sql;	    
	}

	public String getStrQueryUpdateSQL()
	{
		String id = getStrValueOfAttribute(ID_UNIVERSAL);
		if(id == null) return null;
		String upd = "";
		for (String attrName : this.__listSortNameAttribute) {
			String vValue = getStrValueOfAttribute(attrName);
			if(vValue != null){
				if(upd.length() == 0 )
					upd += attrName + " = " + formatarCampoTextoParaSQL(vValue);
				else upd +=  ", " + attrName + " = " + formatarCampoTextoParaSQL(vValue);

			}	
		}

		String sql = " UPDATE " + this.__name + " SET " + upd  + " WHERE id = '" + id + "' ";

		return sql;	    
	}

	public boolean update(){
		return update(true);
	}
	public boolean update(boolean pIsToInsertInTableSincronizador){
		String vId = getStrValueOfAttribute(ID_UNIVERSAL);
		String vTableName = this.getName();
		//Atualiza todos os atributos da tabela
		ContentValues v_content = new ContentValues();
		for (String attrName : this.__listSortNameAttribute) {
			if(attrName.compareTo(ID_UNIVERSAL) == 0) continue;
			else{
				String vValue = getStrValueOfAttribute(attrName);

				if(vValue == null)
					v_content.putNull(attrName);
				else
					v_content.put(attrName, vValue);
			}
		}

		if(vId == null) return false;
		else{
			SQLiteDatabase vSQLiteDatabase = null;
			try{
				Database vDatabase = this.getDatabase();
				if(vDatabase == null) return false;
				else{
					vSQLiteDatabase = vDatabase.getSQLiteDatabase();
					if(vSQLiteDatabase == null) return false;
					else{
						int vNumberRowsAffected = vSQLiteDatabase.update(
								vTableName, 
								v_content, 
								" id= ?", 
								new String[]{vId});
						//insere na tabela sincronizador
											

						if(vNumberRowsAffected > 0 ) return true;
						else return false;
					}

				}
			} catch(Exception ex){
				//Log.d(TAG, ex.getMessage());
				return false;
			} finally{
				if(vSQLiteDatabase != null){
					vSQLiteDatabase.close();
				}
			}
		}
	}



	public boolean updateId(String pNewId, boolean isToInsertInTableSynchronize){
		String vId = getStrValueOfAttribute(ID_UNIVERSAL);
		this.setAttrStrValue(ID_UNIVERSAL, pNewId);
		String vTableName = this.getName();
		ArrayList<Attribute> vListAttrWithValue = getListAttributeWithStrValue();
		//Atualiza todos os atributos da tabela
		ContentValues v_content = new ContentValues();
		for (Attribute attr : vListAttrWithValue) {
			String attrName = attr.getName();
			if(!attr.isEmpty()){

				String vValue = HelperString.checkIfIsNull(attr.getStrValue());
				v_content.put(attrName, vValue);	
			}
		}

		if(vId == null) return false;
		else{
			Database vDatabase = this.getDatabase();
			if(vDatabase == null) return false;
			else{

				SQLiteDatabase vSQLiteDatabase = vDatabase.getSQLiteDatabase();
				if(vSQLiteDatabase == null) return false;
				else{
					int vNumberRowsAffected = vSQLiteDatabase.update(
							vTableName, 
							v_content, 
							"id= ?", 
							new String[]{vId});
					//insere na tabela sincronizador
					if(vNumberRowsAffected > 0 ) {
						
						return true;
					}
					else return false;
				}

			}
		}
	}



	public String getStrQueryInsertSQL(){
		String vCabecalho = getStrCabecalhoInsert();
		String vCorpoInsert = getStrCorpoInsert();
		if(vCabecalho != null && vCorpoInsert != null)
			if(vCabecalho.length() > 0 && vCorpoInsert.length() > 0)
				return vCabecalho + vCorpoInsert;
		return null;
	}

	public String getStrCabecalhoInsert(){
		String v_corpo = "";
		for (String vNameAttr : this.__listSortNameAttribute) {
			Attribute vAttr = this.getAttribute(vNameAttr);
			if(vAttr == null) continue;
			else if(vAttr.isAutoIncrement() && vAttr.isPrimaryKey())
				continue;
			if(v_corpo.length() == 0 )
				v_corpo += " `" + vNameAttr + "` ";
			else v_corpo += ", `" + vNameAttr + "` ";

		}
		if(v_corpo.length() > 0 )
			return "INSERT INTO " + getName() + " (" + v_corpo + ") VALUES";
		else return null;
	}

	public static String formatarCampoTextoParaSQL(String pTexto){
		if(pTexto == null) return "null";
		else if(pTexto.length() == 0) return "";
		else{
			String v_strConteudoIdentado = pTexto.replace("'", "\'");
			return " '" + v_strConteudoIdentado + "' ";
		}
	}

	public String getStrCorpoInsert(){
		String v_corpo = "";
		for (String vNameAttr : this.__listSortNameAttribute) {
			Attribute vAttr = this.getAttribute(vNameAttr);
			if(vAttr == null) continue;
			else if(vAttr.isAutoIncrement() && vAttr.isPrimaryKey()) continue;
			String vStrValue = vAttr.getStrValue();
			if(v_corpo.length() == 0 )
				v_corpo += " " + formatarCampoTextoParaSQL(vStrValue) ; 
			else v_corpo += ", " + formatarCampoTextoParaSQL(vStrValue) ;

		}
		if(v_corpo.length() > 0 )
			return "(" + v_corpo + ") ";
		else return null;
	}

	public ArrayList<Integer> getAllListId(){
		try{
			Cursor cursor = null;
			__database.openIfNecessary();

			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
			cursor = v_SQLiteDatabase.query(getName(), new String[]{ID_UNIVERSAL}, null, null, null,  "", ID_UNIVERSAL);
			ArrayList<Integer> v_listId = new ArrayList<Integer>();
			try{
				if (cursor.moveToFirst()) {

					do {

						String v_strValue = cursor.getString(0);
						Integer vIndex = HelperInteger.parserInt(v_strValue);
						if(vIndex != null)
							v_listId.add(vIndex);

						if(cursor.isLast()) break;
					}while (cursor.moveToNext()); 
				}
			} catch(Exception ex){

			}

			if (cursor != null && !cursor.isClosed()) {

				cursor.close();
			}
			return v_listId;
		} catch(Exception ex){
			return null;
		}
	}

	public ArrayList<Table> getListTable(){
		return getListTable(null, true);
	}
	
	public ArrayList<Table> getListTable(String p_vectorOrderAttrNameToOrderBy[]){
		return getListTable(p_vectorOrderAttrNameToOrderBy, true);
	}
	
	
	public ArrayList<Table> getListTable(String p_vectorOrderAttrNameToOrderBy[], boolean pIsToSetIdCorporacao){		
		Cursor cursor = null;
		SQLiteDatabase v_SQLiteDatabase= null;
		try{
			
			ArrayList<Attribute> v_vetorAttributeWithStrValue = this.getListAttributeWithStrValue();
			boolean v_validadeArg = false;
			ArrayList<String> vListArg = new ArrayList<String>(); 


			int v_index = 0;
			for (Attribute v_attr : v_vetorAttributeWithStrValue) {

				//v_vetorArg[v_index] = v_vetorChave[v_index] + "=" + p_tupleKey.getStrValue(v_strKey);
				if(!v_validadeArg){
					v_validadeArg = true;	
				}
				String vStrValue = v_attr.getStrValue();
				if(HelperString.checkIfIsNull(vStrValue) != null){
					vListArg.add(vStrValue);

					v_index += 1;
				}
			}
			String[] v_vetorArg = new String[vListArg.size()];
			vListArg.toArray(v_vetorArg);

			String v_querWhere = null;

			v_SQLiteDatabase= __database.getSQLiteDatabase();

			if(!v_validadeArg){

				cursor = v_SQLiteDatabase.query(getName(), null, null, null, null,  "", this.getStrOrderBy(p_vectorOrderAttrNameToOrderBy) );
			} else{
				v_querWhere = this.getStrQueryWhereComOsAtributos(v_vetorAttributeWithStrValue);
				String vOrderBy = this.getStrOrderBy(p_vectorOrderAttrNameToOrderBy) ;

				cursor = v_SQLiteDatabase.query(getName(), null, v_querWhere, v_vetorArg, null,  "", vOrderBy);	
			}


			ArrayList<Table> v_listEXTDAO = new ArrayList<Table>();
			try{
				if (cursor.moveToFirst()) {

					do {

						Table v_table = this.factory();

						boolean v_validade = false;
						for(int i = 0; i < __listSortNameAttribute.size(); i ++){
							String v_strNameAttr = __listSortNameAttribute.get(i);
							String v_strValue = cursor.getString(i);
							v_table.setAttrStrValue(v_strNameAttr, v_strValue);
							v_validade = true;
						}
						if(v_validade){
							v_listEXTDAO.add(v_table);
						}

						if(cursor.isLast()) break;
					}while (cursor.moveToNext()); 
				}
			} catch(Exception ex){
				//if(ex != null)
					//Log.d(TAG, ex.getMessage());
			}


			return v_listEXTDAO;
		}catch(Exception ex){

			//if(ex != null)
				//Log.d(TAG, ex.getMessage());
		} finally{
			if (cursor != null && !cursor.isClosed()) {

				cursor.close();
			}
			if(v_SQLiteDatabase != null)
				if(v_SQLiteDatabase.isOpen())
					v_SQLiteDatabase.close();
		}
		return null;
	}

	public ArrayList<Table> getListTable(String p_vectorOrderAttrNameToOrderBy[], String pLimit, boolean pIsToSetIdCorporacao ){		

		try{
			
			ArrayList<Attribute> v_vetorAttributeWithStrValue = this.getListAttributeWithStrValue();
			boolean v_validadeArg = false;
			ArrayList<String> vListArg = new ArrayList<String>(); 

			for (Attribute v_attr : v_vetorAttributeWithStrValue) {

				//v_vetorArg[v_index] = v_vetorChave[v_index] + "=" + p_tupleKey.getStrValue(v_strKey);
				if(!v_validadeArg){
					v_validadeArg = true;	
				}
				String vStrValue = v_attr.getStrValue();
				if(HelperString.checkIfIsNull(vStrValue) != null){
					vListArg.add(vStrValue);
				}
			}
			String[] v_vetorArg = new String[vListArg.size()];
			vListArg.toArray(v_vetorArg);

			String v_querWhere = null;
			Cursor cursor = null;
			__database.openIfNecessary();
			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
			if(!v_validadeArg){

				cursor = v_SQLiteDatabase.query(getName(), null, null, null, null,  "", this.getStrOrderBy(p_vectorOrderAttrNameToOrderBy), pLimit );
			} else{
				v_querWhere = this.getStrQueryWhereComOsAtributos(v_vetorAttributeWithStrValue);
				String vOrderBy = this.getStrOrderBy(p_vectorOrderAttrNameToOrderBy) ;

				cursor = v_SQLiteDatabase.query(getName(), null, v_querWhere, v_vetorArg, null,  "", vOrderBy, pLimit);	
			}


			ArrayList<Table> v_listEXTDAO = new ArrayList<Table>();
			try{
				if (cursor.moveToFirst()) {

					do {

						Table v_table = this.factory();

						boolean v_validade = false;
						for(int i = 0; i < __listSortNameAttribute.size(); i ++){
							String v_strNameAttr = __listSortNameAttribute.get(i);
							String v_strValue = cursor.getString(i);
							v_table.setAttrStrValue(v_strNameAttr, v_strValue);
							v_validade = true;
						}
						if(v_validade){
							v_listEXTDAO.add(v_table);
						}

						if(cursor.isLast()) break;
					}while (cursor.moveToNext()); 
				}
			} catch(Exception ex){

			}

			if (cursor != null && !cursor.isClosed()) {

				cursor.close();
			}
			__database.close();
			return v_listEXTDAO;
		}catch(Exception ex){

		}
		return null;
	}

	public Table getFirstTupla(String p_vectorOrderAttrNameToOrderBy[], String pLimit ){		

		try{
			ArrayList<Attribute> v_vetorAttributeWithStrValue = this.getListAttributeWithStrValue();
			boolean v_validadeArg = false;
			ArrayList<String> vListArg = new ArrayList<String>();

			for (Attribute v_attr : v_vetorAttributeWithStrValue) {

				//v_vetorArg[v_index] = v_vetorChave[v_index] + "=" + p_tupleKey.getStrValue(v_strKey);
				if(!v_validadeArg){
					v_validadeArg = true;	
				}
				String vStrValue = v_attr.getStrValue();
				if(HelperString.checkIfIsNull(vStrValue) != null){
					vListArg.add(vStrValue);			
				}
			}
			String[] v_vetorArg = new String[vListArg.size()];
			vListArg.toArray(v_vetorArg);

			String v_querWhere = null;
			Cursor cursor = null;
			__database.openIfNecessary();
			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
			if(!v_validadeArg){

				cursor = v_SQLiteDatabase.query(getName(), null, null, null, null,  "", this.getStrOrderBy(p_vectorOrderAttrNameToOrderBy), pLimit );
			} else{
				v_querWhere = this.getStrQueryWhereComOsAtributos(v_vetorAttributeWithStrValue);
				String vOrderBy = this.getStrOrderBy(p_vectorOrderAttrNameToOrderBy) ;
				cursor = v_SQLiteDatabase.query(getName(), null, v_querWhere, v_vetorArg, null,  "", vOrderBy, pLimit);	
			}

			Table vTupla = null;
			try{
				if (cursor.moveToFirst()) {

					do {
						vTupla = this.factory();
						boolean v_validade = false;
						for(int i = 0; i < __listSortNameAttribute.size(); i ++){
							String v_strNameAttr = __listSortNameAttribute.get(i);
							String v_strValue = cursor.getString(i);
							vTupla.setAttrStrValue(v_strNameAttr, v_strValue);
							v_validade = true;
						}
						if(v_validade){
							return vTupla;
						}

						if(cursor.isLast()) break;
					}while (cursor.moveToNext()); 
				}
			} catch(Exception ex){

			}finally{
				if (cursor != null && !cursor.isClosed()) {
					cursor.close();
				}
			}
		}catch(Exception ex){

		}
		return null;
	}

	public String getLastIdInserted(){		

		try{
			Cursor cursor = null;
			String vStrTableName = getName();

			__database.openIfNecessary();
			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
			cursor = v_SQLiteDatabase.rawQuery("SELECT MAX(id) FROM " + vStrTableName, null );

			String vId = null;
			try{
				if (cursor.moveToFirst()) {

					do {
						vId = cursor.getString(0);
						break;
					}while (cursor.moveToNext()); 
				}
			} catch(Exception ex){}

			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
			return vId;
		}catch(Exception ex){

		}
		return null;
	}

	private String getStrBooleanValue(boolean pCheck){
		String vStrValue = "";

		if(pCheck) vStrValue = "1"; 
		else vStrValue = "0";

		return vStrValue;
	}

	public abstract Table factory();
	
	
	public abstract void procedureAfterSynchInsert(Context pContext, String pNewId, String pOldId);

	private void formatInsertToSQLiteAttributoNormalizado(){
		for (String vAttributeName : __listSortNameAttribute ) {
			Attribute vAttr = getAttribute(vAttributeName);
			if(vAttr != null && vAttr.isNormalizado()){
				Attribute vAttrFoiNormalizado = vAttr.getAttributeNormalizado();
				if(!vAttrFoiNormalizado.isEmpty()){
					String vValue = vAttrFoiNormalizado.getStrValue();
					String vWordWithoutAccent = HelperString.getWordWithoutAccent(vValue);
					vAttr.setStrValue(vWordWithoutAccent);
				} else {
					vAttr.setStrValue(null);
				}
			}
		}
	}
	public void formatToSQLite(){
		
		formatInsertToSQLiteAttributoNormalizado();
	}

	public long insert(boolean pIsToInsertInSynchronizeTable){
		SQLiteDatabase v_SQLiteDatabase= null;
		SQLiteStatement insertStmt = null;
		try{


			v_SQLiteDatabase= __database.getSQLiteDatabase();
			if(v_SQLiteDatabase != null){
				String vStrQueryInsert = getStrQueryInsert();
				if(vStrQueryInsert == null) return -1;
				insertStmt = v_SQLiteDatabase.compileStatement(vStrQueryInsert);
				int v_index = 1 ;
				boolean vValidade = false;
				for (String v_strNameElement : __listSortNameAttribute) {
					Attribute v_attr = __hashNameAttributeByAttribute.get(v_strNameElement);
					if( (v_attr.isAutoIncrement() && v_attr.isPrimaryKey() && v_attr.isEmpty()))
						continue;
					if(v_attr.bind(  insertStmt, v_index)){
						if(!vValidade) 
							vValidade = true;
					}
					v_index  += 1;
				}
				if(!vValidade) return -1;
				Long vId =  insertStmt.executeInsert();

				if(vId != -1){
					this.setAttrStrValue(ID_UNIVERSAL, vId.toString());

							
				}
				return vId;
			} else{
				//Log.e("insert", "SQLiteDatabase object is null.");
				return -1;
			}

		}catch (Exception e) {
			// TODO: handle exception
			//Log.e("insert", "Error na consulta insert na tabela " + getName() + " : " + e.toString());
			return -1;
		} finally{
			if(insertStmt != null)
				insertStmt.close();
			if(v_SQLiteDatabase != null && v_SQLiteDatabase.isOpen())
				v_SQLiteDatabase.close();
		}

	}

	public long insert(){
		return insert(true);
	}

	public void clearData(){
		for (String v_strNameElement : __listSortNameAttribute) {
			Attribute v_attr = __hashNameAttributeByAttribute.get(v_strNameElement);
			v_attr.clear();
		}
	}


	public ArrayList<Integer> removeAllTuplas(){
		ArrayList<Integer> vListId = getAllListId();
		if(vListId != null)
			for (Integer vId : vListId) {
				if(vId != null)
					remove(String.valueOf(vId), false);
			}
		return vListId;
	}

	public String getStrQueryInsert(){
		boolean vValidade = false;
		String v_strDefTotal = "";
		for (String v_strNameElement : __listSortNameAttribute) {

			Attribute v_attr = __hashNameAttributeByAttribute.get(v_strNameElement);

			if(! (v_attr.isAutoIncrement() && v_attr.isPrimaryKey() && v_attr.isEmpty())){
				String v_strDef = " " + v_attr.getName() ;

				if(v_strDefTotal.length() > 0 ){
					v_strDefTotal += ", " + v_strDef;
				} else{
					v_strDefTotal +=  v_strDef;
				}	
			}
		}

		String v_strCreate ="INSERT INTO " +
				__name + 
				" ( " + v_strDefTotal + " )";

		v_strDefTotal = "";
		for (String v_strNameElement : __listSortNameAttribute) {
			Attribute v_attr = __hashNameAttributeByAttribute.get(v_strNameElement);

			if(! (v_attr.isAutoIncrement() && v_attr.isPrimaryKey() && v_attr.isEmpty())){
				if(v_strDefTotal.length() > 0 ){
					v_strDefTotal += ", ?";
				} else{
					v_strDefTotal +=  "?";
				}
			}
		}
		v_strCreate += " VALUES (" + v_strDefTotal + "); ";
		return v_strCreate;
	}

	public String getStrQueryWhereDoSelect(){

		ArrayList<Attribute> v_listAttrKey = this.getListAttributeKey();

		String v_strDefTotal = "";
		for (Attribute v_attr : v_listAttrKey) {
			String v_strDef = "";
			if(v_attr.getStrValue() != null){
				Attribute.SQL_TYPE v_sqlType =  v_attr.getSQLType();	
				switch (v_sqlType) {
				case TEXT:
					v_strDef = v_attr.getName() + " LIKE ? " ;
					break;
				default:
					v_strDef = v_attr.getName() + " = ? " ;
					break;
				}
			} else{
				v_strDef = v_attr.getName() + "  IS NULL " ;
			}
			if(v_strDefTotal.length() > 0 ){
				v_strDefTotal += " AND " + v_strDef;
			} else{
				v_strDefTotal +=  v_strDef;
			}

			//				}
		}

		return v_strDefTotal;
	}


	public String getStrQueryWhereComOsAtributos(ArrayList<Attribute> p_listAttr){
		if(p_listAttr == null){
			return null;
		}
		else if(p_listAttr.size() == 0){
			return null;
		}

		String v_strDefTotal = "";
		for (Attribute v_attr : p_listAttr) {
			String v_strDef = "";

			if(HelperString.checkIfIsNull( v_attr.getStrValue()) != null) {
				Attribute.SQL_TYPE v_sqlType =  v_attr.getSQLType();	
				switch (v_sqlType) {
				case TEXT:
					v_strDef = v_attr.getName() + " LIKE ? " ;
					break;
				default:
					v_strDef = v_attr.getName() + " = ? " ;
					break;
				}
			} else{
				v_strDef = v_attr.getName() + "  IS NULL " ;
			}
			if(v_strDefTotal.length() > 0 ){
				v_strDefTotal += " AND " + v_strDef;
			} else{
				v_strDefTotal +=  v_strDef;
			}

			//				}
		}

		return v_strDefTotal;
	}

	public String[] getVetorStrQueryCreateIndex(){

		Integer i = 0 ;
		ArrayList<String> vList = new ArrayList<String>();
		for (String v_strNameElement : __listSortNameAttribute) {
			Attribute v_attr = __hashNameAttributeByAttribute.get(v_strNameElement);
			if(v_attr.isPrimaryKey() || v_attr.isFK()){
				String vIndex = "CREATE INDEX " + getName() + "_" + i.toString() + 
						" ON " + getName() + " (" + v_strNameElement + " ASC); ";

				vList.add(vIndex);
				i += 1;
			}
		}
		String vStrUniqueKey = getStrUniqueKey();
		if(vStrUniqueKey.length() > 0 )
			vList.add(vStrUniqueKey);

		if(vList.size() > 0 ){
			String[] vVetor = new String[vList.size()] ;  
			vList.toArray(vVetor);
			return vVetor;
		} else return null;
	}



	public Attribute getNewAtributoNormalizado( String pStrName){
		Attribute vAttr =  getAttribute(pStrName);
		if(vAttr != null){
			return vAttr.getNewAtributoNormalizado(vAttr);
		} else return null;
	}

	public String getStrQueryCreate(){


		String v_strDefTotal = "";
		String v_strFKTotal = "";
		boolean vIsChaveDefinida = false;

		for (String v_strNameElement : __listSortNameAttribute) {

			Attribute v_attr = __hashNameAttributeByAttribute.get(v_strNameElement);
			String v_strDef = "";

			if(v_attr.isPrimaryKey() && v_attr.isAutoIncrement()){
				v_strDef +=  v_attr.getName() + " " + v_attr.getStrSQLType() ;
				v_strDef += " PRIMARY KEY ";
				v_strDef += " AUTOINCREMENT ";
				vIsChaveDefinida = true;

			} else{
				v_strDef +=  v_attr.getName() + " " + v_attr.getStrSQLDeclaration() ;
			}

			if(! v_attr.__isNull){
				v_strDef += " NOT NULL ";
			}

			if(v_attr.getStrDefault() != null){
				v_strDef += " DEFAULT " + v_attr.getStrDefault();
			}
			//				if(v_attr.__isPrimaryKey){
			//					v_strDef += " PRIMARY KEY ";	
			//				}

			String v_strFK = "";

			if(v_attr.getStrTableFK() != null && v_attr.getStrAttributeFK() != null){
				v_strFK = " FOREIGN KEY ("+v_attr.getName()+") REFERENCES " + 
						v_attr.getStrTableFK() +" ("+v_attr.getStrAttributeFK()+") " +
						" ON UPDATE " + v_attr.getStrOnUpdate() +
						" ON DELETE " + v_attr.getStrOnDelete() ;

				v_strFKTotal += ", " + v_strFK;
			}

			if(v_strDefTotal.length() > 0 ){
				v_strDefTotal += ", " + v_strDef;
			} else{
				v_strDefTotal +=  v_strDef;
			}
		}		

		String v_strKey = "";
		ArrayList<Attribute> v_listStrKey = this.getListAttributeKey();
		if(! vIsChaveDefinida)
			if( v_listStrKey.size() > 1 ){

				for (Attribute v_attr : v_listStrKey) {
					String v_strAttr = v_attr.getName();
					if(v_strKey.length() > 0 )
						v_strKey += ", " + v_strAttr;	
					else v_strKey +=v_strAttr;
				}	
				v_strKey += " CONSTRAINT pk_primary_" + getName() + " PRIMARY KEY ( "  + v_strKey + " ) ";
			} else {
				Attribute v_attr = v_listStrKey.get(0);
				v_strKey += " PRIMARY KEY ("  + v_attr.getName() + ") ";
			}


		String v_strCreate =" CREATE TABLE " +
				__name +
				" (" + 
				v_strDefTotal ;
		if(v_strKey.length() > 0 )
			v_strCreate += " , " + v_strKey;
		if(v_strFKTotal.length() > 0 )
			v_strCreate += v_strFKTotal;

		v_strCreate += "); ";

		return v_strCreate;
	}
	public String getStrUniqueKey(){
		String vStrUniqueKey = "";
		if(__containerUniqueKey != null){
			//			CREATE UNIQUE INDEX i1 ON parent(c, d);
			String vTableName = getName();
			String vVetorUniqueKeyAttr[] = __containerUniqueKey.__vetorAttr;
			String vFKName = "UNIQUE_KEY_" + vTableName + "_0";
			//			vStrUniqueKey = " UNIQUE KEY `" + vFKName + "` (" ;
			vStrUniqueKey = "CREATE UNIQUE INDEX " + vFKName + " ON " + vTableName + " (" ;
			String vStrVetorAttrUnique = "";
			for (String vAttrUniqueKey : vVetorUniqueKeyAttr) {
				if(vStrVetorAttrUnique.length() > 0 ){
					vStrVetorAttrUnique += "," + vAttrUniqueKey ;
				}else {
					vStrVetorAttrUnique += vAttrUniqueKey ;
				}

			}
			vStrUniqueKey += vStrVetorAttrUnique + ") ";
		}
		return vStrUniqueKey;
	}



}

