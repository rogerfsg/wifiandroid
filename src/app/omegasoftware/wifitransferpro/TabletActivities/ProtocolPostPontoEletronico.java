package app.omegasoftware.wifitransferpro.TabletActivities;

import android.content.Context;
import app.omegasoftware.wifitransferpro.date.HelperDate;
import app.omegasoftware.wifitransferpro.gps.GPSContainer;




public class ProtocolPostPontoEletronico {

	private String __usr;
	private HelperDate __helperDate;
	private final String __delimiter = ";";
	private GPSContainer __gps ;
	String __foto;
	public ProtocolPostPontoEletronico (Context p_context, GPSContainer p_gps, String p_usr, String p_foto){
		__usr = p_usr;
		__helperDate = new HelperDate();
		__gps = p_gps;
		__foto = p_foto;
	}
	
	
	public String getPost(){
		
		String[] v_coluna = null;
		String latitude = "null";
		String longitude = "null";
		if(__gps != null){
			if(__gps.isInitialized()){
				v_coluna = new String[] {__usr, 
						__helperDate.getDateDisplay(),
						String.valueOf(__gps.__latitude),
						String.valueOf(__gps.__longitude)};
				longitude = String.valueOf(__gps.__longitude );
				latitude = String.valueOf(__gps.__latitude) ; 
			} 
		}
		v_coluna = new String[] {__usr, 
				__helperDate.getDateDisplay(),
				latitude,
				longitude, 
				__foto};
		
		String v_post = "";
		for(int i = 0 ; i < v_coluna.length; i++){
			if(v_post.length() == 0 ){
				v_post += v_coluna[i];
			} else{
				v_post += __delimiter + v_coluna[i];	
			}
		}
		
		return v_post;
	}
}

