package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class MultipleRemovePlaylistArquivo extends InterfaceHTML{

	public MultipleRemovePlaylistArquivo(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		StringBuilder vBuilder= new StringBuilder();
		String vStrAction = HelperFile.getNomeSemBarra(__helper.GET("next_action"));
		
		vBuilder.append("      <form name=\"form_list_arquivo\" id=\"form_list_arquivo\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
		vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");

		vBuilder.append("            <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
		vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
		vBuilder.append("           	<input type=\"hidden\" name=\"class\" id=\"class\" value=\"Arquivo\">");
		vBuilder.append("            <input type=\"hidden\" name=\"action\" id=\"action\" value=\"multipleRemoverArquivoPlaylist\">");
		
		vBuilder.append("            <input type=\"hidden\" name=\"arquivo_playlist\" id=\"arquivo_playlist\" value=\"" + __helper.GET("arquivo_playlist")+ "\">");
		vBuilder.append("            <input type=\"hidden\" name=\"playlist\" id=\"playlist\" value=\"" + __helper.GET("playlist")+ "\">");
		vBuilder.append("            <input type=\"hidden\" name=\"" + EXTDAOArquivo.NOME+ "1\" id=\"" + EXTDAOArquivo.NOME+ "1\" value=\"" + __helper.GET(EXTDAOArquivo.NOME)+ "\">");
		vBuilder.append("            <input type=\"hidden\" name=\"" + EXTDAOArquivo.PATH+ "1\" id=\"" + EXTDAOArquivo.PATH+ "1\" value=\"" + __helper.GET(EXTDAOArquivo.PATH)+ "\">");

		vBuilder.append("    		<table width=\"350\" align=\"center\" class=\"tabela_popup\">");
		
		vBuilder.append("    			<tr>");
		vBuilder.append("    				<td height=\"11\" align=\"left\" valign=\"middle\" class=\"textos5\" style=\"padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;\" >");
		
		vBuilder.append(ContainerMultiple.getHTML("checkbox_playlist"));
		vBuilder.append(HelperString.ucFirst( context.getString(R.string.mensagem_exclusao)));
		
		vBuilder.append("    				</td>");
		vBuilder.append("    			</tr>");
		vBuilder.append("    			<tr>");
		vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

		
		vBuilder.append("    					<input name=\"botaoSim\" type=\"submit\" value=\"" + context.getString(R.string.yes).toUpperCase() + "\" class=\"botoes_form\" style=\"cursor: pointer;\" />" +
				"</div>");
		vBuilder.append("    					<input name=\"botaoNao\" type=\"submit\" value=\"" + context.getString(R.string.no).toUpperCase() + "\" class=\"botoes_form\" style=\"cursor: pointer;\" />" +
				"</div>");
		vBuilder.append("    				</td>");
		vBuilder.append("    			</tr>");
		vBuilder.append("    		</table>");
		vBuilder.append("    	</form>");

		return new ContainerResponse(__helper, vBuilder);
	}

}
