package app.omegasoftware.wifitransferpro.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.location.Location;
import android.util.Log;



public class DataHelper {

   private static final String DATABASE_NAME = "unimed.db";
   private static final int DATABASE_VERSION = 1;
   private static final String TABLE_NAME = "medico";

   private Context context;
   private SQLiteDatabase db;

   private SQLiteStatement insertStmt;
   private static final String INSERT = "INSERT INTO " 
      + TABLE_NAME + 
      "(nome, " +
      "CRM, " +
      "telefone, " +
      "celular, " +
      "email, " +
      "fax, " +
      "logradouro, " +
      "numero, " +
      "complemento, " +
      "cidade_id_INT, " +
      "bairro_id_INT) " +
      "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

   
   public DataHelper(Context context) {
	   
      this.context = context;
      OpenHelper openHelper = new OpenHelper(this.context);
      this.db = openHelper.getWritableDatabase();
      this.insertStmt = this.db.compileStatement(INSERT);
      
   }   

   public void deleteAll() {
	   
      this.db.delete(TABLE_NAME, null, null);
      
   }


   private static class OpenHelper extends SQLiteOpenHelper {

      OpenHelper(Context context) {
    	  
         super(context, DATABASE_NAME, null, DATABASE_VERSION);
         
      }

      @Override
      public void onCreate(SQLiteDatabase db) {
    	  
         db.execSQL("CREATE TABLE " + TABLE_NAME + "(id  PRIMARY KEY, " +
         		"nome TEXT, " +
         		"CRM TEXT, " +
         		"telefone TEXT, " +
         		"celular TEXT, " +
         		"email TEXT, " +
         		"fax TEXT, " +
         		"logradouro, " +
         		"numero TEXT, " +
         		"complemento  TEXT, " +
         		"cidade_id_INT INTEGER, " +
         		"bairro_id_INT INTEGER)");
         
      }

      @Override
      public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
         
    	  //Log.w("Example", "Upgrading database, this will drop tables and recreate.");
         db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
         onCreate(db);
         
      }
   }
}
