package app.omegasoftware.wifitransferpro.file;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import android.content.Context;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class ContainerTypeOrderFile {
	Boolean orderName;
	Boolean orderSize;
	Boolean orderDate;
	Boolean orderType;

	public static String ORDER_NAME = "ORDER_NAME";
	public static String ORDER_SIZE = "ORDER_SIZE";
	public static String ORDER_DATE = "ORDER_DATE";
	public static String ORDER_TYPE = "ORDER_TYPE";

	public enum TYPE_ORDER{
		TYPE,
		NAME,
		DATE,
		SIZE,
	}

	public ContainerTypeOrderFile(){

	}

	public String getImageHTML(TYPE_ORDER pType, String pTipo, String pPage, String pSufixGet){
		
		String vConstant = "<img class=\"icones_list\" src=\"";
		
		
		if(pSufixGet == null ) pSufixGet="";
		else pSufixGet = "&" +pSufixGet;

		Boolean vValidade = getValidate(pType);

		if(vValidade == null) 
			vConstant +=  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + "square.png";
		else if(!vValidade)
			vConstant +=  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + "down.png";
		else 
			vConstant +=  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + "up.png";
		//Se nao tiver nenhuma ordenacao no atual atributo
		if(vValidade == null)
			vValidade = false;
		else if(!vValidade) // Se a ordenacao do atual atributo for ASC
			vValidade = true;
		else vValidade = null; // se a ordenacao do atual atributo for DESC

		
		String vLinkHTML =  getLinkHTML(pType, vValidade);
		if(vLinkHTML == null || vLinkHTML.length() == 0) vLinkHTML = "";
		else vLinkHTML = "&" +vLinkHTML;
		
		vConstant += "\"   onclick=\"javascript:document.location.href='index.php5?tipo=" +pTipo + "&page=" + pPage + pSufixGet + vLinkHTML +"'\">&nbsp;";
		return vConstant;
	}
	
public String getImageHTML(TYPE_ORDER pType, String pURL, String pSufixGet){
		
		String vConstant = "<img class=\"icones_list\" src=\"";
		
		
		if(pSufixGet == null ) pSufixGet="";
		else pSufixGet = "&" +pSufixGet;

		Boolean vValidade = getValidate(pType);

		if(vValidade == null) 
			vConstant +=  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + "square.png";
		else if(!vValidade)
			vConstant +=  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + "down.png";
		else 
			vConstant +=  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + "up.png";
		//Se nao tiver nenhuma ordenacao no atual atributo
		if(vValidade == null)
			vValidade = false;
		else if(!vValidade) // Se a ordenacao do atual atributo for ASC
			vValidade = true;
		else vValidade = null; // se a ordenacao do atual atributo for DESC

		
		String vLinkHTML =  getLinkHTML(pType, vValidade);
		if(vLinkHTML == null || vLinkHTML.length() == 0) vLinkHTML = "";
		else vLinkHTML = "&" +vLinkHTML;
		
		vConstant += "\"   onclick=\"javascript:document.location.href='" + pURL + pSufixGet + vLinkHTML +"'\">&nbsp;";
		return vConstant;
	}

	public static String getIdentify(TYPE_ORDER pType){
		switch (pType) {
		case NAME:
			return ORDER_NAME;
		case SIZE:
			return ORDER_SIZE;
		case DATE:
			return ORDER_DATE;
		case TYPE:
			return ORDER_TYPE;
		default:
			return null;
		}
	}

	public boolean compare(TYPE_ORDER pVetorTypeOrder[], ContainerCompareFile pContainer1, ContainerCompareFile pContainer2){
		File vFile1 = pContainer1.getFile();
		File vFile2 = pContainer2.getFile();
		
		int vCompare = 0;
		for (TYPE_ORDER vTypeORDER : pVetorTypeOrder) {
			Boolean vIsAsc = getValidate(vTypeORDER);
			switch (vTypeORDER) {
			case NAME:

				String vNameFile1 = HelperFile.getNameOfFileInPath(vFile1.getName()).toLowerCase();
				String vNameFile2 = HelperFile.getNameOfFileInPath(vFile2.getName()).toLowerCase();
				vCompare = vNameFile1.compareTo(vNameFile2);
				if(vCompare < 0 && vIsAsc)
					return true; 
				else if(vCompare > 0 && !vIsAsc)
					return true;
				break;
			case SIZE:
				if(vFile1.isDirectory()) return false;
				else if(vFile2.isDirectory()) return false;
				long vSizeFile1 = vFile1.length();
				long vSizeFile2 = vFile2.length();
				if(vSizeFile1 < vSizeFile2) 
					vCompare =  -1; 
				else if (vSizeFile1 == vSizeFile2)
					vCompare = 0;
				else vCompare = 1;
				if(vCompare < 0 && vIsAsc)
					return true; 
				else if(vCompare > 0 && !vIsAsc)
					return true;
				break;
			case DATE:
				long vDateFile1 = vFile1.lastModified();
				long vDateFile2 = vFile2.lastModified();
				if(vDateFile1 < vDateFile2) 
					vCompare = -1; 
				else if (vDateFile1 == vDateFile2)
					vCompare = 0;
				else vCompare = 1;
				if(vCompare < 0 && vIsAsc)
					return true; 
				else if(vCompare > 0 && !vIsAsc)
					return true;
				break;
			case TYPE:
				ContainerTypeFile vContainerType1 = pContainer1.getContainerTypeFile();
				ContainerTypeFile vContainerType2 = pContainer2.getContainerTypeFile();
				String vLabel1 = vContainerType1.getLabel();
				String vLabel2 = vContainerType2.getLabel();
				vCompare = vLabel1.compareTo(vLabel2);
				if(vCompare < 0 && vIsAsc)
					return true; 
				else if(vCompare > 0 && !vIsAsc)
					return true;
				break;

			default:
				vCompare = 0;
				break;
			}
			
			return false;
		}
		
		return false;
	}




	class TypeFileFilter implements FilenameFilter{

		public boolean accept(File dir, String filename) {
			// TODO Auto-generated method stub
			return false;
		}

	}

	public File[] getListFileOfDirectory(File pDirectory){
		if(pDirectory.isDirectory()){
			File[] vList = pDirectory.listFiles();

			if(vList != null && vList.length > 0 ){
				TYPE_ORDER vVetorTypeOrder[] = TYPE_ORDER.values();
				ArrayList<TYPE_ORDER> vListTypeOrder = new ArrayList<TYPE_ORDER>();
				for (TYPE_ORDER vTypeORDER : vVetorTypeOrder) {
					Boolean vIsAsc = getValidate(vTypeORDER);
					if(vIsAsc != null){
						vListTypeOrder.add(vTypeORDER);
					}
				}
				if(vListTypeOrder == null || vListTypeOrder.size() == 0 ) return vList;
				TYPE_ORDER vVetorTypeOrderValid[] = new TYPE_ORDER[vListTypeOrder.size()];
				vListTypeOrder.toArray(vVetorTypeOrderValid);
				
				for(int i = 0 ; i < vList.length; i++){
					ContainerCompareFile vContainer1 = new ContainerCompareFile(vList[i]);
					for(int j = i + 1 ; j < vList.length; j++){
						File vFileAux = vList[j];
						ContainerCompareFile vContainer2 = new ContainerCompareFile(vFileAux);
						if(compare(vVetorTypeOrderValid, vContainer1, vContainer2)){
							vContainer1 = new ContainerCompareFile(vList[j]);
							vList[j] = vList[i];
							vList[i] = vContainer1.getFile(); 
						} else if(compare(vVetorTypeOrderValid, vContainer1, vContainer2)){
							vContainer1 = new ContainerCompareFile(vList[j]);
							vList[j] = vList[i];
							vList[i] = vContainer1.getFile();
						}	
					}
					vList[i] = vContainer1.getFile();
				}
			}
			return vList;
		} else return null;
	}


	public File[] getListFileOfDirectory(File vList[]){
		
			

			if(vList != null && vList.length > 0 ){
				TYPE_ORDER vVetorTypeOrder[] = TYPE_ORDER.values();
				ArrayList<TYPE_ORDER> vListTypeOrder = new ArrayList<TYPE_ORDER>();
				for (TYPE_ORDER vTypeORDER : vVetorTypeOrder) {
					Boolean vIsAsc = getValidate(vTypeORDER);
					if(vIsAsc != null){
						vListTypeOrder.add(vTypeORDER);
					}
				}
				if(vListTypeOrder == null || vListTypeOrder.size() == 0 ) return vList;
				TYPE_ORDER vVetorTypeOrderValid[] = new TYPE_ORDER[vListTypeOrder.size()];
				vListTypeOrder.toArray(vVetorTypeOrderValid);
				
				for(int i = 0 ; i < vList.length; i++){
					ContainerCompareFile vContainer1 = new ContainerCompareFile(vList[i]);
					for(int j = i + 1 ; j < vList.length; j++){
						File vFileAux = vList[j];
						ContainerCompareFile vContainer2 = new ContainerCompareFile(vFileAux);
						if(compare(vVetorTypeOrderValid, vContainer1, vContainer2)){
							vContainer1 = new ContainerCompareFile(vList[j]);
							vList[j] = vList[i];
							vList[i] = vContainer1.getFile(); 
						} else if(compare(vVetorTypeOrderValid, vContainer1, vContainer2)){
							vContainer1 = new ContainerCompareFile(vList[j]);
							vList[j] = vList[i];
							vList[i] = vContainer1.getFile();
						}	
					}
					vList[i] = vContainer1.getFile();
				}
			}
			return vList;
		
	}
	
	public Boolean getValidate(TYPE_ORDER pType){
		switch (pType) {
		case NAME:
			return orderName;
		case SIZE:
			return orderSize;
		case DATE:
			return orderDate;
		case TYPE:
			return orderType;
		default:
			return null;
		}
	}

	public String getButtonTitulo(Context pContext, String pLabel, String pTipo, String pPage, String pSufixGet, TYPE_ORDER pTypeOrderNew){
		if(pSufixGet == null ) pSufixGet="";
		else pSufixGet = "&" +pSufixGet;

		Boolean vValidade = getValidate(pTypeOrderNew);
		//Se nao tiver nenhuma ordenacao no atual atributo
		if(vValidade == null)
			vValidade = false;
		else if(!vValidade) // Se a ordenacao do atual atributo for ASC
			vValidade = true;
		else vValidade = null; // se a ordenacao do atual atributo for DESC

		String vLinkHTML =  getLinkHTML(pTypeOrderNew, vValidade);
		if(vLinkHTML == null || vLinkHTML.length() == 0) vLinkHTML = "";
		else vLinkHTML = "&" +vLinkHTML;
		return "<input class=\"button_list_titulos\" value=\"" + pLabel + "\" type=\"button\" " +
		"onclick=\"javascript:document.location.href='index.php5?tipo=" +pTipo + "&page=" + pPage + pSufixGet + vLinkHTML +"'\">" ;
	}

	
	public String getButtonTitulo(Context pContext, String pLabel, String pURL, String pSufixGet, TYPE_ORDER pTypeOrderNew){
		if(pSufixGet == null ) pSufixGet="";
		else pSufixGet = "&" +pSufixGet;

		Boolean vValidade = getValidate(pTypeOrderNew);
		//Se nao tiver nenhuma ordenacao no atual atributo
		if(vValidade == null)
			vValidade = false;
		else if(!vValidade) // Se a ordenacao do atual atributo for ASC
			vValidade = true;
		else vValidade = null; // se a ordenacao do atual atributo for DESC

		String vLinkHTML =  getLinkHTML(pTypeOrderNew, vValidade);
		if(vLinkHTML == null || vLinkHTML.length() == 0) vLinkHTML = "";
		else vLinkHTML = "&" +vLinkHTML;
		return "<input class=\"button_list_titulos\" value=\"" + pLabel + "\" type=\"button\" " +
		"onclick=\"javascript:document.location.href='" + pURL + pSufixGet + vLinkHTML +"'\">" ;
	}


	public String getLinkHTML(TYPE_ORDER pTypeOrderNew, Boolean pIsAsc){

		TYPE_ORDER vVetorOrder[] = TYPE_ORDER.values();
		String vStrGet = "";
		boolean vIsFirst = true;
		for(int i = 0 ; i < vVetorOrder.length; i++){
			Boolean vValidade = getValidate(vVetorOrder[i]);
			if(pTypeOrderNew != null && pTypeOrderNew == vVetorOrder[i]){
//				Atualiza para o proximo estado
				if(pIsAsc != null){
					if(pIsAsc)
						vValidade = true;
					else vValidade = false;
				}
				String vKey = getIdentify(vVetorOrder[i]);
	
				if(vValidade != null && vKey != null && vKey.length() > 0 && pIsAsc != null){
					if(vValidade){
						if(!vIsFirst)
							vStrGet += "&";
						else vIsFirst = false;
						vStrGet += vKey + "=" + getStringValueOfBoolean(vValidade);
					} else{
						if(!vIsFirst)
							vStrGet += "&";
						else vIsFirst = false;
						vStrGet += vKey + "=" + getStringValueOfBoolean(vValidade);
					}
				}
			} else {
				String vKey = getIdentify(vVetorOrder[i]);
				
				if(vValidade != null && vKey != null && vKey.length() > 0){
					if(vValidade){
						if(!vIsFirst)
							vStrGet += "&";
						else vIsFirst = false;
						vStrGet += vKey + "=" + getStringValueOfBoolean(vValidade);
					} else{
						if(!vIsFirst)
							vStrGet += "&";
						else vIsFirst = false;
						vStrGet += vKey + "=" + getStringValueOfBoolean(vValidade);
					}
				}
			}

		}
		return vStrGet;
	}

	

	public void setByGet(HelperHttp pHelper){
		orderName = getBooleanOrder(pHelper.GET(ORDER_NAME));
		orderDate = getBooleanOrder(pHelper.GET(ORDER_DATE));
		orderSize = getBooleanOrder(pHelper.GET(ORDER_SIZE));
		orderType = getBooleanOrder(pHelper.GET(ORDER_TYPE));
	}

	public void setByPost(HelperHttp pHelper){
		orderName = getBooleanOrder(pHelper.POST(ORDER_NAME));
		orderDate = getBooleanOrder(pHelper.POST(ORDER_DATE));
		orderSize = getBooleanOrder(pHelper.POST(ORDER_SIZE));
		orderType = getBooleanOrder(pHelper.POST(ORDER_TYPE));
	}

	public static Boolean getBooleanOrder(String pOrder){
		if(pOrder == null) return null;
		else if(pOrder.compareTo(getStringValueOfBoolean(true)) ==0) return true;
		else if(pOrder.compareTo(getStringValueOfBoolean(false)) ==0) return false;
		else return null;
	}

	public static String getStringValueOfBoolean(boolean pIsAsc){
		if(pIsAsc) return "ASC";
		else return "DESC";
	}
}
