package app.omegasoftware.wifitransferpro.listener;

//http://stackoverflow.com/questions/5563749/android-viewflipper-not-flipping
import android.app.Activity;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.common.container.ContainerActionDetail;


public class SlideSimpleOnGestureListener extends SimpleOnGestureListener {

	private String __IdCustomItemList;
	short __modeSearch;	

	ViewFlipper viewFlipper ;
	
	OnLongClickListener __onLongClickListener;
	
	private static Double SWIPE_MIN_DISTANCE = null;
	private static Integer SWIPE_THRESHOLD_VELOCITY = null;
	
	static Animation slideRightOut = null;
	static Animation slideRightIn = null;
	static Animation slideRightInHalf = null;
	View view;
	Activity __activity;
	public SlideSimpleOnGestureListener(
			Activity pActivity,
			View pView, 
			String pIdCustomItemList, 
			short p_modeSearch){
		__activity = pActivity;
		view = pView;
		viewFlipper = (ViewFlipper) pView.findViewById(R.id.flipper);
		if(slideRightIn == null)
			slideRightIn = AnimationUtils.loadAnimation(pView.getContext(), R.anim.slide_right_in);
		if(slideRightInHalf == null)
			slideRightInHalf = AnimationUtils.loadAnimation(pView.getContext(), R.anim.slide_right_in_half);
		if(slideRightOut == null)
			slideRightOut = AnimationUtils.loadAnimation(pView.getContext(), R.anim.slide_right_out);
		if(SWIPE_MIN_DISTANCE == null || SWIPE_THRESHOLD_VELOCITY== null ){
			SWIPE_MIN_DISTANCE = 50.0;
			SWIPE_THRESHOLD_VELOCITY = 200;
			final ViewConfiguration vc = ViewConfiguration.get(pView.getContext());
			if(vc != null){
				SWIPE_MIN_DISTANCE = vc.getScaledEdgeSlop() / (4.0);
				SWIPE_THRESHOLD_VELOCITY = vc. getScaledMinimumFlingVelocity();
			}
		}
		this.__IdCustomItemList = pIdCustomItemList;
		__modeSearch = p_modeSearch;
		
	}
	
	
	public SlideSimpleOnGestureListener(
			View pView, 
			String pIdCustomItemList, 
			short p_modeSearch, 
			OnLongClickListener pOnLongClickListener){
		view = pView;
		viewFlipper = (ViewFlipper) pView.findViewById(R.id.flipper);
		if(slideRightIn == null)
			slideRightIn = AnimationUtils.loadAnimation(pView.getContext(), R.anim.slide_right_in);
		if(slideRightInHalf == null)
			slideRightInHalf = AnimationUtils.loadAnimation(pView.getContext(), R.anim.slide_right_in_half);
		if(slideRightOut == null)
			slideRightOut = AnimationUtils.loadAnimation(pView.getContext(), R.anim.slide_right_out);
		if(SWIPE_MIN_DISTANCE == null || SWIPE_THRESHOLD_VELOCITY== null ){
			SWIPE_MIN_DISTANCE = 50.0;
			SWIPE_THRESHOLD_VELOCITY = 200;
			final ViewConfiguration vc = ViewConfiguration.get(pView.getContext());
			if(vc != null){
				SWIPE_MIN_DISTANCE = vc.getScaledEdgeSlop() / 3.5;
				SWIPE_THRESHOLD_VELOCITY = vc. getScaledMinimumFlingVelocity();
			}
		}
		this.__IdCustomItemList = pIdCustomItemList;
		__modeSearch = p_modeSearch;
		
		__onLongClickListener  = pOnLongClickListener;
		
	}
	@Override
	public void onLongPress(MotionEvent e){
		if(__onLongClickListener != null)
		__onLongClickListener.onLongClick(null);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		try {
			float vDiference = Math.abs(e1.getY() - e2.getY());
			float vVelocity = Math.abs(velocityX);
			
			// right to left swipe
			if (vDiference > SWIPE_MIN_DISTANCE
					&& vVelocity > SWIPE_THRESHOLD_VELOCITY) {

				//				viewFlipper.setOutAnimation(slideRightIn);
				viewFlipper.setInAnimation(slideRightIn);
				//				viewFlipper.setInAnimation(slideRightOut);
				viewFlipper.showNext();
				action(view);
				viewFlipper.showPrevious();
			} else if(vDiference > SWIPE_MIN_DISTANCE / 4){
				viewFlipper.setInAnimation(slideRightInHalf);
				viewFlipper.showNext();
				viewFlipper.showPrevious();
			}

			//			else if (vDiference > SWIPE_MIN_DISTANCE
			//					&&  vVelocity > SWIPE_THRESHOLD_VELOCITY) {
			//				
			//				viewFlipper.setOutAnimation(slideRightIn);
			//				viewFlipper.setInAnimation(slideRightIn);
			//				viewFlipper.showPrevious();
			//			}
		} catch (Exception e) {
			// nothing
		}
		return false;
	}


	public void action(View pView){
		ContainerActionDetail.actionDetail(__activity, __modeSearch, __IdCustomItemList);
	}

}

