package app.omegasoftware.wifitransferpro.common.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import app.omegasoftware.wifitransferpro.database.Table;



public class TableDependentFKAutoCompleteTextView {
	
	LinkedHashMap<String, String> __hashIdByDefinition;
	
	String __attributeFK = Table.NOME_UNIVERSAL;
	String __attributeDependent ;
	String __attributeValueOfFK  = Table.NOME_UNIVERSAL;
	Table __table;
	
//	Table :: EXTDAOUsuarioCorporacao
//	__attributeDependentName :: uc.USUARIO_ID_INT
//	Table desejada :: EXTDAOCorporacao 
//	__attributeName :: c.nome
//	id da hash :: pTable.id
//	pTable = usuario_corporacao uc
//	__attributeDependent = USUARIO_ID_INT
//	__attributeFK = uc.CORPORACAO_ID_INT
//	__attributeValueOfFK = c.NOME

	public TableDependentFKAutoCompleteTextView(
			Table pTable,
			String pAttributeDependent,
			String pAttributeFK, 
			String pAttributeValueOfFK){
		__table = pTable;
		__attributeDependent = pAttributeDependent;
		__attributeFK = pAttributeFK;
		__attributeValueOfFK = pAttributeValueOfFK;
	}
	
	
	public boolean isEmpty(){
		if(__hashIdByDefinition == null) return true;
		else if(__hashIdByDefinition.size() == 0 ) return true;
		else return false;
	}
	
	

	
//	Table :: EXTDAOUsuarioCorporacao
//	__attributeDependentName :: uc.USUARIO_ID_INT
//	Table desejada :: EXTDAOCorporacao 
//	__attributeName :: c.nome
//	id da hash :: pTable.id
//	=========================
//	RETORNO
//	c.id, c.nome
	public LinkedHashMap<String, String> getHashIdByDefinition(String pValueAttributeDependent){
//		__attributeDependent = USUARIO_ID_INT
//		__attributeFK = uc.CORPORACAO_ID_INT
//		__attributeValueOfFK = c.NOME
		__table.clearData();
		__table.setAttrStrValue(__attributeDependent, pValueAttributeDependent);
		LinkedHashMap<String, String> vHash = new LinkedHashMap<String, String>(); 
//		List of EXTDAOUsuarioCorporacao
		ArrayList<Table> vList = __table.getListTable();
		for (Table tupla : vList) {

			String vName = tupla.getValorDoAtributoDaChaveExtrangeira(__attributeFK, __attributeValueOfFK);
			String vId = tupla.getStrValueOfAttribute(__attributeFK);
			vHash.put(vId, vName);
		}
		return vHash;
	}
	
//	Table :: EXTDAOUsuarioCorporacao
//	__attributeDependentName :: uc.USUARIO_ID_INT
//	Table desejada :: EXTDAOCorporacao 
//	__attributeName :: c.nome
//	id da hash :: pTable.id
	public String[] getVetorStrId(String pValueAttributeDependent){
		
		__hashIdByDefinition = getHashIdByDefinition(pValueAttributeDependent);
		if(__hashIdByDefinition == null) return new String[]{};
		Set<String> vListKey = __hashIdByDefinition.keySet();
		ArrayList<String> vListId = new ArrayList<String>(); 
		for (String vKey : vListKey) {
			String vContent = __hashIdByDefinition.get(vKey);
			vListId.add(vContent);
		}
		String vVetorId[] = new String[vListId.size()];
		vListId.toArray(vVetorId);
		return vVetorId;
	}
	

	public String getIdOfTokenIfExist(String pToken){
		if(__hashIdByDefinition == null) return null;
		Set<String> vListKey = __hashIdByDefinition.keySet();
		for (String vKey : vListKey) {
			String vContent = __hashIdByDefinition.get(vKey);
			if(pToken.compareTo(vContent) == 0 ) return vKey;
			
		}
		return null;
	}

	
	public String getTokenOfId(String pId){
		if(__hashIdByDefinition == null) return null;
		if(__hashIdByDefinition.containsKey(pId))
			return __hashIdByDefinition.get(pId);
		else return null;
	}
	
//	//Retorna o id correspondente a tupla da tabela
//	public String addTupleIfNecessay(Table pTable, String pTokenDefinition){
//		String vId = getIdOfTokenIfExist(pTokenDefinition);
//		if(vId == null){
//			pTable.clearData();
//			pTable.setAttrStrValue(__attributeName, pTokenDefinition);
//			Long pId = pTable.insert();
//			return pId.toString();
//		} else{
//			pTable.clearData();
//			pTable.setAttrStrValue(Table.ID_UNIVERSAL, vId);
//			pTable.select();
//			return vId;
//		}
//	}
	
	//Envia o objeto Table com todos os atributos que compoe a unicidade da tupla
//	__attributeDependent = USUARIO_ID_INT
//	__attributeFK = uc.CORPORACAO_ID_INT
//	__attributeValueOfFK = c.NOME
//	pTableFK = EXTDAOCorporacao
		public String addTupleIfNecessay(Table pTableFK){
			String vValueAttribute = pTableFK.getStrValueOfAttribute(__attributeFK);
			
			String vId = getIdOfTokenIfExist(vValueAttribute);
			if(vId != null) return vId;
			else {
				pTableFK.formatToSQLite();
				Long vNewId = pTableFK.insert();
				if(vNewId >= 0 ) return vNewId.toString(); 
				else{
					pTableFK.setAttrStrValue(Table.ID_UNIVERSAL, null);
					Table vTupla = pTableFK.getFirstTupla();
					if(vTupla != null) return vTupla.getStrValueOfAttribute(Table.ID_UNIVERSAL);
					else return null;
					
				}
			}
		}
}
