package app.omegasoftware.wifitransferpro.http.pages;

import java.io.File;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Link;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM_TOOLTIP;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class TipoMultipleDownload extends InterfaceHTML{

	public TipoMultipleDownload(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}


	@Override
	public ContainerResponse getContainerResponse() {
		String vStrPath = __helper.GET("novo_path") != null ? __helper.GET("novo_path") : __helper.GET(EXTDAOArquivo.PATH);
		if(vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		String vNome = HelperFile.getLastNameOfPath(vStrPath);
		String vReturn = "";
		if(vStrPath != null && vStrPath.length() > 0 ){
			File vFileDiretorioCorrente = new File(vStrPath);
			vReturn += "      <form name=\"form_list_arquivo\" id=\"form_list_arquivo\" action=\"download.php5\" method=\"POST\" enctype=\"multipart/form-data ";
			vReturn += "    		  onsubmit=\"return validarCampos();\" >";
			vReturn += "            <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">";
			vReturn += "			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">";
			vReturn += "           	<input type=\"hidden\" name=\"tipo\" id=\"tipo\" value=\"pages\">";
			vReturn += "            <input type=\"hidden\" name=\"page\" id=\"page\" value=\"MultipleDownloadArquivo\">";
			vReturn += "            <input type=\"hidden\" name=\"is_unique\" id=\"is_unique\" value=\"0\">";
			if(vFileDiretorioCorrente.isDirectory()){

				vReturn += ContainerMultiple.getHTML();

				
				String valuePath = (__helper.GET(EXTDAOArquivo.PATH) == null) ? "" : __helper.GET(EXTDAOArquivo.PATH) ;
				
				vReturn += "            <input type=\"hidden\" name=\"" + EXTDAOArquivo.PATH+ "\" id=\"" + EXTDAOArquivo.PATH+ "\" value=\"" + valuePath+ "\">";
				String valueNovoPath = ((__helper.GET("novo_path") != null) ? 
						__helper.GET("novo_path") :
							valuePath);
				valueNovoPath = HelperFile.getNomeSemBarra(valueNovoPath);

				vReturn += "    		<table width=\"350\" align=\"top\" class=\"tabela_popup\">";

				vReturn += "    			<tr>";
				vReturn += "    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >";
				
				vReturn +=  HelperString.ucFirst(context.getString(R.string.nome)) + ": <input type=\"text\" maxlength=\"200\" id=\"" + EXTDAOArquivo.NOME+ "\" name=\"" + EXTDAOArquivo.NOME+ "\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\"" + vNome + "\"/>";
				vReturn += "    				</td>";
				vReturn += "    			</tr>";
				vReturn += "    			<tr>";
				vReturn += "    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >";

				vReturn += "    				</td>";
				vReturn += "    			</tr>";
				vReturn += "    			<tr>";
				vReturn += "    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >";
				vReturn += "    					<input id=\"botaoIsUnique\" name=\"txtBotao\" type=\"button\" value=\"" + context.getString(R.string.download_unique_file).toUpperCase() + "\" class=\"botoes_form\" style=\"cursor: pointer;\" />" ;

				vReturn += "    				</td>";
				vReturn += "    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >";
				vReturn += "    					<input id=\"botaoZip\" name=\"txtBotao\" type=\"button\" value=\"" + context.getString(R.string.download_zip).toUpperCase() + "\" class=\"botoes_form\" style=\"cursor: pointer;\" />" ;

				vReturn += "    				</td>";

				vReturn += "    			</tr>";

				vReturn += "    		</table>";
				
				vReturn += "<br/>";
				vReturn += "<br/>";


			}
			vReturn += "<script>";
			vReturn += "jQuery(document).ready(function(){";

			vReturn += "jQuery('#botaoIsUnique').click(function(){";

			vReturn += "jQuery('#is_unique').val(\"1\");";
			vReturn += "jQuery('#form_list_arquivo').submit();";

			vReturn += "});";

			vReturn += "jQuery('#botaoZip').click(function(){";

			vReturn += "jQuery('#is_unique').val(\"0\");";
			vReturn += "jQuery('#form_list_arquivo').submit();";

			vReturn += "});";

			vReturn += "});";
			vReturn += "</script>";
			vReturn += "    	</form>";

		}

		return new ContainerResponse( __helper, new StringBuilder(vReturn));
	}
}
