package app.omegasoftware.wifitransferpro.http.pages;

import java.io.File;
import java.util.ArrayList;

import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivoPlaylist;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class PlayerPlaylist extends InterfaceHTML{

	public PlayerPlaylist(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		//		String vStrReturn =  Javascript.importarBibliotecaJavascriptMusicPlayer();

		StringBuilder vBuilder = new StringBuilder();
		String vPlaylist = __helper.GET("playlist");
		if(vPlaylist != null && vPlaylist.length() > 0 ){
			Database db = new DatabaseWifiTransfer(__helper.context);
			EXTDAOArquivoPlaylist vObj = new EXTDAOArquivoPlaylist(db);
			ArrayList<File> vListFile =  vObj.getListFile(vPlaylist);
			if(vListFile != null && vListFile.size() > 0 ){
				vBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				vBuilder.append("<playlist version=\"1\" xmlns=\"http://xspf.org/ns/0/\"> ");
				vBuilder.append("<trackList>");
				int i = 0 ;
				for (File vFile : vListFile) {
					String vPath = vFile.getAbsolutePath();
					String vFileName = HelperFile.getNameOfFileInPath(vPath);
					vBuilder.append("<track><creator>");
					vBuilder.append(String.valueOf(i));
					vBuilder.append("</creator>");
					vBuilder.append("<title>");
					vBuilder.append(vFileName);
					vBuilder.append("</title>");
					vBuilder.append("<location>");
					vBuilder.append("index.php5?tipo=pages&page=PlayerMusic&path_completo=" + vPath);
					vBuilder.append("</location>");
					vBuilder.append("</track>");
					i += 1;
				}

				vBuilder.append("</trackList>");
				vBuilder.append("</playlist>");
			}
			db.close();
		}
		return new ContainerResponse(__helper, vBuilder);

	}

}
