package app.omegasoftware.wifitransferpro.http.pages;
//package app.omegasoftware.riodejaneirobeer.http.pages;
//
//import app.omegasoftware.riodejaneirobeer.file.OmegaFileConfiguration;
//import app.omegasoftware.riodejaneirobeer.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
//import app.omegasoftware.riodejaneirobeer.http.ContainerResponse;
//import app.omegasoftware.riodejaneirobeer.http.InterfaceHTML;
//import app.omegasoftware.riodejaneirobeer.http.recursos.classes.classe.HelperHttp;
//import app.omegasoftware.riodejaneirobeer.http.recursos.classes.classe.Javascript;
//
//public class CopyOfUploadPagina extends InterfaceHTML{
//
//	public CopyOfUploadPagina(HelperHttp pHelperHTTP) {
//		super(pHelperHTTP);
//		// TODO Auto-generated constructor stub
//	}
//
//	@Override
//	public ContainerResponse getContainerResponse() {
//		String vRelativePathUploader = OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.LIBS) + "multiple_uploader/";
//		
//		// TODO Auto-generated method stub
//		String vStrReturn = "";
//		vStrReturn += "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\"";
//		vStrReturn += "	\"http://www.w3.org/TR/html4/strict.dtd\">";
//		vStrReturn += "<html lang=\"en\" xml:lang=\"en\">";
//		vStrReturn += "<head>";
//		vStrReturn += "	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";
//		vStrReturn += "	<title>Attach a File - Standalone Showcase from digitarald.de</title>";
//		
//		vStrReturn += "	<meta name=\"author\" content=\"Harald Kirschner, digitarald.de\" />";
//		vStrReturn += "	<meta name=\"copyright\" content=\"Copyright 2009 Harald Kirschner\" />";
//		
//		vStrReturn += "	<!-- Framework CSS -->";
//		vStrReturn += "	<link rel=\"stylesheet\" href=\"http://github.com/joshuaclayton/blueprint-css/raw/master/blueprint/screen.css\" type=\"text/css\" media=\"screen, projection\">";
//		vStrReturn += "	<link rel=\"stylesheet\" href=\"http://github.com/joshuaclayton/blueprint-css/raw/master/blueprint/print.css\" type=\"text/css\" media=\"print\">";
//		vStrReturn += "	<!--[if IE]><link rel=\"stylesheet\" href=\"http://github.com/joshuaclayton/blueprint-css/raw/master/blueprint/ie.css\" type=\"text/css\" media=\"screen, projection\"><![endif]-->";
//		vStrReturn += "<!--[if lte IE 7]>";
//		vStrReturn += "	<script type=\"text/javascript\" src=\"http://getfirebug.com/releases/lite/1.2/firebug-lite-compressed.js\"></script>";
//		vStrReturn += "<![endif]-->";
//		
//		
//		vStrReturn += "	<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/mootools/1.2.2/mootools.js\"></script>";
//		
//		vStrReturn += "	<script type=\"text/javascript\" src=\"" + vRelativePathUploader + "/source/Fx.ProgressBar.js\"></script>";
//		
//		vStrReturn += "	<script type=\"text/javascript\" src=\"" + vRelativePathUploader + "source/Swiff.Uploader.js\"></script>";
//		
//		vStrReturn += "	<script type=\"text/javascript\" src=\"" + vRelativePathUploader + "source/FancyUpload3.Attach.js\"></script>";
//		
//		
//		vStrReturn += "	<!-- See script.js -->";
//		vStrReturn += "	<script type=\"text/javascript\">";
//		
//		
//		vStrReturn += "		/**";
//		vStrReturn += " * FancyUpload Showcase";
//		vStrReturn += " *";
//		vStrReturn += " * @license		MIT License";
//		vStrReturn += " * @author		Harald Kirschner <mail [at] digitarald [dot] de>";
//		vStrReturn += " * @copyright	Authors";
//		vStrReturn += " */";
//		
//		vStrReturn += "window.addEvent('domready', function() {";
//		
//		vStrReturn += "	/**";
//		vStrReturn += "	 * Uploader instance";
//		vStrReturn += "	 */";
//		vStrReturn += "	var up = new FancyUpload3.Attach('demo-list', '#demo-attach, #demo-attach-2', {";
//		vStrReturn += "		path: '" + vRelativePathUploader + "source/Swiff.Uploader.swf',";
//		vStrReturn += "		url: 'index.php5?tipo=pages&page=Upload',";
//		vStrReturn += "		fileSizeMax: 2 * 1024 * 1024,";
//		
//		vStrReturn += "		verbose: true,";
//		
//		vStrReturn += "		onSelectFail: function(files) {";
//		vStrReturn += "			files.each(function(file) {";
//		vStrReturn += "				new Element('li', {";
//		vStrReturn += "					'class': 'file-invalid',";
//		vStrReturn += "					events: {";
//		vStrReturn += "						click: function() {";
//		vStrReturn += "							this.destroy();";
//		vStrReturn += "						}";
//		vStrReturn += "					}";
//		vStrReturn += "				}).adopt(";
//		vStrReturn += "					new Element('span', {html: file.validationErrorMessage || file.validationError})";
//		vStrReturn += "				).inject(this.list, 'bottom');";
//		vStrReturn += "			}, this);	";
//		vStrReturn += "		},";
//		
//		vStrReturn += "		onFileSuccess: function(file) {";
//		vStrReturn += "			new Element('input', {type: 'checkbox', 'checked': true}).inject(file.ui.element, 'top');";
//		vStrReturn += "			file.ui.element.highlight('#e6efc2');";
//		vStrReturn += "		},";
//		
//		vStrReturn += "		onFileError: function(file) {";
//		vStrReturn += "			file.ui.cancel.set('html', 'Retry').removeEvents().addEvent('click', function() {";
//		vStrReturn += "				file.requeue();";
//		vStrReturn += "				return false;";
//		vStrReturn += "			});";
//		vStrReturn += "			";
//		vStrReturn += "			new Element('span', {";
//		vStrReturn += "				html: file.errorMessage,";
//		vStrReturn += "				'class': 'file-error'";
//		vStrReturn += "			}).inject(file.ui.cancel, 'after');";
//		vStrReturn += "		},";
//		
//		vStrReturn += "		onFileRequeue: function(file) {";
//		vStrReturn += "			file.ui.element.getElement('.file-error').destroy();";
//		vStrReturn += "			";
//		vStrReturn += "			file.ui.cancel.set('html', 'Cancel').removeEvents().addEvent('click', function() {";
//		vStrReturn += "				file.remove();";
//		vStrReturn += "				return false;";
//		vStrReturn += "			});";
//		vStrReturn += "			";
//		vStrReturn += "			this.start();";
//		vStrReturn += "		}";
//		
//		vStrReturn += "	});";
//		
//		vStrReturn += "});";
//		
//		
//		vStrReturn += "	</script>";
//		
//		
//		
//		vStrReturn += "	<!-- See style.css -->";
//		vStrReturn += "	<style type=\"text/css\">";
//		vStrReturn += "		a.hover {";
//		vStrReturn += "	color: red;";
//		vStrReturn += "}";
//		
//		vStrReturn += "#demo-list {";
//		vStrReturn += "	padding: 0;";
//		vStrReturn += "	list-style: none;";
//		vStrReturn += "	margin: 0;";
//		vStrReturn += "}";
//		
//		vStrReturn += "#demo-list .file-invalid {";
//		vStrReturn += "	cursor: pointer;";
//		vStrReturn += "	color: #514721;";
//		vStrReturn += "	padding-left: 48px;";
//		vStrReturn += "	line-height: 24px;";
//		vStrReturn += "	background: url(assets/error.png) no-repeat 24px 5px;";
//		vStrReturn += "	margin-bottom: 1px;";
//		vStrReturn += "}";
//		vStrReturn += "#demo-list .file-invalid span {";
//		vStrReturn += "	background-color: #fff6bf;";
//		vStrReturn += "	padding: 1px;";
//		vStrReturn += "}";
//		
//		vStrReturn += "#demo-list .file {";
//		vStrReturn += "	line-height: 2em;";
//		vStrReturn += "	padding-left: 22px;";
//		vStrReturn += "	background: url(assets/attach.png) no-repeat 1px 50%;";
//		vStrReturn += "}";
//		
//		vStrReturn += "#demo-list .file span,";
//		vStrReturn += "#demo-list .file a {";
//		vStrReturn += "	padding: 0 4px;";
//		vStrReturn += "}";
//		
//		vStrReturn += "#demo-list .file .file-size {";
//		vStrReturn += "	color: #666;";
//		vStrReturn += "}";
//		
//		vStrReturn += "#demo-list .file .file-error {";
//		vStrReturn += "	color: #8a1f11;";
//		vStrReturn += "}";
//		
//		vStrReturn += "#demo-list .file .file-progress {";
//		vStrReturn += "	width: 125px;";
//		vStrReturn += "	height: 12px;";
//		vStrReturn += "	vertical-align: middle;";
//		vStrReturn += "	background-image: url(" + vRelativePathUploader + "assets/progress-bar/progress.gif);";
//		vStrReturn += "}";
//		vStrReturn += "	</style>";
//		
//		
//		vStrReturn += "</head>";
//		vStrReturn += "<body>";
//		
//		vStrReturn += "	<div class=\"container\">";
//		
//		vStrReturn += "		<h1><a href=\"http://digitarald.de/project/fancyupload/\">FancyUpload</a> Standalone Showcase from <a href=\"http://digitarald.de/\">digitarald.de</a></h1>";
//		
//		vStrReturn += "		<h2>Attach a File</h2>";
//		
//		
//		
//		vStrReturn += "		<!-- See index.html -->";
//		vStrReturn += "		<div>";
//		vStrReturn += "			<a href=\"#\" id=\"demo-attach\">Attach a file</a>";
//		
//		vStrReturn += "<ul id=\"demo-list\"></ul>";
//		
//		vStrReturn += "<a href=\"#\" id=\"demo-attach-2\" style=\"display: none;\">Attach another file</a>		</div>";
//		
//		
//		vStrReturn += "	</div>";
//		
//		vStrReturn += "	<div class=\"container quiet\" style=\"line-height: 5em;\">";
//		vStrReturn += "		� 2008-2009 by <a href=\"http://digitarald.de/\">Harald Kirschner</a> and available under <a href=\"http://www.opensource.org/licenses/mit-license.php\">The MIT License</a>";
//		vStrReturn += "	</div>";
//		
//		vStrReturn += "</body>";
//		vStrReturn += "</html>";
//		
//		return new ContainerResponse(__helper, vStrReturn); 
//	}
//
//}
