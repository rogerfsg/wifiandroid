package app.omegasoftware.wifitransferpro.common.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;


import android.app.Activity;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Table;
import android.os.Handler;
import android.os.Message;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class TableAutoCompleteTextView {

	LinkedHashMap<String, String> __hashIdByDefinition;

	String __attributeName = Table.NOME_UNIVERSAL;
	boolean __isToUpperCase = true;
	AutoCompleteTextView autoCompleteEditText;
	ArrayAdapter<String> arrayAdapter;
	Table __table;
	Activity activity;
	AutoCompleteTextViewHandler __handler;
	
	public TableAutoCompleteTextView(
			Activity pActivity,
			Table pTable,
			AutoCompleteTextView pAutoCompleteEditText){
		activity = pActivity;
		__table = pTable;

		autoCompleteEditText = pAutoCompleteEditText;
		updateAdapter(true);
		if(pAutoCompleteEditText != null)
			__handler = new AutoCompleteTextViewHandler();
	}


	public TableAutoCompleteTextView(
			Table pTable, 
			String pStrAttributeName, 
			boolean pIsToUpperCase){
		__isToUpperCase = pIsToUpperCase;
		__hashIdByDefinition = pTable.getHashMapIdByAttributeValue(pIsToUpperCase, pStrAttributeName);
		__attributeName = pStrAttributeName;
		__handler = new AutoCompleteTextViewHandler();
	}

	public void updateAdapter(){
		updateAdapter(false);
	}

	private void updateAdapter(boolean pIsFirstTime){
		__hashIdByDefinition = __table.getHashMapIdByDefinition(true);
		if(autoCompleteEditText != null){
			arrayAdapter = new ArrayAdapter<String>(
					activity,
					R.layout.spinner_item,
					getVetorStrId());

			autoCompleteEditText.setAdapter(arrayAdapter);
			if(!pIsFirstTime)
				autoCompleteEditText.postInvalidate();
		}
	}


	public String[] getVetorStrId(){
		if(__hashIdByDefinition == null) return new String[]{};
		Set<String> vListKey = __hashIdByDefinition.keySet();
		ArrayList<String> vListId = new ArrayList<String>(); 
		for (String vKey : vListKey) {
			String vContent = __hashIdByDefinition.get(vKey);
			vListId.add(vContent);
		}
		String vVetorId[] = new String[vListId.size()];
		vListId.toArray(vVetorId);
		return vVetorId;
	}



	public String getIdOfTokenIfExist(String pToken){
		if(__hashIdByDefinition == null) return null;
		Set<String> vListKey = __hashIdByDefinition.keySet();
		for (String vKey : vListKey) {
			String vContent = __hashIdByDefinition.get(vKey);
			if(pToken.compareTo(vContent) == 0 ) return vKey;

		}
		return null;
	}



	public String getTokenOfId(String pId){
		if(__hashIdByDefinition == null) return null;
		if(__hashIdByDefinition.containsKey(pId))
			return __hashIdByDefinition.get(pId);
		else return null;
	}


	//Retorna o id correspondente a tupla da tabela
	public String addTupleIfNecessay(Table pTable, String pTokenDefinition){
		String vId = getIdOfTokenIfExist(pTokenDefinition);
		if(vId == null){
			pTable.clearData();
			pTable.setAttrStrValue(__attributeName, pTokenDefinition);
			Long pId = pTable.insert();
			return pId.toString();
		} else{
			pTable.clearData();
			pTable.setAttrStrValue(Table.ID_UNIVERSAL, vId);
			pTable.select();
			return vId;
		}
	}

	//Envia o objeto Table com todos os atributos que compoe a unicidade da tupla
	public String addTupleIfNecessay(Table pTable){
		String vValueAttribute = pTable.getStrValueOfAttribute(__attributeName);
		String vId = getIdOfTokenIfExist(vValueAttribute);
		if(vId != null) return vId;
		else {

			pTable.formatToSQLite();
			Long vNewId = pTable.insert();

			if(vNewId >= 0 ){
				
//				updateAdapter(false);
				if(__handler != null)
					__handler.sendEmptyMessage(0);
				return vNewId.toString(); 
			}
			else return null;
		} 

	}
	

	public class AutoCompleteTextViewHandler extends Handler{
		
		public AutoCompleteTextViewHandler(){
			
		}
		
		@Override
	    public void handleMessage(Message msg)
	    {
			updateAdapter();
	    }
	}
}
