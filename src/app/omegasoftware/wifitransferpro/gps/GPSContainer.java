package app.omegasoftware.wifitransferpro.gps;

import android.content.Intent;

public class GPSContainer {
	public Double __latitude;
	public Double __longitude;
	public Boolean __gpsState;
	public GPSContainer(Double p_latitude, Double p_longitude){
		__latitude = p_latitude;
		__longitude = p_longitude;
	}
	public GPSContainer(Intent p_intent){
		loadIntent(p_intent);
	}
	
	public GPSContainer(){
		clear();
	}
	
	public boolean isInitialized(){
		if(__latitude != null && __longitude != null)
			return true;
		else return false;
	}
	
	public void clear(){
		__latitude = null ;
		__longitude = null ;
		__gpsState = false;
	}
	
	public void loadIntent(Intent p_intent){
		__gpsState = p_intent.getExtras().getBoolean(GPSConfiguration.SS_GPS_STATE);
		__latitude = p_intent.getExtras().getDouble(GPSConfiguration.SS_GPS_LOCATION_LATITUDE);
		__longitude = p_intent.getExtras().getDouble(GPSConfiguration.SS_GPS_LOCATION_LONGITUDE);
	}
	
}
