package app.omegasoftware.wifitransferpro.http;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

public class HelperDchpInfo {
	    public String   s_dns1 ;
	    public String   s_dns2;     
	    public String   s_gateway;  
	    public String   s_ipAddress;    
	    public String   s_leaseDuration;    
	    public String   s_netmask;  
	    public String   s_serverAddress;
	    
	    DhcpInfo d;
	    WifiManager wifii;

	    /** Called when the activity is first created. */
	    
	    public HelperDchpInfo(Context pContext) {
	        
	        
	        wifii= (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);
	        WifiInfo vInfo = wifii.getConnectionInfo();
	        String vIp = Formatter.formatIpAddress( vInfo.getIpAddress());
	        d=wifii.getDhcpInfo();
	        String vSSID = vInfo.getSSID();
	        s_dns1="DNS 1: "+String.valueOf(d.dns1);
	        s_dns2="DNS 2: "+String.valueOf(d.dns2);    
	        s_gateway="Default Gateway: "+String.valueOf(d.gateway);    
	        s_ipAddress="IP Address: "+String.valueOf(d.ipAddress); 
	        s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);     
	        s_netmask="Subnet Mask: "+String.valueOf(d.netmask);    
	        s_serverAddress="Server IP: "+String.valueOf(d.serverAddress);
	        
	        int i = 0 ;
	        i += 1;
	    }
	    
	    public String getIp()
	    {
	    	if(d == null || d.ipAddress == 0 ) return null;
	    	
	        return Formatter.formatIpAddress(d.ipAddress);
	    }
	    
	    public String getGateway()
	    {
	    	if(d == null || d.gateway == 0 ) return null;
	    	
	        return Formatter.formatIpAddress(d.gateway);
	    }


}
