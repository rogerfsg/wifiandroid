package app.omegasoftware.wifitransferpro.TabletActivities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.wifitransferpro.R;

import app.omegasoftware.wifitransferpro.PrincipalActivity;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.activity.OmegaRegularActivity;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivoTemporario;
import app.omegasoftware.wifitransferpro.http.HelperNetwork;
import app.omegasoftware.wifitransferpro.http.IndexServer;
import app.omegasoftware.wifitransferpro.listener.SendEmailClickListener;
import app.omegasoftware.wifitransferpro.service.ServiceApache;
import app.omegasoftware.wifitransferpro.service.ServiceCheckFoto;

public class PaginaInicialActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "PaginaInicialActivityMobile";
	
	private ToggleButton servidorToggleButton;
	private Button emailLinkButton;
	private Button configuracaoButton;
	private Button ajudaButton;
	private Button versaoProButton;
	private TextView urlTextView;
	private TextView urlServicoTextView;
	private ServicoListener servicoListener;

	private HandleUpdatePorta handleUpdatePorta;
	
	private enum TYPE_SERVICE{
		APACHE,
		CHECK_FOTO
	}

	public static final int ERROR_DIALOG_NO_WIFI = 222;
	public static final int DIALOG_PORTA_ATUALIZADA = 223;
	
	
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.unimedbh_dialog_error_textview);
		Button vOkButton = (Button) vDialog.findViewById(R.id.unimedbh_dialog_ok_button);

		try{
			switch (id) {
			
			case ERROR_DIALOG_NO_WIFI:
				vTextView.setText(getResources().getString(R.string.dialog_no_wifi));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_NO_WIFI);
					}
				});
				break;
			case DIALOG_PORTA_ATUALIZADA:
				vTextView.setText(getResources().getString(R.string.porta_atualizada));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(DIALOG_PORTA_ATUALIZADA);
					}
				});
				break;
			default:
				//			vDialog = this.getErrorDialog(id);
				return super.onCreateDialog(id);
			}
		} catch(Exception ex){
			return null;
		}
		return vDialog;

	}
	
	static Intent __serviceApache = null;
	static Intent __serviceCheckFoto = null;
	static Activity __paginaInicialActivity;
	String __idUsuario = null;
	private Typeface customTypeFace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pagina_inicial_layout);
		
		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		__paginaInicialActivity = this;
		
		
		new CustomDataLoader(this).execute();
	}

	private static void stopAllService(){
		TYPE_SERVICE vVetorType[]  = TYPE_SERVICE.values();
		if(vVetorType != null && vVetorType.length > 0 ){
			for (TYPE_SERVICE typeService : vVetorType) {
				stopService(typeService);	
			}
			
		}
	}
	
	private static void startService(TYPE_SERVICE pType){
		switch (pType) {
		case APACHE:
			try{
				if(__serviceApache == null){
					__serviceApache = new Intent(__paginaInicialActivity, ServiceApache.class);
					__paginaInicialActivity.startService(__serviceApache);	
				}
			} catch(Exception ex){
				
			}	
			break;
		case CHECK_FOTO:
			
			try{
				if(__serviceCheckFoto == null){
					__serviceCheckFoto = new Intent(__paginaInicialActivity, ServiceCheckFoto.class);
					__paginaInicialActivity.startService(__serviceCheckFoto);
				}
			} catch(Exception ex){
				
			}	
			break;

		default:
			break;
		}
	}
	
	private static boolean isServiceRunning(TYPE_SERVICE pType){
		switch (pType) {
		case APACHE:
			if(__serviceApache != null)
				return true;
			else return false;
			
		case CHECK_FOTO:
			if(__serviceCheckFoto != null)
				return true;
			else return false;
			
		default:
			return false;
		}
	}
	
	private static void stopService(TYPE_SERVICE pType){
		switch (pType) {
		case APACHE:
			try{
				if(__serviceApache != null){
					__paginaInicialActivity.stopService(__serviceApache);
					__serviceApache = null;
				}	
			} catch(Exception ex){
				
			}	
			break;
		case CHECK_FOTO:
			
			try{
				if(__serviceCheckFoto != null){
					__paginaInicialActivity.stopService(__serviceCheckFoto);
					__serviceCheckFoto = null;
				}	
			} catch(Exception ex){
				
			}	
			break;

		default:
			break;
		}
		
		
		
	}
	
	public static void logout(Context pContext){
		EXTDAOArquivoTemporario.deleteAllArquivoTemporario(pContext);
		
		Intent intent = new Intent(pContext, PrincipalActivity.class);
		
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		logout();
		pContext.startActivity(intent);
	}
	
	public static void logout(){
		try{
			stopAllService();
		}catch(Exception ex){
			
		}
	}
	
	@Override
	public boolean loadData() {
		return true;	
	}

	@Override
	public void finish() {
		if(!servidorToggleButton.isChecked()){
			logout();
		}
		try{
			super.finish();
		}catch(Exception ex){

		}
	}
	
	@Override
	public void onBackPressed(){
		if(!servidorToggleButton.isChecked()){
			logout();
		}
		super.onBackPressed();
	}
	
	@Override
	public void initializeComponents() {
		
		EXTDAOArquivoTemporario.deleteAllArquivoTemporario(this);
		
		urlServicoTextView = (TextView) findViewById(R.id.url_servico_textview);
		urlTextView = (TextView) findViewById(R.id.url_textview);
		emailLinkButton = (Button) findViewById(R.id.email_link_button);
		emailLinkButton.setOnClickListener( new SendEmailClickListener(getResources().getString(R.string.app_name), urlTextView));
		
		configuracaoButton = (Button) findViewById(R.id.configuracao_button);
		configuracaoButton.setOnClickListener(new ConfiguracaoButtonListener(this));
		ajudaButton = (Button) findViewById(R.id.ajuda_button);
		ajudaButton.setOnClickListener(new AjudaButtonListener(this));
		versaoProButton = (Button) findViewById(R.id.versao_pro_button);
		versaoProButton.setVisibility(OmegaConfiguration.VISIBILITY_HIDDEN);
		String vIp =  null;
		for(int i = 0 ; i < 3; i++){
			vIp = HelperNetwork.getIp(this);
			if(vIp != null) break;
		}
		
			
		if(vIp != null){
			urlTextView.setText(IndexServer.getRootAddress(vIp));	
		} else{
			String vSemWifi = getResources().getString( R.string.sem_wifi);
			urlTextView.setText(vSemWifi);
		}
		
		servidorToggleButton = (ToggleButton) findViewById(R.id.servidor_togglebutton);
		
		handleUpdatePorta = new HandleUpdatePorta(this, servidorToggleButton, TYPE_SERVICE.APACHE);
		
		if(isServiceRunning(TYPE_SERVICE.APACHE))
			servidorToggleButton.setChecked(true);
		else servidorToggleButton.setChecked(false);
		servicoListener = new ServicoListener(this, servidorToggleButton, TYPE_SERVICE.APACHE);
		servidorToggleButton.setOnCheckedChangeListener(servicoListener);
		startService(TYPE_SERVICE.CHECK_FOTO);
		
		this.applyTypeFace();
		
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_CONFIGURACAO:
			switch (resultCode) {
			case OmegaConfiguration.RESULT_PORTA:
				handleUpdatePorta.sendEmptyMessage(HandleUpdatePorta.WHAT_UPDATE_PORTA_E_DESLIGA_SERVICO);
				break;

			default:
				break;
			}
			break;

		default:
			break;
		}
	}
	
	public void applyTypeFace()
	{

		//Search mode buttons
		urlServicoTextView.setTypeface(this.customTypeFace);
		emailLinkButton.setTypeface(this.customTypeFace);
		configuracaoButton.setTypeface(this.customTypeFace);
		ajudaButton.setTypeface(this.customTypeFace);
		versaoProButton.setTypeface(this.customTypeFace);
		urlTextView.setTypeface(this.customTypeFace);
		servidorToggleButton.setTypeface(this.customTypeFace);

	}
	
	private class ServicoListener implements OnCheckedChangeListener 
	{
		
		Activity __activity;
		ToggleButton __toggleButton;
		TYPE_SERVICE __typeService;
		public ServicoListener(Activity pActivity, ToggleButton pToggleButton, TYPE_SERVICE pTypeService){
			__activity = pActivity;
			__typeService = pTypeService;
			__toggleButton = pToggleButton;
		}

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			new ServicoOnCheckChangeLoader(__activity, __toggleButton, __typeService, isChecked).execute();
		}
	}	

	public class HandleUpdatePorta extends Handler{
		static public final int WHAT_UPDATE_PORTA_E_DESLIGA_SERVICO = 0;
		Activity __activity;
		ToggleButton __togglerButton;
		TYPE_SERVICE __typeService;
		public HandleUpdatePorta(Activity pActivity, ToggleButton pToggleButton, TYPE_SERVICE pTypeService){
			__togglerButton = pToggleButton;
			__typeService = pTypeService;
			__activity = pActivity;
		}

		@Override
		public void handleMessage(Message msg)
		{
			if(msg != null){
				switch (msg.what) {
				case WHAT_UPDATE_PORTA_E_DESLIGA_SERVICO:
					__togglerButton.setChecked(false);
					__togglerButton.setTextColor(R.color.dark_gray);
					String vServicoForaDoAr = __activity.getResources().getString(R.string.servico_desligado);
					urlTextView.setText(vServicoForaDoAr);
					stopService(__typeService);
					showDialog(DIALOG_PORTA_ATUALIZADA);
					break;
				
				default:
					break;
				}
			}
		}
	}
	

	public class ServicoOnCheckChangeLoader extends AsyncTask<String, Integer, Boolean>
	{

		Integer dialogId = null;
		Activity __activity = null;
		boolean __isChecked = false;
		TYPE_SERVICE __typeService;
		ToggleButton __toggleButton = null;
		HandlerToggleButton __handlerToggleButton;
		public ServicoOnCheckChangeLoader(Activity pActivity, ToggleButton pToggleButton, TYPE_SERVICE pTypeService, boolean pIsChecked){
			__activity = pActivity;
			__isChecked = pIsChecked;
			__toggleButton = pToggleButton;
			__typeService =pTypeService;
					
			__handlerToggleButton = new HandlerToggleButton(__toggleButton, __typeService);
		}

		public class HandlerToggleButton extends Handler{
			static public final int WHAT_FALSE = 0;
			static public final int WHAT_TRUE = 1; 
			ToggleButton __togglerButton;
			TYPE_SERVICE __typeService;
			public HandlerToggleButton(ToggleButton pToggleButton, TYPE_SERVICE pTypeService){
				__togglerButton = pToggleButton;
				__typeService = pTypeService;
			}

			@Override
			public void handleMessage(Message msg)
			{
				if(msg != null){
					switch (msg.what) {
					case WHAT_FALSE:
						__toggleButton.setChecked(false);
						__toggleButton.setTextColor(R.color.dark_gray);
						String vServicoForaDoAr = __activity.getResources().getString(R.string.servico_desligado);
						urlTextView.setText(vServicoForaDoAr);
						stopService(__typeService);
						break;
					case WHAT_TRUE:
						Bundle vParameter = msg.getData();
						String vIp = null;
						if(vParameter != null)
							vIp = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_IP);
						if(vIp != null)
							urlTextView.setText(IndexServer.getRootAddress(vIp));
						
						__toggleButton.setTextColor(R.color.white);
						__toggleButton.setChecked(true);
						startService(__typeService);
						break;
					default:
						break;
					}
				}
			}
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				String vIp = HelperNetwork.getIp(__activity);
				if(vIp == null){
					dialogId = ERROR_DIALOG_NO_WIFI;
					__handlerToggleButton.sendEmptyMessage(HandlerToggleButton.WHAT_FALSE);
					return false;
				}
				else if(__toggleButton != null && __typeService != null){
					if(!__isChecked)
					{
						__handlerToggleButton.sendEmptyMessage(HandlerToggleButton.WHAT_FALSE);
					}
					else
					{
						Message vMessage = new Message();
						vMessage.what = HandlerToggleButton.WHAT_TRUE;
						Bundle vParameter = new Bundle();
						vParameter.putString(OmegaConfiguration.SEARCH_FIELD_IP, vIp);
						vMessage.setData(vParameter);
						__handlerToggleButton.sendMessage(vMessage);
					}
				}
			}
			catch(Exception e){
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}finally{

			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);
			
			if(dialogId != null){
				showDialog(dialogId);
			}
		}
	}
	private class ConfiguracaoButtonListener implements OnClickListener
	{
		Activity __activity;
		public ConfiguracaoButtonListener(Activity pActivity){
			__activity = pActivity;
		}

		public void onClick(View v) {
			Intent intent = new Intent(getApplicationContext(), ConfiguracaoActivityMobile.class);
			startActivityForResult(intent, OmegaConfiguration.ACTIVITY_CONFIGURACAO);
		}

	}	
	private class AjudaButtonListener implements OnClickListener
	{
		Activity __activity;
		public AjudaButtonListener(Activity pActivity){
			__activity = pActivity;
		}

		public void onClick(View v) {
			Intent intent = new Intent(getApplicationContext(), AjudaActivityMobile.class);
			startActivity(intent);
			

		}

	}
}
