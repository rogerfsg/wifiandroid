package app.omegasoftware.wifitransferpro.listener;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class EmailClickListener implements OnClickListener {

private Integer __buttonId = null;
	
	public EmailClickListener(int pButtonId){
		__buttonId = pButtonId;
	}
	public void onClick(View v) {
		// TODO Auto-generated method stub
	
		Button view = (Button) v.findViewById(__buttonId);
		if(view == null)
		{
			view = (Button) v;
		}
		
		String emailAddress = (String) view.getText();
		
		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ emailAddress });
		intent.setType("text/plain");
		v.getContext().startActivity(intent);
		
	}
	
	
}
