package app.omegasoftware.wifitransferpro.common.ItemList;


public class EntidadeItemList extends CustomItemList {

	
	
	public static final String NOME = "nome";
	public static final String ID_TIPO_ENTIDADE = "id_tipo_entidade";
	public static final String NOME_TIPO_ENTIDADE = "nome_tipo_entidade";
	public static final String DISTANCIA_MEU_PONTO_E_PONTO_ORIGEM = "distancia";
	public static final String IS_FAVORITO = "is_favorito";
	
	
	
	public EntidadeItemList(
			String pId,
			String pNome,
			String pIdTipoEntidade,
			String pNomeTipoEntidade, 
			String pDistancia,
			String pIsFavorito) {
		super(pId, 
				new ContainerItem[]{
				new ContainerItem(NOME, pNome),
				new ContainerItem(ID_TIPO_ENTIDADE, pIdTipoEntidade),
				new ContainerItem(NOME_TIPO_ENTIDADE, pNomeTipoEntidade),
				new ContainerItem(DISTANCIA_MEU_PONTO_E_PONTO_ORIGEM, pDistancia),
				new ContainerItem(IS_FAVORITO, pIsFavorito)});
		
	}
	
}
