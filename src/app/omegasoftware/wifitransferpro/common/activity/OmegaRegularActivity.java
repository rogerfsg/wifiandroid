package app.omegasoftware.wifitransferpro.common.activity;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.TabletActivities.AboutActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;

public abstract class OmegaRegularActivity extends Activity {

	private String TAG = "OmegaRegularActivity";

	protected String __vetorTabelaRelacionada[];
	protected boolean __isExecutingFinishTask = false;
	protected enum TYPE_SYNC {INITIAL, END, INITIAL_AND_END, NO_SYNC};
	protected TYPE_SYNC __typeSync = TYPE_SYNC.NO_SYNC;

	String __nomeTag = null;


	public abstract boolean loadData();
	public abstract void initializeComponents();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TAG))
			__nomeTag = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_TAG);
	}

	public void setTag(String p_tag)
	{
		this.TAG = p_tag;
	}

	private void setVetorTabelaRelacionada(String pVetorTabelaRelacionada[]){
		__vetorTabelaRelacionada = pVetorTabelaRelacionada;
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;

	}

	@Override
	public void finish(){
		if(__vetorTabelaRelacionada != null && __vetorTabelaRelacionada.length > 0 && !__isExecutingFinishTask &&
				(__typeSync == TYPE_SYNC.END || __typeSync == TYPE_SYNC.INITIAL_AND_END)){
			__isExecutingFinishTask = true;


			super.finish();
		}
		else {

			super.finish();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.unimedbh_dialog_error_textview);
		Button vOkButton = (Button) vDialog.findViewById(R.id.unimedbh_dialog_ok_button);

		try{
			switch (id) {
			case OmegaConfiguration.ON_LOAD_DIALOG:
				ProgressDialog vDialogAux = new ProgressDialog(this);

				vDialogAux.setMessage(getResources().getString(R.string.string_loading));
				return vDialogAux;
			case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
				vTextView.setText(getResources().getString(R.string.dialog_noresult_alert));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
						finish();
					}
				});
				break;
			
			default:
				//			vDialog = this.getErrorDialog(id);
				return null;
			}
		} catch(Exception ex){
			return null;
		}
		return vDialog;

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;

		switch (item.getItemId()) {

		case R.id.menu_favorito:
			
			break;

		case R.id.menu_sobre:
			intent = new Intent(this, AboutActivityMobile.class);
			break;

		default:
			return super.onOptionsItemSelected(item);

		}

		if(intent != null){
			startActivity(intent);	
		}

		return true;

	}


	@Override
	protected void onDestroy(){

		super.onDestroy();
	}

	public void procedureBeforeLoadData(){

	}

	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity __activity;
		public CustomDataLoader(Activity pActivity){
			__activity = pActivity;
			__typeSync = TYPE_SYNC.NO_SYNC;
		}
		public CustomDataLoader(Activity pActivity, String[] pVetorTabelaRelacionada, TYPE_SYNC pTypeSync){
			__activity = pActivity;
			setVetorTabelaRelacionada(pVetorTabelaRelacionada);
			__typeSync =  pTypeSync;
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			procedureBeforeLoadData();
			
			return loadData();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);	

			if(result)
			{

				initializeComponents();
			}

			try
			{
				dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);	
			}
			catch(Exception e)
			{
			}

		}

	}

}
