package app.omegasoftware.wifitransferpro.mapa;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class InfoItemizedOverlay extends  BalloonItemizedOverlay<OverlayItem> {


	private List<OverlayItem> mListOverlay = new ArrayList<OverlayItem>();
	private List<BalloonOverlayView> mListBalloonOverlayView = new ArrayList<BalloonOverlayView>();

	private Context c;


	public InfoItemizedOverlay(Drawable defaultMarker, MapView c) {

		super(boundCenterBottom(defaultMarker), c);
		this.c = c.getContext();

	}

	private void mostrarBalao(int indice){
		onTap(indice);
	}

	public void limparOverlay(){

		this.hideBalloon();
		mListOverlay.clear();
	}

	public void remove(Integer pIndex){

		mListOverlay.remove(pIndex);

		BalloonOverlayView vBalao = mListBalloonOverlayView.get(pIndex);

		if(vBalao != null) 
			mapView.removeView(vBalao);

		mListBalloonOverlayView.remove(pIndex);

	}

	public void clear(){

		mListOverlay.clear();
		mListBalloonOverlayView.clear();
		//		populate();
	}

	public void addOverlay(OverlayItem overlay) {
		if(overlay != null){
			mListOverlay.add(overlay);
			mostrarBalao(mListOverlay.size() - 1);

			if(addLastBallon(mListOverlay.size() - 1))
				populate( );
		}
	}

	@Override
	protected OverlayItem createItem(int i) {
		OverlayItem vItem = null;
		if(mListOverlay.size() > i){
			vItem = mListOverlay.get(i);
		}

		return vItem;
	}

	private boolean addLastBallon(int i){

		if(this.balloonView == null) return false;
		else {
			mListBalloonOverlayView.add(balloonView);

			return true;
		} 

	}

	@Override
	public int size() {

		return mListOverlay.size();
		//tablet, iphone, mobileandroid, iphone - segunda - flavialarcao@yahoo.com.br  - 
	}

	@Override
	protected boolean onBalloonTap(int index) {

		Toast.makeText(c, "onBalloonTap for overlay index " + index,
				Toast.LENGTH_LONG).show();

		return true;

	}

}
