package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class UploadPagina extends InterfaceHTML{

//	public static int SIZE_FILE_MAX = 100 * 1024 * 1024 * 1024; 
	public static int SIZE_FILE_MAX = 30 * 1024 * 1024;
	public UploadPagina(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		String vRelativePathUploader = OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.LIBS) + "multiple_uploader/";
		String vStrPath = __helper.GET("path");
		if (vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		vStrPath = HelperFile.getNomeSemBarra(vStrPath);
		vStrPath += "/";
		
		// TODO Auto-generated method stub
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("      <form name=\"form_upload_arquivo\" id=\"form_upload_arquivo\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
		vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");
		vBuilder.append("           <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
		vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
		vBuilder.append("           <input type=\"hidden\" name=\"class\" id=\"class\" value=\""
				+ EXTDAOArquivo.NAME + "\">");
		vBuilder.append("            <input type=\"hidden\" name=\"action\" id=\"action\" value=\"uploadListaArquivo\">");
		
		vBuilder.append("            <input type=\"hidden\" name=\"path\" id=\"path\" value=\"" + vStrPath + "\">");
//		vBuilder.append("            <input type=\"hidden\" name=\"checkbox_is_override\" id=\"checkbox_is_override\" value=\"" + __helper.POST("checkbox_is_override") + "\">");
		
		vBuilder.append("	<script type=\"text/javascript\" src=\"" + OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.LIBS) + "mootools/mootools.js\"></script>");
		
		vBuilder.append("	<script type=\"text/javascript\" src=\"" + vRelativePathUploader + "source/Fx.ProgressBar.js\"></script>");
		vBuilder.append("	<script type=\"text/javascript\" src=\"" + vRelativePathUploader + "source/Swiff.Uploader.js\"></script>");
		vBuilder.append("	<script type=\"text/javascript\" src=\"" + vRelativePathUploader + "source/FancyUpload3.Attach.js\"></script>");



		vBuilder.append("	<!-- See style.css -->");
		vBuilder.append("	<style type=\"text/css\">");
		vBuilder.append("		a.hover {");
		vBuilder.append("	color: red;");
		vBuilder.append("}");

		vBuilder.append("#demo-list {");
		vBuilder.append("	padding: 0;");
		vBuilder.append("	list-style: none;");
		vBuilder.append("	margin: 0;");
		vBuilder.append("}");

		vBuilder.append("#demo-list .file-invalid {");
		vBuilder.append("	cursor: pointer;");
		vBuilder.append("	color: #514721;");
		vBuilder.append("	padding-left: 48px;");
		vBuilder.append("	line-height: 24px;");
		vBuilder.append("	background: url(assets/error.png) no-repeat 24px 5px;");
		vBuilder.append("	margin-bottom: 1px;");
		vBuilder.append("}");
		vBuilder.append("#demo-list .file-invalid span {");
		vBuilder.append("	background-color: #fff6bf;");
		vBuilder.append("	padding: 1px;");
		vBuilder.append("}");

		vBuilder.append("#demo-list .file {");
		vBuilder.append("	line-height: 2em;");
		vBuilder.append("	padding-left: 22px;");
		vBuilder.append("}");

		vBuilder.append("#demo-list .file span,");
		vBuilder.append("#demo-list .file a {");
		vBuilder.append("	padding: 0 4px; " +
							"color: #555555;");
		vBuilder.append("}");

		vBuilder.append("#demo-list .file .file-size {");
		vBuilder.append("	color: #666;");
		vBuilder.append("}");

		vBuilder.append("#demo-list .file .file-error {");
		vBuilder.append("	color: #000000;");
		vBuilder.append("}");

		vBuilder.append("#demo-list .file .file-progress {");
		vBuilder.append("	width: 125px;");
		vBuilder.append("	height: 12px;");
		vBuilder.append("	vertical-align: middle;");
		vBuilder.append("	background-image: url(" + vRelativePathUploader + "assets/progress-bar/progress.gif);");
		vBuilder.append("}");
		vBuilder.append("	</style>");
		vBuilder.append("	<!-- See script.js -->");
		vBuilder.append("	<script type=\"text/javascript\">");
		
		vBuilder.append("	var up;");
		vBuilder.append("	var pathUpload = '" + vStrPath + "';");

		
		
		vBuilder.append("window.addEvent('domready', function() {");
		
		vBuilder.append("jQuery('#checkbox_is_override').change(function(){");

		vBuilder.append("up.options.url = 'index.php5?tipo=pages&page=Upload&path=' + pathUpload + '&is_override=' + jQuery(this).attr('checked');");
		
		vBuilder.append("});");

		vBuilder.append("	/**");
		vBuilder.append("	 * Uploader instance");
		vBuilder.append("	 */");
		vBuilder.append("	    up = new FancyUpload3.Attach('demo-list', '#demo-attach, #demo-attach-2', {");
		vBuilder.append("		path: '" + vRelativePathUploader + "source/Swiff.Uploader.swf',");
//		vBuilder.append("		url: 'index.php5?tipo=pages&page=Upload' + jQuery('#override_files').attr('checked'),");
		vBuilder.append("		url: 'index.php5?tipo=pages&page=Upload&path=" + vStrPath + "&is_override=false',"    );
		vBuilder.append("		fileSizeMax: " + String.valueOf(SIZE_FILE_MAX) + ",");

		vBuilder.append("		verbose: true,");

		vBuilder.append("		onSelectFail: function(files) {");
		vBuilder.append("			files.each(function(file) {");
		vBuilder.append("				new Element('li', {");
		vBuilder.append("					'class': 'file-error',");
		vBuilder.append("					events: {");
		vBuilder.append("						click: function() {");
		vBuilder.append("							this.destroy();");
		vBuilder.append("						}");
		vBuilder.append("					}");
		vBuilder.append("				}).adopt(");
		vBuilder.append("					new Element('span', {html: file.validationErrorMessage || file.validationError})");
		vBuilder.append("				).inject(this.list, 'bottom');");
		vBuilder.append("			}, this);	");
		vBuilder.append("		},");
		
		vBuilder.append("		onFileSuccess: function(file) {");
		vBuilder.append("			new Element('img', {'src' : '/adm/imgs/icone_excluir.png', 'class': 'imagem_remover_arquivo'}).inject(file.ui.element, 'top');");
		vBuilder.append("			file.ui.element.highlight('#e6efc2');");
		vBuilder.append("			addEventRemoverArquivo();");
		vBuilder.append("		},");

		vBuilder.append("		onFileError: function(file) {");
		vBuilder.append("			file.ui.cancel.set('html', 'Retry').removeEvents().addEvent('click', function() {");
		vBuilder.append("				file.requeue();");
		vBuilder.append("				return false;");
		vBuilder.append("			});");
		vBuilder.append("			");
		vBuilder.append("			new Element('span', {");
		vBuilder.append("				html: '',");
		vBuilder.append("				'class': 'file-error'");
		vBuilder.append("			}).inject(file.ui.cancel, 'after');");
		vBuilder.append("		},");

		vBuilder.append("		onFileRequeue: function(file) {");
		vBuilder.append("			file.ui.element.getElement('.file-error').destroy();");
		vBuilder.append("			");
		vBuilder.append("			file.ui.cancel.set('html', 'Cancel').removeEvents().addEvent('click', function() {");
		vBuilder.append("				file.remove();");
		vBuilder.append("				return false;");
		vBuilder.append("			});");
		vBuilder.append("			");
		vBuilder.append("			this.start();");
		vBuilder.append("		}");

		vBuilder.append("	});");

		vBuilder.append("});");
		
		vBuilder.append("function addEventRemoverArquivo(){");

		vBuilder.append("jQuery('.imagem_remover_arquivo').click(function(){");

		vBuilder.append("var nomeDoArquivo = jQuery(this).siblings('.file-title').html();");
				
		vBuilder.append("jQuery(this).parent('.file').remove();");
				
		vBuilder.append("jQuery.ajax({");
		vBuilder.append("url: 'actions.php5?class=arquivo&action=removerArquivoUpload&path="+ vStrPath + "z&nome_arquivo=' + nomeDoArquivo,");
		vBuilder.append("success: function(data){");
		vBuilder.append("alert(\"");
		vBuilder.append(__helper.context.getString(R.string.arquivo_removido_com_sucesso));
		vBuilder.append("\");");		
		vBuilder.append("}");
		vBuilder.append("});");

		vBuilder.append("});");

		vBuilder.append("}");

		vBuilder.append("	</script>");
		vBuilder.append("<table width=\"350\" align=\"top\" class=\"tabela_list\">");

		vBuilder.append("<colgroup>");
		vBuilder.append("<col width=\"90%\" />");
		vBuilder.append("<col width=\"10%\" />");
		vBuilder.append("</colgroup>");
		vBuilder.append("<thead>");

		vBuilder.append("    			<tr>");
		vBuilder.append("    				<td class=\"td_list_titulos\" colspan=\"2\">");

		vBuilder.append("<img class=\"icones_list\" src=\""
			+ OmegaFileConfiguration
					.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
			+ "icone_upload.png\">&nbsp;");
		
		vBuilder.append(__helper.context.getString(R.string.upload_arquivo));
		vBuilder.append("    			</td>");
		vBuilder.append("    			</tr>");
		vBuilder.append("    			<tr>");
		vBuilder.append("    			<td class=\"td_list_titulos\">");
		vBuilder.append("<input id=\"checkbox_is_override\" type=\"checkbox\" value=\"1\" name=\"checkbox_is_override\">");
		
		vBuilder.append(__helper.context.getString(R.string.is_override));
		vBuilder.append("    			</td>");
		vBuilder.append("    			<td class=\"td_list_conteudo\" align=\"right\">");
		
		vBuilder.append("<img class=\"icones_list\" src=\""
				+ OmegaFileConfiguration
						.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
				+ "new_window.png\" " +
				" onclick=\"javascript:window.open('popup.php5?tipo=pages&page=UploadPagina&path=" + vStrPath + "', '123324')\">&nbsp;");
		vBuilder.append("    			</td>");
		vBuilder.append("    			</tr>");
		vBuilder.append("    			<tr>");
		vBuilder.append("    			<td class=\"td_list_conteudo\" colspan=\"2\">");
		
		vBuilder.append(vStrPath);
		vBuilder.append("    			</td>");
		
		vBuilder.append("    			</tr>");
		
		vBuilder.append("    			<tr>");
		vBuilder.append("    			<td class=\"td_list_conteudo\" colspan=\"2\">");


		vBuilder.append("			<a href=\"#\" id=\"demo-attach\">");
		vBuilder.append(__helper.context.getString(R.string.clique_aqui));
		vBuilder.append("</a>");
		vBuilder.append("    			</td>");
		vBuilder.append("    			</tr>");
		
		vBuilder.append("</thead>");
		vBuilder.append("<tbody>");
		vBuilder.append("    			<tr>");
		vBuilder.append("    			<td class=\"td_list_conteudo\" colspan=\"2\">");
		vBuilder.append("<ul id=\"demo-list\"></ul>");
		vBuilder.append("<a href=\"#\" id=\"demo-attach-2\" style=\"display: none;\">");
		vBuilder.append(__helper.context.getString(R.string.novo_arquivo));
		vBuilder.append("</a>");
		vBuilder.append("    			</td>");
		vBuilder.append("    			</tr>");
		vBuilder.append("</tbody>");
		
		vBuilder.append("</table>");
		vBuilder.append("</form>");
		return new ContainerResponse(__helper,vBuilder); 
	}

}
