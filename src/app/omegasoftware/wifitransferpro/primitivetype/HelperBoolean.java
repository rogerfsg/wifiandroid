package app.omegasoftware.wifitransferpro.primitivetype;

public class HelperBoolean {
	public static Boolean valueOfStringSQL(String pToken){
		if(pToken == null) return null;
		else if(pToken.compareTo("0") == 0 ) return false;
		else if(pToken.compareTo("1") == 0 ) return true;
		else return null;
	}
}
