var offsetxpoint=-60; //Customize x offset of tooltip
var offsetypoint= 20; //Customize y offset of tooltip
var ie=document.all;
var ns6=document.getElementById && !document.all;
var enabletip = false;
var isFilhoAtivado = false;
var tipobj;

if (ie||ns6)
	tipobj=document.getElementById? document.getElementById("ToolTip"): "";

function ietruebody()
{
    return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}


topColor = "#001429";
subColor = "#f9f9f9";

function tip(TContent, isFilhoLocal, titulo, dimensoesDiv){
    
	if(!tipobj){
		
		return;
		
	}
	
	if (ns6||ie){
    	
    	if(isFilhoLocal == true){
    	
    		isFilhoAtivado = true;
    		
    	}
    	else{
    		
    		if(isFilhoAtivado){
    		
    			return;
    			
    		}

    	}
    	
    	var larguraDiv = 0;

    	if(document.getElementById(TContent)){

    		var conteudo = document.getElementById(TContent).innerHTML;
    		var largura = document.getElementById(TContent).style.display;
    		var altura = document.getElementById(TContent).offsetHeight;
    		TContent = conteudo;    		
    		larguraDiv = largura;
    		
    	}
    	else{
    	
	    	 var linhas = TContent.toLowerCase().split("<br />");
	         var i;
	         var maiorLinha = 0;
	         for(i=0; i < linhas.length; i++){
	         	
	         	linhas[i] = linhas[i].trim();
	         	
	         	if(linhas[i].length > maiorLinha){
	         		
	         		maiorLinha = linhas[i].length;
	         	}
	         	
	         }
	
	         var larguraDiv = Math.ceil((maiorLinha*6.8) + 15);
	         
    	}
    	
    	var alturaDiv = 0;
    	
    	if(dimensoesDiv !== undefined && dimensoesDiv.constructor.toString().indexOf("Array") != -1){
    		
    		larguraDiv = dimensoesDiv[0];
    		alturaDiv = dimensoesDiv[1];
    		
    		
    	}
         
         var strTitulo = "";
         
         if(titulo != undefined){
        	 
        	 strTitulo = '<tr><td width="100%" style="vertical-align: top; text-align: center; font-weight: bold; height: 30px;">'+
             '<font class="tooltipcontent" style="color:#ff0000;">'+titulo+'</font>'+
             '</td></tr>';
        	 
         }

        thetext = 
		'<table border="0" width="100%" cellspacing="0" cellpadding="0">'+
        '<tr><td width="100%" bgcolor="#D9D9DB">'+
        '<table border="0" width="100%" cellspacing="3" cellpadding="0">'+
        '<tr><td width="100%" bgcolor='+subColor+' style="padding-top:10px; padding-bottom:10px;">'+
        '<table border="0" width="95%" cellpadding="0" cellspacing="1" align="center">'+
        strTitulo +
        '<tr><td width="100%">'+
        '<font class="tooltipcontent">'+TContent+'</font>'+
        '</td></tr>'+
        '</table>'+
        '</td></tr>'+
        '</table>'+
        '</td></tr>'+
        '</table>';
        
        tipobj.innerHTML=thetext;
        
        if(larguraDiv){
        
        	tipobj.style.width = larguraDiv + "px";
        
		}
        
        if(alturaDiv){
        	
        	tipobj.style.height = alturaDiv + "px";
        	
        }
	
        enabletip = true;
        
        return false;
    }
}

function positiontip(e){
    if (enabletip){
        var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
        var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;

        //Descobre o quao longe esta o mouse da borda da janela
        var rightedge=ie&&!window.opera? ietruebody().clientWidth-event.clientX-offsetxpoint : window.innerWidth-e.clientX-offsetxpoint-20;
        var bottomedge=ie&&!window.opera? ietruebody().clientHeight-event.clientY-offsetypoint : window.innerHeight-e.clientY-offsetypoint-20;

        var leftedge=(offsetxpoint<0)? offsetxpoint*(-1) : -1000;

        //checa se o espaco horizontal basta
        if (rightedge<tipobj.offsetWidth)
        //move a posicao horizontal do menu para esquerda baseado na largura
            tipobj.style.left=ie? ietruebody().scrollLeft+event.clientX-tipobj.offsetWidth+"px" : window.pageXOffset+e.clientX-tipobj.offsetWidth+"px";
        else if (curX<leftedge)
            tipobj.style.left="5px";
        else
        //posiciona o menu onde o mause esta marcndo
        tipobj.style.left=curX+offsetxpoint+"px";

        //idem verticalmente
        if (bottomedge<tipobj.offsetHeight)
            tipobj.style.top=ie? ietruebody().scrollTop+event.clientY-tipobj.offsetHeight-offsetypoint+"px" : window.pageYOffset+e.clientY-tipobj.offsetHeight-offsetypoint+"px"
        else
            tipobj.style.top=curY+offsetypoint+"px";
        tipobj.style.visibility="visible";
    }
   //***carrinho***
   // coord(e);
   //***carrinho***
}

function notip(isFilhoLocal){
	
	if(isFilhoLocal == true){
	
		isFilhoAtivado = false;
		
	}
	
    if (ns6||ie){
        enabletip=false;
        tipobj.style.visibility="hidden";
        tipobj.style.left="-1000px";
        tipobj.style.backgroundColor='';
        tipobj.style.width='';
    }
}

document.onmousemove=positiontip;
