package app.omegasoftware.wifitransferpro.gps;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class GPSServiceBroadcastReceiver extends BroadcastReceiver{
	

	public static final String TAG = "GPSServiceBroadcastReceiver";
	private GPSContainer __gpsContainer;
	
	public GPSServiceBroadcastReceiver(Activity p_activity){
		__gpsContainer = new GPSContainer();

        try {
        	//Starts the TruckTrackerService
        	Intent serviceIntent = new Intent(p_activity, GPSService.class);
        	p_activity.startService(serviceIntent);
        }
        catch (Exception e) {
        	//Log.d(TAG,"onCreate(): Could not start service");
        }
	}
	@Override
	public void onReceive(Context context, Intent intent) {

		//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());

		if(intent.getAction().equals(GPSConfiguration.REFRESH_UI_INTENT)) {
			__gpsContainer.loadIntent(intent);

			//	    			gpsStateButton.setChecked();
			//	    			if(intent.getExtras().getInt(TruckTrackerConfiguration.SS_INTERNET_STATE) == TruckTrackerConfiguration.INTERNET_ACTIVE_CONNECTED)
			//	    			{
			//	    				internetStateButton.setChecked(true);
			//	    			}
			//	    			else
			//	    			{
			//	    				internetStateButton.setChecked(false);
			//	    			}

		}
		//	    		else if(intent.getAction().equals(TruckTrackerConfiguration.TAKE_PICTURE_INTENT))
		//				{
		//	    			Intent camera = new Intent(getApplicationContext(), TruckTrackerCameraActivity.class);
		//	    			startActivity(camera);
		//				}
	}
	public GPSContainer getGPSContainer(){
		return __gpsContainer;
	}
}

