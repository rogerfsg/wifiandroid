package app.omegasoftware.wifitransferpro.database.EXTDAO;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;

public class EXTDAOPesquisaArquivo extends Table{

	public static final String NAME = "pesquisa_arquivo";
	public static String ID = "id";
	public static String PATH = "path";
	public static String PESQUISA_ID_INT = "pesquisa_id_INT";
	
	public EXTDAOPesquisaArquivo(Database p_database) {
		super(NAME, p_database);

		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));		
		super.addAttribute(new Attribute(PATH, "path", true, Attribute.SQL_TYPE.TEXT, false, null, false, null, 255));
		super.addAttribute(new Attribute(PESQUISA_ID_INT, EXTDAOPesquisa.NAME, EXTDAOPesquisa.ID,  "arquivo da pesquisa", true, Attribute.SQL_TYPE.LONG, true, null, false, null, 11, Attribute.TYPE_RELATION_FK.CASCADE, Attribute.TYPE_RELATION_FK.CASCADE));

	}
	public File[] getListFileDaPesquisa(String pIdPesquisa){
		
		ArrayList<String> vListPathPesquisaArquivo = getListPathArquivoDaPesquisa(pIdPesquisa);
		if(vListPathPesquisaArquivo != null){
			File vVetorRetorno[] = new File[vListPathPesquisaArquivo.size()];
			
			for(int i = 0 ; i < vVetorRetorno.length; i++){
				String vPath = vListPathPesquisaArquivo.get(i);
				File vFile=  new File(vPath);
				vVetorRetorno[i] = vFile;
			}
			return vVetorRetorno;
		}
		
		return null;
	}
	
	public ArrayList<String> getListPathArquivoDaPesquisa(String pIdPesquisa){		

		try{
			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
			
			String vQuery = "SELECT pa." + EXTDAOPesquisaArquivo.PATH + ", " +
					" FROM " + EXTDAOPesquisaArquivo.NAME + " AS pa " + 
					" WHERE pa." + EXTDAOPesquisaArquivo.PESQUISA_ID_INT + " = ?" ;
			

			Cursor vCursor = v_SQLiteDatabase.rawQuery(vQuery, 
					new String[]{
					pIdPesquisa});

			
			ArrayList<String> vListStrId = new ArrayList<String>();
			if (vCursor.moveToFirst()) {
				do {
					String vToken = vCursor.getString(0);
					vListStrId.add(vToken);
											
				} while (vCursor.moveToNext());
			}

			if (vCursor != null && !vCursor.isClosed()) {	    	  
				vCursor.close();
			}
			return vListStrId;
		}catch(Exception ex){

		}
		return null;
	}

	@Override
	public void populate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAOPesquisaArquivo(getDatabase());
	}

	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId,
			String pOldId) {
		// TODO Auto-generated method stub
		
	}

}
