package app.omegasoftware.wifitransferpro.http;

import java.io.InputStream;
import java.util.Properties;

import android.content.Context;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public abstract class InterfaceHTML{
	public Context context;
	public String uri;
	public String method;
	public Properties header;
	public Properties parms;
	public Properties files;
	protected HelperHttp __helper;
	 
	
	public InterfaceHTML( HelperHttp pHelperHTTP){
		__helper = pHelperHTTP;
		context = pHelperHTTP.context;
		uri = pHelperHTTP.uri;
		method = pHelperHTTP.method;
		header = pHelperHTTP.header;
		parms = pHelperHTTP.parms;
		files = pHelperHTTP.files;
	}
	
	public abstract ContainerResponse getContainerResponse();
	
	
	
	public NanoHTTPD.Response getHTML(){
		ContainerResponse vRetorno = getContainerResponse();
		
		if(vRetorno == null )
			return __helper.nanoHTTPD.factoryResponse(NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, "");
		else return vRetorno.getResponse();
	}
}
