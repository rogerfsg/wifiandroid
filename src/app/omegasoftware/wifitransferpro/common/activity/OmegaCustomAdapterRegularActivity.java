package app.omegasoftware.wifitransferpro.common.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.TabletActivities.AboutActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.Adapter.CustomAdapter;

public abstract class OmegaCustomAdapterRegularActivity extends Activity {

	private String TAG = "OmegaCustomAdapterRegularActivity";

	protected CustomAdapter adapter;
	private GridView resultGridView;
	protected abstract void buildAdapter();
	public abstract void initializeComponents();
	protected abstract void loadCommomParameters(Bundle pParameter);

	protected String __nomeTag = null;


	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		if(getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TAG))
			__nomeTag = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_TAG);
		setContentView(R.layout.factory_menu_activity_mobile_layout);        
		this.resultGridView = (GridView) findViewById(R.id.search_result_tablet_gridview);

		try{
			new CustomDataLoader(this).execute();
		}
		catch(Exception e)
		{
			Log.e(TAG, e.getMessage());
		}
	}


	protected void loadParameters()	
	{
		Bundle vParameters = getIntent().getExtras();
		if(vParameters != null)
		{
			loadCommomParameters(vParameters);			
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;

	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.unimedbh_dialog_error_textview);
		Button vOkButton = (Button) vDialog.findViewById(R.id.unimedbh_dialog_ok_button);

		try{
			switch (id) {
			case OmegaConfiguration.ON_LOAD_DIALOG:
				ProgressDialog vDialogAux = new ProgressDialog(this);

				vDialogAux.setMessage(getResources().getString(R.string.string_loading));
				return vDialogAux;
			case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
				vTextView.setText(getResources().getString(R.string.dialog_noresult_alert));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
						finish();
					}
				});
				break;
			default:
				//			vDialog = this.getErrorDialog(id);
				return null;
			}
		} catch(Exception ex){
			return null;
		}
		return vDialog;

	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;



		switch (item.getItemId()) {

		case R.id.menu_favorito:
			
			break;
		case R.id.menu_sobre:
			intent = new Intent(this, AboutActivityMobile.class);
			break;

		default:
			return super.onOptionsItemSelected(item);

		}
		if(intent != null){
			startActivity(intent);	
		}
		return true;

	}

	public boolean loadData() {
		loadParameters();
		buildAdapter();
		if(adapter.getCount() == 0 ) adapter = null;
		return (adapter!=null?true:false);
	}



	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub

		super.onDestroy();
	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub

		super.onResume();
	}
	@Override
	public void finish(){

		super.finish();

	}

	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity __activity;
		public CustomDataLoader(Activity pActivity){
			__activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				return loadData();
			}
			catch(Exception e)
			{
				if(e != null)
					Log.e(TAG, e.getMessage());			
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			if(result)
			{
				initializeComponents();


			}
			if(adapter!= null && adapter.getCount() <= 0){
				dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);
				showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
			}
			else{
				resultGridView.setAdapter(adapter);
				try{
					dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);
				} catch(Exception ex){

				}
				//				adapter.notifyDataSetChanged();
			}

		}

	}

}
