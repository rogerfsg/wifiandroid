package app.omegasoftware.wifitransferpro.listener;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class GestureOnTouchListener implements OnTouchListener{
	GestureDetector __gestorDetector;
	public GestureOnTouchListener(GestureDetector pGestorDetector){
		__gestorDetector = pGestorDetector;
		
	}
	
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		__gestorDetector.onTouchEvent(event);
		return true;
	}
	

}
