package app.omegasoftware.wifitransferpro.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.util.Log;

public class FilesZipper {

	private static final String TAG = "FilesZipper";
	
	private static final int BUFFER = 2048; 
	 
	private ArrayList<File> files = null; 
	private File[] vetorFile;
	private File zipFile = null; 
	
	public FilesZipper(ArrayList<File> files, File zipFile) {
	  this.files = files; 
	  this.zipFile = zipFile;
	}
	
	public FilesZipper(File files[], File zipFile) {
	  this.vetorFile = files; 
	  this.zipFile = zipFile;
	}
	
	public static File getNewZipFile(String pPathDir){
		return new File(pPathDir, 
				HelperFile.generateRandomFileName(OmegaFileConfiguration.ZIP_FILES_EXTENSION));
	}
	private void zipList(){
		try{ 
		BufferedInputStream origin = null; 
	      FileOutputStream dest = new FileOutputStream(zipFile); 
	 
	      ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest)); 
	 
	      byte data[] = new byte[BUFFER]; 
	 
	      for(File file : this.files) {
	    	  if(file == null){
				  continue;
			  }
	        //Log.d(TAG, "zip(): Zipping file " + file.getName()); 
	        FileInputStream fi = new FileInputStream(file); 
	        origin = new BufferedInputStream(fi, BUFFER); 
	        ZipEntry entry = new ZipEntry(file.getName());
	        out.putNextEntry(entry); 
	        int count; 
	        while ((count = origin.read(data, 0, BUFFER)) != -1) { 
	          out.write(data, 0, count); 
	        } 
	        origin.close();
	        
	      } 
	      out.close();
		 } catch(Exception e) { 
		      //Log.e(TAG,"zip(): " + e.getMessage()); 
		    } 
	}
	private void zipVetor(){
		if(vetorFile == null || zipFile == null){
			return;
		}
	    try{ 
	      BufferedInputStream origin = null; 
	      FileOutputStream dest = new FileOutputStream(zipFile); 
	 
	      ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest)); 
	 
	      byte data[] = new byte[BUFFER]; 
	 
	      for(File file : this.vetorFile) {
	    	  if(file == null){
				  continue;
			  }
	        //Log.d(TAG, "zip(): Zipping file " + file.getName()); 
	        FileInputStream fi = new FileInputStream(file); 
	        origin = new BufferedInputStream(fi, BUFFER); 
	        ZipEntry entry = new ZipEntry(file.getName());
	        out.putNextEntry(entry); 
	        int count; 
	        while ((count = origin.read(data, 0, BUFFER)) != -1) { 
	          out.write(data, 0, count); 
	        } 
	        origin.close();
	        
	      } 
	      out.close();
	      
	    } catch(Exception e) { 
	      //Log.e(TAG,"zip(): " + e.getMessage()); 
	    } 
	}
	
	
	
	public void zip() { 
		if((vetorFile == null && files == null) || zipFile == null){
			return;
		}
	    try{ 
	    	if(vetorFile != null || vetorFile.length > 0 )
	    		zipVetor();
    		else{
    			zipList();
    		}
	      
	      
	    } catch(Exception e) { 
	      //Log.e(TAG,"zip(): " + e.getMessage()); 
	    } 
	 
	  } 
	
}
