package app.omegasoftware.wifitransferpro.database;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivoPlaylist;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivoTemporario;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAODiretorioImagemArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOImagemArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOLink;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPesquisa;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPesquisaArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPlaylist;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOTipoArquivo;
import app.omegasoftware.wifitransferpro.file.HelperFile;

public abstract class Database {
	private static String TAG = "Database";
	static boolean __isCheckCreate = false;
	static boolean __isDatabaseFromFile = false;
	private Context context;
	public static String NULL = "null";
	private SQLiteDatabase db;
	private String __databaseName = "";
	private String __databasePath = "";
	private int __databaseVersion;
	private static Table __vectorTable[] = new Table[] {
		new EXTDAOArquivo(null),
		new EXTDAOArquivoTemporario(null),
		new EXTDAOTipoArquivo(null),
		new EXTDAOPesquisa(null),
		new EXTDAOPesquisaArquivo(null),
		new EXTDAOImagemArquivo(null),
		new EXTDAODiretorioImagemArquivo(null),
		new EXTDAOPlaylist(null),
		new EXTDAOArquivoPlaylist(null),
		new EXTDAOLink(null),
	};

	public static String[] getListOfTableSearch(String pListTableAll[] , String[] pListTableSearch){
		ArrayList<String> vList = new ArrayList<String>();
		for (String vTableSearch : pListTableSearch) {
			for (String vTable : pListTableAll) {

				if(vTable.compareTo(vTableSearch) == 0 ) vList.add(vTable);
			}
		}
		String vVetor[] = new String[vList.size()];
		vList.toArray(vVetor);
		return vVetor;
	}

	public static Table[] getListAllTable(){
		return __vectorTable;
	}

	
	
	public static ArrayList<String> getListNameHierarquicalOrderOfVetorTable(String[] pVetorTabela){
		ArrayList<String> vList = new ArrayList<String>(); 

		for (Table vTable : __vectorTable) {
			for (String pTableName: pVetorTabela) {
				if(vTable.isEqual(pTableName))
					vList.add(pTableName);
			}
		}
		return vList;
	}

	public static ArrayList<Table> getListTableHierarquicalOrderOfVetorTable(Table[] pVetorTabela){
		ArrayList<Table> vList = new ArrayList<Table>(); 

		for (Table vTable : __vectorTable) {
			for (Table pTable: pVetorTabela) {
				if(vTable.isEqual(pTable.getName()))
					vList.add(pTable.factory());
			}
		}
		return vList;
	}	

	public Database(
			Context context, 
			String p_databaseName, 
			int p_databaseVersion, 
			String p_databasePath) {
		try{
			this.context = context;
			this.__databasePath = p_databasePath;
			this.__databaseName = p_databaseName;
			this.__databaseVersion = p_databaseVersion;
			boolean databaseWasCreated = false;
//			if (!Database.__isCheckCreate ) {
//
//				SharedPreferences vSavedPreferences = context.getSharedPreferences(
//						OmegaConfiguration.PREFERENCES_NAME, 0);
//				databaseWasCreated = vSavedPreferences.getBoolean(
//						OmegaConfiguration.DATABASE_CREATED, false);			
//
//				if(!databaseWasCreated){
//					//if(true){
//						if(this.procedimentoCriaBancoDeDadosDoArquivo()){
//							SharedPreferences.Editor vEditor = vSavedPreferences.edit();
//							vEditor.putBoolean(OmegaConfiguration.DATABASE_CREATED, true);
//							vEditor.commit();
//							Database.__isCheckCreate = true;
//						}
//					}	
//			}
			open();
			if (!Database.__isCheckCreate) {
				SharedPreferences vSavedPreferences = context.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		databaseWasCreated = vSavedPreferences.getBoolean(
				OmegaConfiguration.DATABASE_CREATED, false);
				if (!databaseWasCreated) {
					if (!Database.__isDatabaseFromFile) {
						if(this.createDatabaseStructure()){
							checkSharedPreferenceDatabaseCreated();
						}
					}	
				}
			}	

			for (Table table : __vectorTable) {
				table.setDatabase(this);
			}


		}
		catch(Exception ex){
			//Log.d(TAG, "Constructor", ex.getCause());
		}
	}
	public ArrayList<Long> convertCursorToArrayListId(Cursor pCursor, String[] pVetorChave )
	{
		ArrayList<Long> vArrayList = new ArrayList<Long>();
		int v_index = 0;
		if (pCursor.moveToFirst()) {

			do {
				v_index = 0;
				for (String v_strNameAttr : pVetorChave) {				 
					try
					{
						Long vKeyLong = Long.parseLong(pCursor.getString(v_index));
						vArrayList.add(vKeyLong);
					}
					catch (Exception e) {
					}
					v_index += 1;
				}   
			} while (pCursor.moveToNext());
		}

		if (pCursor != null && !pCursor.isClosed()) {	    	  
			pCursor.close();
		}
		return vArrayList;

	}


	public Table factoryTable(String p_nomeTabela){
		for (Table v_table : __vectorTable) {
			if(v_table.isEqual(p_nomeTabela)){
				Table v_newTable =  v_table.factory();
				v_newTable.setDatabase(this);
				return v_newTable;
			}
		}
		return null;
	}

	public String[] getVetorNomeTabela(){
		String[] v_nomeTabela = new String[Database.__vectorTable.length];
		int i = 0 ;
		for (Table table : Database.__vectorTable) {
			String v_strName = table.getName();
			v_nomeTabela[i] = v_strName;
			i += 1;
		}
		return v_nomeTabela;

	}

	public ArrayList<Table> getListTableDependent(String pStrName){
		ArrayList<Table> vList = new ArrayList<Table>();
		for (Table vTable : Database.__vectorTable) {
			if(vTable.isTableDependent(pStrName))
				vList.add(vTable.factory());
		}
		return vList;
	}


	public boolean hasTable(String p_nomeTabela){
		int i = 0 ;
		for (Table table : Database.__vectorTable) {
			if(table.isEqual(p_nomeTabela)){
				return true;
			}
		}
		return false;
	}

	public void synchronizeDatabase(){

	}

	public void dropAndInitializeDatabase(){

		for (Table v_table : __vectorTable) {
			try{
				db.execSQL("DROP TABLE IF EXISTS " + v_table.getName());	
			}catch(Exception ex){

			}

		}
		this.createDatabaseStructure();
	}

	public Table getTable(String pTableName){

		for (Table v_table : __vectorTable) {
			if(v_table.getName().compareTo(pTableName) == 0 ) return v_table;

		}
		return null;
	}

	public void dropDatabase(){
		for (Table v_table : __vectorTable) {
			dropTableOfDatabase(v_table.getName());
		}
	}

	public void dropTableOfDatabase(String pTableName){
		Table vTable = getTable(pTableName);

		if(vTable == null) return;
		try{
			db.execSQL("DROP TABLE IF EXISTS " + pTableName);
		} catch(Exception ex){

		}

	}

	private void checkSharedPreferenceDatabaseCreated(){

		SharedPreferences vSavedPreferences = context.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putBoolean(
				OmegaConfiguration.DATABASE_CREATED, true);
		vEditor.commit();
		Database.__isCheckCreate = true;
	}

	public static boolean isDatabaseInitialized(Context pContext){
		if(pContext == null) return false;

		SharedPreferences vSavedPreferences = pContext.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		if(!vSavedPreferences.contains(OmegaConfiguration.DATABASE_CREATED)) return false;
		else{
			return vSavedPreferences.getBoolean(OmegaConfiguration.DATABASE_CREATED, false);
		}
	}


	public boolean createTableStructure(String pTableName){
		Table vTable = getTable(pTableName);
		if(vTable != null){
			try{
				db.execSQL(vTable.getStrQueryCreate());	
			}catch(Exception ex){
				return false;
			}

			return true;
		}
		return false;
	}

	public boolean createDatabaseStructure() {
		boolean v_validade = true;
		for (Table v_table : __vectorTable) {
			try {
				String vStrCreateTable = v_table.getStrQueryCreate();
				//Log.i(TAG, "Table " + v_table + " : " + vStrCreateTable);
				db.execSQL(vStrCreateTable);
				String[] vVetorQueryIndex = v_table.getVetorStrQueryCreateIndex();
				if(vVetorQueryIndex != null){
					for (String vQueryIndex : vVetorQueryIndex) {
						db.execSQL(vQueryIndex);
					}
				}
			} catch (Exception ex) {
				String message =ex.getMessage();

				if( ! message.contains("already exists"))
					v_validade = false;
				//Log.e("createDatabaseStructure", ex.toString());
			}



		}
		return v_validade;

	}

	public boolean isOpen() {
		return this.db.isOpen();
	}

	public boolean openIfNecessary(){
		if(db != null){
			if(!this.db.isOpen())
			{
				db.close();
				return open();
			}
		}
		else{
			open();
		}
		return true;
	}

	public String getName() {
		return this.__databaseName;
	}

	public void populate() {
		for (Table v_table : this.getVectorTable()) {
			v_table.populate();
		}
	}

	public Table[] getVectorTable() {
		return __vectorTable;
	}

	public boolean open() throws SQLException {
		try {
			try {
				if(db != null){
					db.close();
				}
			}
			catch(Exception ex){
				//Log.e(TAG, ex.getMessage());
			}
			OpenHelper openHelper = new OpenHelper(__databaseName,
					__databaseVersion, __databasePath, this.context);

			this.db = openHelper.getWritableDatabase();
			return true;
			// this.db = openHelper.getReadableDatabase();
		} catch (Exception e) {
			// TODO: handle exception
			//Log.e("open(String p_databaseName, int p_databaseVersion)",
			//		"Error: " + e.toString());
			return false;
		}

	}

	public boolean procedimentoCriaBancoDeDadosDoArquivo() {
		OpenHelper openHelper = new OpenHelper(__databaseName, 1,
				__databasePath, this.context);
		try {

			return openHelper.createDataBase();

		} catch (IOException ioe) {

			throw new Error("Unable to create database");

		}

	}

	public void close() {
		try{
			if (this.db != null) {
				//			if(this.db.isOpen())
				this.db.close();
			}
		} catch(Exception ex){

		}
	}

	@Override
	protected void finalize(){
		try{
			close();
			try {
				super.finalize();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch(Exception ex){

		}
	}

	public SQLiteDatabase getSQLiteDatabase() {
		openIfNecessary();
		return db;
	}

	public String[] getResultSetComoVetorDoUmUnicoObjeto(
			String p_strQuery, String[] p_vetorArg){

		try{
			ArrayList<String> v_listResult = new ArrayList<String>();

			SQLiteDatabase v_SQLiteDatabase= getSQLiteDatabase();

			Cursor cursor = v_SQLiteDatabase.rawQuery(p_strQuery, p_vetorArg);

			if (cursor.moveToFirst()) {

				do {
					String v_token = cursor.getString(0);
					v_listResult.add(v_token);

				} while (cursor.moveToNext());
			}

			if (cursor != null && !cursor.isClosed()) {
				cursor.close();

			}
			String[] vetor =new String[v_listResult.size()]; 
			v_listResult.toArray(vetor); 
			return vetor;
		}catch(Exception ex){
			return null;
		}

	}

	public String[] getResultSetDoPrimeiroObjeto(
			String p_strQuery, String[] p_vetorArg, int pNumberOfSelectArg){

		try{
			ArrayList<String> v_listResult = new ArrayList<String>();

			SQLiteDatabase v_SQLiteDatabase= getSQLiteDatabase();

			Cursor cursor = v_SQLiteDatabase.rawQuery(p_strQuery, p_vetorArg);

			if (cursor.moveToFirst()) {

				do {
					for(int i = 0 ; i < pNumberOfSelectArg; i++){
						String v_token = cursor.getString(i);
						v_listResult.add(v_token);

					}
					break;

				} while (cursor.moveToNext());
			}

			if (cursor != null && !cursor.isClosed()) {
				cursor.close();

			}
			String[] vetor =new String[v_listResult.size()]; 
			v_listResult.toArray(vetor); 
			return vetor;
		}catch(Exception ex){
			return null;
		}

	}
	private class OpenHelper extends SQLiteOpenHelper {

		String __databaseName = "";
		int __databaseVersion = 0;
		String __databasePath = "";

		Context __context = null;

		OpenHelper(String p_databaseName, int p_databaseVersion,
				String p_databasePath, Context p_context) {
			super(context, p_databaseName, null, p_databaseVersion);
			this.__databaseName = p_databaseName;
			this.__databaseVersion = p_databaseVersion;
			this.__databasePath = p_databasePath;
			this.__context = p_context;
		}

		/**
		 * Check if the database already exist to avoid re-copying the file each
		 * time you open the application.
		 * 
		 * @return true if it exists, false if it doesn't
		 */
		private boolean checkDataBase() {

			SQLiteDatabase checkDB = null;

			try {
				String myPath = this.__databasePath;
				checkDB = SQLiteDatabase.openDatabase(myPath, null,
						SQLiteDatabase.OPEN_READONLY);

			} catch (SQLiteException e) {

				// database does't exist yet.

			}

			if (checkDB != null) {

				checkDB.close();

			}

			return checkDB != null ? true : false;
		}


		/**
		 * Copies your database from your local assets-folder to the just
		 * created empty database in the system folder, from where it can be
		 * accessed and handled. This is done by transfering bytestream.
		 * */
		private boolean copyDataBase() throws IOException {
			// TODO retirar comentario abaxo
			// String v_vetorArquivoBanco[] =
			// FileSystemHelper.getListOfFileInDirectoryAssets("database",
			// __context);

//			String v_vetorArquivoBanco[] = new String[] {
//					"guidelondon.db_out_0", 
//					"guidelondon.db_out_1", 
//					"guidelondon.db_out_2", 
//					"guidelondon.db_out_3",
//					"guidelondon.db_out_4",
//					"guidelondon.db_out_5"};

//			Path to the just created empty db
			File databaseOut = context.getDatabasePath(__databaseName);
			String outFileName = databaseOut.getAbsolutePath();
			
			
			
//			String outFileName = "/data/data/android.UnimedBHVersioned/databases/" + __databaseName ;

			
			String vVetorFile[] = __context.getAssets().list("");
			Integer i = 0 ;
			ArrayList<String> vListFileOfDatabase = new ArrayList<String>();
			while(true){
				boolean vIsFileFound = false;
				for (String vFileAsset : vVetorFile) {
					String vSufix = "db_out_" + i.toString();
					if(vFileAsset.endsWith(vSufix)){
						vListFileOfDatabase.add(vFileAsset);
						vIsFileFound = true;
						break;
					}
					
				}
				if(!vIsFileFound) break;
				else i += 1;
			}
			String vVetorArquivoBanco[] = new String[vListFileOfDatabase.size()];
			vListFileOfDatabase.toArray(vVetorArquivoBanco);
			boolean vValidade = HelperFile.mergeListFileInAssetsInFile(vVetorArquivoBanco, outFileName, __context);
			if(vValidade){
				for (String vFileArquivo : vVetorArquivoBanco) {
					File vFileAssets = new File(vFileArquivo);
					vFileAssets.delete();
				}
			}
			return vValidade;
//			return false;
		}


		/**
		 * Creates a empty database on the system and rewrites it with your own
		 * database.
		 * */
		public boolean createDataBase() throws IOException {

			boolean dbExist = checkDataBase();

			if (dbExist) {
				// do nothing - database already exist
			} else {

				// By calling this method and empty database will be created
				// into the default system path
				// of your application so we are gonna be able to overwrite that
				// database with our database.
				this.getReadableDatabase();

				try {

					return copyDataBase();

				} catch (IOException e) {

					throw new Error("Error copying database");

				}
			}
			return false;

		}

		@Override
		public void onCreate(SQLiteDatabase db) {

		}



		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

			//Log.w("Example",
			//		"Upgrading database, this will drop tables and recreate.");
			if (this.__databaseVersion == 1) {
				for (Table v_table : __vectorTable) {

					db.execSQL("DROP TABLE IF EXISTS " + v_table.getName());
				}

				onCreate(db);
			}

		}
	}
}
