package app.omegasoftware.wifitransferpro.http.pages;

import java.io.File;

import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOImagemArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class Upload extends InterfaceHTML{

	public Upload(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		String pathTmp = __helper.FILES("Filedata");
		
		boolean validade = false;
		String vMsg = "";
		if(pathTmp != null){
			String vPath = __helper.GET("path");
			String vIsOverride = __helper.GET("is_override");
//			String vIsOverride = __helper.POST("checkbox_is_override");
			
			
			if(vPath == null)
				vPath = HelperHttp.getPathRoot();
			
			String vFileName = __helper.GET("Filename");
			
			File vFile = new File(vPath, vFileName);
			if(vFile.exists() && vIsOverride != null && vIsOverride.compareTo("1") == 0)
				vFile.delete();
			if(!vFile.exists()){
				File vFileTemp = new File(pathTmp);
				if(vFileTemp.exists()){
					vFileTemp.renameTo(vFile);
//					HelperFile.copyFile(vFileTemp, vFile);
//					vFileTemp.delete();
				}	
			}
			ContainerTypeFile vContainerTypeFile = ContainerTypeFile.getContainerTypeFile(vFile);
			if(vContainerTypeFile.typeFile == TYPE_FILE.IMAGE){
				Database db = new DatabaseWifiTransfer(__helper.context);
				db.openIfNecessary();
				EXTDAOImagemArquivo vObjImagemArquivo = new EXTDAOImagemArquivo(db);
				vObjImagemArquivo.setAttrStrValue(EXTDAOImagemArquivo.PATH, vFile.getAbsolutePath());
				vObjImagemArquivo.insert(false);
				db.close();
			}
			validade = true;
			vMsg = "Arquivo baixado";
		} else {
			validade = false;
			vMsg = "Error de download";
		}
		
		return new ContainerResponse(__helper, new StringBuilder(vMsg));
	}

}
