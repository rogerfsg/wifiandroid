package app.omegasoftware.wifitransferpro.TabletActivities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.activity.OmegaRegularActivity;
import app.omegasoftware.wifitransferpro.gps.GPSServiceLocationListener;
import app.omegasoftware.wifitransferpro.mapa.HelperMapa;

import com.google.android.maps.GeoPoint;

public class TracarRotaActivityMobile extends OmegaRegularActivity {
	public static String TAG = "TracarRotaActivityMobile";
	Button __myLocationButton;
	EditText __logradouroEditText ;
	Button __okButton;
	GPSServiceLocationListener __locationListener;
	private final static int DIALOG_ERROR_FORM_GPS_LOW_SIGNAL = 100;
	private final static int DIALOG_ERROR_FORM_ADDRESS_INEXISTENT = 101;
	private final static int DIALOG_ERROR_FORM_MISSING_ADDRESS = 102;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.filter_traca_rota_activity_mobile_layout);
		new CustomDataLoader(this).execute();
	}


	private void applyTypeFace(){
		
		Typeface  customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		((TextView) findViewById(R.id.filter_origim_point_textview)).setTypeface(customTypeFace);
		__myLocationButton.setTypeface(customTypeFace);
		__logradouroEditText.setTypeface(customTypeFace);
		
	}
	
	
	@Override
	public boolean loadData() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void initializeComponents() {
		__locationListener = GPSServiceLocationListener.construct(this);
		// TODO Auto-generated method stub
		__myLocationButton = (Button) findViewById(R.id.my_location_button);
		__myLocationButton.setOnClickListener(new LocationButtonOnClickListener());
		__okButton = (Button) findViewById(R.id.ok_button);
		__okButton.setOnClickListener(new OkButtonOnClickListener());
		__logradouroEditText = (EditText) findViewById(R.id.logradouro_edittext);
		String vAddress = getSaveAddress();
		if(vAddress != null)
			__logradouroEditText.setText(vAddress);
		applyTypeFace();
	}
	
	/**
	 * Creates loading dialog
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.unimedbh_dialog_error_textview);
		Button vOkButton = (Button) vDialog.findViewById(R.id.unimedbh_dialog_ok_button);

		
		switch (id) {
		case DIALOG_ERROR_FORM_GPS_LOW_SIGNAL:
			vTextView.setText(getResources().getString(R.string.low_signal_gps));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(DIALOG_ERROR_FORM_GPS_LOW_SIGNAL);
					
				}
			});
			
			break;
		case DIALOG_ERROR_FORM_MISSING_ADDRESS:
			vTextView.setText(getResources().getString(R.string.missing_address));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(DIALOG_ERROR_FORM_MISSING_ADDRESS);
					
				}
			});
			
			break;
		case DIALOG_ERROR_FORM_ADDRESS_INEXISTENT:
			vTextView.setText(getResources().getString(R.string.address_inexistent));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(DIALOG_ERROR_FORM_ADDRESS_INEXISTENT);
					
				}
			});
			
			break;
		default:
			super.onCreateDialog(id);
		}
		
		return vDialog;
		
	}
	
	public class OkButtonOnClickListener implements OnClickListener{
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			String vStrLogradouro = __logradouroEditText.getText().toString();
			if(vStrLogradouro.length() == 0 ){
				showDialog(DIALOG_ERROR_FORM_MISSING_ADDRESS);
				return;
			}else{
				GeoPoint vOrigemLocation = HelperMapa.getGeoPointDoEndereco(vStrLogradouro);
				
				Integer vLatitude = vOrigemLocation.getLatitudeE6();
				Integer vLongitude = vOrigemLocation.getLongitudeE6();
				if(vLatitude == null || vLongitude == null){
					showDialog(DIALOG_ERROR_FORM_ADDRESS_INEXISTENT);
					return;
				}else{
					saveAddress(vStrLogradouro);
					Intent vIntent = getIntent();
					vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LATITUDE, vLatitude);
					vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LONGITUDE, vLongitude);
					setResult(OmegaConfiguration.RESULT_CODE_PONTO_ORIGEM, vIntent);
					finish();
				}	
			}
			
		}
	}
	
	public class LocationButtonOnClickListener implements OnClickListener{

		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Integer vLatitude = __locationListener.getLatitude();
			Integer vLongitude = __locationListener.getLongitude();
			if(vLatitude == null || vLongitude == null){
				showDialog(DIALOG_ERROR_FORM_GPS_LOW_SIGNAL);
			} else {
				Intent vIntent = getIntent();
				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LATITUDE, vLatitude);
				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LONGITUDE, vLongitude);
				setResult(OmegaConfiguration.RESULT_CODE_PONTO_ORIGEM, vIntent);
				finish();
			}
		}
		
	}

	//Return saved Plano ID
	private String getSaveAddress()
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		return vSavedPreferences.getString(OmegaConfiguration.SAVED_ADDRESS, null);
	}
	//Save Plano ID in the preferences
	private void saveAddress(String pAddress)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putString(OmegaConfiguration.SAVED_ADDRESS, pAddress);
		vEditor.commit();
	}

	
}
