package app.omegasoftware.wifitransferpro.file;

import java.io.File;
import java.io.FileFilter;

public class FilterFileIsDirectory implements FileFilter {
	boolean __isDirectoryNeeded = false;
	
	public FilterFileIsDirectory(boolean pIsDirectoryNeeded){
		__isDirectoryNeeded = pIsDirectoryNeeded;
	}
	
	
	public boolean accept(File pFile) {
		
		if(pFile.isDirectory()){
			if(! __isDirectoryNeeded)
				return false ;
			else return true;
		
		}
		else{
			if(!__isDirectoryNeeded)
				return true;
			else return false;
		}
	}

}
