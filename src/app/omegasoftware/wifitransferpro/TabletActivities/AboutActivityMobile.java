package app.omegasoftware.wifitransferpro.TabletActivities;


import android.graphics.Typeface;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.common.activity.OmegaRegularActivity;
import app.omegasoftware.wifitransferpro.listener.BrowserOpenerListener;
import app.omegasoftware.wifitransferpro.listener.EmailClickListener;
import app.omegasoftware.wifitransferpro.listener.PhoneClickListener;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class AboutActivityMobile extends OmegaRegularActivity {
	public static String TAG = "AboutActivityMobile";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.about_activity_mobile_layout);
		
		Button omegaPhoneNumberButton = (Button) findViewById(R.id.omega_phone_button);
		omegaPhoneNumberButton.setText(R.string.omega_phone_number);
		omegaPhoneNumberButton.setOnClickListener(new PhoneClickListener(R.id.omega_phone_button));
		
		Button vendasWebMailButton = (Button) findViewById(R.id.omega_email_button);
		vendasWebMailButton.setText(R.string.omega_email_button);
		vendasWebMailButton.setOnClickListener(new EmailClickListener(R.id.omega_email_button));
				
		
		Button atDomiciliarBrowser = (Button) findViewById(R.id.website_omega_button);		
		atDomiciliarBrowser.setOnClickListener(new BrowserOpenerListener(getResources().getString(R.string.omega_site_url), this));
		
		initializeComponents();
		applyTypeFace();
	}
	
	private void applyTypeFace(){
		
		Typeface  customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		((TextView) findViewById(R.id.information_dialog_textview)).setTypeface(customTypeFace);
		((Button) findViewById(R.id.website_omega_button)).setTypeface(customTypeFace);
		
		((Button) findViewById(R.id.omega_phone_button)).setTypeface(customTypeFace);
	}
	
	
	@Override
	public boolean loadData() {
		//
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void initializeComponents() {
		
		// TODO Auto-generated method stub
		
	}
	
}
