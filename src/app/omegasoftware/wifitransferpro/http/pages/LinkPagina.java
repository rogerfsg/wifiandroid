package app.omegasoftware.wifitransferpro.http.pages;

import java.util.ArrayList;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOLink;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Link;

public class LinkPagina extends InterfaceHTML{


	public static int LIMIT_LINK = 30;

	public LinkPagina(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {

		String vFileName = __helper.GET(EXTDAOArquivo.NOME);
		String vStrPath = __helper.GET(EXTDAOArquivo.PATH);
		vFileName = (vFileName == null) ? "" : vFileName;
		vStrPath = (vStrPath == null) ? "" : vStrPath;
		// TODO Auto-generated method stub
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("	<!-- Framework CSS -->");

		vBuilder.append("<table width=\"350\" align=\"top\" class=\"tabela_list\">");

		vBuilder.append("<colgroup>");
		vBuilder.append("<col width=\"50%\" />");
		vBuilder.append("<col width=\"50%\" />");

		vBuilder.append("</colgroup>");
		vBuilder.append("<thead>");

		vBuilder.append("			<tr class=\"tr_list_titulos\">");
		vBuilder.append("				<td class=\"td_list_titulos\" >");


		//		Icone adicionar playlist
		Properties vLinkAddPlaylist = new Properties();
		vLinkAddPlaylist
		.put("onmouseover",
				"tip(\'"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ADD_LINK)
						+ "\', this);");
		vLinkAddPlaylist.put("onmouseout", "notip();");
		Link vObjLinkAddPlaylist = new Link(
				250,
				500,
				context.getResources().getString(
						R.string.criar_link_pagina),
						"popup.php5?tipo=pages&page=AdicionarLinkPagina&"
								+ EXTDAOArquivo.NOME + "=" + vFileName
								+ "&" + EXTDAOArquivo.PATH + "="
								+ vStrPath,
								vLinkAddPlaylist,
								"<img class=\"icones_list\" src=\""
										+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
										+ "icon_add_link.png\" onmouseover=\"javascript:tip('"
										+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ADD_LINK)
										+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
										true);

		vBuilder.append(vObjLinkAddPlaylist.montarLink());

		vBuilder.append(__helper.context.getString(R.string.criar_link_pagina));
		vBuilder.append("		</td>");
		vBuilder.append("				<td class=\"td_list_titulos\" >");


		//		Icone adicionar playlist
		Properties vLinkListsLink = new Properties();
		vLinkListsLink
		.put("onmouseover",
				"tip(\'"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_VER_TODOS_LINKS)
						+ "\', this);");
		vLinkListsLink.put("onmouseout", "notip();");
		Link vObjLinkListsLink = new Link(
				500,
				700,
				context.getResources().getString(
						R.string.links),
						"popup.php5?tipo=lists&page=link",
						vLinkListsLink,
						"<img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
								.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "icon_link.png\" onmouseover=\"javascript:tip('"
								+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_VER_TODOS_LINKS)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
								true);

		vBuilder.append(vObjLinkListsLink.montarLink());

		vBuilder.append(__helper.context.getString(R.string.todos_links));
		vBuilder.append("		</td>");

		vBuilder.append("	</tr>");
		vBuilder.append("</thread>");
		vBuilder.append("    		</table>");
		vBuilder.append("<table class=\"tabela_list\">");
		vBuilder.append("<tbody>");

		Database db = new DatabaseWifiTransfer(__helper.context);
		try{
			EXTDAOLink vObjLink = new EXTDAOLink(db);
			ArrayList<Table> vListTupla = vObjLink.getListTable();
			vBuilder.append("<tr class=\"tr_list_conteudo_par\">");
			vBuilder.append("<td  class=\"td_list_conteudo\">");
			vBuilder.append("<div  class=\"div_galeria\">");
			int vLimit= LIMIT_LINK;
			if(vLimit > vListTupla.size())
				vLimit = vListTupla.size();
			if(vListTupla != null)
				for(int i = 0 ; i < vLimit; i++){


					Table vTupla = vListTupla.get(i);
					String vURLLink = vTupla.getStrValueOfAttribute(EXTDAOLink.URL);
					String vImagemLink = vTupla.getStrValueOfAttribute(EXTDAOLink.PATH_IMAGEM);
					String vNomeLink = vTupla.getStrValueOfAttribute(EXTDAOLink.NOME);

					vBuilder.append("<div  class=\"div_imagem_link\">");

					vBuilder.append("<img class=\"imagem_link\" src=\""
							+ vImagemLink + "\" onmouseover=\"javascript:tip('"
							+ __helper
							.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_LINK)
							+ "')\" onclick=\"javascript:location.href='" + vURLLink + "'\" onmouseout=\"javascript:notip()\">&nbsp;");

					vBuilder.append("</br>");

					vBuilder.append(vNomeLink);

					vBuilder.append("</br>");

					vBuilder.append("</div>");

				}
			vBuilder.append("</div>");
			vBuilder.append("</td>");
			vBuilder.append("</tr>");

			vBuilder.append("</tbody>");
			vBuilder.append("</table>");
		} catch(Exception ex){

		} finally{
			db.close();	
		}

		return new ContainerResponse(__helper,new StringBuilder( vBuilder)); 
	}

}
