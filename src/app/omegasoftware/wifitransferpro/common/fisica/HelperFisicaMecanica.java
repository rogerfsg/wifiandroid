package app.omegasoftware.wifitransferpro.common.fisica;

public class HelperFisicaMecanica {
	public static final String UNIDADE_KM = "km";
	
	public static final String UNIDADE_METRO = "m";
	
	public static Float getDistanciaMetro(Float pDistancia, String pUnidadeMedida){
		if(pUnidadeMedida != null){
			pUnidadeMedida = pUnidadeMedida.trim().toLowerCase();
			if(pUnidadeMedida.compareTo(UNIDADE_KM) == 0){
				Float vDistancia = pDistancia * 1000;
				return vDistancia;
			} else if(pUnidadeMedida.compareTo(UNIDADE_METRO) == 0){
				
				return pDistancia;
			}
		}
		return null;
	}
	
}
