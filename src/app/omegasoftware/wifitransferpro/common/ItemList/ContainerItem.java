package app.omegasoftware.wifitransferpro.common.ItemList;

public class ContainerItem{
	
	public String[] vetorConteudo;
	public String nome;
	
	public ContainerItem(String p_nome, String[] p_vetorConteudo){
		vetorConteudo = p_vetorConteudo;
		nome = p_nome;
	}
	
	public ContainerItem(String p_nome, String p_valor){
		nome = p_nome;
		vetorConteudo = new String[1];
		vetorConteudo[0] = p_valor;
	}
	
	public String getNome(){
		return nome;
	}
	
	public boolean isEqual(String p_nome){
		
		if(nome.compareTo(p_nome) == 0 )
			return true;
		else return false;
	}
	
	public String getConteudo(){
		if(vetorConteudo == null){
			return null;
		}else if (vetorConteudo.length == 0  )
			return null;
		if(vetorConteudo.length > 1 ){
			return getStrAtributoMultiValorado();
		} else return vetorConteudo[0];
	}
	
	private String getStrAtributoMultiValorado(){
		String vEspecialtyDescription = "";
		
		for (int i = 0; i < vetorConteudo.length; i++) {
			vEspecialtyDescription += vetorConteudo[i];
			if(i < (vetorConteudo.length - 1 ))
			{
				vEspecialtyDescription += ", ";	
			}
			
		}		
		return vEspecialtyDescription;
	}
		
}
