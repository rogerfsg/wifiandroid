package app.omegasoftware.wifitransferpro.database.EXTDAO;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;

public class EXTDAOImagemArquivo extends Table{

	public static final String NAME = "imagem_arquivo";
	public static String ID = "id";
	public static String PATH = "path";
	

	public EXTDAOImagemArquivo(Database p_database) {
		super(NAME, p_database);

		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));		
		super.addAttribute(new Attribute(PATH, "path", true, Attribute.SQL_TYPE.TEXT, false, null, false, null, 255));
	

		setUniqueKey(new String[] {PATH});
	}

	public String getIdOfPath(String pPath){
		try{
			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();

			String vQuery = "SELECT pa." + EXTDAOImagemArquivo.ID + ", " +
					" FROM " + EXTDAOImagemArquivo.NAME + " AS pa " +
					" WHERE pa." + EXTDAOImagemArquivo.PATH + "= ?" +
					" LIMIT 0,1 ";


			Cursor vCursor = v_SQLiteDatabase.rawQuery(vQuery, 
					new String[]{
					pPath});

			String vId = null;

			if (vCursor.moveToFirst()) {
				do {
					vId = vCursor.getString(0);

				} while (vCursor.moveToNext());
			}

			if (vCursor != null && !vCursor.isClosed()) {	    	  
				vCursor.close();
			}
			return vId;
		}catch(Exception ex){

		}
		return null;
	}

	private boolean checkValidatyOfFile(String vPath, File pFile, String vIdImagemArquivo, String pVetorPrefixoPath[],String pVetorExtension[]){
		for (String vExtension : pVetorExtension) {
			if(vPath.endsWith(vExtension)){
				if(!pFile.exists()){
					remove(vIdImagemArquivo, false);
					continue;
				}else{
					boolean vValidadePrefixo = false;
					if(pVetorPrefixoPath == null || pVetorPrefixoPath.length == 0 ){
						vValidadePrefixo = true;
					}else{
						for(String vPrefixo : pVetorPrefixoPath){
							if(vPath.startsWith(vPrefixo)){

								vValidadePrefixo = true;
								break;
							}	
						}
					}
					if(vValidadePrefixo)
						return true;
					else break;
				}
			}
		}
		return false;
	}


	public ArrayList<File> getListFileImagem(String pVetorPrefixoPath[],String pVetorExtension[]){		

		try{

			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();

			String vQuery = "SELECT pa." +EXTDAOImagemArquivo.ID + ", pa." + EXTDAOImagemArquivo.PATH +
					" FROM " + EXTDAOImagemArquivo.NAME + " AS pa ";

			Cursor vCursor = v_SQLiteDatabase.rawQuery(vQuery, new String[]{});
			ArrayList<File> vListFile = new ArrayList<File>();
			if (vCursor.moveToFirst()) {
				do {
					String vId = vCursor.getString(0);
					String vPath = vCursor.getString(1);

					File vFile = new File(vPath);


					if(checkValidatyOfFile(vPath, vFile, vId, pVetorPrefixoPath, pVetorExtension)){
						vListFile.add(vFile);
					}

				} while (vCursor.moveToNext());
			}

			if (vCursor != null && !vCursor.isClosed()) {	    	  
				vCursor.close();
			}
			return vListFile;
		}catch(Exception ex){
		}
		return null;
	}
	public class ContainerQuery{
		public String query;
		public String vetorArg[];
		public ContainerQuery(String pQuery, ArrayList<String> pListArg){
			if(pListArg != null){
				vetorArg = new String[pListArg.size()];
				pListArg.toArray(vetorArg);
			}
			else vetorArg = new String[0];
			
			query = pQuery;
		}
	}
	public ContainerQuery getQueryListFileImagem(boolean pIsCount, String pVetorPrefixoPath[], String pVetorExtension[], String pVetorPrefixoProibidoPath[], Integer pLimiteInferior, Integer pLimiteSuperior){

		String vQuery = "";
		if(!pIsCount)
			vQuery += " SELECT pa." +EXTDAOImagemArquivo.ID + ", pa." + EXTDAOImagemArquivo.PATH +
			" FROM " + EXTDAOImagemArquivo.NAME + " AS pa ";
		else 
			vQuery += " SELECT COUNT(pa." +EXTDAOImagemArquivo.ID + ") " +
					" FROM " + EXTDAOImagemArquivo.NAME + " AS pa ";

		ArrayList<String> vListArgumento = new ArrayList<String>();

		String vQueryPrefixPath = "";
		if(pVetorPrefixoPath != null && pVetorPrefixoPath.length > 0){

			for (String vPrefixPath : pVetorPrefixoPath) {
				if(vQueryPrefixPath.length() == 0 )
					vQueryPrefixPath += " pa." + EXTDAOImagemArquivo.PATH + " LIKE ? "; 
				else
					vQueryPrefixPath += " OR pa." + EXTDAOImagemArquivo.PATH + " LIKE ? ";

				vListArgumento.add(vPrefixPath + "%");
			}
		}
		String vQuerySufixExtension = "";
		if(pVetorExtension != null && pVetorExtension.length > 0){
			for (String vExtension : pVetorExtension) {
				if(vQuerySufixExtension.length() == 0 )
					vQuerySufixExtension += " pa." + EXTDAOImagemArquivo.PATH + " LIKE ? "; 
				else
					vQuerySufixExtension += " OR pa." + EXTDAOImagemArquivo.PATH + " LIKE ? ";

				vListArgumento.add( "%" + vExtension);
			}
		}
		
		String vQueryPrefixoProibidoPath = "";
		if(pVetorPrefixoProibidoPath != null && pVetorPrefixoProibidoPath.length > 0){
			for (String vPrefixPath : pVetorPrefixoProibidoPath) {
				if(vQueryPrefixoProibidoPath.length() == 0 )
					vQueryPrefixoProibidoPath += " pa." + EXTDAOImagemArquivo.PATH + " NOT LIKE ? "; 
				else
					vQueryPrefixoProibidoPath += " AND pa." + EXTDAOImagemArquivo.PATH + " NOT LIKE ? ";

				vListArgumento.add(vPrefixPath + "%");
			}
		}
		
		String vQueryWhere = "";
		if(vQueryPrefixPath.length() > 0 ){
			vQueryWhere += " (" + vQueryPrefixPath + ") ";
		}

		if(vQuerySufixExtension.length() > 0 ){
			if(vQueryWhere.length() > 0 )
				vQueryWhere += " AND " + " (" + vQuerySufixExtension + ") ";
			else{
				vQueryWhere += " (" + vQuerySufixExtension + ") ";
			}
		}
		
		if(vQueryPrefixoProibidoPath.length() > 0 ){
			if(vQueryWhere.length() > 0 )
				vQueryWhere += " AND " + " (" + vQueryPrefixoProibidoPath + ") ";
			else{
				vQueryWhere += " (" + vQueryPrefixoProibidoPath + ") ";
			}
		}
		
		if(vQueryWhere.length() > 0 )
			vQueryWhere = " WHERE " + vQueryWhere;
		
		String vQueryLimit = "";

		if(!pIsCount){
			if(pLimiteInferior != null){
				vQueryLimit += pLimiteInferior.toString();
			}

			if(pLimiteSuperior != null){
				vQueryLimit += ", " + pLimiteSuperior.toString();
			}
			if(vQueryLimit.length() > 0 )
				vQueryLimit = " LIMIT " + vQueryLimit;
		}

		return new ContainerQuery(vQuery + vQueryWhere + vQueryLimit, vListArgumento);
	}



	public Integer getCountListFileImagemFast(String pVetorPrefixoPath[], String pVetorExtension[], String pVetorPrefixoProibidoPath[]){

		try{

			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();

			ContainerQuery vContainerQuery = getQueryListFileImagem(true, pVetorPrefixoPath, pVetorExtension, pVetorPrefixoProibidoPath, null, null);

			Cursor vCursor = v_SQLiteDatabase.rawQuery(vContainerQuery.query, vContainerQuery.vetorArg);
			Integer vCount = null;
			if (vCursor.moveToFirst()) {
				do {
					vCount = HelperInteger.parserInteger( vCursor.getString(0));
					break;

				} while (vCursor.moveToNext());
			}

			if (vCursor != null && !vCursor.isClosed()) {	    	  
				vCursor.close();
			}


			return vCount;
		}catch(Exception ex){
		}
		return null;
	}

	public ArrayList<File> getListFileImagemFast(String pVetorPrefixoPath[], String pVetorExtension[], String pVetorPrefixoProibidoPath[], Integer pLimitInferior, Integer pLimiteSuperior){		

		try{

			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();

			ContainerQuery vContainer = getQueryListFileImagem(false, pVetorPrefixoPath, pVetorExtension,  pVetorPrefixoProibidoPath, pLimitInferior, pLimiteSuperior);

			Cursor vCursor = v_SQLiteDatabase.rawQuery(vContainer.query, vContainer.vetorArg);
			ArrayList<File> vListFile = new ArrayList<File>();
			if (vCursor.moveToFirst()) {
				do {
					String vId = vCursor.getString(0);
					String vPath = vCursor.getString(1);

					File vFile = new File(vPath);
					if(!vFile.exists()){
						remove(vId, false);
						continue;
					} else {
						vListFile.add(vFile);	
					}
				} while (vCursor.moveToNext());
			}

			if (vCursor != null && !vCursor.isClosed()) {	    	  
				vCursor.close();
			}


			return vListFile;
		}catch(Exception ex){
		}
		return null;
	}


	//	public ArrayList<File> getListFileImagem(String pVetorPrefixoPath[],String pVetorExtension[]){
	//		
	//		HashMap<String, String> vListPathPesquisaArquivo = getListPathArquivoImage();
	//		if(vListPathPesquisaArquivo != null){
	//			ArrayList<File> vListFile = new ArrayList<File>();
	//			Set<String> vSetId = vListPathPesquisaArquivo.keySet();
	//			Iterator<String> it = vSetId.iterator();
	//			while(it.hasNext()){
	//				String vId = it.next();
	//				String vPath = vListPathPesquisaArquivo.get(vId);
	//				File vFile = new File(vPath);
	//				if(checkValidatyOfFile(vPath, vFile, vId, pVetorPrefixoPath, pVetorExtension))
	//					vListFile.add(vFile);
	//			}
	//			return vListFile;
	//		}
	//		
	//		return null;
	//	}

	public ArrayList<File> getListFileImagem(){

		HashMap<String, String> vListPathPesquisaArquivo = getListPathArquivoImage();
		if(vListPathPesquisaArquivo != null){
			ArrayList<File> vListFile = new ArrayList<File>();
			Set<String> vSetId = vListPathPesquisaArquivo.keySet();
			Iterator<String> it = vSetId.iterator();
			while(it.hasNext()){
				String vId = it.next();
				String vPath = vListPathPesquisaArquivo.get(vId);
				File vFile=  new File(vPath);
				if(!vFile.exists()){
					remove(vId, false);
				}else{
					vListFile.add(vFile);
				}
			}
			return vListFile;
		}

		return null;
	}

	public HashMap<String, String> getListPathArquivoImage(){		

		try{
			HashMap<String, String> vHashIdPorPath = new HashMap<String, String>();
			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();

			String vQuery = "SELECT pa.id, pa." + EXTDAOImagemArquivo.PATH + " " +
					" FROM " + EXTDAOImagemArquivo.NAME + " AS pa ";


			Cursor vCursor = v_SQLiteDatabase.rawQuery(vQuery, new String[]{});

			if (vCursor.moveToFirst()) {
				do {
					String vId = vCursor.getString(0);
					String vPath = vCursor.getString(1);
					vHashIdPorPath.put(vId, vPath);						
				} while (vCursor.moveToNext());
			}

			if (vCursor != null && !vCursor.isClosed()) {	    	  
				vCursor.close();
			}
			return vHashIdPorPath;
		}catch(Exception ex){
			int i = 0 ; 
			i += 1;
		}
		return null;
	}

	@Override
	public void populate() {
		// TODO Auto-generated method stub

	}

	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAOImagemArquivo(getDatabase());
	}

	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId,
			String pOldId) {
		// TODO Auto-generated method stub

	}

}
