package app.omegasoftware.wifitransferpro.common.ItemList;

import java.util.ArrayList;


import android.app.LauncherActivity.ListItem;
import android.content.Context;
import android.content.Intent;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.Adapter.PhoneEmailAdapter;
import app.omegasoftware.wifitransferpro.common.Adapter.WebBrowserAdapter;
import app.omegasoftware.wifitransferpro.listener.BrowserOpenerListener;

public class AppointmentPlaceItemList extends ListItem {

	private int id;
	
	private String address;
	private String neighborhood;
	private String city;
	private String uf;
	private String country;
	private String idNeighborhood;
	private String idCity;
	private String idUf;
	private String idCountry;
	private String cep;
	private ArrayList<String> phones;
	private ArrayList<String> emails;
	private Context context;
	private String number;
	private String complemento;
	private String site;

	public AppointmentPlaceItemList(){
		
	}

	public void putDataInIntent(Intent intent){
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, this.getAddress());
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, this.getNumber());
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, this.getComplemento());
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, this.getIdNeighborhood());
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, this.getIdCity());
	}
	
	public AppointmentPlaceItemList(
			Context context, 
			int id, 
			String address)
	{
		
		this(context, 
				id, 
				address, 
				null, 
				null, 
				null,
				null, 
				null, 
				null, 
				null, 
				null, 
				null); 
	}
	
	public AppointmentPlaceItemList(
			Context context, 
			int id, 
			String address,
			ArrayList<String> phones, 
			String email)
	{
		
		this(context, 
				id, 
				address, 
				null, 
				null, 
				null, 
				null,
				null, 
				null, 
				null, 
				phones, 
				email); 
	}
	
	
	public AppointmentPlaceItemList(
			Context context, 
			int id, 
			String address, 
			String number, 
			String complemento, 
			String neighborhood,
			String city, 
			String uf, 
			String country,
			String pIdNeighborhood,
			String idCity, 
			String idUf, 
			String idCountry,
			String cep, 
			ArrayList<String> phones, 
			String email)
	{
		
		this(context, 
				id, 
				address, 
				number, 
				complemento, 
				neighborhood, 
				city, 
				uf,
				country,
				cep, 
				phones, 
				email);
		
		this.idNeighborhood = pIdNeighborhood;
		this.idCity = idCity;
		this.idUf = idUf; 
		this.idCountry = idCountry;
	}
	

	public AppointmentPlaceItemList(
			Context context, 
			int id, 
			String address, 
			String number, 
			String complemento, 
			String neighborhood, 
			String city, 
			String uf, 
			String country,
			String cep, 
			ArrayList<String> phones, 
			String email)
	{
		this.context = context;
		this.id = id;
		
		if(neighborhood != null)
			if(neighborhood.length() > 0 )
				this.neighborhood = neighborhood;

		if(address != null)
			if(address.length() > 0 )
				this.address = address;

		if(city  != null)
			if(city.length() > 0 )
				this.city = city;

		if(uf != null)
			if(uf.length() > 0 )
				this.uf = uf;
		if(country  != null)
			if(country.length() > 0 )
				this.country = country;
		if(cep != null)
			if(cep.length() > 0 )
				this.cep = cep;

		if(phones != null)
			if(phones.size() > 0 ){
				this.phones = phones;
			}
		
		if(email != null){
			this.emails =  new ArrayList<String>();
			this.emails.add(email);
		}

		if(number != null)
			if(number.length() > 0 )
				this.number = number;

		if(complemento != null)
			if(complemento.length() > 0 )
				this.complemento = complemento;
	}
	
	public AppointmentPlaceItemList(
			Context context, 
			int id, 
			String address, 
			String number, 
			String complemento, 
			String neighborhood, 
			String city, 
			String uf, 
			String country,
			String cep, 
			ArrayList<String> phones, 
			String email, 
			String pSite)
	{
		this.context = context;
		this.id = id;
		site = pSite;
		if(neighborhood != null)
			if(neighborhood.length() > 0 )
				this.neighborhood = neighborhood;

		if(address != null)
			if(address.length() > 0 )
				this.address = address;

		if(city  != null)
			if(city.length() > 0 )
				this.city = city;

		if(uf != null)
			if(uf.length() > 0 )
				this.uf = uf;
		if(country  != null)
			if(country.length() > 0 )
				this.country = country;
		if(cep != null)
			if(cep.length() > 0 )
				this.cep = cep;

		if(phones != null)
			if(phones.size() > 0 ){
				this.phones = phones;
			}
		
		if(email != null){
			this.emails =  new ArrayList<String>();
			this.emails.add(email);
		}

		if(number != null)
			if(number.length() > 0 )
				this.number = number;

		if(complemento != null)
			if(complemento.length() > 0 )
				this.complemento = complemento;
	}

	public String getNumber(){
		return this.number;
	}

	public String getComplemento(){
		return this.complemento;
	}

	public String getStrAddress(){
		String v_result = "";
		if(this.getAddress() != null){
			v_result += this.getAddress();
		}
		if(this.getNumber() != null){
			if(v_result.length() > 0 )
				v_result += ", " + this.getNumber();
			else v_result += this.getNumber();
		}
		if(this.getNeighborhood() != null){
			if(v_result.length() > 0 )
				v_result += ", " + this.getNeighborhood() ;
			else v_result += this.getNeighborhood() ;
		}

		if(this.getCity() != null){
			if(v_result.length() > 0 )
				v_result += " - " + this.getCity();
			else v_result += this.getCity();
		}
		if(this.getUf() != null){
			if(v_result.length() > 0 )
				v_result += " / " + this.getUf();
			else v_result += this.getUf();
		}
		
		if(this.getCountry() != null){
			if(v_result.length() > 0 )
				v_result += " / " + this.getCountry();
			else v_result += this.getCountry();
		}		
		if(v_result.length() > 0 ){
			return v_result ;	
		} else return null;
	}
	
	public String getStrAddressSimplificado(){
		String v_result = "";
		if(this.getAddress() != null){
			v_result += this.getAddress();
		}
		if(this.getNumber() != null){
			if(v_result.length() > 0 )
				v_result += ", " + this.getNumber();
			else v_result += this.getNumber();
		}
		if(this.getNeighborhood() != null){
			if(v_result.length() > 0 )
				v_result += ", " + this.getNeighborhood() ;
			else v_result += this.getNeighborhood() ;
		}
		
		if(v_result.length() > 0 ){
			return v_result ;	
		} else return null;
	}

	
	public void clear(){
		this.neighborhood = null;
		this.address = null;
		this.city = null;
		
		this.cep = null;
		this.idCity = null;
		this.idCountry = null;
		this.idNeighborhood = null;
		this.idUf = null;
		this.uf = null;
		this.cep = null;
		
		if(this.phones != null)
			this.phones.clear();
		
		if(emails != null)
			this.emails.clear();

		this.number = null;
		this.complemento = null;
	}

	public int getId()
	{
		return id;
	}

	public String getAddress()
	{
		return this.address;
	}
	public String getNeighborhood(){
		return neighborhood;
	}

	public String getIdNeighborhood(){
		return idNeighborhood;
	}
	
	public String getIdCity()
	{
		return this.idCity;
	}
	
	public String getIdUf()
	{
		return this.idUf;
	}
	
	public String getUf()
	{
		return this.uf;
	}
	
	public String getCountry()
	{
		return this.country;
	}
	
	public String getCity()
	{
		return this.city;
	}
	public String getCEP()
	{
		if(this.cep == null){
			return null;
		} else if(cep.length() == 0){
			return null;
		}
		if(this.cep.contains("-"))
		{
			return this.cep;
		}

		String cep = "";
		try
		{
			cep = this.cep.substring(0, 5) + "-" + this.cep.substring(5,8);
		}
		catch(Exception e)
		{
			cep = this.cep;
		}

		return cep;
	}
	public String getCEPWithStartingString()
	{
		String v_cep = this.getCEP();
		if(v_cep == null)
			return null; 
		else
			return "CEP: " + v_cep;
	}

	public boolean hasEmail(){
		if(this.emails == null) return false; 
		else if(this.emails.size() > 0 ) return true;
		else return false;
	}
	
	public boolean hasPhone(){
		if(this.phones == null) return false;
		else if(this.phones.size() > 0 ) return true;
		else return false;
	}
	
	public WebBrowserAdapter getWebBrowserAdapter(){
		if(site != null && site.trim().length() > 0 ){
			return new WebBrowserAdapter( context, site);	
		} else return null;
	}
	
	public PhoneEmailAdapter getEmailAdapter()
	{
		if(this.emails == null) return null;
		else return new PhoneEmailAdapter(this.context, this.emails, PhoneEmailAdapter.EMAIL_TYPE);
	}
	public PhoneEmailAdapter getPhoneAdapter()
	{
		if(this.phones == null) return null;
		else return new PhoneEmailAdapter(this.context, this.phones, PhoneEmailAdapter.PHONE_TYPE);
	}	
	
	
}

