package app.omegasoftware.wifitransferpro.database.EXTDAO;

import android.content.Context;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.database.DAO.DAOPlaylist;

public class EXTDAOPlaylist extends DAOPlaylist{

	public static final String NAME = "playlist";
	public static String ID = "id";
	
	public static String NOME = "nome";
	
	public EXTDAOPlaylist(Database p_database) {
		super(NAME, p_database);

		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));
		
		super.addAttribute(new Attribute(NOME, "nome", true, Attribute.SQL_TYPE.TEXT, false, null, false, null, 255));
		
	}

	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAOPlaylist(this.getDatabase());
	}

	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId, String pOldId) {
		
	}

	@Override
	public void populate() {
		// TODO Auto-generated method stub
		
	}
	

}
