package app.omegasoftware.wifitransferpro.common.TextWatcher;

import android.widget.EditText;

public class PhoneTextWatcher extends MaskNumberTextWatcher{
	//	(31) 3333 - 3333
	//123456789A
	
	public PhoneTextWatcher(EditText pEditText){
		super(pEditText, new int[]{0, 3, 9}, new String[] {"(", ") ", " - "}, 10);
	}


}