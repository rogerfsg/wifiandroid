package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.file.HelperPhone;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class InformacaoTelefonePagina extends InterfaceHTML{

	public InformacaoTelefonePagina(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		

		// TODO Auto-generated method stub
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("	<!-- Framework CSS -->");

		vBuilder.append("<table width=\"350\" align=\"top\" class=\"tabela_list\">");

		vBuilder.append("<colgroup>");
		vBuilder.append("<col width=\"80%\" />");
		vBuilder.append("<col width=\"20%\" />");
		vBuilder.append("</colgroup>");
		vBuilder.append("<thead>");

		vBuilder.append("    			<tr class=\"tr_list_titulos\">");
		vBuilder.append("    				<td class=\"td_list_titulos\" colspan=\"2\">");

		vBuilder.append("<img class=\"icones_list\" src=\""
			+ OmegaFileConfiguration
					.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
			+ "icone_celular.png\">&nbsp;");
		
		vBuilder.append(__helper.context.getString(R.string.telefone));
		vBuilder.append("    			</td>");
		vBuilder.append("    			</tr>");
		

		String vVetorTituloInformacao[] = {
				__helper.context.getString(R.string.total_memoria_interna),
				__helper.context.getString(R.string.total_memoria_externa),
				__helper.context.getString(R.string.livre_memoria_interna),
				__helper.context.getString(R.string.livre_memoria_externa),
				__helper.context.getString(R.string.bateria),
				};
		
		String vTotalInternalMemory = HelperPhone.getTotalInternalMemorySize();
		String vFreeInternalMemory = HelperPhone.getAvailableInternalMemorySize();
		
		String vTotalExternalMemory = HelperPhone.getTotalExternalMemorySize();
		String vFreeExternalMemory = HelperPhone.getAvailableExternalMemorySize();
		
		String vBateria = String.valueOf(HelperPhone.getCharge(__helper.context) * 100 ) + " %";
		
		String vVetorConteudoInformacao[] = {
				vTotalInternalMemory,
				vTotalExternalMemory,
				vFreeInternalMemory,
				vFreeExternalMemory,
				vBateria,
		};
		
		
		for(int i = 0 ; i < vVetorTituloInformacao.length; i++){
			String vTitulo = vVetorTituloInformacao[i];
			String vConteudo = vVetorConteudoInformacao[i];
			
			String classTr = (i % 2 == 0) ? "tr_list_conteudo_impar"
					: "tr_list_conteudo_par";
			vBuilder.append("<tr class=\"" + classTr + "\">");
			
			vBuilder.append("<td>");
			vBuilder.append(vTitulo);
			vBuilder.append("</td>");
			vBuilder.append("<td>");
			vBuilder.append(vConteudo);
			vBuilder.append("</td>");
			vBuilder.append("</tr>");
		}
		vBuilder.append("</thead>");
		vBuilder.append("</tbody>");
		vBuilder.append("</table>");

		return new ContainerResponse(__helper,new StringBuilder( vBuilder)); 
	}

}
