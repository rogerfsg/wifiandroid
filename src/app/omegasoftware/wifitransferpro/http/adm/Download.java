package app.omegasoftware.wifitransferpro.http.adm;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.imports.MensagemPopup;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Javascript;

public class Download extends InterfaceHTML{

	public Download(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		
		if(__helper.GET("tipo") != null && __helper.GET("page") != null){
			vContainer.copyContainerResponse(__helper.getContainerResponsePagina(__helper.GET("tipo"), __helper.GET("page")));
		} else if(__helper.POST("tipo") != null && __helper.POST("page") != null){
			vContainer.copyContainerResponse(__helper.getContainerResponsePagina(__helper.POST("tipo"), __helper.POST("page")));
		}
		
		return vContainer;
	}

}
