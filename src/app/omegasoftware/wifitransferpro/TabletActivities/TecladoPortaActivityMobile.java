package app.omegasoftware.wifitransferpro.TabletActivities;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.activity.OmegaRegularActivity;
import app.omegasoftware.wifitransferpro.common.activity.OmegaCustomAdapterListActivity.CustomDataLoader;
import app.omegasoftware.wifitransferpro.http.HelperPorta;

public class TecladoPortaActivityMobile extends OmegaRegularActivity {
	private TextView txtResult;  // Reference to EditText of result

	private static final int DIALOG_ERROR_PORTA_INVALIDA = 909;
	
	private String inStr = "";  // Current input string
	// Previous operator: '+', '-', '*', '/', '=' or ' ' (no operator)
	
	//Search mode toggle buttons
	int LIMITE_MAXIMO_NUMERO_TELEFONE = 5;
	
	//Constants
	public static final String TAG = "TecladoPortaActivityMobile";
	int __origemChamado = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		
		//Log.d(TAG, "onCreate(): Activity created");
		setContentView(R.layout.teclado_porta_activity_mobile_layout);
		
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;	
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.unimedbh_dialog_error_textview);
		Button vOkButton = (Button) vDialog.findViewById(R.id.unimedbh_dialog_ok_button);

		

		switch (id) {
		case DIALOG_ERROR_PORTA_INVALIDA:
			vTextView.setText(getResources().getString(R.string.numero_invalido_porta) + HelperPorta.getStrIntervalo());
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(DIALOG_ERROR_PORTA_INVALIDA);
				}
			});
			break;
		default:
			return super.onCreateDialog(id);
			
		}
		return vDialog;
	}
	
	@Override
	public void initializeComponents() {
		// Retrieve a reference to the EditText field for displaying the result.
		txtResult = (EditText)findViewById(R.id.txtResultId);
		
//		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager., 0);
        
        txtResult.setText("");
		Bundle vParameter = getIntent().getExtras();
		if(vParameter != null){
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE));{
				String vTelefone = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE);
				if(vTelefone != null && !vTelefone.startsWith("DIGITE")){
					inStr = vTelefone;
					txtResult.append(inStr);	
				}
				
				
			}
		}
		__origemChamado = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_TECLADO_TELEFONE_ORIGEM_CHAMADO);
		//Activity listener
		CustomServiceListener listener = new CustomServiceListener();
		
		// Register listener (this class) for all the buttons
		((Button)findViewById(R.id.btnNum0Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum1Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum2Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum3Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum4Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum5Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum6Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum7Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum8Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum9Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.cadastrar_button)).setOnClickListener(listener);
		((Button)findViewById(R.id.backspace_button)).setOnClickListener(listener);
		((Button)findViewById(R.id.apagar_tudo_button)).setOnClickListener(listener);
	}
	private void onCadastroButtonClicked(){
		if(! HelperPorta.checkValidadeNumeroPorta(inStr)){
			showDialog(DIALOG_ERROR_PORTA_INVALIDA);
			return;
		}else{
			Intent v_intent = getIntent();
			v_intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE, inStr);
			setResult(__origemChamado, v_intent);
			finish( );	
		}
	}
	private class CustomServiceListener implements OnClickListener
	{

		
		// On-click event handler for all the buttons

		public void onClick(View view) {
			switch (view.getId()) {
			// Number buttons: '0' to '9'
			case R.id.btnNum0Id:
			case R.id.btnNum1Id:
			case R.id.btnNum2Id:
			case R.id.btnNum3Id:
			case R.id.btnNum4Id:
			case R.id.btnNum5Id:
			case R.id.btnNum6Id:
			case R.id.btnNum7Id:
			case R.id.btnNum8Id:
			case R.id.btnNum9Id:
				if(inStr.length() >= LIMITE_MAXIMO_NUMERO_TELEFONE)
					break;
				else{
					String inDigit = ((Button)view).getText().toString();
					int vStartCursor = txtResult.getSelectionStart();
					int vEndCursor = txtResult.getSelectionEnd();
					if(vStartCursor != vEndCursor){
						String vInfText = inStr.substring(0, vStartCursor);
						String vSupText = inStr.substring( vEndCursor) ;
						inStr = vInfText + vSupText;
//						Atualizando o ponteiro para a nova posicao
						vStartCursor = vEndCursor - (vEndCursor - vStartCursor);
					}
					if(vStartCursor == inStr.length()){
						inStr += inDigit;
						txtResult.append(inDigit);
//						txtResult.setText(inStr);	
					}
					else{						
						String vInfText = inStr.substring(0, vStartCursor) + inDigit;
						String vSupText = inStr.substring(vStartCursor);
						
						txtResult.setText("");
						txtResult.append(vInfText);
						inStr = vInfText + vSupText;   // accumulate input digit
						txtResult.setTextKeepState(inStr);
					}
					
					break;
				}

				// Operator buttons: '+', '-', '*', '/' and '='
			case R.id.cadastrar_button:
				onCadastroButtonClicked();
				break;
			case R.id.backspace_button:
				
				int vStartCursor = txtResult.getSelectionStart();
				int vEndCursor = txtResult.getSelectionEnd();
				if(vStartCursor != vEndCursor){
					String vInfText = inStr.substring(0, vStartCursor);
					String vSupText = inStr.substring( vEndCursor) ;
					inStr = vInfText + vSupText;
//					Atualizando o ponteiro para a nova posicao
					vStartCursor = vEndCursor - (vEndCursor - vStartCursor);
				}
				else if(vStartCursor == inStr.length() && inStr.length() > 0){
					inStr = inStr.substring(0, inStr.length() - 1);
					txtResult.setText("");
					txtResult.append(inStr);
//					txtResult.setText(inStr);	
				}
				else if(vStartCursor > 0){
					String vInfText = inStr.substring(0, vStartCursor - 1);
					String vSupText = inStr.substring(vStartCursor);
					txtResult.setText("");
					txtResult.append(vInfText);
					inStr = vInfText + vSupText;   // accumulate input digit
					txtResult.setTextKeepState(inStr);
				}
				break;

				// Clear button
			case R.id.apagar_tudo_button:   

				inStr = "";
				txtResult.setText("");
				break;
			}
		}
	}

}
