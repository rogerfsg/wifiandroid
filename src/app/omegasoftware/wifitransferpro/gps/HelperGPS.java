package app.omegasoftware.wifitransferpro.gps;


import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Random;

import org.apache.http.HttpResponse;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;

public class HelperGPS{

	private static final String TAG = "HelperGPS";
	
	/**
	 * This method checks if GPS is enabled
	 * @param p_context
	 * @return {boolean}
	 */
	

	

	public static double getDistanciaMinimaKMOrMile(Context pContext, boolean pIsMile){
		double vDistancia = EXTDAOArquivo.getDistanciaMinimaMetros(pContext);
		double vDistanciaKM = vDistancia / 1000;
		if(pIsMile) return vDistanciaKM / 1.6;
		else return vDistanciaKM;
	}
	
	
	
    public static boolean checkIfGPSIsEnabled(Context p_context)
    {
		//Getting location manager service
    	LocationManager manager = (LocationManager) p_context.getSystemService(Context.LOCATION_SERVICE);
    	
    	//Check if GPS is available
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        	//It is available
        	//Log.d(TAG, "checkIfGPSIsEnabled(): GPS enabled");
        	return true;
        }
        else
        {
        	//Log.d(TAG, "checkIfGPSIsEnabled(): GPS not enabled");
        	return false;
        }
    }
    
    public static boolean startGPSIfNecessary(Context p_context){
		if(!HelperGPS.checkIfGPSIsEnabled(p_context))
		{
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			p_context.startActivity(intent);
			if(HelperGPS.checkIfGPSIsEnabled(p_context)) return true;
			else return false;
		} else 
			return true;

    }
    
	public static String generateRandomString()
	{
		Random random = new Random();
		String hash = Long.toHexString(random.nextLong());
		return hash;
	}
	
	public static String getIMEI(Context p_context)
	{
		TelephonyManager telephonyManager = (TelephonyManager) p_context.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();
	}
	
	public static String readResponseContent(HttpResponse p_response) throws IllegalStateException, IOException
	{
		Reader reader = null;
		try {
			reader = new InputStreamReader(p_response.getEntity().getContent());
			StringBuffer sb = new StringBuffer();
			{
				int read;
				char[] cbuf = new char[1024];
				while ((read = reader.read(cbuf)) != -1)
					sb.append(cbuf, 0, read);
			}
			return sb.toString();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String formatBytesSize(long p_size)
	{
		if(p_size < 1024)
		{
			return p_size + " B";
		}
		else if(p_size > 1024 && p_size < 1024*1024)
		{
			return (p_size/1024) + " KB";
		}
		else if(p_size > 1024*1024)
		{
			return (p_size/(1024*1024)) + " MB";
		}
		return "";
	}
	
	private static Double C_RADIUS_EARTH_KM  = 6370.97327862;
	private static Double C_RADIUS_EARTH_MI  = 3958.73926185;
	private static Double C_PI  = 3.14159265358979;
//	1 metro possui esse valor em coordenada
	private static Double COORDENADA_POR_METRO = 9.1095647494827430839135533577254e-6; 
	
	
	public static Integer parserCoordenadaStringToInteger(String pCoordenada){
		if(pCoordenada == null || pCoordenada.length() == 0 ) return null;
		else {
			Integer vCoordenada = HelperInteger.parserInteger(pCoordenada);
			if(vCoordenada != null ){
				return vCoordenada;
			}
			else return null;
		}
	}
	
	public static Double parserCoordenadaStringToDouble(String pCoordenada){
		
		Integer vCoordenada = parserCoordenadaStringToInteger(pCoordenada);
		if(vCoordenada == null) return null;
		else {
			Double vValor = ((double)vCoordenada)/1000000;
			return vValor;
		}
	}
	
	public static Boolean isCoordenadaMaiorQueDistanciaMinima(double lat1, double lat2, double lon1, double lon2, double distanciaMinimaMetro){
		if(isDistanciaMaiorQueDistanciaMinima(lat1, lat2, distanciaMinimaMetro)) return true;
		else if(isDistanciaMaiorQueDistanciaMinima(lon1, lon2, distanciaMinimaMetro)) return true;
		else return false;
	}
	
	private static Boolean isDistanciaMaiorQueDistanciaMinima(double pCoordenada1, double pCoordenada2, double pDistanciaMinimaMetro){
		
		
		double vDiferenca =0;
		if(pCoordenada1 > pCoordenada2)
			vDiferenca = Math.abs( pCoordenada1 - pCoordenada2);
		else vDiferenca = Math.abs( pCoordenada2 - pCoordenada1);
		if ( vDiferenca >  COORDENADA_POR_METRO * pDistanciaMinimaMetro)
			return true;
		else return false;
	}
	
	public static Double getDistancia(double lat1, double lon1, double lat2, double lon2, boolean isMile){
		//var R = 6371; // km
        //var dLat = (lat2-lat1).toRad();
        //var dLon = (lon2-lon1).toRad();
        //var lat1 = lat1.toRad();
        //var lat2 = lat2.toRad();

        //var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        //        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
        //var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        //var d = R * c;

		Double R = 6371.0;
		Double dLat = toRad(lat2 - lat1);
        Double dLon = toRad(lon2 - lon1);
        lat1 = toRad(lat1);
        lat2 = toRad(lat2);
        Double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + 
                            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        Double vDistance = R * c;
        if(isMile)
        	return vDistance / 1.6;
        else return vDistance;
	}
	
	public static Double toRad(Double pValue ){ 
		return (pValue / 180) * C_PI;
	}
    
}
