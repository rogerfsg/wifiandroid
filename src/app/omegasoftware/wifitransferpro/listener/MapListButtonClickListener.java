package app.omegasoftware.wifitransferpro.listener;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.wifitransferpro.TabletActivities.ShowAddressOnMapActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.ItemList.AppointmentPlaceItemList;

public class MapListButtonClickListener implements OnClickListener {

	long addressId;
	ArrayList<String> __arrayListEndereco = new ArrayList<String>();
	Activity __activity;
	String __idTupla;
	public MapListButtonClickListener(long p_addressId, String pIdTupla, AppointmentPlaceItemList appointmentPlace, Activity pActivity)
	{
		__idTupla = pIdTupla;
		this.addressId = p_addressId;
		if(appointmentPlace != null){
			String v_addr =appointmentPlace.getStrAddress() ;
			if(v_addr != null){
				__arrayListEndereco.add(v_addr);	
			}
		}
		__activity = pActivity;
	}
	
	public void onClick(View v) {
		
		
		try{
			Intent vIntent = __activity.getIntent();
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, __idTupla);
			__activity.setResult(OmegaConfiguration.RESULT_CODE_FOCO_PONTO, vIntent);
			__activity.finish();
		} catch(Exception ex){
			//Log.e("onClick", ex.toString());
		}
		
		
	}
}
