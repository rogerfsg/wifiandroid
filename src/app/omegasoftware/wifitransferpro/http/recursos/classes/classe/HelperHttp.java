package app.omegasoftware.wifitransferpro.http.recursos.classes.classe;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

import android.content.Context;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.ContainerTypeOrderFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.file.ContainerTypeOrderFile.TYPE_ORDER;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.HttpConfiguration;
import app.omegasoftware.wifitransferpro.http.IndexServer;
import app.omegasoftware.wifitransferpro.http.NanoHTTPD;
import app.omegasoftware.wifitransferpro.http.lists.ContainerLists;
import app.omegasoftware.wifitransferpro.http.pages.ContainerPages;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class HelperHttp {

	public static String COMANDO_FECHAR_DIALOG= "parent.jQuery('#div_dialog').dialog('close');";
	public static String COMANDO_FECHAR_DIALOG_ATUALIZANDO = "parent.document.location = parent.document.location.toString().replace(/nivel_visualizacao/gi, \"old\").replace(/vaga_visualizacao/gi, \"old\")";
	public static double TEMPO_PADRAO_FECHAR_DIALOG = 1.4	;
	public Context context;
	public String uri;
	public String method; 
	public Properties header; 
	public Properties parms; 
	public Properties files ;
	public IndexServer nanoHTTPD;
	public HelperHttp(Context context, IndexServer nanoHTTPD, String uri, String method, Properties header, Properties parms, Properties files ){
		this.context = context;
		this.uri = uri;
		this.method = method;
		this.header = header;
		this.parms = parms;
		this.files = files;
		this.nanoHTTPD = nanoHTTPD;
	}
	
	public static enum TYPE_GET{
		PAGE,
		ACTION
	};
	
	public class ContainerGET{
		
		
		TYPE_GET __type;
		String __strGet;
		String __tipo;
		String __page;
		String __class;
		String __action;
		String __get;
		
		public ContainerGET(){
			
			if(parms.containsKey("tipo") && parms.containsKey("page")){
				__type = TYPE_GET.PAGE;
				__tipo = parms.getProperty("tipo");
				__page = parms.getProperty("page");
			} else if(parms.containsKey("class") && parms.containsKey("action")){
				__type = TYPE_GET.ACTION;
				__class = parms.getProperty("class");
				__action = parms.getProperty("action");
			}
			
			__get = getStrGET();
		}
		
		public String getURL(){
			if(__get == null || __get.length() == 0  )
				return uri;
			else  return uri + "?" + __get;
		}
		
		public String getStrGET(){
			Enumeration<Object> vListKeys = parms.keys();
			String vValue = "";
			while(vListKeys.hasMoreElements()){
				String vKey = (String) vListKeys.nextElement();
				
				if(vKey == null || vKey.length() == 0 ) continue;
				else{
					if(vValue.length() > 0)
						vValue += "&";
					
					String vContent = (String) parms.getProperty(vKey);
					
					vValue += vKey + "=" + vContent;	
				}
			}
			return vValue;
		}
		
	}
	
	public ContainerGET getContainerGET(){
		return new ContainerGET();
	}
	
	public String getOriginPage(){
		ContainerGET vContainer = new ContainerGET();
		return vContainer.getURL();
	}
	
	public String getUrlAction(String actionPar){
		return getUrlAction(actionPar, null);
	}

	public String URI(){
		if(uri != null && uri.length() > 0 ){
			String vURI = uri;
			if(vURI.startsWith("/"))
				vURI = vURI.substring(1, vURI.length());
				
			return vURI;
		} else return "";
	}
	
	public String HEADER(String pKey){
		if(header.containsKey(pKey))
			return (String) header.get(pKey);
		else return null;
	}
	
	public static StringBuilder getHTMLTreePath(String pURL, String pKeyGetPath, String vStrPath){
		StringBuilder vBuilder = new StringBuilder();
		String vVetorPath[] = vStrPath.split("[/]");
		String vPathDiretorio = "";
		if(vVetorPath == null || vVetorPath.length == 0 )
			vBuilder.append("<input class=\"botoes_path\" value=\"/"
					+ "\" type=\"botoes_path\" onclick=\"javascript:document.location.href='" + pURL 
					+ "&" + pKeyGetPath + "=" + vPathDiretorio
					+ "'\">");
		else{

			for(int i = 0 ;  i < vVetorPath.length; i++){
				if(i == 0 ){
					vBuilder.append("<input class=\"botoes_path\" value=\"/"
							+ "\" type=\"button\" onclick=\"javascript:document.location.href='" + pURL
							+ "&" + pKeyGetPath + "=" + vPathDiretorio
							+ "'\">");
				}
				String vDiretorio = vVetorPath[i];

				if (vDiretorio == null || vDiretorio.length() == 0)
					continue;
				vPathDiretorio += "/" + vDiretorio;
				vBuilder.append("<input class=\"botoes_path\" value=\""
						+ vDiretorio
						+ "\" type=\"botoes_path\" onclick=\"javascript:document.location.href='" + pURL
						+ "&" + pKeyGetPath + "=" + vPathDiretorio
						+ "'\">");
			}
		}
		return vBuilder;
	}

	public static StringBuilder getHTMLTabelaExplorerSimplificada(HelperHttp __helper, String pKeyGetPath, String vStrPath, String pClasseTabela, String pURLDestino){
		StringBuilder vBuilder = new StringBuilder();
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		if (vStrPath != null && vStrPath.length() > 0) {
			vStrPath = HelperHttp.getPathComBarra(vStrPath);
			File vDirectory = new File(vStrPath);
			if (vDirectory.isDirectory()) {
				//		TABELA ==================================================================================================================================
				vBuilder.append("<table class=\"" +pClasseTabela + "\">");
				vBuilder.append("<colgroup>");
				vBuilder.append("<col width=\"30%\" />");
				vBuilder.append("<col width=\"70%\" />");
				vBuilder.append("</colgroup>");

				vBuilder.append("<thead>");
				vBuilder.append("<tr class=\"tr_list_titulos\">");

				ContainerTypeOrderFile vContainerOrder = new ContainerTypeOrderFile();
				vContainerOrder.setByGet(__helper);
				String vLinkOrder = vContainerOrder.getLinkHTML(null, null);
				String vAppendLinkOrder = ((vLinkOrder.length()) > 0 ? ("&" + vLinkOrder) : "");

				//				CRIA DIRETORIO ====================================================================================
				vBuilder.append("<td class=\"td_list_titulos\">");				

				vBuilder.append( "      <form name=\"form_list_cria_diretorio\" id=\"form_list_cria_diretorio\" action=\"popup.php5\" method=\"POST\" enctype=\"multipart/form-data\" ");
				vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");
				
				vBuilder.append("           <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
				vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
				vBuilder.append("           <input type=\"hidden\" name=\"tipo\" id=\"tipo\" value=\"pages\">");
				vBuilder.append("           <input type=\"hidden\" name=\"page\" id=\"page\" value=\"CriaDiretorio\">");
				
				vBuilder.append("            <input type=\"hidden\" name=\"" + EXTDAOArquivo.PATH + "\" id=\"" + EXTDAOArquivo.PATH + "\" value=\"" + vStrPath + "\">");
				
				HelperHttp.ContainerGET vContainerGET = __helper.getContainerGET();
				String vOriginGet = "";
				if(vContainerGET != null ){
					String vStrGet = vContainerGET.getStrGET();
					String vURI = __helper.URI();
					if(vStrGet != null && vStrGet.length() > 0 ){
						vOriginGet = vURI + "?" + vStrGet;
					}
				}
				
				vBuilder.append("			<input type=\"hidden\" name=\"origin_action\" id=\"origin_action\" value=\"" + vOriginGet + "\">");
				
				vBuilder.append("    					<img name=\"criaDiretorioBotao\" type=\"imagem\"  class=\"icones_list\" style=\"cursor: pointer;\" " +
						"src=\"" + OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS) + "add_folder.png\" onclick=\"javascript:document.getElementById('form_list_cria_diretorio').submit();\" onmouseover=\"javascript:tip('" + __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_CRIA_DIRETORIO) + "')\" onmouseout=\"javascript:notip()\"/>&nbsp;");
				
				//				DIRETORIO ACIMA ICON ================================================================================================================================

				vBuilder.append(" <img class=\"icones_list\" src=\""
						+ OmegaFileConfiguration
						.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
						+ "up_folder.png\" "
						+ "onmouseover=\"javascript:tip('"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_DIRETORIO_ACIMA)
						+ "')\" "
						+ "onclick=\"javascript:document.location.href='"+ pURLDestino
						+ "&" + pKeyGetPath
						+ "=" + HelperHttp.getStrPathUp(vStrPath));
				if(vAppendLinkOrder != null)
					vBuilder.append("&" + vAppendLinkOrder);
				vBuilder.append("'\""
						+ "onmouseout=\"javascript:notip()\">&nbsp;");
				//				RAIZ ICON ================================================================================================================================

				vBuilder.append("<img class=\"icones_list\" src=\""
						+ OmegaFileConfiguration
						.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
						+ "home_folder.png\" "
						+ "onmouseover=\"javascript:tip('"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_RAIZ)
						+ "')\" "
						+ "onclick=\"javascript:document.location.href='"+ pURLDestino
						+ "&" + pKeyGetPath
						+ "=" + HelperHttp.getPathRoot());
				if(vAppendLinkOrder != null)
					vBuilder.append("&" + vAppendLinkOrder);
				vBuilder.append("'\""
						+ "onmouseout=\"javascript:notip()\">&nbsp;");


				vBuilder.append(vContainerOrder.getImageHTML(TYPE_ORDER.TYPE, pURLDestino, pKeyGetPath + "=" + vStrPath));
				vBuilder.append( "      </form>");
				vBuilder.append("</td>");
				
				vBuilder.append("<td class=\"td_list_titulos\">");

				vBuilder.append(vContainerOrder.getButtonTitulo(
						__helper.context,
						HelperString.ucFirst(__helper.context.getResources().getString(
								R.string.arquivo_nome)), pURLDestino,
								pKeyGetPath + "=" + vStrPath, TYPE_ORDER.NAME));
				vBuilder.append(vContainerOrder.getImageHTML(TYPE_ORDER.NAME, pURLDestino, pKeyGetPath + "=" + vStrPath));


				vBuilder.append("</td>");
				//				=========================================================================================

				vBuilder.append("</tr>");
				vBuilder.append("</thead>");
				vBuilder.append("<tbody>");

				File vVetorFile[] = vContainerOrder
						.getListFileOfDirectory(vDirectory);
				if (vVetorFile == null || vVetorFile.length == 0) {
					vBuilder.append("<tr class=\"tr_list_conteudo_impar\">");
					vBuilder.append("<td  colspan=\"6\">");
					vBuilder.append(HelperHttp.imprimirMensagem(
							HelperString.ucFirst(__helper.context.getResources().getString(
									R.string.diretorio_sem_arquivo)),
									HelperHttp.TYPE_MENSAGEM.MENSAGEM_OK, null));
					vBuilder.append("</td>");
					vBuilder.append("</tr>");
				} else {

					for (int i = 0; i < vVetorFile.length; i++) {
						File vFile = vVetorFile[i];
						ContainerTypeFile vContainerType = ContainerTypeFile
								.getContainerTypeFile(vFile);

						String classTr = (i % 2 == 0) ? "tr_list_conteudo_impar"
								: "tr_list_conteudo_par";
						String vStri = String.valueOf(i);
						String vFileName = vFile.getName();
						boolean isDirectory = (vContainerType.typeFile == TYPE_FILE.FOLDER) ? true : false;

						vBuilder.append("<tr class=\"" + classTr + "\">");

						vBuilder.append("<td  style=\"text-align: center; padding-left: 5px;\">");
						vBuilder.append("<img class=\"icones_list\" src=\""
								+ vContainerType.getImage()
								+ "\">&nbsp;");
						vBuilder.append("</td>");
						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");

						if (!isDirectory)
							vBuilder.append(vFileName);
						else {
							vBuilder.append("<a href=\"javascript:document.location.href='" + pURLDestino
									+ "&" + pKeyGetPath
									+ "="
									+ vStrPath
									+ vFileName
									+ "/"
									+ "'\" class=\"link_padrao\" onmouseout=\"notip();\" onmouseover=\"tip('"
									+ __helper
									.getMensagemTooltip(TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_DIRETORIO)
									+ "');\">" + vFileName + "</a>");
						}
						vBuilder.append("</td>");


						vBuilder.append("</tr>");
					}

					vBuilder.append("</tbody>");
					vBuilder.append("</table>");

				}
			}
		}
		return vBuilder;
	}

	public StringBuilder importarHeadSimplificado(){
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">");
		return vBuilder;
	}
	
	public StringBuilder importarHead(){
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">");
		vBuilder.append(HelperHttp.getTagDoFavicon(context));
		vBuilder.append(HelperHttp.carregarArquivoCss(1, "adm/css/", "padrao"));
		vBuilder.append(new Javascript(this).importarTodasAsBibliotecas());
		vBuilder.append(HelperHttp.imprimirComandoJavascript("var dialogo = false"));
		return vBuilder;
	}

	public static String imprimirCabecalhoParaFormatarAction() {

		String vRetorno = carregarArquivoCss("/adm/css/", "padrao");

		vRetorno += carregarArquivoJavascript("/recursos/js/", "core");
		vRetorno += carregarArquivoJavascript("/recursos/js/", "pngfix");

		vRetorno += imprimirComandoJavascriptComTimer("document.title='" + HttpConfiguration.TITULO_PAGINA + "'", null, false);
		return vRetorno;
	}

	public static String imprimirComandoJavascript(String stringComando) {

		return getComandoJavascript(stringComando);
	}

	public String getUrlAction(String actionPar, String id) {

		int posInicial = 0;
		String action = "";
		String page = "";

		String vRetorno = "";
		if (actionPar.contains("ajax")) {

			posInicial = actionPar.indexOf("ajax") + 5;

			action = actionPar.substring(0, actionPar.indexOf("ajax") + 4) ;
			page = actionPar.substring(posInicial);
		} else {
			action = actionPar.substring(0, actionPar.indexOf("_"));
			page = actionPar.substring(actionPar.indexOf("_") + 1);
		}

		if (action.compareTo("add")== 0) {

			vRetorno = "index.php5?tipo=forms&page=" + page + "";
		} else if (action.compareTo("add_ajax") == 0) {

			vRetorno = "index.php5?tipo=ajax_pages&page=" + page + "";
		} else if (action.compareTo("edit") == 0) {

			vRetorno = "index.php5?tipo=forms&page=" + page + "&id1=" + id;
		} else if (action.compareTo("edit_ajax") == 0) {

			vRetorno = "index.php5?tipo=ajax_pages&page=" + page + "&id1=" + id;
		} else if (action.compareTo("list") == 0) {

			if (POST("url_origem") != null) {
				//TODO CUIDADO
				//jQuerystrRet = self::getUrlDecodificada(self::POST("url_origem"));
				vRetorno = POST("url_origem");
			} else {

				vRetorno = "index.php5?tipo=lists&page=" + page;
			}
		} else {

			vRetorno = "index.php5?tipo=" + action + "&page=" + page;
		}

		return vRetorno ;
	}

	public static String mudarLocation(String location) {

		String vReturn = "<script language=\"javascript\">";
		vReturn += "document.location.href=\"" + location + "\";";
		vReturn += "</script>";
		return vReturn;
	}


	public String POST(String pKey){
		if(parms.containsKey(pKey))
			return parms.getProperty(pKey);
		else return null;
	}



	public String GET(String pKey){
		if(parms.containsKey(pKey))
			return parms.getProperty(pKey);
		else return null;
	}

	public String FILES(String pKey){
		if(files.containsKey(pKey))
			return files.getProperty(pKey);
		else return null;
	}

	public static enum TYPE_MENSAGEM{
		MENSAGEM_OK,
		MENSAGEM_ERRO,
		MENSAGEM_INFO,
		MENSAGEM_WARNING,
		MENSAGEM_ASK,
		MENSAGEM_ANDROID
	}

	public static String nl2br(String pToken){
		if(pToken != null && pToken.length() > 0 ){
			return pToken.replace("/n", "</br>");
		} else return null;
	}

	public static String getTagDoFavicon(Context pContext) {
		return "";
		//        pContext.getResources().getDrawable(id);
		//
		//        return "<link rel=\"shortcut icon\" href=\"{jQuerypath}favicon.ico\" type=\"image/x-icon\" />";
	}


	public static int acharNumeroRaiz(){

		String retorno = "";
		int numero = 0;

		while (true) {
			File vFile = new File(retorno + "__root");
			if (!vFile.exists()) {
				retorno += "../";
				numero++;
			} else {
				break;
			}
		}

		return numero;
	}

	public static String acharRaiz(){
		return OmegaFileConfiguration.getPath(OmegaFileConfiguration.TYPE_PATH_FILE.WWW);
		//		
		//		String retorno = "";
		//		while (true) {
		//			File vFile = new File(retorno + "__root");
		//			if (!vFile.exists()) {
		//				retorno += "../";
		//			} else {
		//				break;
		//			}
		//		}
		//
		//		return retorno;
	}

	public static String getStringNiveisAteRaiz(int pNivel) {

		String strRetorno = "";
		for (int i = 0; i < pNivel; i++) {
			strRetorno += "../";
		}
		return strRetorno;
	}

	public static String getPathComBarra(String path) {
		if (! path.endsWith("/")) {
			return path + "/";
		}
		return path;
	}



	public static String carregarArquivoCss(Integer niveisRaiz, String diretorioApartirRaiz, String pArquivoSemExtensao) {
		String strRaiz = "";
		if(niveisRaiz == null)
			strRaiz = acharRaiz();
		else
			strRaiz = getStringNiveisAteRaiz(niveisRaiz);

		diretorioApartirRaiz = getPathComBarra(diretorioApartirRaiz);

		String strPrint = "<link type=\"text/css\" rel=\"stylesheet\" href=\"" + strRaiz + diretorioApartirRaiz + pArquivoSemExtensao + ".css\" />\n";

		return strPrint;

	}

	public static String getComandoJavascript(String stringComando) {

		if (stringComando != null && stringComando.length() > 0 ) {
			String string = "<script type=\"text/javascript\">";
			string += 	stringComando;
			string += 	"</script>";
			return string;
		} return null;
	}

	public static String carregarArquivoJavascript(String diretorioApartirRaiz, String arquivoSemExtensao) {

		diretorioApartirRaiz = getPathComBarra(diretorioApartirRaiz);

		String strPrint = "<script type=\"text/javascript\" src=\"" + diretorioApartirRaiz + arquivoSemExtensao +".js\" ></script>\n";

		return strPrint;
	}

	public static String imprimirComandoJavascriptComTimer(
			String stringComando, 
			Double timerEmSegundos, 
			boolean infinitamente) {

		return getComandoJavascriptComTimer(stringComando, timerEmSegundos, infinitamente, true);
	}

	public static String getComandoJavascriptComTimer(
			String stringComando, 
			Double timerEmSegundos, 
			boolean infinitamente, 
			boolean imprimirTagScript) {

		if (stringComando.length() > 0 ) {

			double intervalo = 0;
			boolean vValidadeTempo = false;
			if (timerEmSegundos != null && timerEmSegundos > 0 ) {
				vValidadeTempo = true;
				intervalo = timerEmSegundos * 1000;
			}
			String funcaoJs = "";
			if (infinitamente)
				funcaoJs = "setInterval";
			else
				funcaoJs = "setTimeout";
			String string = "";
			if(vValidadeTempo){
				if (imprimirTagScript) {
	
					string += "<script type=\"text/javascript\">";
				}
				
				string += funcaoJs + "(function(){ " + stringComando + " }, " + intervalo + ");";
	
				if (imprimirTagScript) {
	
					string += "</script>";
				}
			}

			return string;
		}
		return "";
	}

	public static String carregarArquivoCss(String diretorioApartirRaiz, String pArquivoSemExtensao) {

		diretorioApartirRaiz = getPathComBarra(diretorioApartirRaiz);

		String strPrint = "<link type=\"text/css\" rel=\"stylesheet\" href=\"" + diretorioApartirRaiz + pArquivoSemExtensao + ".css\" />\n";

		return strPrint;

	}

	public enum TYPE_MENSAGEM_TOOLTIP{
		MENSAGEM_EXCLUSAO,
		TOOLTIP_EXCLUSAO,
		TOOLTIP_EDICAO,
		TOOLTIP_VISUALIZACAO,
		TOOLTIP_DOWNLOAD,
		TOOLTIP_ACESSAR_DIRETORIO,
		TOOLTIP_MOVER_ARQUIVO,
		TOOLTIP_VISUALIZAR_IMAGEM,
		TOOLTIP_TOCAR_MUSICA,
		TOOLTIP_CRIA_DIRETORIO,
		TOOLTIP_DIRETORIO_ACIMA,
		TOOLTIP_RAIZ,
		TOOLTIP_COPY_ARQUIVO,
		TOOLTIP_ZIP_ARQUIVO,
		TOOLTIP_UNZIP_ARQUIVO,
		TOOLTIP_NOVA_PLAYLIST,
		TOOLTIP_ADICIONAR_MUSICA_PLAYLIST,
		TOOLTIP_ADD_LINK,
		TOOLTIP_ACESSAR_LINK,
		TOOLTIP_VER_TODOS_LINKS,
	};

	public static Integer BOTOES_DE_PAGINACAO_POR_LINHA = 30;

	public static StringBuilder getHTMLPaginacao(Integer numeroPaginas, Integer paginaAtual, String pURL, String pAnexoURL){
		if(paginaAtual == null)
			paginaAtual = 0;
		if(numeroPaginas == null)
			numeroPaginas = 0;
		if (numeroPaginas > 1) {

			StringBuilder vHTML = new StringBuilder();
			vHTML.append("<fieldset class=\"fieldset_paginacao\">");
			vHTML.append("<legend class=\"legend_paginacao\">Pagina��o</legend>");

			vHTML.append("    <table class=\"table_paginacao\">");


			for (int i = 1; i <= numeroPaginas; i++) {

				String  classTr = (i == paginaAtual) ? "td_paginacao_pag_atual" : "td_paginacao";

				if ((i - 1) % BOTOES_DE_PAGINACAO_POR_LINHA == 0) {

					vHTML.append("<tr class=\"tr_paginacao\">");
				}


				vHTML.append("<td class=" + classTr + " onclick=\"javascript:location.href='"+ pURL + "&pagina=" + String.valueOf(i) + "&numero_paginas=" + String.valueOf(numeroPaginas) + "&" + pAnexoURL + "'\">"
						+ String.valueOf(i) +
						"</td>");

				if ((i - 1) % BOTOES_DE_PAGINACAO_POR_LINHA == BOTOES_DE_PAGINACAO_POR_LINHA-1) {

					vHTML.append("</tr>");
				}
			}


			vHTML.append("</table>");

			vHTML.append("</fieldset>");
			return vHTML;
		}
		return null;
	}

	public static String getPathRoot(){
		return "/";
	}

	public static String getStrPathUp(String pPath){
		if(pPath == null || pPath.length() == 0 || pPath.compareTo(getPathRoot()) == 0 ) return pPath;
		else{
			String vVetorPath[] = pPath.split("[/]");
			ArrayList<String> vList = new ArrayList<String>(); 
			for (String vPath : vVetorPath) {
				if(vPath == null) continue;
				else if(vPath.trim().length() == 0 ) continue;
				else{
					vList.add(vPath);
				}
			}
			String vPathUp = "";
			for(int i = 0 ; i < vList.size() - 1; i ++){
				if(i == 0 )
					vPathUp += vList.get(i);
				else
					vPathUp += "/" + vList.get(i);
			}
			return vPathUp;
		}
	}


	public String getMensagemTooltip(TYPE_MENSAGEM_TOOLTIP pType){
		switch (pType) {
		case TOOLTIP_RAIZ:
			return context.getString(R.string.tooltip_raiz);
		case TOOLTIP_DIRETORIO_ACIMA:
			return context.getString(R.string.tooltip_diretorio_acima);
		case TOOLTIP_CRIA_DIRETORIO:
			return context.getString(R.string.tooltip_mensagem_cria_diretorio);
		case MENSAGEM_EXCLUSAO:
			return context.getString(R.string.mensagem_exclusao);

		case TOOLTIP_EXCLUSAO:

			return context.getString(R.string.tooltip_exclusao);
		case TOOLTIP_EDICAO:

			return context.getString(R.string.tooltip_edicao);
		case TOOLTIP_VISUALIZACAO:

			return context.getString(R.string.tooltip_visualizacao);
		case TOOLTIP_DOWNLOAD:
			return context.getString(R.string.tooltip_download);
		case TOOLTIP_ACESSAR_DIRETORIO:
			return context.getString(R.string.tooltip_acesso_diretorio);

		case TOOLTIP_VISUALIZAR_IMAGEM:
			return context.getString(R.string.tooltip_visualizar_imagem);
		case TOOLTIP_TOCAR_MUSICA:
			return context.getString(R.string.tooltip_tocar_musica);
		case TOOLTIP_MOVER_ARQUIVO:
			return context.getString(R.string.tooltip_mover_arquivo);
		case TOOLTIP_COPY_ARQUIVO:
			return context.getString(R.string.tooltip_diretorio_copy);
		case TOOLTIP_ZIP_ARQUIVO:
			return context.getString(R.string.tooltip_diretorio_zip);
		case TOOLTIP_UNZIP_ARQUIVO:
			return context.getString(R.string.tooltip_diretorio_unzip);
		case TOOLTIP_NOVA_PLAYLIST:
			return context.getString(R.string.tooltip_nova_playlist);
		case TOOLTIP_ADICIONAR_MUSICA_PLAYLIST:
			return context.getString(R.string.tooltip_musica_playlist);
		case TOOLTIP_ADD_LINK:
			return context.getString(R.string.tooltip_add_link);
		case TOOLTIP_ACESSAR_LINK:
			return context.getString(R.string.tooltip_acessar_link);
		case TOOLTIP_VER_TODOS_LINKS:
			return context.getString(R.string.tooltip_ver_todos_links);
			
			
		default:
			return "";
		}
	}
	

	public NanoHTTPD.Response getHTMLPagina(String pTipo, String pPage){
		ContainerResponse vContainer = getContainerResponsePagina(pTipo, pPage);
		if(vContainer != null) return vContainer.getResponse();
		else return null;
	}

	
	
	public ContainerResponse getContainerResponsePagina(String pTipo, String pPage){
		if(pTipo != null && pTipo.length() > 0 && 
				pPage != null && pPage.length() > 0 ){
			if(pTipo.compareTo("lists") == 0 ){
				return ContainerLists.getContainerResponse(pPage, this);
			}
			else if (pTipo.compareTo("pages") == 0 ){
				return ContainerPages.getContainerResponse(pPage, this);
			}
		}

		return null;
	}


	public static String imprimirMensagem(String mensagem, TYPE_MENSAGEM tipoMensagem , String linkImagem) {


		String strLink =  "";
		if(linkImagem != null && linkImagem.length() > 0 ){

			strLink = "onclick=\"document.location.href='{jQuerylinkImagem}'\"";

		}
		else{
			strLink = "";
		}

		//substitui \n por <br />
		mensagem = nl2br(mensagem);
		String vMensagemRetorno = "";
		switch (tipoMensagem) {
		case MENSAGEM_OK:
			vMensagemRetorno += "<div class='div_mensagem_ok'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_ok'>&nbsp;</td><td class='div_conteudo_mensagem'>" + mensagem + "</td></tr></table></div>"; 
			break;
		case MENSAGEM_INFO:
			vMensagemRetorno += "<div class='div_mensagem_info'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_info'>&nbsp;</td><td class='div_conteudo_mensagem'>" + mensagem + "</td></tr></table></div>"; 
			break;
		case MENSAGEM_WARNING:
			vMensagemRetorno += "<div class='div_mensagem_warning'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_warning'>&nbsp;</td><td class='div_conteudo_mensagem'>" + mensagem + "</td></tr></table></div>"; 
			break;
		case MENSAGEM_ERRO:
			vMensagemRetorno += "<div class='div_mensagem_erro'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_erro'>&nbsp;</td><td class='div_conteudo_mensagem'>" + mensagem + "</td></tr></table></div>";
			break;
		case MENSAGEM_ASK:
			vMensagemRetorno += "<div class='div_mensagem_info'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_info'>&nbsp;</td><td class='div_conteudo_mensagem'>" + mensagem + "</td></tr></table></div>";
			break;
		case MENSAGEM_ANDROID:
			vMensagemRetorno += "<div class='div_mensagem_android'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'><tr><td class='td_mensagem_android' " + linkImagem + ">&nbsp;</td><td class='div_conteudo_mensagem_android'>" + mensagem + "</td></tr></table></div>";
			break;
		default:
			break;
		}

		vMensagemRetorno +=  "\n";

		return vMensagemRetorno;
	}

	public static String getTituloDasPaginas(){
		return HttpConfiguration.TITULO_PAGINA;
	}
}
