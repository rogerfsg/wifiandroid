package app.omegasoftware.wifitransferpro.http.adm;

import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.HelperHTTPD;
import app.omegasoftware.wifitransferpro.http.HttpConfiguration;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.NanoHTTPD;
import app.omegasoftware.wifitransferpro.http.imports.Mensagens;
import app.omegasoftware.wifitransferpro.http.imports.MenuSuperior;
import app.omegasoftware.wifitransferpro.http.imports.TopoPagina;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Javascript;

public class Index extends InterfaceHTML{

	public Index(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public ContainerResponse getContainerResponse() {
		ContainerResponse vResponseRetorno = new ContainerResponse(__helper, new StringBuilder());
		// TODO Auto-generated method stub
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append( "<html>" + 
				"<head>" +
				"<title>" + HelperHttp.getTituloDasPaginas() + "</title>");
		
		StringBuilder vStrBibliotecasBasicas = __helper.importarHead();
		vBuilder.append(vStrBibliotecasBasicas);
		vBuilder.append("</head>");
		
		vBuilder.append(" <body leftmargin=\"0\" topmargin=\"0\" id=\"body_identificator\">");

		vBuilder.append(Javascript.imprimirCorpoDoTooltip());
		
		vResponseRetorno.appendData(vBuilder);
		vResponseRetorno.copyContainerResponse(new Mensagens(__helper).getContainerResponse());
		vBuilder = new StringBuilder();
		
		vBuilder.append("<table width=\"100%\" class=\"table_geral\" cellpadding=\"0\" cellspacing=\"0\">");
		vBuilder.append("	<tr class=\"fundo_menu_topo\">");
		vBuilder.append("			<td align=\"center\" width=\"100%\">");
		vBuilder.append("				<table width=\"100%\">");
		vBuilder.append("   				<tr>");
		vBuilder.append("						<td>");
		
		
		vResponseRetorno.appendData(vBuilder);
		vResponseRetorno.copyContainerResponse(new TopoPagina(__helper).getContainerResponse());
		vBuilder = new StringBuilder();

		vBuilder.append("</td>");
		vBuilder.append("</tr>");
		vBuilder.append("<tr>");
		vBuilder.append("<td id=\"espacamento_menu_superior\">");

		vResponseRetorno.appendData(vBuilder);
		vResponseRetorno.copyContainerResponse(new MenuSuperior(__helper).getContainerResponse());
		vBuilder = new StringBuilder();
		
		
		vBuilder.append("</td>");
		vBuilder.append("</tr>");
		vBuilder.append("<tr>");
		
		
		if (__helper.GET("tipo") == null && __helper.GET("page")  == null){
			vResponseRetorno.appendData(vBuilder);
			vResponseRetorno.copyContainerResponse(HttpConfiguration.getContainerResponseHTMLPaginaInicialPadrao(__helper));
        }else{
        	vResponseRetorno.appendData(vBuilder);
			vResponseRetorno.copyContainerResponse(__helper.getContainerResponsePagina(__helper.GET("tipo"), __helper.GET("page")));
        }
		
		vBuilder = new StringBuilder();
		
		vBuilder.append("</tr>");
		vBuilder.append("</table>");
		vBuilder.append("</td>");
		vBuilder.append("</tr>");
		vBuilder.append("</table>");
		vBuilder.append("</body>");
		vBuilder.append("</html>");
		
		vResponseRetorno.appendData(vBuilder);
		
		
		return vResponseRetorno;
		
	}
	
	

}
