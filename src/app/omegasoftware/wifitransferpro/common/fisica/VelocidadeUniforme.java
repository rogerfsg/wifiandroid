package app.omegasoftware.wifitransferpro.common.fisica;

public class VelocidadeUniforme {
	
	
	public Integer __distanciaMetro = null;
	public Integer __tempoSegundo = null;
	
	public VelocidadeUniforme(Integer pDistanciaMetro, Integer pTempoSegundo){
		__distanciaMetro = pDistanciaMetro;
		__tempoSegundo = pTempoSegundo;
	}
}
