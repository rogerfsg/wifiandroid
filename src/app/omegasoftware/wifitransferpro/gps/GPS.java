package app.omegasoftware.wifitransferpro.gps;




import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;


import android.os.Bundle;
import app.omegasoftware.wifitransferpro.common.activity.OmegaMapActivity;
import app.omegasoftware.wifitransferpro.mapa.HelperMapa;
import app.omegasoftware.wifitransferpro.mapa.InfoItemizedOverlay;


public class GPS
{
    private LocationManager lm;
    private LocationListener locationListener;
    private OmegaMapActivity activity;
    public InfoItemizedOverlay infoOverlay;
    
	private double ultimaLatitude;
	private double ultimaLongitude;
	public HelperMapa __mapaHelper;
	
    public GPS(Context act){
    	
    	activity = (OmegaMapActivity ) act;

    	lm = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);    
    	
	    locationListener = new MyLocationListener();
	    
	    lm.requestLocationUpdates(
	        LocationManager.GPS_PROVIDER, 
	        0, 
	        0, 
	        locationListener);        

    }
    
    
    
    public double getLatitude(){
    	return ultimaLatitude;
    }
    
    public double getLongitude(){
    	return ultimaLongitude;
    }
    
    public static double metrosToGraus(double p_metros){
    	double v_graus = 0 ;
    	
    	v_graus = p_metros / 111120;
    	return v_graus;
    }
    
    public static double grausToMetros(double p_graus){
    	double v_metros = 0 ;
    	
    	v_metros = p_graus * 111120;
    	return v_metros;
    }
    
    
    
    private class MyLocationListener implements LocationListener 
    {    	
        public void onLocationChanged(Location loc) {
            
        	if (loc != null) {
        		if(activity != null){
//        			activity.acoesAoMudarDePosicao(loc);	
        		}
        			
        		ultimaLatitude = loc.getLatitude();
        		ultimaLongitude = loc.getLongitude();
            }
        }

        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub
        }

        public void onStatusChanged(String provider, int status, 
            Bundle extras) {
            // TODO Auto-generated method stub
        }
        
    }        
    
}
