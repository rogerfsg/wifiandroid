package app.omegasoftware.wifitransferpro.http.imports;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.pages.LinkPagina;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class TopoPagina extends InterfaceHTML{

	public TopoPagina(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("<style type=\"text/css\">");

		vBuilder.append("   .header{");

		vBuilder.append("       border-bottom: 3px solid #009AD9;");
		vBuilder.append("       line-height: normal;");
		vBuilder.append("       vertical-align: middle;");
		vBuilder.append("       font-size: 30px;");
		vBuilder.append("       font-weight: bold;");
		vBuilder.append("       letter-spacing: -2px;");
		vBuilder.append("       line-height: 38px;");
		vBuilder.append("       font-family: arial,Helvetica,sans-serif;");


		vBuilder.append("   }");

		vBuilder.append("   .footer{");

		vBuilder.append("       height: 150px;");
		vBuilder.append("       border-top: 3px solid #009AD9;");
		vBuilder.append("       line-height: normal;");
		vBuilder.append("       margin-left: auto;");
		vBuilder.append("       margin-right: auto;");
		vBuilder.append("       width: 100%;");
		vBuilder.append("       font-family: arial,Helvetica,sans-serif;");
		vBuilder.append("       font-size: 14px;");
		vBuilder.append("       font-weight: bold;");


		vBuilder.append("   }");

		vBuilder.append("   .footer table td{");

		vBuilder.append("       text-align: center;");
		vBuilder.append("       width: 500px;");

		vBuilder.append("   }");

		vBuilder.append("   .footer img{");

		vBuilder.append("       height: 130px;");
		vBuilder.append("       width: 130px;");

		vBuilder.append("   }");

		vBuilder.append("   .container {");
		vBuilder.append("       height: 300px;");
		vBuilder.append("       width: 400px;");
		vBuilder.append("       position: static;");
		vBuilder.append("       margin-left: auto;");
		vBuilder.append("       margin-right: auto;");
		vBuilder.append("       text-align: center;");
		vBuilder.append("   }");
		vBuilder.append("   ul.thumb {");
		vBuilder.append("       float: left;");
		vBuilder.append("       list-style: none;");
		vBuilder.append("       padding: 10px;");
		vBuilder.append("       width: 900px;");

		vBuilder.append("   }");

		vBuilder.append("   ul.thumb li > a{");

		vBuilder.append("       display: block;");
		vBuilder.append("       margin-bottom: 20px;");
		vBuilder.append("       min-height: 100px;");

		vBuilder.append("   }");

		vBuilder.append("   ul.thumb li {");
		vBuilder.append("       margin: 15px;");
		vBuilder.append("       margin-left: 0px;");
		vBuilder.append("       padding: 0px;");
		vBuilder.append("       padding-bottom: 5px;");
		vBuilder.append("       float: left;");
		vBuilder.append("       position: relative;");
		vBuilder.append("       width: 115px;");
		vBuilder.append("       height: 100px;");
		vBuilder.append("       margin-bottom: 50px;");
		vBuilder.append("   }");
		vBuilder.append("   ul.thumb li img {");
		vBuilder.append("       width: 100px; height: 100px;");
		vBuilder.append("       border: 1px solid #ddd;");
		vBuilder.append("       padding: 5px;");
		vBuilder.append("       background: #f0f0f0;");
		vBuilder.append("       position: absolute;");
		vBuilder.append("       left: 0; top: 0;");
		vBuilder.append("       -ms-interpolation-mode: bicubic; ");
		vBuilder.append("   }");
		vBuilder.append("   ul.thumb li img.hover {");
		vBuilder.append("       background:url(thumb_bg.png) no-repeat center center;");
		vBuilder.append("       border: none;");
		vBuilder.append("   }");
		vBuilder.append("   #main_view {");
		vBuilder.append("       float: left;");
		vBuilder.append("       padding: 9px 0;");
		vBuilder.append("       margin-left: -10px;");
		vBuilder.append("   }");

		vBuilder.append("   ul.thumb li h2 {");
		vBuilder.append("       font-size: 1em;");
		vBuilder.append("       font: normal 10px Verdana, Arial, Helvetica, sans-serif;");
		vBuilder.append("       font-weight: normal;");
		vBuilder.append("       text-transform: uppercase;");
		vBuilder.append("       padding: 10px;");
		vBuilder.append("       margin-top: 5px;");
		vBuilder.append("       background: #f0f0f0;");
		vBuilder.append("       width: 90px;");
		vBuilder.append("       min-width: px;");
		vBuilder.append("       border: 1px solid #ddd;");
		vBuilder.append("       text-align: center;");
		vBuilder.append("       height: 60px;");
		vBuilder.append("       min-height: 100px;");
		vBuilder.append("       vertical-align: middle;");
		vBuilder.append("       display: table-cell;");

		vBuilder.append("   }");

		vBuilder.append("   ul.thumb li h2 a ");
		vBuilder.append("   {");
		vBuilder.append("       height: 20%; ");
		vBuilder.append("       text-decoration: none; ");
		vBuilder.append("       color: #777; ");
		vBuilder.append("       vertical-align: middle;");
		vBuilder.append("       font-size: 13px;");
		vBuilder.append("       font-weight: bold;");
		vBuilder.append("   }");

		vBuilder.append("   ul.thumb li span { /*--Used to crop image--*/");
		vBuilder.append("       width: 100px;");
		vBuilder.append("       height: 100px;");
		vBuilder.append("       overflow: hidden;");
		vBuilder.append("       display: block;");
		vBuilder.append("   }");

		vBuilder.append("</style>");

		vBuilder.append("<script type=\"text/javascript\"> ");

		vBuilder.append("   jQuery(document).ready(function(){");
		
		vBuilder.append("       jQuery(\"ul.thumb li\").hover(function() {");
		vBuilder.append("           jQuery(this).css({'z-index' : '10'});");
		vBuilder.append("           jQuery(this).find('img').addClass(\"hover\").stop()");
		vBuilder.append("           .animate({");
		vBuilder.append("               marginTop: '-50px', ");
		vBuilder.append("               marginLeft: '-50px', ");
		vBuilder.append("               top: '50%', ");
		vBuilder.append("               left: '50%', ");
		vBuilder.append("               width: '128px', ");
		vBuilder.append("               height: '128px',");
		vBuilder.append("               padding: '20px' ");
		vBuilder.append("           }, 200);");
		vBuilder.append("       } , function() { ");
		vBuilder.append("           jQuery(this).css({'z-index' : '0'});");
		vBuilder.append("           jQuery(this).find('img').removeClass(\"hover\").stop()");
		vBuilder.append("           .animate({");
		vBuilder.append("               marginTop: '0', ");
		vBuilder.append("               marginLeft: '0',");
		vBuilder.append("               top: '0', ");
		vBuilder.append("               left: '0', ");
		vBuilder.append("               width: '100px', ");
		vBuilder.append("               height: '100px', ");
		vBuilder.append("               padding: '5px'");
		vBuilder.append("           }, 400);");
		vBuilder.append("       });");
		vBuilder.append("   });");
		vBuilder.append(" </script> ");		

		vBuilder.append("<table id=\"topo_pagina\" width=\"100%\" height=\"56\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"display: true; " +
				"background-color: white; background-repeat:none; background-position:bottom;\" bgcolor=\"#ff6600\">");
		vBuilder.append("	<tr>");
		vBuilder.append("		<td class=\"td_left\">");
		vBuilder.append("			<table class=\"tabela_cabecalho_usuario\">");
		vBuilder.append("				<tr>");
		vBuilder.append("					<td>");
								
		vBuilder.append("   <div class=\"container\">");						
		vBuilder.append("   <ul class=\"thumb\">");
		vBuilder.append("       <li style=\"z-index: 0;\">");
		vBuilder.append("           <a href=\"index.php5?tipo=lists&page=arquivo\"><span><img style=\"margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 100px; height: 100px; padding: 5px; overflow: hidden;\" class=\"\" src=\"adm/imgs/especifico/topo_folders.png\" alt=\"\"></span></a>");
		vBuilder.append("           <h2><a href=\"index.php5?tipo=lists&page=arquivo\">" + context.getString(R.string.topo_folders)+ "</a></h2>");
		vBuilder.append("       </li>");
//		vBuilder.append("       <li style=\"z-index: 0;\">");
//		vBuilder.append("           <a href=\"index.php5?tipo=lists&page=pesquisa\"><span><img style=\"margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 100px; height: 100px; padding: 5px; overflow: hidden;\" class=\"\" src=\"adm/imgs/especifico/topo_searches.png\" alt=\"\"></span></a>");
//		vBuilder.append("           <h2><a href=\"index.php5?tipo=lists&page=pesquisa\">" + context.getString(R.string.topo_searches)+ "</a></h2>");
//		vBuilder.append("       </li>");
		vBuilder.append("       <li style=\"z-index: 0;\">");
		vBuilder.append("           <a href=\"index.php5?tipo=lists&page=imagem\"><span><img style=\"margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 100px; height: 100px; padding: 5px; overflow: hidden;\" class=\"\" src=\"adm/imgs/especifico/topo_pictures.png\" alt=\"\"></span></a>");
		vBuilder.append("           <h2><a href=\"index.php5?tipo=lists&page=imagem\">" + context.getString(R.string.topo_image)+ "</a></h2>");
		vBuilder.append("       </li>");
		vBuilder.append("       <li style=\"z-index: 0;\">");
		vBuilder.append("           <a href=\"index.php5?tipo=lists&page=playlist_musica\"><span><img style=\"margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 100px; height: 100px; padding: 5px; overflow: hidden;\" class=\"\" src=\"adm/imgs/especifico/topo_music.png\" alt=\"\"></span></a>");
		vBuilder.append("           <h2><a href=\"index.php5?tipo=lists&page=playlist_musica\">" + context.getString(R.string.topo_music)+ "</a></h2>");
		vBuilder.append("       </li>");
//		vBuilder.append("       <li style=\"z-index: 0;\">");
//		vBuilder.append("           <a href=\"index.php5?tipo=lists&page=video\"><span><img style=\"margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 100px; height: 100px; padding: 5px; overflow: hidden;\" class=\"\" src=\"adm/imgs/especifico/topo_movie.png\" alt=\"\"></span></a>");
//		vBuilder.append("           <h2><a href=\"index.php5?tipo=lists&page=video\">" + context.getString(R.string.topo_movie)+ "</a></h2>");
//		vBuilder.append("       </li>");
//		vBuilder.append("       <li style=\"z-index: 0;\">");
//		vBuilder.append("           <a href=\"index.php5?tipo=lists&page=tocador\"><span><img style=\"margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 100px; height: 100px; padding: 5px; overflow: hidden;\" class=\"\" src=\"adm/imgs/especifico/topo_play.png\" alt=\"\"></span></a>");
//		vBuilder.append("           <h2><a href=\"index.php5?tipo=lists&page=tocador\">" + context.getString(R.string.topo_play)+ "</a></h2>");
//		vBuilder.append("       </li>");

		vBuilder.append("   </ul>");
		vBuilder.append("   </div>");
		vBuilder.append("				</td>");
		vBuilder.append("			</tr>");
		vBuilder.append(" 		</table>");
		vBuilder.append("</table>");
		vBuilder.append("				<td id=\"td_right\">");
		
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		vContainer.appendData(vBuilder);
		ContainerResponse vContainerLinkPagina = new LinkPagina(__helper).getContainerResponse();
		vContainer.copyContainerResponse(vContainerLinkPagina);
		vBuilder = new StringBuilder();
		
		vBuilder.append("				</td>");
		
		
		vBuilder.append("	</tr>");
		
		
		vContainer.appendData(vBuilder);
		return vContainer;
		
	}

}
