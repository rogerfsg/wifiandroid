package app.omegasoftware.wifitransferpro.common.container;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

public class ContainerRouteInformation{
	public Float __distancia =null;
	public String __unidadeDistancia=null;
	public Integer __tempoHora=null;
	public Integer __tempoMinuto=null;
	public Integer __tempoDia=null;
	public ContainerRouteInformation(Float pDistancia,
			String pUnidadeDistancia, Integer pTempoDia, Integer pTempoHora, Integer pTempoMinuto){
		__distancia = pDistancia;
		__unidadeDistancia = pUnidadeDistancia;
		__tempoDia = pTempoDia;
		__tempoHora = pTempoHora;
		__tempoMinuto = pTempoMinuto;
	}

	public ContainerRouteInformation(Float pDistancia,
			String pUnidadeDistancia, LinkedHashMap<String, Integer> pHashDate){
		__distancia = pDistancia;
		__unidadeDistancia = pUnidadeDistancia;
		
		
		Set<String> vListKeyDate =  pHashDate.keySet();
		Iterator<String> it = vListKeyDate.iterator();
		while(it.hasNext()) {
			String vToken  =it.next();
			if(vToken.contains("day")){
				__tempoDia = pHashDate.get(vToken);
			} else if(vToken.contains("hour")){
				__tempoHora = pHashDate.get(vToken);
			} else if(vToken.contains("min")){
				__tempoMinuto = pHashDate.get(vToken);		
			}  
		}
	}

	public String printDistancia(){
		if(__distancia != null && __unidadeDistancia != null)
			return String.valueOf(__distancia) + " " + __unidadeDistancia;
		else return null;
	}

	public String printTempo(){
		return printTempo(__tempoDia, __tempoHora, __tempoMinuto);
	}
	
	public static String printTempo(Integer dia, Integer hora, Integer minuto){
		String vStrTempo = "";
		boolean vValidade = false;
		if(dia != null ){
			if(dia > 0 ){
				if(dia == 1)
					vStrTempo += String.valueOf(dia) + " dia" ;
				else  if(dia > 1)
					vStrTempo += String.valueOf(dia) + " dias" ;
				vValidade = true;
			}
			
		}
		if(hora != null){
			if(hora > 0 ){
			if(vValidade){
				vStrTempo += " " ;
			}
			if(hora == 1)
				vStrTempo += String.valueOf(hora) + " hora" ;
			else if(hora > 1) 
				vStrTempo += String.valueOf(hora) + " horas" ;
			vValidade = true;
			}
		}

		if(minuto != null){
			if(minuto > 0){
				if(vValidade){
					vStrTempo += " " ;
				}
				if(minuto == 1)
					vStrTempo += String.valueOf(minuto) + " min" ;
				else if(minuto > 1)
					vStrTempo += String.valueOf(minuto) + " mins" ;
				vValidade = true;	
			}
			
		}
		return vStrTempo;
	}

	public Integer getTempoHora(){
		return __tempoHora;
	}

	public Integer getTempoDia(){
		return __tempoDia;
	}

	public Integer getTempoMinuto(){
		return __tempoMinuto;
	}

	public String getUnidadeDistancia(){
		return __unidadeDistancia;
	}

	public Float getDistancia(){
		return __distancia;
	}
}

