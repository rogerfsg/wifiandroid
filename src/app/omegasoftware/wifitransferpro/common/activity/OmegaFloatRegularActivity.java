package app.omegasoftware.wifitransferpro.common.activity;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.listener.CloseButtonActivityClickListenerTablet;



public class OmegaFloatRegularActivity extends OmegaMapActivity {


	//Close button
	private Button closeButton;
	String __nomeTag = null;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	/**
	 * Creates loading dialog
	 */
	@Override
	protected Dialog onCreateDialog(int id) {

		ProgressDialog dialog = null;

		switch (id) {
		case OmegaConfiguration.ON_LOAD_DIALOG:
			dialog = new ProgressDialog(this);
			dialog.setMessage(getResources().getString(R.string.string_loading));
			break;

		default:
			break;
		}

		return dialog;

	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public boolean loadData() {
		return true;
	}

	@Override
	public void initializeComponents(){

		//Attach search button to its listener
		this.closeButton = (Button) findViewById(R.id.close_img_button);
		this.closeButton.setOnClickListener(new CloseButtonActivityClickListenerTablet(this));

	}


}
