package app.omegasoftware.wifitransferpro.TabletActivities;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.activity.OmegaRegularActivity;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.http.HelperPorta;
import app.omegasoftware.wifitransferpro.listener.TecladoPortaOnClickListener;

public class AjudaActivityMobile extends OmegaRegularActivity {
	public static String TAG = "AboutActivityMobile";
	private Button problemaConexaoButton;
	private Button emailButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.ajuda_activity_mobile_layout);
		
		this.problemaConexaoButton = (Button) findViewById(R.id.problema_conexao_button);
		problemaConexaoButton.setOnClickListener(new ProblemaConexaoButtonListener(this));
		
		this.emailButton = (Button) findViewById(R.id.email_suporte_button);
		emailButton.setOnClickListener(new EmailButtonListener(this));
		
		
		initializeComponents();
		applyTypeFace();
	}

	private void applyTypeFace(){
		
		Typeface  customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		problemaConexaoButton.setTypeface(customTypeFace);
		
		emailButton.setTypeface(customTypeFace);
		
	}
	
	
	@Override
	public boolean loadData() {
		//
		// TODO Auto-generated method stub
		return true;
	}
	
	
	@Override
	public void initializeComponents() {
		
		// TODO Auto-generated method stub
		
	}
	
	
	
	private class ProblemaConexaoButtonListener implements OnClickListener
	{
		Activity __activity;
		public ProblemaConexaoButtonListener(Activity pActivity){
			__activity = pActivity;
		}

		public void onClick(View v) {
			Intent intent = new Intent(getApplicationContext(), ProblemaConexaoActivityMobile.class);
			startActivity(intent);
			

		}
	}
	
	private class EmailButtonListener implements OnClickListener
	{
		Activity __activity;
		public EmailButtonListener(Activity pActivity){
			__activity = pActivity;
		}

		public void onClick(View v) {
			Intent intent = new Intent(getApplicationContext(), EmailActivityMobile.class);
			startActivity(intent);
			

		}
	}	
	
	
}
