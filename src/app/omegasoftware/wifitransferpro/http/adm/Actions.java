package app.omegasoftware.wifitransferpro.http.adm;

import app.omegasoftware.wifitransferpro.database.GenericDAO;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.imports.Header;
import app.omegasoftware.wifitransferpro.http.pages.AnexoAdicionarPlaylistPagina;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class Actions extends InterfaceHTML{

	public Actions(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}
	ContainerResponse vContainer;
	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		String vClasse = "";
		String vAction = "";
		String vRegistroDaOperacao = "";
		
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append( "<html>" + 
				"<head>" +
				"<title>" + HelperHttp.getTituloDasPaginas() + "</title>");
		
		StringBuilder vStrBibliotecasBasicas = __helper.importarHead();
		vBuilder.append(vStrBibliotecasBasicas);
		vBuilder.append("</head>");
		vBuilder.append(" <body leftmargin=\"0\" topmargin=\"0\" id=\"body_identificator\">");
		
		vContainer = new ContainerResponse(__helper, vBuilder);
		vBuilder = new StringBuilder();
		
		if(__helper.POST("class") != null){
			vClasse = __helper.POST("class");
			vAction = __helper.POST("action");
			vRegistroDaOperacao = __helper.POST("id1");
		}else if(__helper.GET("class") != null){
			vClasse = __helper.GET("class");
			vAction = __helper.GET("action");
			vRegistroDaOperacao = __helper.GET("id1");
		} else{
			return null;
		}
		
		
		DatabaseWifiTransfer vDatabase = new DatabaseWifiTransfer(context);
		GenericDAO vObjEXTDAO = (GenericDAO) vDatabase.factoryTable(vClasse);
		
		ContainerResponse vContainerAction = null; 
		if(vAction.compareTo("add") == 0){
			vContainerAction = vObjEXTDAO.actionAdd(__helper); 
		} else if(vAction.compareTo("edit") == 0){
			vContainerAction = vObjEXTDAO.actionEdit(__helper);
		}else if(vAction.compareTo("remove") == 0){
			vContainerAction = vObjEXTDAO.actionRemove(__helper);
		} else{
			vContainerAction = vObjEXTDAO.callUserFunc(__helper, vAction);
		}
		
		vDatabase.close();
		if(vContainerAction != null)
			vContainer.copyContainerResponse(vContainerAction);
		
		vBuilder.append("</body>");
		vBuilder.append("</html>");
		vContainer.appendData(vBuilder);
		
		return vContainer;
//		return __helper.nanoHTTPD.factoryResponse(vRetorno);
	}

}
