package app.omegasoftware.wifitransferpro.listener;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;

public class HourClickListener implements OnClickListener
{


	Integer __idListContainerHour;
	Activity __activity;
	
	FactoryHourClickListener __factory;
	public HourClickListener(
			Activity pActivity, 
			FactoryHourClickListener pFactory,
			Integer pIdListContainerHour){
		__idListContainerHour = pIdListContainerHour;
		__activity = pActivity;
		__factory = pFactory;

	}

	public void onClick(View v) {
		__factory.setLastContainerHour(__idListContainerHour);
		__activity.showDialog(FactoryHourClickListener.TIME_DIALOG_ID);
	}	
	
	public void setTimeOfView(int hour, int minute){
		__factory.setLastContainerHour(__idListContainerHour);
		__factory.setDateOfView(hour, minute);
	}
}


