package app.omegasoftware.wifitransferpro.http.recursos.classes.classe;

import java.net.URLEncoder;

public class Javascript {
	HelperHttp __helper;

	
	
    public static String pathNiveisAteARaiz = null;
//    public static Integer numeroNiveisAteARaiz = null;
    
	public Javascript(HelperHttp pHelper){
		__helper = pHelper;
	}
	
	public String importarBibliotecaJavascriptPadrao(){

    	String vStrReturn = HelperHttp.carregarArquivoJavascript("recursos/js/", "core");
    	vStrReturn += HelperHttp.carregarArquivoJavascript("recursos/js/", "ajax");
    	vStrReturn += HelperHttp.carregarArquivoJavascript("recursos/js/", "especifico");

		return vStrReturn;
    }
	
	public static String importarBibliotecaJavascriptUpload(){
		String vStrRaizLibs = "recursos/libs/multiple_uploader/source/";
    	String vStrReturn = HelperHttp.carregarArquivoJavascript(vStrRaizLibs, "Swiff.Uploader");

    	vStrReturn += HelperHttp.carregarArquivoJavascript(vStrRaizLibs, "FancyUpload3.Attach" );
    	vStrReturn += HelperHttp.carregarArquivoJavascript(vStrRaizLibs, "FancyUpload2");
    	vStrReturn += HelperHttp.carregarArquivoJavascript(vStrRaizLibs, "Fx.ProgressBar");
    	vStrReturn += HelperHttp.carregarArquivoJavascript(vStrRaizLibs, "Uploader");

		return vStrReturn;
    }
	
	public static StringBuilder importarBibliotecaJavascriptTree(){
		
		String vStrRaizLibs = "recursos/libs/tree/";
		
		StringBuilder vBuilder = new StringBuilder();
    	
    	vBuilder.append(HelperHttp.carregarArquivoJavascript(vStrRaizLibs, "jqueryFileTree"));
    	vBuilder.append(HelperHttp.carregarArquivoCss( vStrRaizLibs , "jqueryFileTree"));
    	
		return vBuilder;
    }
	
	public static String importarBibliotecaJavascriptMusicPlayer(String pPathMusic, String pNomeMusica){
		String vStrRaizLibs = "recursos/libs/music_player/";
		String vStrReturn = ""; 
		vStrReturn += "<script language=\"JavaScript\" src=\"" +vStrRaizLibs + "audio-player.js\"></script>";
		vStrReturn += "<object type=\"application/x-shockwave-flash\" data=\"" +vStrRaizLibs + "player.swf\" id=\"audioplayer1\" height=\"24\" width=\"290\">";
		vStrReturn += "<param name=\"movie\" value=\"" +vStrRaizLibs + "player.swf\">";
		
		String vURL = URLEncoder.encode("index.php5?tipo=pages&page=PlayerMusic&path=" + pPathMusic + "&nome=" +pNomeMusica);
		vStrReturn += "<param name=\"FlashVars\" value=\"playerID=audioplayer1&soundFile=" + vURL+ "\">";
		vStrReturn += "<param name=\"quality\" value=\"high\">";
		vStrReturn += "<param name=\"menu\" value=\"false\">";
		vStrReturn += "<param name=\"wmode\" value=\"transparent\">";
		vStrReturn += "</object>";
		vStrReturn += "</script>";    	

		return vStrReturn;
    }
	
	public static String importarBibliotecaJavascriptMusicPlayer(String pPathCompletoMusic){
		String vStrRaizLibs = "recursos/libs/music_player/";
		String vStrReturn = ""; 
		vStrReturn += "<script language=\"JavaScript\" src=\"" +vStrRaizLibs + "audio-player.js\"></script>";
		vStrReturn += "<object type=\"application/x-shockwave-flash\" data=\"" +vStrRaizLibs + "player.swf\" id=\"audioplayer1\" height=\"24\" width=\"290\">";
		vStrReturn += "<param name=\"movie\" value=\"" +vStrRaizLibs + "player.swf\">";
		
		String vURL = URLEncoder.encode("index.php5?tipo=pages&page=PlayerMusic&path_completo=" + pPathCompletoMusic);
		vStrReturn += "<param name=\"FlashVars\" value=\"playerID=audioplayer1&soundFile=" + vURL+ "\">";
		vStrReturn += "<param name=\"quality\" value=\"high\">";
		vStrReturn += "<param name=\"menu\" value=\"false\">";
		vStrReturn += "<param name=\"wmode\" value=\"transparent\">";
		vStrReturn += "</object>";
		vStrReturn += "</script>";    	

		return vStrReturn;
    }
	
	public static String importarBibliotecaJavascriptPlaylistPlayer(String pIdPlaylist){
		String vStrRaizLibs = "recursos/libs/playlist_player/";
		
		String vStrReturn = ""; 
		
		vStrReturn += "<object ";
		vStrReturn += " data=\"";
		vStrReturn += vStrRaizLibs + "/mp3player.swf?autostart=true&playlist=" ;
		vStrReturn += URLEncoder.encode("xml.php5?tipo=pages&page=PlayerPlaylist&playlist=" + pIdPlaylist);
		vStrReturn += "\"";
		vStrReturn += " type=\"application/x-shockwave-flash\" width=\"100%\" height=\"500\">";
		vStrReturn += "<param name=\"movie\" value=\"'";
		vStrReturn += vStrRaizLibs + "/mp3player.swf?autostart=true&playlist=" ;
		vStrReturn += URLEncoder.encode("xml.php5?tipo=pages&page=PlayerPlaylist&playlist=" + pIdPlaylist);
		vStrReturn += "\" />";
		vStrReturn += " </object>";

		return vStrReturn;
    }
	
	

    public String importarBibliotecaJQuery(){

    	String path = pathNiveisAteARaiz + "recursos/libs/jquery";
    	String str = "";
    	str = HelperHttp.carregarArquivoJavascript(path + "/", "jquery-1.5.1.min");
    	str += HelperHttp.carregarArquivoJavascript(path + "/", "jquery-ui-1.8.6.custom.min");
        str += HelperHttp.carregarArquivoJavascript(path + "/", "jquery.pngFix");
    	str += HelperHttp.carregarArquivoJavascript(path + "/", "jquery.maskMoney");

    	str += HelperHttp.carregarArquivoJavascript(path + "/", "jquery.autocomplete");
    	str += HelperHttp.carregarArquivoCss( path + "/css/autocomplete", "autocomplete");
    	str += HelperHttp.carregarArquivoCss( path + "/css/smoothness", "jquery-ui-1.8.6.custom");
    	str += HelperHttp.carregarArquivoJavascript(path + "/", "jquery-ui-timepicker-addon");
    	str += HelperHttp.carregarArquivoCss(path + "/css/timepicker", "style");
    	str += HelperHttp.carregarArquivoJavascript( path + "/", "jquery.ui.datepicker-pt-BR");

    	str += HelperHttp.getComandoJavascript("jQuery(document).ready(function () {" +
												"jQuery(\"#div_dialog\").dialog({autoOpen: false, height: 50, maxHeight: 600," +
												"close: function(event, ui){ jQuery(\"#div_dialog\").dialog(\"option\", \"height\", 50);" + 
												"document.getElementById('conteudo_dialog').src='about:blank'; }" +
													 "});"+
			     					"});");

    	str += "<div id='div_dialog' style='display: none;'>";
    	str += "<iframe id=\'conteudo_dialog\' class=\'conteudo_dialog\' src=\'\' frameborder=\'0\' style=\'width:100%; border:0; background-color:#ffffff; height:100%; overflow:auto;\'></iframe>";
    	str += "</div>";
    	return str;
    }
    

	public static String importarBibliotecaTooltip(){

	    String vPath = "/recursos/libs/tooltip/";

	    String str = HelperHttp.carregarArquivoCss(vPath, "tooltip");

        return str;

    }
    
    public static String imprimirCorpoDoTooltip(){

        String path = pathNiveisAteARaiz + "recursos/libs/tooltip/";

        String str = "<input type=\"hidden\" id=\"ultimo_popup_selecionado\" value=\"\" />";
		str += "<div id=\"repositorio_invisivel\" style=\"display: none;\"></div>";
        str += "<div id=\"ToolTip\"></div>";

        str += HelperHttp.carregarArquivoJavascript(path, "tooltip");

        return str;

    }
//    
//    public static String imprimirCorpoDoTooltip(){
//
//        String path = pathNiveisAteARaiz + "recursos/libs/tooltip/";
//
//        String str = "<input type=\"hidden\" id=\"ultimo_popup_selecionado\" value=\"\" />";
//		str  += "<div id=\"repositorio_invisivel\" style=\"display: none;\"></div>";
//		str  += "<div id=\"ToolTip\"></div>";
//
//        str += HelperHttp.carregarArquivoJavascript(path, "tooltip");
//
//        return str;
//
//    }
    
    public static String importarBibliotecaSpryMenuBar(){

        String vPath = "/recursos/libs/spry_menubar";

        String str = HelperHttp.carregarArquivoJavascript(vPath, "SpryMenuBar");
		str += HelperHttp.carregarArquivoCss(vPath, "SpryMenuBar");

		return str;
    }
    
    public String importarTodasAsBibliotecas(){
        setRaizDaEstrutura();
        
        String str = importarBibliotecaTooltip();
        str += importarBibliotecaSpryMenuBar();
        str += importarBibliotecaJavascriptPadrao();
        str += importarBibliotecaJQuery();
        String vBibliotecaValidacao = importarBibliotecaDeValidacao();
        str += vBibliotecaValidacao ;
        str += importarBibliotecaDragAndDrop();

        return str;
    }
    
    public static String importarBibliotecaDeValidacao(){

    	String path = "/recursos";

        String str = HelperHttp.carregarArquivoJavascript(path + "/libs/spry_validation", "SpryValidationTextField");
        str += HelperHttp.carregarArquivoJavascript(path + "/libs/spry_validation", "SpryValidationCheckbox");
        str += HelperHttp.carregarArquivoJavascript(path + "/libs/spry_validation", "SpryValidationRadio");
        str += HelperHttp.carregarArquivoJavascript(path + "/libs/spry_validation", "SpryValidationSelect");
        str += HelperHttp.carregarArquivoJavascript(path + "/libs/spry_validation", "SpryUtils");

        str += HelperHttp.carregarArquivoCss(path + "/libs/spry_validation", "SpryValidationTextField");
        str += HelperHttp.carregarArquivoCss(path + "/libs/spry_validation", "SpryValidationSelect");

        String vStrValidacoes = HelperHttp.carregarArquivoJavascript(path + "/js/", "validacoes") ;

        str += vStrValidacoes;
        return str;

    }
    
    public static String getComandoParaAtualizarDialog(){

        return HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO;
    }
    
    public String importarBibliotecaDragAndDrop(){

        String path = pathNiveisAteARaiz + "recursos/libs/drag_and_drop";

        String str = HelperHttp.carregarArquivoJavascript(path + "/", "jquery.event.drag.min");
        str += HelperHttp.carregarArquivoJavascript(path + "/", "jquery.event.drop.min");

        return str;
    }
    

    public static void setRaizDaEstrutura(){
    	if(pathNiveisAteARaiz == null){
	    	pathNiveisAteARaiz = "/";
//	    	numeroNiveisAteARaiz = HelperHttp.acharNumeroRaiz();
	    }
    }

}
