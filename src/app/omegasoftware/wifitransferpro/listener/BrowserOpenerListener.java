package app.omegasoftware.wifitransferpro.listener;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;

public class BrowserOpenerListener implements OnClickListener {

	String url = "";
	Context context;
	public BrowserOpenerListener(String p_url, Context context)
	{
		this.url = p_url;
		this.context = context;
	}
	
	public void onClick(View v) {
		
		Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(this.url));
		viewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(viewIntent);  
		
	}
	
}
