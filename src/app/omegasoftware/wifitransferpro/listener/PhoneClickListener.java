package app.omegasoftware.wifitransferpro.listener;


import android.content.Intent;
import android.net.Uri;
import app.omegasoftware.wifitransferpro.R;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class PhoneClickListener implements OnClickListener {

	private Integer __buttonId = null;
	
	public PhoneClickListener(int pButtonId){
		__buttonId = pButtonId;
	}
	
//	public PhoneClickListener(){
//		__buttonId = R.id.email_or_phone_button;
//	}
	
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Button view = null;

		if(__buttonId == R.id.email_or_phone_button){
			view = (Button) v.findViewById(R.id.email_or_phone_button);	
		} else{
			view  = (Button) v;
		}
		
		
		String phoneNumber = (String) view.getText();

		String uri = "tel:" + phoneNumber.trim() ;
		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setData(Uri.parse(uri));
		
		v.getContext().startActivity(intent);
		
	}
	
	
}
