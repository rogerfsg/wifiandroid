package app.omegasoftware.wifitransferpro.common.Adapter;

import java.util.ArrayList;


import android.content.Context;
import android.graphics.Typeface;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.listener.BrowserOpenerListener;
import app.omegasoftware.wifitransferpro.listener.EmailClickListener;
import app.omegasoftware.wifitransferpro.listener.PhoneClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

public class WebBrowserAdapter extends BaseAdapter {

	private Typeface customTypeFace;
	
	Context context;
	ArrayList<String> data;

	
	public WebBrowserAdapter(Context context, ArrayList<String> data)
	{
		this.context = context;
		this.data = data;
		
		
		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
		
	}
	public WebBrowserAdapter(Context context, String data)
	{
		this.context = context;
		this.data = new ArrayList<String>();
		this.data.add(data);
		
		
		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
		
	}

	public int getCount() {
		return this.data.size();
	}

	public String getItem(int position) {
		return this.data.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		String value = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.phone_or_email_button, null);
		
		Button valueButton = (Button) layoutView.findViewById(R.id.email_or_phone_button);
		
		
		valueButton.setTypeface(this.customTypeFace);
		
		
		valueButton.setOnClickListener(new BrowserOpenerListener(value, context ));
		
		if(value != null && value.startsWith("http://")){
			value = value.replace("http://", "");
			valueButton.setText(value);
		}
		
		return layoutView;
	}
	
}