package app.omegasoftware.wifitransferpro.common.container;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

public abstract class AbstractContainerClassItem {
	Class<?> __class;
	String __tag;
	int __idStringNome;
	Integer __drawableId;
	boolean __isComumATodosUsuarios = false;
	String __tagPai = null;
	public AbstractContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag,
			Integer pDrawable){
		__class = pClass;
		__tag = pTag;
		__drawableId = pDrawable;
		__idStringNome = pIdStringNome;
	}
	
	public AbstractContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag,
			Integer pDrawable,
			boolean pIsComumATodosUsuario){
		__class = pClass;
		__tag = pTag;
		__drawableId = pDrawable;
		__idStringNome = pIdStringNome;
		__isComumATodosUsuarios = pIsComumATodosUsuario;
	}
	
	
	public String getTagPai(){
		return __tagPai;
	}
	
	public boolean isComumATodosUsuarios(){
		return __isComumATodosUsuarios;
	}
	
	
	
	public abstract OnClickListener getOnClickListener(
			Activity pActivity);
	
	public boolean isEqual(String pTagCompare){
		if(pTagCompare == null) return false;
		
		String vTagCompare1 = __tag.toLowerCase();
		String vTagCompare2 = pTagCompare.toLowerCase();
		if(vTagCompare1.compareTo(vTagCompare2) ==0 ) return true;
		else return false;
	}
	
	public Integer getDrawableId(){
		return __drawableId;
	}
	
	public String getTag(){
		return __tag;
	}
	
	public Class<?> getClassTarget(){
		return __class;
	}
	public String getNome(Context pContext){
		return pContext.getResources().getString(__idStringNome);
		
	}
}
