package app.omegasoftware.wifitransferpro.http.pages;

import java.io.File;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class ListsArquivoUnzip extends InterfaceHTML{

	public ListsArquivoUnzip(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}


	@Override
	public ContainerResponse getContainerResponse() {
		String vStrPath = __helper.GET("novo_path") != null ? __helper.GET("novo_path") : __helper.GET(EXTDAOArquivo.PATH);
		if(vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		
		StringBuilder vBuilder = new StringBuilder();
		if(vStrPath != null && vStrPath.length() > 0 ){
			File vFileDiretorioCorrente = new File(vStrPath);
			if(vFileDiretorioCorrente.isDirectory()){
				
				String vNome = (__helper.GET(EXTDAOArquivo.NOME) == null) ? "" : __helper.GET(EXTDAOArquivo.NOME) ;
				
				
				String vPath = (__helper.GET(EXTDAOArquivo.PATH) == null) ? "" : __helper.GET(EXTDAOArquivo.PATH) ;
				vBuilder.append("            <input type=\"hidden\" name=\"" + EXTDAOArquivo.NOME+ "1\" id=\"" + EXTDAOArquivo.NOME+ "1\" value=\"" + vNome + "\">");
				vBuilder.append("            <input type=\"hidden\" name=\"" + EXTDAOArquivo.PATH+ "1\" id=\"" + EXTDAOArquivo.PATH+ "1\" value=\"" + vPath+ "\">");
				String valueNovoPath = ((__helper.GET("novo_path") != null) ? 
							__helper.GET("novo_path") :
							vPath);
				valueNovoPath = HelperFile.getNomeSemBarra(valueNovoPath);
				
				String vSufixURL = "&" + "novo_nome" + "='+document.getElementById('novo_nome').value+'" + "&" + EXTDAOArquivo.PATH +"=" + vPath + "&" + EXTDAOArquivo.NOME + "=" + vNome;
				
				vBuilder.append("      <form name=\"form_list_arquivo\" id=\"form_list_arquivo\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
				vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");
				vBuilder.append("            <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
				vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
				vBuilder.append("           	<input type=\"hidden\" name=\"class\" id=\"class\" value=\"Arquivo\">");
				vBuilder.append("            <input type=\"hidden\" name=\"action\" id=\"action\" value=\"unzipArquivo\">");
				vBuilder.append("            <input type=\"hidden\" name=\"is_extract_here\" id=\"is_extract_here\" value=\"0\">");
				
				vBuilder.append("            <input type=\"hidden\" name=\"path\" id=\"path\" value=\"" + vPath + "\">");
				vBuilder.append("            <input type=\"hidden\" name=\"nome\" id=\"nome\" value=\"" + vNome + "\">");
				
				vBuilder.append("    		<table width=\"350\" align=\"top\" class=\"tabela_popup\">");
				
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td colspan=\"3\" height=\"11\" align=\"left\" valign=\"middle\" class=\"textos5\" style=\"padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;\" >");
				
				vBuilder.append(HelperHttp.imprimirMensagem(context.getString(R.string.informacao_mover_arquivo), TYPE_MENSAGEM.MENSAGEM_INFO, null));
				
				
				vBuilder.append("    				</td>");
				vBuilder.append("    			</tr>");
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td height=\"22\" align=\"right\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				vBuilder.append(HelperString.ucFirst(context.getString(R.string.path)) + ":");
				vBuilder.append("    				</td>");
				vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

				vBuilder.append("<input type=\"text\" maxlength=\"200\" id=\"novo_path\" name=\"novo_path\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\"" + vStrPath+ "\"/>");
				vBuilder.append("    				</td>");
				vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				vBuilder.append("    					<input id=\"botaoExtractHere\" name=\"txtBotao\" type=\"button\" value=\"" + context.getString(R.string.extrair_aqui).toUpperCase() + "\" class=\"botoes_form\" style=\"cursor: pointer;\" />") ;
				vBuilder.append("    				</td>");
				vBuilder.append("    			</tr>");
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td height=\"22\" align=\"right\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				vBuilder.append(HelperString.ucFirst(context.getString(R.string.nome_novo_diretorio)) + ":");
				vBuilder.append("    				</td>");
				vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				
				String valueNovoNome = ((__helper.GET("novo_nome") != null) ? 
						__helper.GET("novo_nome") :
						vNome);
				valueNovoNome = HelperFile.getNomeSemBarra(valueNovoNome);
				if(valueNovoNome.contains(".")){
					int vLastIndex = valueNovoNome.lastIndexOf(".");
					valueNovoNome = vNome.substring(0, vLastIndex);
				}
				
				vBuilder.append("<input type=\"text\" maxlength=\"200\" id=\"novo_nome\" name=\"novo_nome\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\"" +valueNovoNome + "\"/>");
				
				vBuilder.append("    				</td>");
				vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				vBuilder.append("    					<input id=\"botaoInNewDirectory\" name=\"txtBotao\" type=\"button\" value=\"" + context.getString(R.string.extrair_novo_diretorio).toUpperCase() + "\" class=\"botoes_form\" style=\"cursor: pointer;\" />") ;

				vBuilder.append("    				</td>");
				vBuilder.append("    			</tr>");
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td height=\"11\" align=\"left\" valign=\"middle\" class=\"textos5\" style=\"padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;\" >");
				
				vBuilder.append("    			</td>");
				vBuilder.append("    			</tr>");
				vBuilder.append("    			<tr>");				
				vBuilder.append("    				<td class=\"td_lists_conteudo\"\" colspan=\"3\" >");
				vBuilder.append(HelperHttp.getHTMLTreePath("popup.php5?tipo=pages&page=ListsArquivoUnzip" + vSufixURL, "novo_path", vStrPath));
				
				vBuilder.append("    			</td>");
				vBuilder.append("    			</tr>");
				
				vBuilder.append("    		</table>");
				vBuilder.append("    	</form>");
				
				vBuilder.append(HelperHttp.getHTMLTabelaExplorerSimplificada(__helper, "novo_path", vStrPath, "tabela_popup","popup.php5?tipo=pages&page=ListsArquivoUnzip&" + vSufixURL));
				
				vBuilder.append("<br/>");
				vBuilder.append("<br/>");
				
				vBuilder.append("<script>");
				vBuilder.append("jQuery(document).ready(function(){");

				vBuilder.append("jQuery('#botaoExtractHere').click(function(){");
				
				vBuilder.append("jQuery('#is_extract_here').val(\"1\");");
				vBuilder.append("jQuery('#form_list_arquivo').submit();");

				vBuilder.append("});");

				vBuilder.append("jQuery('#botaoInNewDirectory').click(function(){");

				vBuilder.append("jQuery('#is_extract_here').val(\"0\");");
				vBuilder.append("jQuery('#form_list_arquivo').submit();");

				vBuilder.append("});");

				vBuilder.append("});");
				vBuilder.append("</script>");
				
				
			}
		}
		
		return new ContainerResponse( __helper, new StringBuilder(vBuilder));
	}

}
