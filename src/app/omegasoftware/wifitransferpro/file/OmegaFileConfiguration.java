package app.omegasoftware.wifitransferpro.file;

import java.io.File;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import app.omegasoftware.wifitransferpro.PrincipalActivity;

public class OmegaFileConfiguration {

	//Error logging configuration
	public static final String ERROR_LOG_FILES_EXTENSION = ".log";
	
	//Camera configuration
	public static final Bitmap.CompressFormat PICTURE_COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;
	public static final int PICTURE_COMPRESS_QUALITY = 50;
	
	//Timing configurations
	public static final long INITIAL_DELAY = 2000;
	public static final long REPORTER_TIMER_INTERVAL = 30000;
	public static final long UI_UPDATE_TIMER_INTERVAL = 10000;
	public static final long PICTURE_TAKER_TIMER_INTERVAL = 120000;
	public static final long TAKE_PICTURE_DELAY = 2000;
	
	//Intents configuration
	public static final String REFRESH_UI_INTENT = "omega.trucktracker.UPDATE_UI_INTENT";
	public static final String TAKE_PICTURE_INTENT = "omega.trucktracker.TAKE_PICTURE_INTENT";
	
	static String PATH_WWW = "/sdcard/.OmegaWifiTransfer/";
	
	//File storage configuration
	public static final String BASE_DIRECTORY_PATH = "";
	public static final String LOG_FILES_EXTENSION = ".txt";
	public static final String ZIP_FILES_EXTENSION = ".zip";
	public static final String PICTURE_FILES_EXTENSION = ".jpeg";
	
	public static final int MAX_MESSAGES_STACK_SIZE = 2000;
	
	//GPS configuration
	public static final int MIN_TIME_LOCATION_UPDATE = 5000; //milliseconds
	public static final int MIN_DISTANCE_LOCATION_UPDATE = 5; //meters

	//Service state intent extras configuration
	public static final String SS_GPS_STATE = "SS_GPS_STATE";
	public static final String SS_INTERNET_STATE = "SS_INTERNET_STATE";
	public static final String SS_STORED_FILES_SIZE = "SS_STORED_FILES_SIZE";
	
	//Internet states
	public static final int INTERNET_INACTIVE = -1;
	public static final int INTERNET_ACTIVE_DISCONNECTED = 0;
	public static final int INTERNET_ACTIVE_CONNECTED = 1;
	
	public static TYPE_PATH_FILE MY_TYPE_PATH_FILE = TYPE_PATH_FILE.WWW; 
	
	public static enum TYPE_PATH_FILE{
		WWW
	}
	
	public static void createDirectoryIfNecessary(String pPath){
		if(pPath != null && pPath.length() > 0 ){
			HelperFile.createDirectory(pPath);
		}
	}
	
	public static String getPath(TYPE_PATH_FILE pType){
		String vPath = null;
		switch (pType) {
		case WWW:
			vPath = PATH_WWW; 
			break;
		
		default:
			break;
		}
		if(vPath != null){
			createDirectoryIfNecessary(vPath);
		}
		return vPath;
	}
	public static String PATH_ASSETS_ROOT_SITE = "site";
	public static String RELATIVE_PATH_DIRETORIO_CSS = "/adm/css/";
	public static String RELATIVE_PATH_DIRETORIO_IMGS = "/adm/imgs/";
	public static String RELATIVE_PATH_DIRETORIO_IMGS_ESPECIFICO = "/adm/imgs/especifico/";
	public static String RELATIVE_PATH_DIRETORIO_IMGS_MENU = "/adm/imgs/menu/";
	public static String RELATIVE_PATH_DIRETORIO_IMGS_PADRAO = "/adm/imgs/padrao/";
	public static String RELATIVE_PATH_DIRETORIO_JAVASCRIPT = "/recursos/libs/jquery/";
	public static String RELATIVE_PATH_DIRETORIO_LIBS = "/recursos/libs/";
	
	
	public static enum TYPE_RELATIVE_PATH {
		CSS,
		IMGS,
		IMGS_ESPECIFICO,
		IMGS_MENU,
		IMGS_PADRAO,
		JAVASCRIPT,
		LIBS
	}
	
	public static String getPathTypeRelativePath(TYPE_RELATIVE_PATH pType){
		switch (pType) {
		case CSS:
			return RELATIVE_PATH_DIRETORIO_CSS;
		case IMGS:
			return RELATIVE_PATH_DIRETORIO_IMGS;
		case IMGS_ESPECIFICO:
			return RELATIVE_PATH_DIRETORIO_IMGS_ESPECIFICO;
		case IMGS_MENU:
			return RELATIVE_PATH_DIRETORIO_IMGS_MENU;
		case IMGS_PADRAO:
			return RELATIVE_PATH_DIRETORIO_IMGS_PADRAO;
		case JAVASCRIPT:
			return RELATIVE_PATH_DIRETORIO_JAVASCRIPT;
		case LIBS:
			return RELATIVE_PATH_DIRETORIO_LIBS;
		default:
			return "";
		}
	}
	
	
	public static void criaHierarquiaArquivosSeNecessario(Context pContext, File pNewDirectoryRootHTML){
		HelperFile.createDirectory( pNewDirectoryRootHTML.getAbsolutePath());
		if(pNewDirectoryRootHTML == null || !pNewDirectoryRootHTML.isDirectory()){
			return;
		} else{
			String vPathRoot = pNewDirectoryRootHTML.getAbsolutePath();
			HelperFile.copyAllDirectoryFromAssets(pContext, PATH_ASSETS_ROOT_SITE, vPathRoot + "/");
			
			
		}
	}
	static boolean CHECK_HIERARQUIA_SITE = false;
	public static File getFileIndexSite(){
		return getFileIndexSite(PrincipalActivity.__principalActivity);
	}
	
	
	public static void createHierarquiaArquivo(Context pContext, boolean pIsToClearBefore){
		
		File vDirectoryWWW = new File(OmegaFileConfiguration.getPath(OmegaFileConfiguration.TYPE_PATH_FILE.WWW));
		if(pIsToClearBefore){
			if(vDirectoryWWW.exists()){
				vDirectoryWWW.delete();
			}
			vDirectoryWWW.mkdirs();
		}
		if(!CHECK_HIERARQUIA_SITE){
			criaHierarquiaArquivosSeNecessario(pContext, vDirectoryWWW);
			CHECK_HIERARQUIA_SITE = true;
		}
	}
	
	public static File getFileIndexSite(Context pContext){
		
		File vDirectoryWWW = new File(OmegaFileConfiguration.getPath(OmegaFileConfiguration.TYPE_PATH_FILE.WWW));
		if(!CHECK_HIERARQUIA_SITE){
			criaHierarquiaArquivosSeNecessario(pContext, vDirectoryWWW);
			CHECK_HIERARQUIA_SITE = true;
		}
		
		File vIndex = new File(OmegaFileConfiguration.getPath(OmegaFileConfiguration.TYPE_PATH_FILE.WWW));
		return vIndex;
	}
}
