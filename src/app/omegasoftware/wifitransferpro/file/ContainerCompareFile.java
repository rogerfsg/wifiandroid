package app.omegasoftware.wifitransferpro.file;

import java.io.File;

import app.omegasoftware.wifitransferpro.file.ContainerTypeOrderFile.TYPE_ORDER;

public class ContainerCompareFile {
	private ContainerTypeFile containerTypeFile;
	private File file;
	public ContainerCompareFile(File pFile){
		file = pFile;
		containerTypeFile = ContainerTypeFile.getContainerTypeFile(pFile);
	}
	
	public ContainerTypeFile getContainerTypeFile(){
		return containerTypeFile;
	}
	
	public File getFile(){
		return file;
	}
}
