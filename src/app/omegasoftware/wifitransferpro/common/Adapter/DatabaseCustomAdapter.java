
package app.omegasoftware.wifitransferpro.common.Adapter;



import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.NameCustomItemListComparator;
import app.omegasoftware.wifitransferpro.common.NumeroStringCustomItemListComparator;
import app.omegasoftware.wifitransferpro.common.ItemList.CustomItemList;
import app.omegasoftware.wifitransferpro.common.ItemList.EntidadeItemList;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;

public abstract class DatabaseCustomAdapter extends BaseAdapter {

	protected ArrayList<CustomItemList> list = new ArrayList<CustomItemList>();
	protected ArrayList<GestureDetector> listSlideGestureDetector = new ArrayList<GestureDetector>();
	protected Activity __activity;
	boolean isAsc;

	String nomeTabela;
	String nomeAtributoItemListOrdenacao;
	boolean __isEditPermitido = false;
	boolean __isRemovePermitido = false;
	
	protected DatabaseCustomAdapter(Activity pActivity, boolean pIsAsc, String nomeTabela, String pNomeAtributoItemListOrdenacao)
	{		
		this.__activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		initializePermissao();
		this.list = loadData(pIsAsc);
		
	}
	
	public ArrayList<CustomItemList> getList(){
		return list;
	}
	
	private void initializePermissao(){
		DatabaseWifiTransfer db = new DatabaseWifiTransfer(__activity);
		
		db.close();
	}
	
	public boolean isEditPermitido(){
		return __isEditPermitido;
	}
	
	public boolean isRemovePermitido(){
		return __isRemovePermitido;
	}
	
	protected DatabaseCustomAdapter(
			Activity pActivity, 
			boolean pIsAsc, 
			String nomeTabela, 
			boolean loadCustomItemList, 
			String pNomeAtributoItemListOrdenacao)
	{
		this.__activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		initializePermissao();
		if(loadCustomItemList)
			this.list = loadData(pIsAsc);
		
	}
	
	
	
	protected void initalizeListCustomItemList(){
		this.list = loadData(this.isAsc);
	}
	
	protected DatabaseCustomAdapter(Activity pActivit, boolean pIsAsc, String nomeTabela, String pNomeAtributoOrdenacao, boolean loadCustomItemList)
	{
		this.__activity = pActivit;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoOrdenacao;
		initializePermissao();
		if(loadCustomItemList)
			this.list = loadData(pIsAsc);
	}

	public String getNomeTabela(){
		return nomeTabela;
	}
	
	public boolean isAsc(){
		return isAsc;
	}

	protected TextView CreateTextView(int pRLayoutId, String pText, View pView)
	{
		return CreateTextView(pRLayoutId, pText, pView, false);
	}

	public void CreateLinearLayout(LinearLayout pLinearLayout, OnClickListener pOnClickListener)
	{
		
		pLinearLayout.setClickable(true);
		pLinearLayout.setFocusable(true);
		if(pOnClickListener != null)
			pLinearLayout.setOnClickListener(pOnClickListener);
	}

	protected void setVisibilityHidden(int pRLayoutId, View pView)
	{
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		vTextView.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
	}
	
	protected TextView CreateTextView(int pRLayoutId, String pText, View pView, Boolean pAppend)
	{
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		if(pText == null || pText.length() == 0){
			setVisibilityHidden(pRLayoutId, pView);
		}else{
			if(pAppend)
				vTextView.setText(new StringBuilder(vTextView.getText() + pText));
			else
				vTextView.setText(pText);
		}
		return vTextView;
	}
		

	public int getCount() {	
		if(this.list == null)
			return 0;
		else return this.list.size();
	}

	public CustomItemList getItem(int pPosition) {
		return this.list.get(pPosition);
	}

	public ArrayList<CustomItemList> loadData(boolean p_isAsc )
	{

		try {

			ArrayList<Long> vListId = getListIdBySearchParameters(p_isAsc);

			ArrayList<CustomItemList> vRetorno = getListCustomItemList( vListId);
			
			Collections.sort(vRetorno, new NameCustomItemListComparator(p_isAsc, nomeAtributoItemListOrdenacao));

			return vRetorno;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}

		return null;
	}


	public ArrayList<CustomItemList> getListCustomItemList(
			ArrayList<Long> pArrayListLong) throws Exception {

		ArrayList<CustomItemList> vArrayFuncionarioItemList = new ArrayList<CustomItemList>();
		DatabaseWifiTransfer vDataBaseUnimed = new DatabaseWifiTransfer(this.__activity);
		Table vObjTable = vDataBaseUnimed.factoryTable(this.nomeTabela);
		try{
			for (Long iLong : pArrayListLong) {

				vObjTable.setAttrStrValue(
						Table.ID_UNIVERSAL,
						iLong.toString());
				vObjTable.select();
				//				CustomItemList v_item = pObj.createFuncionarioItemList();
				CustomItemList v_item = getCustomItemListOfTable(vObjTable);
				if(v_item != null)
					vArrayFuncionarioItemList.add(v_item);
				
				vObjTable.clearData();
			}
			return vArrayFuncionarioItemList;	
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			if(vDataBaseUnimed != null){
				vDataBaseUnimed.close();	
			}

		}
		return null;
	}

	public long getItemId(int pPosition) {		
		CustomItemList vCustom = this.list.get(pPosition);
		String v_id = vCustom.getId();
		try{
			if(v_id != null)
				return Long.parseLong(v_id);
			else return -1;
		} catch(Exception ex){
			return -1;
		}

	}

	protected abstract ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc);

	public abstract CustomItemList getCustomItemListOfTable(Table pObj);

	public void SortList(boolean pAsc) {
		
		Collections.sort(list, new NameCustomItemListComparator(pAsc, nomeAtributoItemListOrdenacao));
	}
	
	public void SortListNearMe(boolean pAsc) {
		Collections.sort(list, new NumeroStringCustomItemListComparator(pAsc, EntidadeItemList.DISTANCIA_MEU_PONTO_E_PONTO_ORIGEM));
		
	}
	
	public  abstract View getView(int position, View convertView, ViewGroup parent) ;

}
