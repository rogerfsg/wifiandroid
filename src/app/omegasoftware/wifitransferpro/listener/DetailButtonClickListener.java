package app.omegasoftware.wifitransferpro.listener;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.wifitransferpro.common.container.ContainerActionDetail;


public class DetailButtonClickListener implements OnClickListener {
	
	private String __IdCustomItemList;
	short __modeSearch;
	Activity __activity;
	public DetailButtonClickListener(Activity pActivity, String pIdCustomItemList, short p_modeSearch)
	{
		this.__IdCustomItemList = pIdCustomItemList;
		__modeSearch = p_modeSearch;
		__activity  = pActivity;
	}

	public void onClick(View pView) {
		ContainerActionDetail.actionDetail(__activity, __modeSearch, __IdCustomItemList);	  
								
	}
}
