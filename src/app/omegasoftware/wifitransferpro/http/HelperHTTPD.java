package app.omegasoftware.wifitransferpro.http;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.http.NanoHTTPD.Response;

public class HelperHTTPD {
	public static InputStream mergeData(InputStream pResponseDestino, InputStream pResponseOrigem){
		
		
		byte buffer[] = new byte[256];
		ArrayList<Byte> vList = new ArrayList<Byte>();
		int len = 0;
		while(len != -1){
			try
			{
				len = pResponseDestino.read(buffer);
			}
			catch ( IOException e)
			{	
				return null;
			}
			if(len > 0 ){
				try {
					for(int i = 0 ; i < len ; i ++){
						byte vByte = buffer[i];
						vList.add(vByte);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
		}
		
		while(len != -1){
			try
			{
				len = pResponseOrigem.read(buffer);
			}
			catch ( IOException e)
			{	
				return null;
			}
			if(len > 0 ){
				try {
					for(int i = 0 ; i < len ; i ++){
						byte vByte = buffer[i];
						vList.add(vByte);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
		}
		
		byte vVetorConteudo [] = new byte[vList.size()];
		for (int i = 0 ; i < vList.size(); i ++) {
			vVetorConteudo[i] = vList.get(i);
		}
		try {
			pResponseDestino.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ByteArrayInputStream( vVetorConteudo);
		
	}
	
	public static void merge(String vTexto, NanoHTTPD.Response pResponseDestino, NanoHTTPD.Response pResponseOrigem){
		
		pResponseDestino.data = HelperHTTPD.mergeData(pResponseDestino.data, vTexto);
		
		HelperHTTPD.mergeResponseHTTPD(pResponseDestino, pResponseOrigem);
	}
	
	public static InputStream mergeData(InputStream pResponseDestino, String pStrOrigem){
		if(pStrOrigem == null || pStrOrigem.length() == 0 ) return pResponseDestino;
		
		byte buffer[] = new byte[256];
		ArrayList<Byte> vList = new ArrayList<Byte>();
		int len = 0;
		while(len != -1){
			try
			{
				len = pResponseDestino.read(buffer);
			}
			catch ( IOException e)
			{	
				return null;
			}
			if(len > 0 ){
				try {
					for(int i = 0 ; i < len ; i ++){
						byte vByte = buffer[i];
						vList.add(vByte);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
		}
		
		byte vVetorByteOrigem[] = pStrOrigem.getBytes();
		for( int i = 0 ; i < vVetorByteOrigem.length; i++){
			vList.add(vVetorByteOrigem[i]);
		}
		byte vVetorConteudo [] = new byte[vList.size()];
		for (int i = 0 ; i < vList.size(); i ++) {
			vVetorConteudo[i] = vList.get(i);
		}
		try {
			pResponseDestino.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ByteArrayInputStream( vVetorConteudo);
		
	}
	
	public static void mergeResponseHTTPD(Response pResponseDestino, Response pResponseOrigem){
		if(pResponseOrigem == null ) return ;
		pResponseDestino.header = mergeHeader(pResponseDestino.header, pResponseOrigem.header);
		pResponseDestino.data = mergeData(pResponseDestino.data, pResponseOrigem.data);
		
	}
	
	public static Properties mergeHeader(Properties pResponseDestino, Properties pResponseOrigem){
		Enumeration vKeysOrigem = pResponseOrigem.keys();
		
		while(vKeysOrigem.hasMoreElements()){
			Object vKeyOrigem = vKeysOrigem.nextElement();
			pResponseDestino.put(vKeysOrigem, pResponseOrigem.get(vKeyOrigem));
		}
		
		return pResponseDestino;
	}
}
