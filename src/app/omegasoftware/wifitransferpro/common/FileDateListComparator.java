package app.omegasoftware.wifitransferpro.common;

import java.io.File;
import java.util.Comparator;

import app.omegasoftware.wifitransferpro.common.ItemList.CustomItemList;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;



public class FileDateListComparator implements Comparator<Object> {
	private boolean isAsc;
	
	public FileDateListComparator(boolean p_isAsc)
	{
		this.isAsc = p_isAsc;
	}
	
    public int compare(Object o1, Object o2)
    {
        return compare((File)o1, (File)o2);
    }
    
    public int compare(File p1, File p2)
    {
    	Long o1 = p1.lastModified();
    	Long o2 = p2.lastModified();
    	
    	if(o1 != null && o2 != null){
    		
			if(isAsc)
            	if(o1 > o2 ) return 1;
            	else if(o1 == o2) return 0;
            	else return -1;
            else
            	if(o1 < o2 ) return 1;
            	else if(o1 == o2) return 0;
            	else return -1;
    		
    	} 
    	
    	return 0;
    }

}
