package app.omegasoftware.wifitransferpro.common.activity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.TabletActivities.AboutActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.date.HelperDate;
import app.omegasoftware.wifitransferpro.mapa.HelperMapa;
import app.omegasoftware.wifitransferpro.mapa.InfoItemizedOverlay;
import app.omegasoftware.wifitransferpro.mapa.LocationTouchOverlay;
import app.omegasoftware.wifitransferpro.mapa.MapaItemizedOverlay;
import app.omegasoftware.wifitransferpro.mapa.container.ContainerMarcadorMapa;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public abstract class OmegaMapActivity extends MapActivity {

	private static String TAG = "OmegaMapActivity";
	public MapView mapView;

	public LocationTouchOverlay m_locationTouchOverlay;
	public MapController mapController;

	public LinkedHashMap<Drawable, InfoItemizedOverlay> __hashIconeKeyByInfoOverlay = new LinkedHashMap<Drawable, InfoItemizedOverlay>(); 
	public LinkedHashMap<Drawable, MapaItemizedOverlay>  __hashIconeKeyByItemizedOverlay = new LinkedHashMap<Drawable, MapaItemizedOverlay>();
	Drawable __listIcon[] = null;

	String __nomeTag = null;
	
	
	protected ContainerMarcadorMapa __principalContainerMarcadorMapa = null;
	protected ArrayList<ContainerMarcadorMapa> __listContainerMarcadorMapa = new ArrayList<ContainerMarcadorMapa>();

	private int __autoIncrement = 0;

	public abstract boolean loadData();
	public abstract void initializeComponents();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle vParameters = getIntent().getExtras();
		try{
			__listIcon = (Drawable[]) vParameters.getParcelableArray(OmegaConfiguration.SEARCH_LIST_MAP_ICONE);	
		}catch(Exception ex){

		}

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;

		
			switch (item.getItemId()) {

			
			
			case R.id.menu_sobre:
				intent = new Intent(this, AboutActivityMobile.class);
				break;

			default:
				return super.onOptionsItemSelected(item);


			}
	

		startActivity(intent);

		return true;

	}

	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean>
	{

		Activity __activity;
		public CustomDataLoader(Activity pActivity){
			__activity = pActivity;
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
		}

		@Override
		protected Boolean doInBackground(String... params) {

			return loadData();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			try
			{
				dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);	
			}
			catch(Exception e)
			{
			}

			if(result)
			{
				myInitializeComponents();
				initializeComponents();
			}
		}

	}

	public void removeListMarcador(ArrayList<ContainerMarcadorMapa> pListContainerMarcadorMapa){
		if(pListContainerMarcadorMapa != null){
			if(pListContainerMarcadorMapa.size() > 0 ){
				for (ContainerMarcadorMapa containerMarcadorMapa : pListContainerMarcadorMapa) {
					removeMarcador(containerMarcadorMapa);
				}
			}
		}
	}
	
	public void removeMarcador(ContainerMarcadorMapa pContainerMarcadorMapa){
		
		Integer vIndex = pContainerMarcadorMapa.getIndex();
		if(vIndex != null){
			
			if(this.__hashIconeKeyByItemizedOverlay.containsKey(vIndex)){
				this.__hashIconeKeyByItemizedOverlay.remove(vIndex);
			}
			
			if(this.__hashIconeKeyByInfoOverlay.containsKey(vIndex)){
				this.__hashIconeKeyByInfoOverlay.remove(vIndex);	
			}
			
		}
	}

	private void initializeIfNecessaryInfoOverlayAndMapItemizedOverlay(Drawable pIcon){
		if(pIcon == null) return;

		if(! this.__hashIconeKeyByInfoOverlay.containsKey(pIcon)){

			InfoItemizedOverlay infoOverlay = new InfoItemizedOverlay(pIcon, mapView);
			this.__hashIconeKeyByInfoOverlay.put(pIcon, infoOverlay );	
		}

		if(! this.__hashIconeKeyByItemizedOverlay.containsKey(pIcon)){

			MapaItemizedOverlay itemizedOverlay = new MapaItemizedOverlay(pIcon, mapView.getOverlays());
			this.__hashIconeKeyByItemizedOverlay.put(pIcon, itemizedOverlay);
		}
	}

	private Integer adicionarMarcador(Drawable pIcon, OverlayItem overlayitem){		
		int vIndex = __autoIncrement;
		InfoItemizedOverlay vInfoItemizedOverlay  = null;
		MapaItemizedOverlay vMapaItemizedOverlay = null;

		initializeIfNecessaryInfoOverlayAndMapItemizedOverlay(pIcon);

		if(this.__hashIconeKeyByInfoOverlay.containsKey(pIcon)){
			vInfoItemizedOverlay = this.__hashIconeKeyByInfoOverlay.get(pIcon);
		}
		if(this.__hashIconeKeyByItemizedOverlay.containsKey(pIcon)){
			vMapaItemizedOverlay = this.__hashIconeKeyByItemizedOverlay.get(pIcon);
		}
		if(vInfoItemizedOverlay != null && vMapaItemizedOverlay != null){
			overlayitem.setMarker(pIcon);
			vInfoItemizedOverlay.addOverlay(overlayitem);
			vMapaItemizedOverlay.addOverlay(overlayitem);
			__autoIncrement ++;
			return vIndex;
		}
		return null;
	}

	private OverlayItem getOverlayItem(Location pLocation){
		OverlayItem overlayItem = new OverlayItem(
				HelperMapa.getGeoPoint(
						pLocation.getLatitude(),
						pLocation.getLongitude()
						), 
						"Localização em " + 
								HelperDate.getDataAtualFormatada(), 
								"Localização: " +
										"Lat: " + (pLocation.getLatitude()) + 
										" Lng: " + (pLocation.getLongitude())
				);
		return overlayItem;
	}

//	public void acoesAoMudarDePosicao(Location location){
//		GeoPoint vGeoPoint = HelperMapa.getGeoPoint(location);
//		if(__principalContainerMarcadorMapa != null){
//			__principalContainerMarcadorMapa.setGeoPoint(vGeoPoint);
//		}
//		
//		GeoPoint pGeoPoint = HelperMapa.getGeoPoint(location);
//		OverlayItem vOverlayItem = getOverlayItem(location);
//		
//		adicionarMarcador(__principalContainerMarcadorMapa.__icone, vOverlayItem);
//
//		irParaPonto(pGeoPoint);
//
//	}

	public void clearListContainerMarcadorMapa(){
		Set<Drawable> vListKey = __hashIconeKeyByItemizedOverlay.keySet();
		for (Drawable vKey : vListKey) {
			MapaItemizedOverlay vItemizedOverlay = __hashIconeKeyByItemizedOverlay.get(vKey);
			vItemizedOverlay.limparOverlay();	
		}
	}

	public void myInitializeComponents() {
		
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);

		mapController = mapView.getController();
		m_locationTouchOverlay = new LocationTouchOverlay(this);

		boolean vValidade = false;
		if(this.__listIcon != null){
			if(this.__listIcon.length > 0 ){
				vValidade = true;
				for (Drawable vIcon : this.__listIcon) {
					initializeIfNecessaryInfoOverlayAndMapItemizedOverlay(vIcon);
				}
			}
		}

		if(!vValidade){
			Drawable vIcon = this.getResources().getDrawable(R.drawable.marcador);
			initializeIfNecessaryInfoOverlayAndMapItemizedOverlay(vIcon);
		}		
	}

	public void loadPrincipalContainerMarcadorMapa(ContainerMarcadorMapa pContainerMarcadorMapa){

		OverlayItem vOverlayItem =  new OverlayItem(
				pContainerMarcadorMapa.__geoPoint,
				pContainerMarcadorMapa.__tituloBalao,
				pContainerMarcadorMapa.__textoBalao);
		
		Integer vIndex = adicionarMarcador(pContainerMarcadorMapa.__icone, vOverlayItem);
		if(vIndex != null)
			pContainerMarcadorMapa.setIndex(vIndex);

		irParaPonto(pContainerMarcadorMapa.__geoPoint);
	}

	public void loadContainerMarcadorMapa(ContainerMarcadorMapa pContainerMarcadorMapa){

		OverlayItem vOverlayItem =  new OverlayItem(
				pContainerMarcadorMapa.__geoPoint,
				pContainerMarcadorMapa.__tituloBalao,
				pContainerMarcadorMapa.__textoBalao);
		
		Integer vIndex = adicionarMarcador(pContainerMarcadorMapa.__icone, vOverlayItem);
		if(vIndex != null)
			pContainerMarcadorMapa.setIndex(vIndex);
	}

	public void irParaPonto(GeoPoint p){
		this.mapController.animateTo(p);
		this.mapController.setZoom(16);       
		this.mapView.invalidate();
	}
	

	@Override
	public void finish(){
		
		super.finish();
		
	}


}
