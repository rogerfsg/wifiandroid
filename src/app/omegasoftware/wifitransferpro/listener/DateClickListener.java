package app.omegasoftware.wifitransferpro.listener;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;


public class DateClickListener implements OnClickListener
{		
	Integer __idListContainerDate;
	Activity __activity;
	FactoryDateClickListener __factoryDateClickListener;
	public DateClickListener(
			Activity pActivity, 
			Integer pIdListContainerDate, 
			FactoryDateClickListener pFactoryDateClickListener){
		__idListContainerDate = pIdListContainerDate;
		__factoryDateClickListener = pFactoryDateClickListener;
		__activity = pActivity;
	}
	public void onClick(View v) {
		__factoryDateClickListener.setLastContainerDate(__idListContainerDate);
    	__activity.showDialog(FactoryDateClickListener.DATE_DIALOG_ID);
    }		
	
	public void setDateOfView(int year, int month, int day){
		__factoryDateClickListener.setLastContainerDate(__idListContainerDate);
		__factoryDateClickListener.setDateOfView(year, month, day);
	}
	
}
