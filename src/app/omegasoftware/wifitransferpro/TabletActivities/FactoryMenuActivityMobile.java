package app.omegasoftware.wifitransferpro.TabletActivities;


import android.os.Bundle;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.Adapter.MenuAdapter;
import app.omegasoftware.wifitransferpro.common.activity.OmegaCustomAdapterRegularActivity;

public class FactoryMenuActivityMobile extends OmegaCustomAdapterRegularActivity {
	public static String TAG = "FactoryMenuActivityMobile";
	
	private String idParent = null;

	@Override
	public void initializeComponents() {

		
	}
	@Override
	public void onBackPressed(){
		
		super.finish();
	}
	
	
	@Override
	public void finish(){
		try{
			super.finish();
		}catch(Exception ex){
			
		}
		
	}

	@Override
	protected void buildAdapter() {
		// TODO Auto-generated method stub
		adapter = new MenuAdapter(
				this,
				__nomeTag);
	}

	@Override
	protected void loadCommomParameters(Bundle pParameter) {
		// TODO Auto-generated method stub
		if(pParameter != null){
			if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME))
				__nomeTag = pParameter.getString(OmegaConfiguration.SEARCH_FIELD_NOME);
			if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID))
				idParent = pParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		}
		
		
	}
	
	
}
