package app.omegasoftware.wifitransferpro.database.EXTDAO;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.database.DAO.DAOArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.FilesZipper;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.HelperZip;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.NanoHTTPD;
import app.omegasoftware.wifitransferpro.http.pages.ContainerMultiple;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;
import app.omegasoftware.wifitransferpro.service.ServiceSearchFile;

public class EXTDAOArquivo extends DAOArquivo{

	public static final String NAME = "arquivo";
	public static String ID = "id";

	public static String NOME = "nome";
	public static String PATH = "path";
	public static String TIPO_ARQUIVO_ID_INT = "tipo_entidade_id_INT";

	public static String TAMANHO_BYTES_INT = "latitude_INT";

	public static String IS_DIRETORIO_BOOLEAN = "is_parser_check_BOOLEAN";
	public EXTDAOArquivo(Database p_database) {
		super(NAME, p_database);

		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));

		super.addAttribute(new Attribute(NOME, "nome", true, Attribute.SQL_TYPE.TEXT, false, null, false, null, 255));
		super.addAttribute(getNewAtributoNormalizado(NOME));
		super.addAttribute(new Attribute(PATH, "path", true, Attribute.SQL_TYPE.TEXT, false, null, false, null, 255));
		super.addAttribute(new Attribute(TAMANHO_BYTES_INT, "tamanho em bytes", true, Attribute.SQL_TYPE.LONG, true, null, false, null, 11));
		super.addAttribute(new Attribute(IS_DIRETORIO_BOOLEAN, "� diret�rio", true, Attribute.SQL_TYPE.LONG, false, null, false, null, 1));

	}


	public static long DISTANCIA_MINIMA_METROS = 1000;

	public static double getDistanciaMinimaMetros(Context pContext){
		return DISTANCIA_MINIMA_METROS;
	}

	public static boolean isMile(Context pContext){
		return true;
	}
	@Override
	public void populate() {



	}

	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAOArquivo(this.getDatabase());
	}

	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId, String pOldId) {

	}

	private ContainerResponse adicionarMusicaNaPlaylist(HelperHttp pHelper, String vPath, String vPlaylist, String vNome){
		

		boolean vValidade = true;
		String vMensagemRetorno ;
		if(vPath == null || vNome == null || vNome.length() == 0 ){
			vValidade = false;
			vMensagemRetorno = pHelper.context.getString(R.string.nome_vazio);
		}else {

			Database db = new DatabaseWifiTransfer(pHelper.context);
			EXTDAOArquivoPlaylist vObj = new EXTDAOArquivoPlaylist(db);
			
			vObj.setAttrStrValue(EXTDAOArquivoPlaylist.PLAYLIST_ID_INT, vPlaylist);
			vObj.setAttrStrValue(EXTDAOArquivoPlaylist.PATH, vPath + "/" + vNome );
			long vId = vObj.insert();
			if(vId > 0 ){
				vValidade = true;
				vMensagemRetorno = pHelper.context.getString(R.string.musica_inserida_com_sucesso);
			}
			else{
				vMensagemRetorno = pHelper.context.getString(R.string.musica_duplicada);
				vValidade = false;
			}
		}
		StringBuilder vBuilder = new StringBuilder();

		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vValidade)
		vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_REDIRECT, NanoHTTPD.MIME_HTML, vBuilder);
	}

	
	public ContainerResponse adicionarMusicaNaPlaylist(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vPlaylist = pHelper.GET("playlist");
		String vPath = HelperFile.getNomeSemBarra( pHelper.GET("path"));
		String vNome = pHelper.GET("nome");
		
		return adicionarMusicaNaPlaylist(pHelper, vPath, vPlaylist, vNome);
	}
	

	public  ContainerResponse multipleAdicionarMusicaNaPlaylist(HelperHttp pHelper){
		setByPost(pHelper, "1");
		
		String vPlaylist = pHelper.GET("playlist");
		String vPath = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.PATH));

		ContainerMultiple vContainerMultiple = new ContainerMultiple();
		vContainerMultiple.setByPost(pHelper);

		String vVetorNomeArquivo[] = vContainerMultiple.getVetorCheckBox();
		if(vVetorNomeArquivo != null && vVetorNomeArquivo.length > 0 )
			for(int i = 0; i < vVetorNomeArquivo.length; i++){
				if(vVetorNomeArquivo[i] != null && vVetorNomeArquivo[i].length() > 0)
					adicionarMusicaNaPlaylist(pHelper, vPath, vPlaylist, vVetorNomeArquivo[i]);
						
			}

		String vMensagemRetorno = pHelper.context.getString(R.string.musicas_inseridas_na_playlist);
		StringBuilder vBuilder = new StringBuilder(); 
		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);
	}
	
	@Override
	public ContainerResponse actionRemove(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vPath = getStrValueOfAttribute(EXTDAOArquivo.PATH);
		String vNome = getStrValueOfAttribute(EXTDAOArquivo.NOME);

		String mensagemSucesso = pHelper.context.getString(R.string.arquivo_removido_com_sucesso);

		if(vNome != null && vNome.length() > 0 &&
				vPath != null && vPath.length() > 0){
			File vArquivo = new File(vPath, vNome);
			if(vArquivo.exists()){
				if(vArquivo.isDirectory()){
					HelperFile.deleteDirectory(vArquivo);
				}else vArquivo.delete();
			}
		}

		String urlSuccess = pHelper.getUrlAction("list_arquivo", "");
		ContainerResponse vContainer = new ContainerResponse(pHelper, NanoHTTPD.MIME_HTML, NanoHTTPD.HTTP_REDIRECT, new StringBuilder());
		vContainer.addHeader("location", urlSuccess + "&" +EXTDAOArquivo.PATH + "=" + vPath + "&msgSucesso=" + mensagemSucesso);
		return vContainer;
	}

public  ContainerResponse removeLink(HelperHttp pHelper){
		
		String vArquivoPlaylist = pHelper.GET("link");
		return removeLink(pHelper, vArquivoPlaylist);
	}
	
	private ContainerResponse removeLink(HelperHttp pHelper, String pLink){
		
		ContainerMensagem vContainer;
		Database db = new DatabaseWifiTransfer(pHelper.context);
		
		EXTDAOLink vObjLink = new EXTDAOLink(db);
		
		vObjLink.remove(pLink, false);
		
		vContainer = new ContainerMensagem(true, pHelper.context.getString(R.string.link_removido_com_sucesso));
		db.close();

		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vContainer.validade)
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		
		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_REDIRECT, NanoHTTPD.MIME_HTML, vBuilder);
	}
	
	private ContainerResponse removerArquivoPlaylist(HelperHttp pHelper, String vArquivoPlaylist ){
		
		ContainerMensagem vContainer;
		Database db = new DatabaseWifiTransfer(pHelper.context);
		
		EXTDAOArquivoPlaylist vObjArquivoPlaylist = new EXTDAOArquivoPlaylist(db);
		
		vObjArquivoPlaylist.remove(vArquivoPlaylist, false);
		
		vContainer = new ContainerMensagem(true, pHelper.context.getString(R.string.musica_removida_com_sucesso));
		db.close();

		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vContainer.validade)
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		
		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_REDIRECT, NanoHTTPD.MIME_HTML, vBuilder);
	}
	
	public  ContainerResponse removerArquivoPlaylist(HelperHttp pHelper){
		
		String vArquivoPlaylist = pHelper.GET("arquivo_playlist");
		return removerArquivoPlaylist(pHelper, vArquivoPlaylist);
	}
	
	public  ContainerResponse multipleRemoverArquivoPlaylist(HelperHttp pHelper){

		ContainerMultiple vContainerMultiple = new ContainerMultiple("checkbox_playlist");
		vContainerMultiple.setByPost(pHelper);

		String vVetorNomeArquivo[] = vContainerMultiple.getVetorCheckBox();
		if(vVetorNomeArquivo != null && vVetorNomeArquivo.length > 0 )
			for(int i = 0; i < vVetorNomeArquivo.length; i++){
				if(vVetorNomeArquivo[i] != null && vVetorNomeArquivo[i].length() > 0)
					removerArquivoPlaylist(pHelper, vVetorNomeArquivo[i]);	
			}

		String vMensagemRetorno = pHelper.context.getString(R.string.musicas_removidas_na_playlist);
		StringBuilder vBuilder = new StringBuilder(); 
		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);
	}
	
	public  ContainerResponse removerPlaylist(HelperHttp pHelper){
		
		String vPlaylist = pHelper.GET("playlist");
		
		ContainerMensagem vContainer;
		Database db = new DatabaseWifiTransfer(pHelper.context);
		
		EXTDAOPlaylist vObj = new EXTDAOPlaylist(db);
		vObj.setAttrStrValue(EXTDAOPlaylist.ID, vPlaylist);
		
		vObj.remove(false);
		vContainer = new ContainerMensagem(true, pHelper.context.getString(R.string.arquivo_removido_com_sucesso));
		db.close();

		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vContainer.validade)
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		
		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_REDIRECT, NanoHTTPD.MIME_HTML, vBuilder);
	}
	
	
	public  ContainerResponse removerArquivoUpload(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vPath = getStrValueOfAttribute(EXTDAOArquivo.PATH);
		String vNome = getStrValueOfAttribute("nome_arquivo");
		ContainerMensagem vContainer;
		if(vPath != null && vNome != null && vPath.length() > 0 && vNome.length() > 0 ){
			File vFile = new File(vPath, vNome);
			if(vFile.exists()){
				vFile.delete();
				ContainerTypeFile vContainerTypeFile = ContainerTypeFile.getContainerTypeFile(vFile);
				if(vContainerTypeFile.typeFile == TYPE_FILE.IMAGE){
					Database db = new DatabaseWifiTransfer(pHelper.context);
					db.openIfNecessary();
					EXTDAOImagemArquivo vObjImagemArquivo = new EXTDAOImagemArquivo(db);
					String vIdImagemArquivo = vObjImagemArquivo.getIdOfPath(vFile.getAbsolutePath());
					if(vIdImagemArquivo != null)	
					vObjImagemArquivo.remove(vIdImagemArquivo, false);
					db.close();
				}
				vContainer = new ContainerMensagem(true, "Arquivo removido com sucesso.");
			} else 
				vContainer = new ContainerMensagem(false, "Arquivo inexistente.");
		} else vContainer = new ContainerMensagem(false, "Parametros invalidos.");



		return new ContainerResponse(pHelper, new StringBuilder(vContainer.mensagem));

	}
	
	public  ContainerResponse adicionarPlaylist(HelperHttp pHelper){
		setByPost(pHelper, "1");
		String vNovoNome = pHelper.POST("novo_nome");
		String vPath = getStrValueOfAttribute(EXTDAOArquivo.PATH);
		String vNome = getStrValueOfAttribute(EXTDAOArquivo.NOME);
		String vMensagemRetorno = "";
		boolean vValidade = false;

		
		if(vNovoNome != null && vNovoNome.length() > 0 ){
			Database db = new DatabaseWifiTransfer(pHelper.context);
			EXTDAOPlaylist vObjPlaylist = new EXTDAOPlaylist(db);
			vObjPlaylist.setAttrStrValue(EXTDAOPlaylist.NOME, vNovoNome);
			Table vTupla = vObjPlaylist.getFirstTupla();
			if(vTupla != null){
				vMensagemRetorno =  pHelper.context.getString(R.string.playlist_duplicada);
			} else{
				long vId = vObjPlaylist.insert();
				if(vId > 0 ){
					vMensagemRetorno =  pHelper.context.getString(R.string.playlist_adicionada_com_sucesso);
					vValidade = true;
				}
			}
			db.close();
			
		} else{
			vMensagemRetorno = pHelper.context.getString(R.string.nome_vazio);
		}
//TODO atualizar id da playlist		
		StringBuilder vBuilder = new StringBuilder();

		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vValidade)
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_REDIRECT, NanoHTTPD.MIME_HTML, vBuilder);

	}
	
	
	
	public  ContainerResponse adicionarLink(HelperHttp pHelper){
		
		String vURI = pHelper.POST("uri");
		String vNovoNome = pHelper.POST("novo_nome");
		
		
		boolean vValidade = false;
		String vMensagemRetorno;
		if(vURI != null && vURI.length() > 0 ){
			Database db = new DatabaseWifiTransfer(pHelper.context);
			EXTDAOLink vObjLink = new EXTDAOLink(db);
			vObjLink.setAttrStrValue(EXTDAOLink.URL, vURI);
			EXTDAOLink.TYPE_ICONE vType = EXTDAOLink.getTypeOfURL(vURI);
			String vPathImagem = EXTDAOLink.getPathIcone(vType);
			vObjLink.setAttrStrValue(EXTDAOLink.PATH_IMAGEM, vPathImagem);
			
			if(vURI != null && vURI.length() > 0 ){
				int vFirstIndex = vURI.indexOf("index.php5");
				if(vFirstIndex >= 0){
					vURI = vURI.substring(vFirstIndex, vURI.length());
				}
			}
			vObjLink.setAttrStrValue(EXTDAOLink.URL, vURI);
			vObjLink.setAttrStrValue(EXTDAOLink.NOME, vNovoNome);
			
			vObjLink.insert(false);
			db.close();
			vMensagemRetorno = pHelper.context.getString(R.string.link_criado_com_sucesso);
			vValidade = true;
		} else{
			vMensagemRetorno = pHelper.context.getString(R.string.nome_vazio);
		}

		StringBuilder vBuilder = new StringBuilder();

		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vValidade)
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_REDIRECT, NanoHTTPD.MIME_HTML, vBuilder);

	}



	public static String[] upload(){
		//		String vToken = "<input type="file" name="sdfoansofd" >";
		return null;
	}

	public static ContainerResponse uploadArquivo(HelperHttp pHelper){

		//		Download objDownload = new Download("emails.csv");
		//	    return objDownload.ds_download(stringRetorno);
		return null;
	}

	public static ContainerResponse downloadArquivo(HelperHttp pHelper){

		//		Download objDownload = new Download("emails.csv");
		//	    return objDownload->df_download(stringRetorno);
		return null;
	}

	public  ContainerResponse multipleCopiarArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vNovoPath = HelperFile.getNomeSemBarra(pHelper.POST("novo_path"));
		String vPath = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.PATH));
		String vNome = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.NOME));

		ContainerMultiple vContainerMultiple = new ContainerMultiple();
		vContainerMultiple.setByPost(pHelper);

		String vVetorNomeArquivo[] = vContainerMultiple.getVetorCheckBox();
		if(vVetorNomeArquivo != null && vVetorNomeArquivo.length > 0 )
			for(int i = 0; i < vVetorNomeArquivo.length; i++){
				if(vVetorNomeArquivo[i] != null && vVetorNomeArquivo[i].length() > 0)
					copiarArquivo(pHelper, vPath, vVetorNomeArquivo[i], vNovoPath, vVetorNomeArquivo[i], true);	
			}

		String vMensagemRetorno = pHelper.context.getString(R.string.arquivo_copiado_com_sucesso);
		StringBuilder vBuilder = new StringBuilder(); 
		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);
	}

	public  ContainerResponse pesquisaArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vPath = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.PATH));
		vPath = (vPath == null || vPath.length() == 0 ) ? vPath = "/" : vPath;
		String vNome = pHelper.POST( "pesquisa_nome");
		String vExtension = pHelper.POST("pesquisa_extensao");

		String vVetorSearchExtension[] = vExtension.split("[ ]");
		String vVetorSearchNome[] = vNome.split("[ ]");
		ArrayList<String> vListNome = new ArrayList<String>();
		ArrayList<String> vListExtension = new ArrayList<String>();

		for (String vToken : vVetorSearchExtension) {
			if(vToken != null && vToken.length() > 0 ){
				String vExtensao = vToken.replace(".","");
				vListExtension.add(vExtensao);
			}
		}

		for (String vToken : vVetorSearchNome) {
			if(vToken != null && vToken.length() > 0 )
				vListNome.add(vToken);
		}
		String vVetorNome[] = new String[vListNome.size()];
		String vVetorExtension[] = new String[vListExtension.size()];

		vListNome.toArray(vVetorNome);
		vListExtension.toArray(vVetorExtension);
		
		DatabaseWifiTransfer db = new DatabaseWifiTransfer(pHelper.context); 
		EXTDAOPesquisa vObjPesquisa = new EXTDAOPesquisa(db);
		vObjPesquisa.setAttrStrValue(EXTDAOPesquisa.IS_RUNNING_BOOLEAN, "1");
		vObjPesquisa.setAttrStrValue(EXTDAOPesquisa.PATH_DIRETORIO, vPath);
		vObjPesquisa.setAttrStrValue(EXTDAOPesquisa.VETOR_EXTENSAO, HelperString.getStrSeparateByDelimiterColumn(vVetorSearchExtension, " "));
		vObjPesquisa.setAttrStrValue(EXTDAOPesquisa.VETOR_NOME, HelperString.getStrSeparateByDelimiterColumn(vVetorSearchNome, " "));
		long vId = vObjPesquisa.insert(false);
		String vIdPesquisa = String.valueOf(vId);
		
		db.close();
		
		
		Intent serviceIntent = new Intent(pHelper.context, ServiceSearchFile.class);
		serviceIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VETOR_NOME, vVetorNome);
		serviceIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VETOR_EXTENSAO, vVetorExtension);
		serviceIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_PATH, vPath);
		serviceIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, vIdPesquisa);
		
		pHelper.context.startService(serviceIntent);

		
		String vMensagemRetorno = pHelper.context.getString(R.string.arquivo_removido_com_sucesso);
		StringBuilder vBuilder = new StringBuilder(); 
		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		
		ContainerResponse vContainer = new ContainerResponse(pHelper, NanoHTTPD.MIME_HTML, NanoHTTPD.HTTP_REDIRECT, new StringBuilder());
		vContainer.addHeader("location", "index.php5?tipo=lists&page=resultado_pesquisa&path="+ vPath+"&pesquisa=" + vIdPesquisa);
		return vContainer;
		
	}
	
	public  ContainerResponse multipleRemoverArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vPath = getStrValueOfAttribute(EXTDAOArquivo.PATH);
		ContainerMultiple vContainerMultiple = new ContainerMultiple();
		vContainerMultiple.setByPost(pHelper);

		String vVetorNomeArquivo[] = vContainerMultiple.getVetorCheckBox();
		if(vVetorNomeArquivo != null && vVetorNomeArquivo.length > 0 )
			for(int i = 0; i < vVetorNomeArquivo.length; i++){
				if(vVetorNomeArquivo[i] != null && vVetorNomeArquivo[i].length() > 0){
					File vArquivo = new File(vPath, vVetorNomeArquivo[i]);
					if(vArquivo.exists()){
						if(vArquivo.isDirectory()){
							HelperFile.deleteDirectory(vArquivo);
						}else vArquivo.delete();
					}
				}
			}

		String vMensagemRetorno = pHelper.context.getString(R.string.arquivo_removido_com_sucesso);
		StringBuilder vBuilder = new StringBuilder(); 
		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);
	}

	public  ContainerResponse multipleMoverArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vNovoPath = HelperFile.getNomeSemBarra(pHelper.POST("novo_path"));
		String vPath = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.PATH));
		String vNome = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.NOME));

		ContainerMultiple vContainerMultiple = new ContainerMultiple();
		vContainerMultiple.setByPost(pHelper);

		String vVetorNomeArquivoToMove[] = vContainerMultiple.getVetorCheckBox();
		if(vVetorNomeArquivoToMove != null && vVetorNomeArquivoToMove.length > 0 )
			for(int i = 0; i < vVetorNomeArquivoToMove.length; i++){
				if(vVetorNomeArquivoToMove[i] != null && vVetorNomeArquivoToMove[i].length() > 0)
					moveArquivo(pHelper, vPath, vVetorNomeArquivoToMove[i], vNovoPath);	
			}

		String vMensagemRetorno = pHelper.context.getString(R.string.arquivo_movido_com_sucesso);
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append( HelperHttp.imprimirCabecalhoParaFormatarAction());
		vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);
	}

	public  ContainerResponse multipleZipArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vNovoNome = HelperFile.getNomeSemBarra(pHelper.POST("novo_nome"));
		String vPath = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.PATH));

		ContainerMultiple vContainerMultiple = new ContainerMultiple();
		vContainerMultiple.setByPost(pHelper);

		String vVetorNomeArquivoToMove[] = vContainerMultiple.getVetorCheckBox();
		if(vVetorNomeArquivoToMove != null && vVetorNomeArquivoToMove.length > 0 ){
			ArrayList<File> vListFile = new ArrayList<File>();

			for(int i = 0; i < vVetorNomeArquivoToMove.length; i++){

				if(vVetorNomeArquivoToMove[i] != null && vVetorNomeArquivoToMove[i].length() > 0){
					File vArquivo = new File(vPath, vVetorNomeArquivoToMove[i]);
					if(vArquivo.exists())
						vListFile.add(vArquivo);
				}
			}
			File vVetorFile[] = new File[vListFile.size()];
			vListFile.toArray(vVetorFile);
			zipVetorArquivo(pHelper, vVetorFile, vPath, vNovoNome);
		}


		String vMensagemRetorno = pHelper.context.getString(R.string.compreassao_realizada_com_sucesso);
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append( HelperHttp.imprimirCabecalhoParaFormatarAction());
		vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);
	}

	public  ContainerResponse uploadListaArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vNovoPath = HelperFile.getNomeSemBarra(pHelper.POST("novo_path"));
		String vPath = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.PATH));
		String vNome = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.NOME));

		ContainerMultiple vContainerMultiple = new ContainerMultiple();
		vContainerMultiple.setByPost(pHelper);

		String vVetorNomeArquivoToMove[] = vContainerMultiple.getVetorCheckBox();
		for(int i = 0; i < vVetorNomeArquivoToMove.length; i++){
			ContainerMensagem vContainer = moveArquivo(pHelper, vPath, vVetorNomeArquivoToMove[i], vNovoPath);	
		}

		String vMensagemRetorno = pHelper.context.getString(R.string.arquivo_movido_com_sucesso);
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append( HelperHttp.imprimirCabecalhoParaFormatarAction());
		vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);
	}


	public  ContainerResponse renomearArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");
		String vNovoNome = pHelper.POST("novo_nome");
		String vPath = getStrValueOfAttribute(EXTDAOArquivo.PATH);
		String vNome = getStrValueOfAttribute(EXTDAOArquivo.NOME);
		String vMensagemRetorno = "";
		boolean vValidade = false;

		if(vNovoNome != null && vNovoNome.length() > 0 ){
			File vArquivo = new File(vPath, vNome);
			if(vArquivo.exists()){
				File vNewFile = new File(vPath, vNovoNome);
				if(!vNewFile.exists()){
					vArquivo.renameTo(vNewFile);

					vValidade = true;
					vMensagemRetorno = pHelper.context.getString(R.string.arquivo_renomeado_com_sucesso);
				} else {
					vMensagemRetorno = pHelper.context.getString(R.string.arquivo_nao_renomeado_nome_existente);
				}
			}
		} else{
			vMensagemRetorno = pHelper.context.getString(R.string.nome_vazio);
		}

		StringBuilder vBuilder = new StringBuilder();

		vBuilder.append(HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vValidade)
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_REDIRECT, NanoHTTPD.MIME_HTML, vBuilder);

	}

	@Override
	public ContainerResponse callUserFunc(HelperHttp pHelper, String pNomeFuncao) {
		if(pNomeFuncao.compareTo("multipleRemoverArquivo") == 0)
			return multipleRemoverArquivo(pHelper);
		else if(pNomeFuncao.compareTo("renomearArquivo") == 0 )
			return renomearArquivo(pHelper);
		else if(pNomeFuncao.compareTo("downloadArquivo") == 0 )
			return downloadArquivo(pHelper);
		else if(pNomeFuncao.compareTo("uploadArquivo") == 0 )
			return uploadArquivo(pHelper);
		else if(pNomeFuncao.compareTo("zipArquivo") == 0 )
			return zipArquivo(pHelper);
		else if(pNomeFuncao.compareTo("moverArquivo") == 0 )
			return moverArquivo(pHelper);
		else if(pNomeFuncao.compareTo("multipleMoverArquivo") == 0 )
			return multipleMoverArquivo(pHelper);
		else if(pNomeFuncao.compareTo("multipleCopiarArquivo") == 0 )
			return multipleCopiarArquivo(pHelper);
		else if(pNomeFuncao.compareTo("multipleZipArquivo") == 0 )
			return multipleZipArquivo(pHelper);
		else if(pNomeFuncao.compareTo("copiarArquivo") == 0 )
			return copiarArquivo(pHelper);
		else if(pNomeFuncao.compareTo("acessarDiretorio") == 0 )
			return acessarDiretorio(pHelper);
		else if(pNomeFuncao.compareTo("criarDiretorio") == 0 )
			return criarDiretorio(pHelper);
		else if(pNomeFuncao.compareTo("unzipArquivo") == 0 )
			return unzipArquivo(pHelper);
		else if(pNomeFuncao.compareTo("uploadListaArquivo") == 0 )
			return uploadListaArquivo(pHelper);
		else if(pNomeFuncao.compareTo("removerArquivoUpload") == 0 )
			return removerArquivoUpload(pHelper);
		else if(pNomeFuncao.compareTo("pesquisaArquivo") == 0 )
			return pesquisaArquivo(pHelper);
		else if(pNomeFuncao.compareTo("adicionarPlaylist") == 0 )
			return adicionarPlaylist(pHelper);
		else if(pNomeFuncao.compareTo("removerPlaylist") == 0 )
			return removerPlaylist(pHelper);
		else if(pNomeFuncao.compareTo("adicionarMusicaNaPlaylist") == 0 )
			return adicionarMusicaNaPlaylist(pHelper);
		else if(pNomeFuncao.compareTo("removerArquivoPlaylist") == 0 )
			return removerArquivoPlaylist(pHelper);
		else if(pNomeFuncao.compareTo("multipleAdicionarMusicaNaPlaylist") == 0 )
			return multipleAdicionarMusicaNaPlaylist(pHelper);
		else if(pNomeFuncao.compareTo("multipleRemoverArquivoPlaylist") == 0 )
			return multipleRemoverArquivoPlaylist(pHelper);
		else if(pNomeFuncao.compareTo("adicionarLink") == 0 )
			return adicionarLink(pHelper);
		else if(pNomeFuncao.compareTo("removeLink") == 0 )
			return removeLink(pHelper);
		
		
		// TODO Auto-generated method stub
		return null;
	}

	public  ContainerResponse zipArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");
		String vNovoNome = pHelper.POST("novo_nome");
		String vPath = getStrValueOfAttribute(EXTDAOArquivo.PATH);
		String vNome = getStrValueOfAttribute(EXTDAOArquivo.NOME);

		ContainerMensagem vContainer = zipArquivo(pHelper, vPath, vNome, vNovoNome);
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append( HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vContainer.validade)
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		String vOriginAction = pHelper.POST("origin_action");
		vOriginAction = (vOriginAction == null || vOriginAction.length() == 0 ) ? null : vOriginAction;
		
		if(vOriginAction != null)
			vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer("document.location.href='" + vOriginAction + "'", HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));
		else
			vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));
		
		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);
	}

	private ContainerMensagem zipArquivo(HelperHttp pHelper, String vPath, String vNome, String vNovoNome){
		File vFile = new File(vPath + vNome);
		if(vNovoNome != null && vNovoNome.length() > 0 ){
			File vArquivo = new File(vPath, vNome);
			File vNewFileZip = new File(vPath, vNovoNome + ".zip");
			if(vArquivo.exists() && !vNewFileZip.exists()){

				File vVetorFile[] = null;
				if(vFile.isDirectory()){
					vVetorFile = vFile.listFiles();
				}else{
					vVetorFile = new File[1];
					vVetorFile[0] = vFile;
				}

				FilesZipper vZip = new FilesZipper(vVetorFile, vNewFileZip);
				vZip.zip();
				if(vVetorFile != null && vVetorFile.length > 0 ){
					return new ContainerMensagem(true, pHelper.context.getString(R.string.compreassao_realizada_com_sucesso));
				}
				else{
					return new ContainerMensagem(false, pHelper.context.getString(R.string.diretorio_vazio));
				}
			}else{
				return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_arquivo_existente));
			}
		} else{
			return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_vazio));
		}
	}

	private ContainerMensagem zipVetorArquivo(HelperHttp pHelper, File pVetorFile[], String vPath, String vNovoNome){

		if(vNovoNome != null && vNovoNome.length() > 0 ){
			File vNewFileZip = new File(vPath, vNovoNome + ".zip");
			if(!vNewFileZip.exists()){
				HelperZip.zipVetorFile(pVetorFile, vNewFileZip);				
				return new ContainerMensagem(true, pHelper.context.getString(R.string.compreassao_realizada_com_sucesso));
			}else{
				return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_arquivo_existente));
			}
		} else{
			return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_vazio));
		}
	}

	public  ContainerResponse criarDiretorio(HelperHttp pHelper){
		setByPost(pHelper, "1");
		String vNovoNome = pHelper.POST("novo_nome");
		String vPath = getStrValueOfAttribute(EXTDAOArquivo.PATH);
		String vOriginAction = pHelper.POST("origin_action");
		vOriginAction = (vOriginAction == null || vOriginAction.length() == 0 ) ? null : vOriginAction;
		
		String vMensagemRetorno = "";
		boolean vValidade = false;

		if(vNovoNome != null && vNovoNome.length() > 0 ){
			File vArquivo = new File(vPath, vNovoNome);
			if(!vArquivo.exists()){
				HelperFile.createDirectory(vPath + vNovoNome);
				vMensagemRetorno = pHelper.context.getString(R.string.diretorio_criado_com_sucesso);
				vValidade = true;
			}else {
				if(vArquivo.isDirectory()){
					vMensagemRetorno = pHelper.context.getString(R.string.nome_diretorio_existente);
					vValidade = false;	
				} else {
					vMensagemRetorno = pHelper.context.getString(R.string.nome_arquivo_existente);
					vValidade = false;
				}
			}

		} else{
			vMensagemRetorno = pHelper.context.getString(R.string.nome_vazio);
			vValidade = false;
		}
		StringBuilder vBuilder = new StringBuilder();		
		vBuilder.append( HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vValidade)
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

//		"http_referer" olhar se no SERVER esta salvar
		 
		if(vOriginAction != null)
			vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer("document.location.href='" + vOriginAction + "'", HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));
		else
			vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));
		
		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);

	}

	public  ContainerResponse acessarDiretorio(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vPath = getStrValueOfAttribute(EXTDAOArquivo.PATH);

		ContainerResponse vContainer = new ContainerResponse(pHelper, NanoHTTPD.MIME_HTML, NanoHTTPD.HTTP_REDIRECT, new StringBuilder());
		//		vContainer.addHeader("location", IndexServer.getRootAddress() + "index.php5?tipo=lists&page=arquivo&path="+ vPath);
		vContainer.addHeader("location", "index.php5?tipo=lists&page=arquivo&path="+ vPath);
		return vContainer;
	}
	class ContainerMensagem{
		public String mensagem;
		public boolean validade;
		public ContainerMensagem(boolean pValidade, String pMensagem){
			validade = pValidade;
			mensagem= pMensagem;
		}
	}

	
	
	private ContainerMensagem moveArquivo(HelperHttp pHelper, String vPath, String vNome, String vNovoPath){
		if(vNovoPath != null && vNovoPath.length() > 0 ){
			File vArquivo = new File(vPath, vNome);
			
			File vDirectory = new File(vNovoPath); 
			if(!vDirectory.exists())
				vDirectory.mkdirs();
			
			File vNewArquivo = new File(vNovoPath, vNome);
			if(!vNewArquivo.exists()){
				if(vArquivo.isDirectory()){
					vArquivo.renameTo(vNewArquivo);
					//					HelperFile.copyAllDirectory(new File(vPath, vNome), new File(vNovoPath , vNome));
					//					vArquivo.delete();
					return new ContainerMensagem(true, pHelper.context.getString(R.string.diretorio_criado_com_sucesso));
				} else{
					vArquivo.renameTo(vNewArquivo);
					//					HelperFile.copyFile(new File(vPath, vNome), new File(vNovoPath , vNome));
					//					vArquivo.delete();
					return new ContainerMensagem(true, pHelper.context.getString(R.string.arquivo_movido_com_sucesso));
				}
			}else {
				if(vArquivo.isDirectory()){
					return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_diretorio_existente));

				} else {
					return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_arquivo_existente));
				}
			}

		} else{
			return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_vazio));
		}
	}

	public  ContainerResponse moverArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");

		String vNovoPath = HelperFile.getNomeSemBarra(pHelper.POST("novo_path"));
		
		
		String vPath = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.PATH));
		String vNome = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.NOME));

		ContainerMensagem vContainer = moveArquivo(pHelper, vPath, vNome, vNovoPath);

		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append( HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vContainer.validade)
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);
	}

	private ContainerMensagem copiarArquivo(HelperHttp pHelper, String vPath, String vNome, String vNovoPath, String vNovoNome, boolean pIsToCreateNewName){
		if(vNovoNome != null && vNovoNome.length() > 0 && vNovoPath != null && vNovoPath.length() > 0 ){
			File vArquivo = new File(vPath, vNome);
			
			File vDirectory = new File(vNovoPath); 
			if(!vDirectory.exists())
				vDirectory.mkdirs();
			
			File vNewArquivo = new File(vNovoPath, vNovoNome);
			
			if(!vNewArquivo.exists() && pIsToCreateNewName){
				int i = 0;
				while(true){
					vNewArquivo = new File(vNovoPath, vNovoNome + "_" + String.valueOf(i));
					if(!vNewArquivo.exists())
						break;
					i+=1;
				}
			}

			if(!vNewArquivo.exists()){
				if(vArquivo.isDirectory()){
					HelperFile.copyAllDirectory(new File(vPath, vNome), new File(vNovoPath , vNovoNome));
					return new ContainerMensagem(true, pHelper.context.getString(R.string.diretorio_criado_com_sucesso));
				} else{
					HelperFile.copyFile(new File(vPath, vNome), new File(vNovoPath , vNovoNome));
					return new ContainerMensagem(true, pHelper.context.getString(R.string.arquivo_criado_com_sucesso));
				}
			}else {
				if(vArquivo.isDirectory()){
					return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_diretorio_existente));
				} else {
					return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_arquivo_existente));
				}
			}

		} else{
			return new ContainerMensagem(false, pHelper.context.getString(R.string.nome_vazio));
		}
	}

	public  ContainerResponse copiarArquivo(HelperHttp pHelper){
		setByPost(pHelper, "1");
		String vNovoNome = HelperFile.getNomeSemBarra( pHelper.POST("novo_nome"));
		String vNovoPath = HelperFile.getNomeSemBarra(pHelper.POST("novo_path"));
		String vPath = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.PATH));
		String vNome = HelperFile.getNomeSemBarra(getStrValueOfAttribute(EXTDAOArquivo.NOME));

		ContainerMensagem vContainer = copiarArquivo(pHelper, vPath, vNome, vNovoPath, vNovoNome, false);
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append( HelperHttp.imprimirCabecalhoParaFormatarAction());
		if(vContainer.validade)
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vContainer.mensagem, TYPE_MENSAGEM.MENSAGEM_ERRO, null));

		String vOriginAction = pHelper.POST("origin_action");
		vOriginAction = (vOriginAction == null || vOriginAction.length() == 0 ) ? null : vOriginAction;
		 
		if(vOriginAction != null)
			vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer("document.location.href='" + vOriginAction + "'", HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));
		else
			vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));
		

		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);

	}
	
	public  ContainerResponse unzipArquivo(HelperHttp pHelper){
		
		String vNovoNome = HelperFile.getNomeSemBarra( pHelper.POST("novo_nome"));
		String vNovoPath = HelperFile.getNomeSemBarra(pHelper.POST("novo_path"));
		String vPath = HelperFile.getNomeSemBarra(pHelper.POST("path"));
		String vNome = HelperFile.getNomeSemBarra(pHelper.POST("nome"));
		String vMensagemRetorno = "";
		boolean vValidade = false;

		if(vNovoNome != null && vNovoNome.length() > 0 && vNovoPath != null && vNovoPath.length() > 0 ){
			File vArquivo = new File(vPath, vNome);
//			File vDiretorioDestino = new File(vNovoPath);

			if(vArquivo.exists()){

				boolean vIsExtractHere = pHelper.POST("is_extract_here")  == null || pHelper.POST("is_extract_here").compareTo("0") == 0 ? false : true;

				try {
					File vDirectory = null;
					if(vIsExtractHere){
						vDirectory = new File(vNovoPath);
					} else{
						vDirectory = new File(vNovoPath, vNovoNome);
					}

					if(!vDirectory.exists())
						vDirectory.mkdirs();

					if(vDirectory.exists())
						HelperZip.unzip(vArquivo, vDirectory);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


				vMensagemRetorno = pHelper.context.getString(R.string.arquivo_extraido_com_sucesso);
				vValidade = true;

			}else {
				if(vArquivo.isDirectory()){
					vMensagemRetorno = pHelper.context.getString(R.string.nome_diretorio_existente);
					vValidade = false;	
				} else {
					vMensagemRetorno = pHelper.context.getString(R.string.nome_arquivo_existente);
					vValidade = false;
				}
			}

		} else{
			vMensagemRetorno = pHelper.context.getString(R.string.nome_vazio);
			vValidade = false;
		}
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append( HelperHttp.imprimirCabecalhoParaFormatarAction());
		
		if(vValidade)
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_OK, null));
		else
			vBuilder.append(HelperHttp.imprimirMensagem(vMensagemRetorno, TYPE_MENSAGEM.MENSAGEM_ERRO, null));
		
		vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, false));
		
		return new ContainerResponse(pHelper, NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, vBuilder);

	}
}
