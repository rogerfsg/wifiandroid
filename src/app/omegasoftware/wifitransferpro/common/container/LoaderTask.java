package app.omegasoftware.wifitransferpro.common.container;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import app.omegasoftware.wifitransferpro.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;

public class LoaderTask extends AsyncTask<String, Integer, Boolean>
{
	public static String TAG = "LoaderTask";
	FactoryMenuActivityMobile __activity;
	
	Class<?> __class;
	String __tag;
	Bundle __bundle;
	
	public LoaderTask(FactoryMenuActivityMobile pActivity, Class<?> pClass){
		__activity = pActivity;
		__class = pClass;
	}
	
	public LoaderTask(FactoryMenuActivityMobile pActivity, Class<?> pClass, String pTag){
		__activity = pActivity;
		__class = pClass;
		__tag = pTag;
	}
	
	public LoaderTask(FactoryMenuActivityMobile pActivity, Class<?> pClass, String pTag, Bundle pBundle){
		__activity = pActivity;
		__class = pClass;
		__tag = pTag;
		__bundle = pBundle;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		__activity.showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
	}
	Intent intent = null;
	@Override
	protected Boolean doInBackground(String... params) {
		try{
			intent = new Intent(
					__activity.getApplicationContext(), 
					__class);
			
			if(__tag != null)
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME, __tag);
			if(__bundle != null)
				intent.putExtras(__bundle);
			__activity.startActivity(intent);
		}
		catch(Exception e)
		{
			//if(e != null)
				//Log.e(TAG, e.getMessage());			
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		__activity.dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);
		
		
	}

}