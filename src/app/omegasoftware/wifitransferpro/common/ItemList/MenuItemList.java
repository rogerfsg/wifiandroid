package app.omegasoftware.wifitransferpro.common.ItemList;

import android.view.View.OnClickListener;

public class MenuItemList extends CustomItemList {
	
	
	public static final String DRAWABLE = "drawable";
	public static final String TEXTVIEW = "textview";
	
	OnClickListener __onClickListener;
	
	public MenuItemList(
			String pId,
			String pDrawable, 
			String pStrTextView,
			OnClickListener pOnClickListener) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(DRAWABLE, pDrawable),
			new ContainerItem(TEXTVIEW, pStrTextView)});
		__onClickListener = pOnClickListener;
		
	}
	public OnClickListener getClickListener(){
		return __onClickListener;
	}
}
