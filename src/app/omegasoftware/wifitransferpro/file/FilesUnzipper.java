package app.omegasoftware.wifitransferpro.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import android.util.Log;

public class FilesUnzipper {

	private static final String TAG = "FilesUnzipper";
	
	private static final int BUFFER = 2048; 
	 
	 private File zipFile = null; 
	
	public FilesUnzipper(File zipFile) {
	 
	  this.zipFile = zipFile;
	}
	
	
	public void unzip(File pDirectory){
		HelperFile.createDirectory(pDirectory, true);
		BufferedOutputStream origin = null; 
		FileInputStream dest = null;
		try {
			dest = new FileInputStream(zipFile);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
	 
	      ZipInputStream out = new ZipInputStream(new BufferedInputStream(dest)); 
	      
	      byte data[] = new byte[BUFFER]; 
	      ZipEntry vEntry = null;
	      try {
			while((vEntry = out.getNextEntry()) != null) {
				  
			    //Log.d(TAG, "zip(): Zipping file " + file.getName()); 
			    FileOutputStream fi = null;
				try {
					if(vEntry.isDirectory())
						HelperFile.createDirectory(new File(pDirectory.getAbsolutePath() , vEntry.getName()), true);
					 File vFileOut = new File(pDirectory.getAbsolutePath() , vEntry.getName());
					if(vFileOut.exists()) {
						continue;
					}
					
					fi = new FileOutputStream(vFileOut);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			    origin = new BufferedOutputStream(fi, BUFFER); 
			    
			    
			    int count; 
			    try {
					while ((count = out.read(data, 0, BUFFER)) != -1) { 
						origin.write(data, 0, count); 
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			    
			    try {
			    	origin.close();
					fi.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	      try {
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      
	}
	
	
}
