package app.omegasoftware.wifitransferpro.http;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;

public class HelperHotSpot {

	Context context;
	WifiManager mainWifi;
	WifiReceiver receiverWifi;
	List<ScanResult> wifiList;
	StringBuilder sb = new StringBuilder();
	/** Called when the activity is first created. */


	public HelperHotSpot(Context pContext) {

		context = pContext;
		mainWifi = (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);


		mainWifi = (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);
		receiverWifi = new WifiReceiver();
		if(!mainWifi.isWifiEnabled()){
			mainWifi.setWifiEnabled(true);
		}
		pContext.registerReceiver(receiverWifi, new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		mainWifi.startScan();

	}


	public void close(){
		try{
			context.unregisterReceiver(receiverWifi);	
		} catch(Exception ex){
			
		}
		
	}

	class WifiReceiver extends BroadcastReceiver {
		public void onReceive(Context c, Intent intent) {
			StringBuilder sb = new StringBuilder();
			wifiList = mainWifi.getScanResults();
			for(int i = 0; i < wifiList.size(); i++){
				sb.append(new Integer(i+1).toString() + ".");
				sb.append((wifiList.get(i)).toString());
				sb.append("n");
			}       
		}
	}




	public static String getLocalIpAddressString() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					Log.d("ip", inetAddress.getHostAddress().toString());
//					if (!inetAddress.isLoopbackAddress()) {
//						return inetAddress.getHostAddress().toString();
//					}
				}
			}
		} catch (Exception ex) {
			Log.e("IPADDRESS", ex.toString());
		}
		return null;
	}
}

