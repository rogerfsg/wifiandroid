package app.omegasoftware.wifitransferpro.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.wifitransferpro.listener.MapListButtonClickListener;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class AppointmentPlaceMapAdapter extends BaseAdapter {

	private ArrayList<AppointmentPlaceItemList> appointmentPlaces;
	private Activity __activity;
	private Typeface customTypeFace;
	private String __idTupla;
	public AppointmentPlaceMapAdapter(Activity __activity, AppointmentPlaceItemList appointmentPlace, String pIdTupla)
	{
		__idTupla = pIdTupla;
		this.appointmentPlaces = new ArrayList<AppointmentPlaceItemList>() ;
		appointmentPlaces.add(appointmentPlace);
		this.__activity = __activity;
		this.customTypeFace = Typeface.createFromAsset(this.__activity.getAssets(),"trebucbd.ttf");
	}

	public int getCount() {
		return this.appointmentPlaces.size();
	}


	public AppointmentPlaceItemList getItem(int position) {
		return this.appointmentPlaces.get(position);
	}
	public long getItemId(int position) {
		return this.appointmentPlaces.get(position).getId();
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		AppointmentPlaceItemList appointmentPlace = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) __activity.getSystemService(__activity.LAYOUT_INFLATER_SERVICE);

		View layoutView = layoutInflater.inflate(R.layout.appointment_place_item_list_layout, null);

		Button buttonShowOnMap = (Button) layoutView.findViewById(R.id.appointment_place_showmap_button);
		buttonShowOnMap.setOnClickListener(new MapListButtonClickListener(this.getItemId(position), __idTupla, appointmentPlace, __activity));

		//		TextView titleTextView = (TextView) layoutView.findViewById(R.id.appointment_place_title);
		//		titleTextView.setText(String.format(__activity.getResources().getString(R.string.appointent_place_title), position + 1));
		boolean vValidade = false;
		
		boolean vValidadeEndereco = false;
		String vAddress = appointmentPlace.getAddress();
		TextView addressTextView = (TextView) layoutView.findViewById(R.id.appointment_place_address);
		if(vAddress != null && vAddress.length() > 0){
			vValidade = true;
			addressTextView.setText(HelperString.ucFirstForEachToken(vAddress));
			addressTextView.setTypeface(this.customTypeFace);
			vValidadeEndereco = true;
		} else {
			addressTextView.setVisibility(OmegaConfiguration.VISIBILITY_HIDDEN);
		}

		TextView cityTextView = (TextView) layoutView.findViewById(R.id.appointment_place_city);
		String vCity = appointmentPlace.getCity();
		if(vCity != null && vCity.length() > 0){
			vValidade = true;
			cityTextView.setText(HelperString.ucFirstForEachToken(vCity));
			cityTextView.setTypeface(this.customTypeFace);
			vValidadeEndereco = true;
		} else cityTextView.setVisibility(OmegaConfiguration.VISIBILITY_HIDDEN);
		
		TextView ufTextView = (TextView) layoutView.findViewById(R.id.appointment_place_uf);
		String vUf = appointmentPlace.getUf();
		if(vUf != null && vUf.length() > 0){
			vValidade = true;
			vValidadeEndereco = true;
			ufTextView.setText(HelperString.ucFirstForEachToken(vUf));
			ufTextView.setTypeface(this.customTypeFace);
		} else ufTextView.setVisibility(OmegaConfiguration.VISIBILITY_HIDDEN);

		TextView countryTextView = (TextView) layoutView.findViewById(R.id.appointment_place_country);
		String vCountry = appointmentPlace.getCountry();
		if(vCountry != null && vCountry.length() > 0){
			vValidade = true;
			vValidadeEndereco = true;
			countryTextView.setText(HelperString.ucFirstForEachToken(vCountry));
			countryTextView.setTypeface(this.customTypeFace);
		} else countryTextView.setVisibility(OmegaConfiguration.VISIBILITY_HIDDEN);
		
		if(!vValidadeEndereco){
			buttonShowOnMap.setVisibility(OmegaConfiguration.VISIBILITY_HIDDEN);
		}
		
		TextView cepTextView = (TextView) layoutView.findViewById(R.id.appointment_place_cep);
		String vCep = appointmentPlace.getCEPWithStartingString();
		if(vCep != null && vCep.length() > 0){
			vValidade = true;
			cepTextView.setText(HelperString.ucFirstForEachToken(vCep));
			cepTextView.setTypeface(this.customTypeFace);	
		} else cepTextView.setVisibility(OmegaConfiguration.VISIBILITY_HIDDEN);

		LinearLayout emailAndPhoneHolder = (LinearLayout) layoutView.findViewById(R.id.email_and_phone_holder_linearlayout);

		PhoneEmailAdapter phoneAdapter = appointmentPlace.getPhoneAdapter();

		PhoneEmailAdapter emailAdapter = appointmentPlace.getEmailAdapter();

		LinearLayout phonesLayout = null;
		boolean vValidadePhoneOrEmail = false;
		if(phoneAdapter != null){
			for(int i=0;i<phoneAdapter.getCount();i++)
			{
				if(phonesLayout == null){
					phonesLayout = new LinearLayout(this.__activity);
					phonesLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
					phonesLayout.setOrientation(LinearLayout.HORIZONTAL);
				}
				if((i % 2 == 0) && i != 0)
				{	
					emailAndPhoneHolder.addView(phonesLayout);
					phonesLayout = new LinearLayout(this.__activity);
					phonesLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
					phonesLayout.setOrientation(LinearLayout.HORIZONTAL);
				}
				phonesLayout.addView(phoneAdapter.getView(i, null, null));
			}
		}
		
		
		if(phonesLayout != null){
			vValidade = true;
			vValidadePhoneOrEmail = true;
			emailAndPhoneHolder.addView(phonesLayout);
		}

		if(emailAdapter != null)
			for(int i=0;i<emailAdapter.getCount();i++)
			{
				vValidade = true;
				vValidadePhoneOrEmail = true;
				emailAndPhoneHolder.addView(emailAdapter.getView(i, null, null));
			}
		WebBrowserAdapter vBrowser = appointmentPlace.getWebBrowserAdapter();
		if(vBrowser != null){
			vValidade = true;
			vValidadePhoneOrEmail = true;
			if(vBrowser != null)
				for(int i=0;i<vBrowser.getCount();i++)
				{
					vValidade = true;
					vValidadePhoneOrEmail = true;
					emailAndPhoneHolder.addView(vBrowser.getView(i, null, null));
				}
			
		}
		if(!vValidadePhoneOrEmail)
			emailAndPhoneHolder.setVisibility(OmegaConfiguration.VISIBILITY_HIDDEN);
		
		if(! vValidade) return null;
		else return layoutView;

	}




}
