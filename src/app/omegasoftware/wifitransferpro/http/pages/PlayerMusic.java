package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Javascript;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Music;

public class PlayerMusic extends InterfaceHTML{

	public PlayerMusic(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
//		String vStrReturn =  Javascript.importarBibliotecaJavascriptMusicPlayer();

		if(__helper.GET("path_completo") != null){
			Music vDownloadArquivo = new Music(
					__helper,
					__helper.GET("path_completo"));
			return vDownloadArquivo.play();
		}else{
			Music vDownloadArquivo = new Music(
					__helper,
					__helper.GET(EXTDAOArquivo.PATH),
					__helper.GET(EXTDAOArquivo.NOME));
			return vDownloadArquivo.play();	
		} 
	}

}
