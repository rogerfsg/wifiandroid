package app.omegasoftware.wifitransferpro.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.listener.BrowserOpenerListener;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class InformationDialogSistemaWeb {
	Dialog __dialog;
	Activity __activity;
	String __keySharedPreferences;
	int __idDialog;
	public InformationDialogSistemaWeb(Activity pActivity, Dialog pDialog, String pKeySharedPreferences, int pIdDialog){
		__dialog = pDialog;
		__activity = pActivity;
		__keySharedPreferences = pKeySharedPreferences;
		__idDialog = pIdDialog;
	}
	
	public class AlertDialogOnClickListener implements OnClickListener{
		
		public void onClick(View v) {
			CheckBox vCheckBoxNaoExibirAlerta = (CheckBox) __dialog.findViewById(R.id.informacao_uso_list_checkbox);
			if(vCheckBoxNaoExibirAlerta.isChecked()){
				saveDialogInformacao();
			}
			__activity.dismissDialog(__idDialog);
		}
	}
//	<TextView
//    style="@style/H3TextGray"
//    android:layout_marginTop="10dp"
//    android:layout_width="wrap_content"
//    android:layout_height="wrap_content"
//    android:layout_marginLeft="@dimen/margin_left_form_textView"
//    android:layout_marginRight="@dimen/margin_right_form_textView"
//    android:text="@string/informacao_uso_list_detalhes" />
	public Dialog getDialogInformation(int pVetorIdString[]){
		boolean vIsSavedDialogInformacao = getSavedDialogInformacao();
		if(vIsSavedDialogInformacao == false){
			AlertDialogOnClickListener vOnClickListener = new AlertDialogOnClickListener();
			__dialog.setContentView(R.layout.dialog_informacao_sistema_web);				
			Button vOkButton = (Button) __dialog.findViewById(R.id.unimedbh_dialog_ok_button);
			Button vSistemaWebButton = (Button) __dialog.findViewById(R.id.website_sistema_android_button);
					
			vSistemaWebButton.setOnClickListener(new BrowserOpenerListener(__activity.getResources().getString(R.string.sistema_android_site_url), __activity));
			LinearLayout vLayout = (LinearLayout) __dialog.findViewById(R.id.conteudo_dialog_informacao_linearlayout);
			for (int i : pVetorIdString) {
				String vConteudo = __activity.getResources().getString(i);
				LinearLayout vLinearLayoutMensagemDeAlerta = (LinearLayout)__activity.getLayoutInflater().inflate(R.layout.textview_information_dialog, null);
				TextView vTextView = (TextView) vLinearLayoutMensagemDeAlerta.findViewById(R.id.information_dialog_textview);
				vTextView.setText(vConteudo);
				vLayout.addView(vLinearLayoutMensagemDeAlerta);
			}
			
			
			vOkButton.setOnClickListener(vOnClickListener);
			return __dialog;
		}
		else return null;
		
	}
	
	protected Boolean getSavedDialogInformacao()
	{
		SharedPreferences vSavedPreferences = __activity.getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		if(vSavedPreferences.contains(__keySharedPreferences))
			return vSavedPreferences.getBoolean(__keySharedPreferences, false);
		else return false;
	}

	//Save Cidade ID in the preferences
	protected void saveDialogInformacao()
	{

		SharedPreferences vSavedPreferences = __activity.getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putBoolean(__keySharedPreferences, true);
		vEditor.commit();

	}
	
}
