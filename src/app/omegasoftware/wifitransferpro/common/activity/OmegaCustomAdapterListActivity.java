package app.omegasoftware.wifitransferpro.common.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.PrincipalActivity;
import app.omegasoftware.wifitransferpro.TabletActivities.AboutActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.Adapter.CustomAdapter;

public abstract class OmegaCustomAdapterListActivity extends ListActivity {

	private String TAG = "OmegaCustomAdapterListActivity";

	protected CustomAdapter adapter;

	String __nomeTag = null;
	
	
	protected abstract void buildAdapter(boolean pIsAsc);
	protected abstract void loadCommomParameters(Bundle pParameter);

	private Typeface customTypeFace;
	private ToggleButton azOrderByButton;
	private ToggleButton zaOrderByButton;
	private ToggleButton nearMeOrderByButton;
	private short currentOrderByMode = OmegaConfiguration.DEFAULT_ORDER_BY_MODE;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		setContentView(R.layout.list_activity_mobile_layout);
		try{
			new CustomDataLoader(this).execute();
		}
		catch(Exception e)
		{
			Log.e(TAG, e.getMessage());
		}
	}


	protected void loadParameters()	
	{
		Bundle vParameters = getIntent().getExtras();
		if(vParameters != null)
		{
			loadCommomParameters(vParameters);			
		}
	}
	
	public void setTag(String p_tag)
	{
		this.TAG = p_tag;
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;

	}

	public class AZOrderByTypeChangeListener implements OnCheckedChangeListener 
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			if(isChecked)
			{

				zaOrderByButton.setChecked(false);
				zaOrderByButton.setTextColor(R.color.dark_gray);

				azOrderByButton.setTextColor(R.color.white);
				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_A_Z);
				ListUpdate(true);
			}
		}

	}


	private void ListUpdate(boolean pAsc)
	{
		adapter.SortList(pAsc);
		adapter.notifyDataSetChanged();
	}

	public class ZAOrderByTypeChangeListener implements OnCheckedChangeListener 
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			if(isChecked)
			{
				azOrderByButton.setChecked(false);
				azOrderByButton.setTextColor(R.color.dark_gray);

				zaOrderByButton.setTextColor(R.color.white);
				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_Z_A);
				ListUpdate(false);

			}
		}
	}
	public void initializeComponents() {

		((ToggleButton) findViewById(R.id.toogle_a_z_button)).setTypeface(this.customTypeFace);
		((ToggleButton) findViewById(R.id.toogle_z_a_button)).setTypeface(this.customTypeFace);

		//Attach toogle_a_z_button to ZAOrderByTypeChangeListener()
		this.zaOrderByButton = (ToggleButton) findViewById(R.id.toogle_z_a_button);
		this.zaOrderByButton.setOnCheckedChangeListener(new ZAOrderByTypeChangeListener());

		//Attach toogle_z_a_button to AZOrderByTypeChangeListener()
		this.azOrderByButton = (ToggleButton) findViewById(R.id.toogle_a_z_button);
		this.azOrderByButton.setOnCheckedChangeListener(new AZOrderByTypeChangeListener());

		//Sets default search mode
		switch (this.currentOrderByMode) {
		case OmegaConfiguration.ORDER_BY_MODE_A_Z:
			this.azOrderByButton.setChecked(true);
			this.zaOrderByButton.setChecked(false);
			this.nearMeOrderByButton.setChecked(false);
			break;
		case OmegaConfiguration.ORDER_BY_MODE_Z_A:
			this.zaOrderByButton.setChecked(true);
			this.azOrderByButton.setChecked(false);
			this.nearMeOrderByButton.setChecked(false);
			break;
		case OmegaConfiguration.ORDER_BY_MODE_NEAR_ME:
			this.zaOrderByButton.setChecked(false);
			this.azOrderByButton.setChecked(false);
			this.nearMeOrderByButton.setChecked(true);
			break;
		}
		setListAdapter(adapter);
	}

	public void performExecute(){
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;


		if(PrincipalActivity.isTablet)
		{
			switch (item.getItemId()) {
			case R.id.menu_favorito:
				
				break;

			case R.id.menu_sobre:
				intent = new Intent(this, AboutActivityMobile.class);
				break;

			default:
				return super.onOptionsItemSelected(item);

			}
		}
		if(intent != null){
			startActivity(intent);	
		}
		return true;

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if(requestCode == OmegaConfiguration.ACTIVITY_FORM_EDIT){

			try{
				boolean vIsModificated = true;
				Bundle pParameter = intent.getExtras();

				if(pParameter != null)
					if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
						vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
				if(vIsModificated)
					performExecute();

			}catch(Exception ex){
				Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
			}
		} 
	}
	private void setOrderByMode(short p_orderByMode){
		this.currentOrderByMode = p_orderByMode;

	}

	protected Dialog CreateAlertDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
		.setCancelable(false)
		.setNeutralButton(R.string.botao_voltar, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				finish();
			}
		});

		return builder.create();
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.unimedbh_dialog_error_textview);
		Button vOkButton = (Button) vDialog.findViewById(R.id.unimedbh_dialog_ok_button);

		switch (id) {
		case OmegaConfiguration.ON_LOAD_DIALOG:
			vDialog = new ProgressDialog(this);
			((ProgressDialog) vDialog).setMessage(getResources().getString(R.string.string_loading));
			break;			
		case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
			vTextView.setText(getResources().getString(R.string.dialog_noresult_alert));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
					finish();
				}
			});
			break;
		default:
			return null;
		}

		return vDialog;

	}

	public boolean loadData() {
		loadParameters();
		buildAdapter(true);
		if(adapter.getSize() == 0 ) adapter = null;
		return (adapter!=null?true:false);
	}

	private void showDialogNoResultIfNecessary()
	{
		if(adapter!= null)
			if(adapter.getCount() <= 0)
			{
				showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
			}
	}
	@Override
	public void finish(){
		
	
		super.finish();
		
	}
	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean>
	{

		Activity __activity;
		
		public CustomDataLoader(Activity pActivity){
			__activity = pActivity;
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				return loadData();
			}
			catch(Exception e)
			{
				Log.e(TAG, e.getMessage());			
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			if(result)
			{
				initializeComponents();
			}

			try
			{
				dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);	
			}
			catch(Exception e)
			{
			}
			showDialogNoResultIfNecessary();

		}
	}

}
