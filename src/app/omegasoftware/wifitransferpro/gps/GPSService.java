package app.omegasoftware.wifitransferpro.gps;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;

import android.location.LocationListener;
import android.location.LocationManager;

import android.os.IBinder;


public class GPSService  extends Service{

	private static final String TAG = "GPSService";
	
	private GPSServiceLocationListener serviceGPSLocationListener;

	private Timer uiUpdaterTimer = new Timer();
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
		
	@Override
	public void onCreate() {
		super.onCreate();
		

		this.serviceGPSLocationListener = new GPSServiceLocationListener( this);
		

//		sendServiceStateMessage();
		setTimer();
	}

	private void setTimer(){
		
		//Starts ui updater timer
		this.uiUpdaterTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				sendServiceStateMessage();
			}
		},
		GPSConfiguration.INITIAL_DELAY,
		GPSConfiguration.UI_UPDATE_TIMER_INTERVAL);
	}

	private void sendServiceStateMessage()
	{
		Intent updateUIIntent = new Intent(GPSConfiguration.REFRESH_UI_INTENT);
		
		updateUIIntent.putExtra(GPSConfiguration.SS_GPS_STATE, HelperGPS.checkIfGPSIsEnabled(getApplicationContext()));
		updateUIIntent.putExtra(GPSConfiguration.SS_GPS_LOCATION_LATITUDE, this.serviceGPSLocationListener.getGeoPoint().getLatitudeE6());
		updateUIIntent.putExtra(GPSConfiguration.SS_GPS_LOCATION_LONGITUDE, this.serviceGPSLocationListener.getGeoPoint().getLongitudeE6());
		sendBroadcast(updateUIIntent);
	}
	
	public GPSServiceLocationListener getHelperLocationListener(){
		
		return serviceGPSLocationListener;
	}
	
}
