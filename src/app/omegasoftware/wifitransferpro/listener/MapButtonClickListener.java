package app.omegasoftware.wifitransferpro.listener;

import java.util.ArrayList;


import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.wifitransferpro.TabletActivities.ShowAddressOnMapActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.wifitransferpro.common.activity.OmegaMapActivity;

public class MapButtonClickListener implements OnClickListener {

	long addressId;
	ArrayList<String> __arrayListEndereco = new ArrayList<String>();
	public MapButtonClickListener(long p_addressId, AppointmentPlaceItemList appointmentPlace)
	{
		
		this.addressId = p_addressId;
		if(appointmentPlace != null){
			String v_addr =appointmentPlace.getStrAddress() ;
			if(v_addr != null){
				__arrayListEndereco.add(v_addr);	
			}
		}
		
	}
	
	public MapButtonClickListener(String p_adress)
	{	
		
		if(p_adress != null){
			__arrayListEndereco.add(p_adress);
		}
	}
	
	public MapButtonClickListener(long p_addressId)
	{
		
		this.addressId = p_addressId;
			
	}
	
	public MapButtonClickListener(long p_addressId, ArrayList<String> p_arrayListEndereco)
	{
		
		this.addressId = p_addressId;
		if(p_arrayListEndereco != null){	
			__arrayListEndereco = p_arrayListEndereco;
		}
		
	}

	public void onClick(View v) {
		
		
		try{
		Intent intent = new Intent(v.getContext(), ShowAddressOnMapActivityMobile.class); 
		
		intent.putExtra(OmegaConfiguration.MAP_ADDRESS, __arrayListEndereco);
		
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		v.getContext().startActivity(intent);
		} catch(Exception ex){
			//Log.e("onClick", ex.toString());
		}
		
		
	}
	
	
	
}
