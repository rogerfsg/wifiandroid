/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.omegasoftware.wifitransferpro.listener;



import java.util.ArrayList;


import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.date.ContainerClickListenerHour;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TimePicker;

/**
 * Basic example of using Hour and time widgets, including
 * {@link android.app.TimePickerDialog} and {@link android.widget.HourPicker}.
 *
 * Also provides a good example of using {@link Activity#onCreateDialog},
 * {@link Activity#onPrepareDialog} and {@link Activity#showDialog} to have the
 * activity automatically save and restore the state of the dialogs.
 */
public class FactoryHourClickListener {


	Activity __activity;
	ArrayList<ContainerClickListenerHour> __listContainerHour = new ArrayList<ContainerClickListenerHour>();
	ContainerClickListenerHour __lastContainerHour = null;
	static public final int TIME_DIALOG_ID = 109998;

	public FactoryHourClickListener(Activity pActivity){
		__activity = pActivity;    


	}

	public static boolean isHourButtonSet(Context pContext, Button pButtonHour){
    	String vStrSelecionarHour = pContext.getResources().getString(R.string.form_selecionar_hora);
    	String vStrButtonHour = pButtonHour.getText().toString();
    	if(vStrButtonHour.equals("")) return false;
    	if(vStrButtonHour.equals(vStrSelecionarHour)) return false;
    	else return true;
    }
	
	public ContainerClickListenerHour setHourClickListener(Button pButton){
		
		HourClickListener vListener = new HourClickListener(__activity,  this, __listContainerHour.size());
		ContainerClickListenerHour vContainer = new ContainerClickListenerHour(pButton,vListener);
		
		__listContainerHour.add(vContainer);
		pButton.setOnClickListener(vListener);
		return vContainer;
	}

	public void setLastContainerHour(int pIndexContainerHour){
		__lastContainerHour = __listContainerHour.get(pIndexContainerHour);
	}

	public Dialog onCreateDialog(int id) {
		switch (id) {

		case TIME_DIALOG_ID:
			return new TimePickerDialog(
					__activity,
                    mTimeSetListener, 
                    __lastContainerHour.getHora(), 
                    __lastContainerHour.getMinuto(), 
                    false);
			
		}
		return null;
	}


	public void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {

		case TIME_DIALOG_ID:

			((TimePickerDialog) dialog).updateTime(
					__lastContainerHour.getHora(), 
					__lastContainerHour.getMinuto());
			break;
		}
	}    

	public void setDateOfView(int hour, int minute){
		__lastContainerHour.setHora(hour);
		__lastContainerHour.setMinuto( minute);
		__lastContainerHour.setTextView();
    }
	private TimePickerDialog.OnTimeSetListener mTimeSetListener =
			new TimePickerDialog.OnTimeSetListener() {

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			setDateOfView(hourOfDay, minute);
		}
	};
}
