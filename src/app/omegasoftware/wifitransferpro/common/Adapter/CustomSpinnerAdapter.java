package app.omegasoftware.wifitransferpro.common.Adapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;

public class CustomSpinnerAdapter extends ArrayAdapter<String> {

	Typeface customTypeFace;
	Context context;
	ArrayList<String> values;
	ArrayList<String> ids;
	int layout;
	int dropDownLayout;
	int count;
	boolean __isToUpperCase = true;
	public CustomSpinnerAdapter(
			Context context,
			LinkedHashMap<String, String> data,
			int layoutResource,
			int firstEntryResource,
			boolean pIsToUpperCase)
	{
		this(context, data, layoutResource, firstEntryResource);
		__isToUpperCase = pIsToUpperCase;
	}
	public CustomSpinnerAdapter(
			Context context,
			LinkedHashMap<String, String> data,
			int layoutResource,
			int firstEntryResource)
	{
		super(context,layoutResource);
		
		//Salvando o context para utilizar no m�todo getView()
		this.context = context;
		
		//Criando o customTypeFace (define o tipo de letra)
		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
				
		//Salvando o layoutResource para utilizar no m�todo getView()
		this.layout = layoutResource;
		this.dropDownLayout = layoutResource;
		
		this.values = new ArrayList<String>();
		this.ids = new ArrayList<String>();
		
		this.values.add(this.context.getResources().getString(firstEntryResource));
		this.ids.add(OmegaConfiguration.UNEXISTENT_ID_IN_DB);
		
		//As op��es s�o definidas no par�metro dataList
		for(String key : data.keySet())
		{
			this.ids.add((String) key);
			if(__isToUpperCase)
				this.values.add(data.get(((String) key).toUpperCase()));
			else
				this.values.add(data.get(((String) key).toLowerCase()));
		}

	}
	
	
	public CustomSpinnerAdapter(Context context,LinkedHashMap<String, String> data,int layoutResource)
	{
		super(context,layoutResource);
		
		//Salvando o context para utilizar no m�todo getView()
		this.context = context;
		
		//Criando o customTypeFace (define o tipo de letra)
		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
				
		//Salvando o layoutResource para utilizar no m�todo getView()
		this.layout = layoutResource;
		this.dropDownLayout = layoutResource;
		
		this.values = new ArrayList<String>();
		this.ids = new ArrayList<String>();		

		//As op��es s�o definidas no par�metro dataList
		for(String key : data.keySet())
		{
			this.ids.add((String) key);
			if(__isToUpperCase)
				this.values.add(data.get((String) key).toUpperCase());
			else
				this.values.add(data.get(((String) key).toLowerCase())); 
				
		}

	}
	
	public CustomSpinnerAdapter(Context context,int dataResource,int layoutResource) {
		super(context,layoutResource);
				
		//Salvando o context para utilizar no m�todo getView()
		this.context = context;

		//Criando o customTypeFace (define o tipo de letra)
		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
		
		//Salvando o layoutResource para utilizar no m�todo getView()
		this.layout = layoutResource;
		this.dropDownLayout = layoutResource;
		
		this.values = new ArrayList<String>();
		this.ids = new ArrayList<String>();
		
		//As op��es s�o definidas no arquivo de resource strings.xml
		
		for(int i=0;i<context.getResources().getStringArray(dataResource).length;i++)
		{
			if(__isToUpperCase)
				this.values.add(context.getResources().getStringArray(dataResource)[i].toUpperCase());
			else
				this.values.add(context.getResources().getStringArray(dataResource)[i].toLowerCase());
				
			this.ids.add(String.valueOf(i));
		}
		
	}
	
	public CustomSpinnerAdapter(Context context,int dataResource,int layoutResource,int firstEntryResource) {
		super(context,layoutResource);
				
		//Salvando o context para utilizar no m�todo getView()
		this.context = context;

		//Criando o customTypeFace (define o tipo de letra)
		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
		
		//Salvando o layoutResource para utilizar no m�todo getView()
		this.layout = layoutResource;
		this.dropDownLayout = layoutResource;
		
		this.values = new ArrayList<String>();
		this.ids = new ArrayList<String>();
		
		this.values.add(this.context.getResources().getString(firstEntryResource));
		this.ids.add(OmegaConfiguration.UNEXISTENT_ID_IN_DB);
		
		//As op��es s�o definidas no arquivo de resource strings.xml
		
		for(int i=0;i<context.getResources().getStringArray(dataResource).length;i++)
		{
			if(__isToUpperCase)
			this.values.add(context.getResources().getStringArray(dataResource)[i].toUpperCase());
			else
				this.values.add(context.getResources().getStringArray(dataResource)[i].toLowerCase());
			this.ids.add(String.valueOf(i));
		}		
	}
	
	public int getCount() {
		return this.values.size();
	}

	public String getItem(int position) {	
		return this.values.get(position);
	}

	public long getItemId(int position) {
		return Long.valueOf(this.ids.get(position));
	}

	public int getPositionFromId(String id)
	{
		//Search for id in the idList, returns the position
		for(int i=0;i<this.ids.size();i++)
		{
			if(this.ids.get(i).equals(id))
			{
				return i;
			}
		}
		
		//Could not find, return first position
		return 0;
	}
	
	public View getView(int position, View convertView, ViewGroup parent)
	{
		
		TextView opcao = (TextView)LayoutInflater.from(this.context).inflate(this.dropDownLayout, parent,false);
		opcao.setText(this.values.get(position));
		opcao.setTypeface(this.customTypeFace);
	
		return opcao;
	}
	
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		
		TextView opcao = (TextView)LayoutInflater.from(this.context).inflate(this.layout, parent,false);
		opcao.setText(this.values.get(position));
		opcao.setTypeface(this.customTypeFace);
		
		return opcao;
	}
	
	public void setDropDownLayout(int dropDownLayout)
	{
		this.dropDownLayout = dropDownLayout;
	}

}
