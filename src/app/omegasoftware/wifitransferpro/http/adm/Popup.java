package app.omegasoftware.wifitransferpro.http.adm;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.imports.MensagemPopup;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Javascript;

public class Popup extends InterfaceHTML{

	public Popup(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		
		StringBuilder vBuilder = new StringBuilder();
		String pTipo = __helper.GET("tipo");
		String pPage = __helper.GET("page");
		vBuilder.append("<html>");
		vBuilder.append("<head>");
		vBuilder.append("<title>");
		vBuilder.append(HelperHttp.getTituloDasPaginas());
		vBuilder.append("</title>");
		vBuilder.append(HelperHttp.getTagDoFavicon(context));
		vBuilder.append(HelperHttp.carregarArquivoCss("/adm/css/", "padrao"));
		vBuilder.append(new Javascript(__helper).importarTodasAsBibliotecas());
		vBuilder.append(HelperHttp.imprimirComandoJavascript("var dialogo = false"));
		vBuilder.append("</head>");
		vBuilder.append("<body leftmargin=\"0\" topmargin=\"0\" id=\"body_identificator\">");
		vBuilder.append(Javascript.imprimirCorpoDoTooltip());
		vContainer.appendData(vBuilder);
		
		vContainer.copyContainerResponse(new MensagemPopup(__helper).getContainerResponse());
		vBuilder = new StringBuilder();
		
		if(__helper.GET("tipo") != null && __helper.GET("page") != null){
		
			if(__helper.GET("dialog") != null){
				vBuilder.append(HelperHttp.imprimirComandoJavascript("jQuery(document).ready(function(){" +
					"setTimeout(\"autoRedimensionarDialog()\", 1000);" +
					"})"));
						
			}
			
			if(__helper.GET("navegacao") != null && __helper.GET("navegacao").compareTo("false") != 0){
				vBuilder.append("<fieldset class=\"fieldset_list\">");
				vBuilder.append("<legend class=\"legend_list\">Navega��o</legend>");
				vBuilder.append("<a href=\"javascript:void(0);\" onclick=\"javascript:history.go(-1);\" class=\"link_padrao\">&lt;- " + context.getResources().getString(R.string.botao_voltar)+ "</a>");
				vBuilder.append("&nbsp;&nbsp;");
				vBuilder.append("<a href=\"javascript:void(0);\" onclick=\"javascript:history.go(+1);\" class=\"link_padrao\">" + context.getResources().getString(R.string.botao_avancar)+" -&gt;</a>");
				vBuilder.append("</fieldset>");
			}
			vContainer.appendData(vBuilder);
			vContainer.copyContainerResponse(__helper.getContainerResponsePagina(__helper.GET("tipo"), __helper.GET("page")));
			vBuilder = new StringBuilder() ;
		}
		vContainer.appendData(vBuilder);
		return vContainer;
	}

}
