package app.omegasoftware.wifitransferpro.mapa;

import android.graphics.drawable.Drawable;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class CustomOverlayItem extends OverlayItem {
	Drawable marker=null;
	
	
	
	public CustomOverlayItem(GeoPoint pt, String name, String snippet,
			Drawable marker) {
		super(pt, name, snippet);

		this.marker=marker;
		
	}

	public void setMarker(Drawable marker){
		this.marker = marker;
	}

	
	public Drawable getMarker() {
		return(marker);
	}
	@Override
	public Drawable getMarker(int stateBitset) {


		setState(marker, stateBitset);

		return(marker);
	}


}