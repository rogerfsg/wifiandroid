package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Image;

public class ImageArquivo extends InterfaceHTML{

	public ImageArquivo(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		
		if(__helper.GET("complete_path") != null){
			Image vDownloadArquivo = new Image(
					__helper,
					__helper.GET("complete_path"));
			return vDownloadArquivo.preview();
		}else {
			Image vDownloadArquivo = new Image(
					__helper,
					__helper.GET(EXTDAOArquivo.PATH),
					__helper.GET(EXTDAOArquivo.NOME));
			return vDownloadArquivo.preview();	
		}
		
		
	}

}
