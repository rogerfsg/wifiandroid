package app.omegasoftware.wifitransferpro.service;
import java.io.IOException;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.PrincipalActivity;
import app.omegasoftware.wifitransferpro.http.HelperWifi;
import app.omegasoftware.wifitransferpro.http.IndexServer;


public class ServiceApache extends Service  {

	//Constants
	public static final String TAG = "ServiceApache";

	IndexServer __indexServer;
	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}
	
	@Override
	public void onDestroy(){
		
		stopForeground(true);
//		setForeground(false);

		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		String vStrStartService = getResources().getString(R.string.start_service_apache);
		String vStrAppName = getResources().getString(R.string.app_name);

		Notification note=new Notification(R.drawable.icon,
							vStrStartService,
							System.currentTimeMillis());
		Intent i=new Intent(this, PrincipalActivity.class);
//	    setForeground(true);
    
	    PendingIntent pi=PendingIntent.getActivity(this, 0,
                                                  i, 0);
     
	    note.setLatestEventInfo(
	    		this, 
	    		vStrStartService,
	    		vStrAppName,
	    		pi);
	    
	  note.flags|=Notification.FLAG_NO_CLEAR;

	  startForeground(1337, note);
	  
	  return(START_REDELIVER_INTENT);
	}
	@Override
	public void onCreate() {
		super.onCreate();
		
		try {
			__indexServer = new IndexServer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
