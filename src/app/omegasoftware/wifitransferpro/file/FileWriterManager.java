package app.omegasoftware.wifitransferpro.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Stack;

import android.util.Log;

public class FileWriterManager {

	private static final String TAG = "FileHttpPostManager";

	protected Stack<String> stackedMessages;
	protected File currentWriteableFile;
	
	protected FileOutputStream currentWriteableFileStream;
	
	protected String __pathDirectory;
	protected String __delimiterMessageInFile;
	
	public FileWriterManager(
			String pPathDirectory, 
			String pDelimiterMessageInFile)
	{	
		//Forca a criacao do diretorio
		HelperFile.createDirectory(pPathDirectory);
		this.__pathDirectory = pPathDirectory;
		__delimiterMessageInFile = pDelimiterMessageInFile;

		this.stackedMessages = new Stack<String>();
	

		initializeFile();

	}
	
	@Override
	protected void finalize(){
		try{
			if(currentWriteableFileStream != null){
				currentWriteableFileStream.close();
			}
		} catch(Exception ex){

			String vMessage = "" ; 
			if (ex != null ) vMessage = ex.getMessage();

			//Log.e(TAG, "Error na destruicao do objeto " + TAG + ". Descricao: "  + vMessage);
		} finally {
			try{
				super.finalize();	
			}
			catch (Throwable e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	protected void initializeFile(){

		File __file = new File(__pathDirectory);

		if(__file.isDirectory()){
			File vLastFileModifie = HelperFile.getFileModified(
					__pathDirectory, 
					false, 
					new FilterFileExtension(new String[]{".jpg" }, true));
			if(vLastFileModifie != null){
				this.currentWriteableFile = vLastFileModifie;
				try {
					this.currentWriteableFileStream = new FileOutputStream(this.currentWriteableFile, true);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else createNewFile();
		}
		else{
			return ;
		}

	}

	protected void createNewFile()
	{
		//If this is not the first file being created
		if(this.currentWriteableFile != null)
		{
			if(this.currentWriteableFile.length() != 0)
			{
				try {
					this.currentWriteableFileStream.close();
				} catch (IOException e) {
					//Log.d(TAG,"createNewFile(): Could not close file output stream");
				}	
			}
			else
			{
				return;
			}
		}

		//Log.d(TAG,"createNewFile(): Creating new file");
		//Creates a new file
		this.currentWriteableFile = new File(__pathDirectory, HelperFile.generateRandomFileName(null));
		try {
			//Creates a new file stream
			this.currentWriteableFileStream = new FileOutputStream(this.currentWriteableFile);
		} catch (FileNotFoundException e) {
			//Log.d(TAG,"TruckTrackerFilesManager(): Could not create file output stream");
			this.currentWriteableFileStream = null;
		}
	}


	public void writeLogInformation(String p_text)
	{
		this.emptyMessagesStack();
		this.checkIfStackIsFull();
		this.stackedMessages.add(p_text);
		this.emptyMessagesStack();
	}

	protected void checkIfStackIsFull()
	{
		if(this.stackedMessages.size() > OmegaFileConfiguration.MAX_MESSAGES_STACK_SIZE)
		{
			//Log.d(TAG,"checkIfStackIsFull(): Stack was full");
			this.stackedMessages.remove(this.stackedMessages.lastElement());
		}
	}

	protected void emptyMessagesStack()
	{
		//Log.d(TAG,"emptyMessagesStack(): Emptying stack");
		Stack<String> auxiliarStack = new Stack<String>();
		while(!this.stackedMessages.isEmpty())
		{
			//Log.d(TAG,"emptyMessagesStack(): Stack size = "  + this.stackedMessages.size());
			String message = this.stackedMessages.pop();
			if(!this.writeInFile(message))
			{
				auxiliarStack.add(message);
			}
		}
		this.stackedMessages = auxiliarStack;
	}

	protected boolean writeInFile(String p_text)
	{
		try
		{
			String text = __delimiterMessageInFile + p_text ;
			//Log.d(TAG,"writeInFile(): " + text);
			this.currentWriteableFileStream.write(text.getBytes());
			this.currentWriteableFileStream.flush();
			return true;
		}
		catch(Exception e)
		{
			this.createNewFile();
			return false;
		}
	}

}
