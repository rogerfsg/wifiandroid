package app.omegasoftware.wifitransferpro.http.pages;

import java.io.File;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.FilterFileIsDirectory;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class ListFileTree extends InterfaceHTML{

	public ListFileTree(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		StringBuilder vBuilder = new StringBuilder();
		
		String vDir = __helper.POST("dir");
		
		if(vDir == null || vDir.length() == 0 )
			vDir = HelperHttp.getPathRoot();
		
		File vRootDirectory = new File(vDir);
		if(vRootDirectory.exists() && vRootDirectory.isDirectory())
		{
			File vVetorChildDirectory[] = vRootDirectory.listFiles(new FilterFileIsDirectory(true));
			if(vVetorChildDirectory != null && vVetorChildDirectory.length > 0 ){
				vBuilder.append("<ul class=\"jqueryFileTree\" style=\"display: none;\">");
				for (File vChildDirectory : vVetorChildDirectory) {
					String vChildPath = vChildDirectory.getAbsolutePath();
					vBuilder.append("<li class=\"directory collapsed\"><a href=\"#\" rel=\"" + vChildPath  + "/\">" +  HelperFile.getLastNameOfPath(vChildPath) + "</a></li>");
				}
				vBuilder.append( "</ul>");
			}
		}

		return new ContainerResponse(__helper, vBuilder);
	}

}
