package app.omegasoftware.wifitransferpro.http.recursos.classes.classe;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.NanoHTTPD;

public class Music {
	public String df_path = "";
	public String df_filename = "";
	File file;
	HelperHttp __helper;
	public Music(
			HelperHttp pHelper, 
			String df_path,  
			String df_filename ) {
		__helper = pHelper;
		this.df_path = df_path;
		this.df_filename = df_filename;
		file = new File(df_path, df_filename);
	}
	
	public Music(
			HelperHttp pHelper, 
			String df_path) {
		__helper = pHelper;
		this.df_path = df_path;
		file = new File(df_path);
	}



	// download file
	public ContainerResponse play() {
		if(file.exists()) {
			ByteArrayInputStream inputFile = null;
			inputFile = new ByteArrayInputStream(HelperFile.readBytesOfFile(file));
			if(inputFile != null){
				
				ContainerResponse vContainer = new ContainerResponse(__helper, null, NanoHTTPD.HTTP_OK, inputFile);
				
				return vContainer;
			}
		}
		return null;
	}

}
