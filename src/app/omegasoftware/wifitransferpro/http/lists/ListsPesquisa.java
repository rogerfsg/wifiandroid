package app.omegasoftware.wifitransferpro.http.lists;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPesquisaArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.ContainerTypeOrderFile;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.file.ContainerTypeOrderFile.TYPE_ORDER;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.pages.InformacaoTelefonePagina;
import app.omegasoftware.wifitransferpro.http.pages.PesquisaAnexoPagina;
import app.omegasoftware.wifitransferpro.http.pages.UploadPagina;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Link;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM_TOOLTIP;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class ListsPesquisa extends InterfaceHTML {

	public ListsPesquisa(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		String vStrPath = __helper.GET("path");
		String vNomeChave = __helper.GET("pesquisa_nome");
		vNomeChave = vNomeChave == null  ? "" : vNomeChave;
		String vExtensaoChave = __helper.GET("pesquisa_extensoes");
		vExtensaoChave = vExtensaoChave == null  ? "" : vExtensaoChave;
		if (vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		vStrPath = HelperFile.getNomeSemBarra(vStrPath);
		vStrPath += "/";
		StringBuilder vBuilder = new StringBuilder();
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		if (vStrPath != null && vStrPath.length() > 0) {
			File vDirectory = new File(vStrPath);
			if (vDirectory.isDirectory()) {

				
				vBuilder.append("<td id=\"td_left\" >");
				vBuilder.append("      <form name=\"form_arquivo\" id=\"form_arquivo\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
				vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");
				vBuilder.append("            <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
				vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
				vBuilder.append("           	<input type=\"hidden\" name=\"class\" id=\"class\" value=\""
						+ EXTDAOArquivo.NAME + "\">");
				vBuilder.append("            <input type=\"hidden\" name=\"action\" id=\"action\" value=\"pesquisaArquivo\">");
				vBuilder.append("            <input type=\"hidden\" name=\"funcao\" id=\"funcao\" value=\"\">");
				vBuilder.append("            <input type=\"hidden\" name=\"novo_nome\" id=\"novo_nome\" value=\"\">");
				vBuilder.append("            <input type=\"hidden\" name=\"novo_path\" id=\"novo_path\" value=\"\">");				
				
				
				vBuilder.append("            <input type=\"hidden\" name=\""
						+ EXTDAOArquivo.NOME + "1\" id=\"" + EXTDAOArquivo.NOME
						+ "1\" value=\"" + __helper.GET(EXTDAOArquivo.NOME)
						+ "\">");
				vBuilder.append("            <input type=\"hidden\" name=\""
						+ EXTDAOArquivo.PATH + "1\" id=\"" + EXTDAOArquivo.PATH
						+ "1\" value=\"" + vStrPath + "\">");
//		TABELA CABECALHO =================================================================================================================
				vBuilder.append("    		<table width=\"350\" align=\"top\" class=\"tabela_list\">");
				
				
				vBuilder.append("<colgroup>");
				vBuilder.append("<col width=\"10%\" />");
				vBuilder.append("<col width=\"60%\" />");
				vBuilder.append("<col width=\"30%\" />");
				vBuilder.append("</colgroup>");
				vBuilder.append("<thead>");

				
				
//		DIRETORIO DE BUSCA DO CABECALHO ==============================================================================================================
				vBuilder.append("<tr class=\"tr_list_conteudo_par\">");
				
				vBuilder.append("    				<td height=\"22\" align=\"right\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				vBuilder.append(HelperString.ucFirst(context.getString(R.string.diretorio_busca)));
				vBuilder.append("    				</td>");
				vBuilder.append("    				<td  colspan =\"2\" height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

				vBuilder.append("<input type=\"text\" maxlength=\"200\" id=\""
						+ EXTDAOArquivo.PATH
						+ "1\" name=\""
						+ EXTDAOArquivo.PATH
						+ "1\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\""
						+ vStrPath + "\"/>");
				
				vBuilder.append("    				</td>");
				vBuilder.append("</tr>");
//		KEYWORD DO NOME DO ARQUIVO ==============================================================================================================		
				vBuilder.append("<tr class=\"tr_list_conteudo_par\">");
				vBuilder.append("    				<td height=\"22\" align=\"right\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				vBuilder.append(HelperString.ucFirst(context.getString(R.string.nome_arquivo)));
				vBuilder.append("    				</td>");
				vBuilder.append("    				<td  colspan =\"2\" height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

				vBuilder.append("<input type=\"text\" maxlength=\"200\" id=\"pesquisa_nome\" name=\"pesquisa_nome\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\"" + vNomeChave + "\"/>");
				vBuilder.append("    				</td>");
				vBuilder.append("</tr>");
				
//		EXTENSOES DO NOME DO ARQUIVO ==============================================================================================================
				vBuilder.append("<tr class=\"tr_list_conteudo_par\">");
				vBuilder.append("    				<td  height=\"22\" align=\"right\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				vBuilder.append(HelperString.ucFirst(context.getString(R.string.extensoes)));
				vBuilder.append("    				</td>");
				vBuilder.append("    				<td  colspan =\"2\" height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

				vBuilder.append("<input type=\"text\" maxlength=\"200\" id=\"pesquisa_extensao\" name=\"pesquisa_extensao\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\"" + vExtensaoChave+ "\"/>");
				vBuilder.append("    				</td>");
				vBuilder.append("</tr>");
//		BOTAO DE BUSCA DO ARQUIVO ==============================================================================================================
				vBuilder.append("<tr class=\"tr_list_conteudo_par\">");
				
				vBuilder.append("<td colspan =\"3\" height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				
				vBuilder.append("<input name=\"txtBotao\" type=\"submit\" value=\""
						+ context.getString(R.string.ok).toUpperCase()
						+ "\" " 
						+ "class=\"botoes_form\" style=\"cursor: pointer;\" />");
				vBuilder.append("</td>");
				vBuilder.append("</tr>");

//		LISTA DE BOTOS DO PATH CORRENTE =================================================================================================		
				
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td height=\"11\" colspan=\"5\" align=\"left\" valign=\"middle\" class=\"textos5\" style=\"padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;\" >");
				String vVetorPath[] = vStrPath.split("[/]");
				String vPathDiretorio = "";
				if(vVetorPath == null || vVetorPath.length == 0 )
					vBuilder.append("<input class=\"botoes_form\" value=\"/"
							+ "\" type=\"button\" onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&"
							+ EXTDAOArquivo.PATH + "=" + vPathDiretorio
							+ "'\">");
				else{
					
					for(int i = 0 ;  i < vVetorPath.length; i++){
						if(i == 0 ){
							vBuilder.append("<input class=\"botoes_form\" value=\"/"
									+ "\" type=\"button\" onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&"
									+ EXTDAOArquivo.PATH + "=" + vPathDiretorio
									+ "'\">");
							
						}
						String vDiretorio = vVetorPath[i];
						
						if (vDiretorio == null || vDiretorio.length() == 0)
							continue;
						vPathDiretorio += "/" + vDiretorio;
						vBuilder.append("<input class=\"botoes_form\" value=\""
								+ vDiretorio
								+ "\" type=\"button\" onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&"
								+ EXTDAOArquivo.PATH + "=" + vPathDiretorio
								+ "'\">");
					}
				}
				vBuilder.append("    			</td>");
				vBuilder.append("    		</tr>");
				vBuilder.append("</thead>");
				vBuilder.append("    		</table>");

				
//		TABELA ==================================================================================================================================
				vBuilder.append("<script>");
				vBuilder.append("function marcarTodosOsCheckBoxesCujoIdContenha(isChecked, conteudoId){");
				vBuilder.append("    var inputs = document.getElementsByTagName(\"input\");");
				vBuilder.append("   for(var i=0; i < inputs.length; i++){");
				vBuilder.append("       if(inputs[i].type == \"checkbox\" && inputs[i].id.indexOf(conteudoId) != -1){");
				vBuilder.append("           inputs[i].checked = isChecked;");
				vBuilder.append("       }");
				vBuilder.append("   }");
				vBuilder.append("}");
				vBuilder.append("</script>");
				
				
				vBuilder.append("<table class=\"tabela_list\">");
				vBuilder.append("<colgroup>");
				
				vBuilder.append("<col width=\"10%\" />");
				vBuilder.append("<col width=\"50%\" />");
				vBuilder.append("<col width=\"30%\" />");
				vBuilder.append("<col width=\"10%\" />");
				
				vBuilder.append("</colgroup>");

				vBuilder.append("<thead>");
				vBuilder.append("<tr class=\"tr_list_titulos\">");
				
				
//				NOME ======================================================================================
				vBuilder.append("<td class=\"td_list_titulos\">");
//				DIRETORIO ACIMA ICON ================================================================================================================================
						
						vBuilder.append(" <img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "up_folder.png\" "
								+ "onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_DIRETORIO_ACIMA)
								+ "')\" "
								+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&"
								+ EXTDAOArquivo.PATH + "="
								+ HelperHttp.getStrPathUp(vStrPath) 
								+  "'\""
								+ "onmouseout=\"javascript:notip()\">&nbsp;");
//				RAIZ ICON ================================================================================================================================
				
						vBuilder.append("<img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "home_folder.png\" "
								+ "onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_RAIZ)
								+ "')\" "
								+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&"
								+ EXTDAOArquivo.PATH + "="
								+ HelperHttp.getPathRoot()
								+ "\""
								+ "onmouseout=\"javascript:notip()\">&nbsp;");
				
				vBuilder.append("</td>");
				vBuilder.append("<td class=\"td_list_titulos\">");
				
				vBuilder.append(HelperString.ucFirst(__helper.context.getResources().getString(
								R.string.arquivo_nome)));
						
				vBuilder.append("</td>");
//				=========================================================================================
				vBuilder.append("<td class=\"td_list_titulos\">");
				vBuilder.append(HelperString.ucFirst( __helper.context.getResources().getString(
								R.string.arquivo_data)));
				
				vBuilder.append("</td>");
				vBuilder.append("<td class=\"td_list_titulos\">");
				vBuilder.append(HelperString.ucFirst(__helper.context.getResources().getString(
								R.string.arquivo_tamanho)));
				vBuilder.append("</td>");
				vBuilder.append("</tr>");
				vBuilder.append("</thead>");
				vBuilder.append("<tbody>");
				
				File vVetorFile[] = vDirectory.listFiles();
				if (vVetorFile == null || vVetorFile.length == 0) {
					vBuilder.append("<tr class=\"tr_list_conteudo_impar\">");
					vBuilder.append("<td  colspan=\"5\">");
					vBuilder.append(HelperHttp.imprimirMensagem(
							HelperString.ucFirst(context.getResources().getString(
									R.string.diretorio_sem_arquivo)),
							HelperHttp.TYPE_MENSAGEM.MENSAGEM_OK, null));
					vBuilder.append("</td>");
					vBuilder.append("</tr>");
				} else {
					 
					for (int i = 0; i < vVetorFile.length; i++) {
						File vFile = vVetorFile[i];
						ContainerTypeFile vContainerType = ContainerTypeFile
								.getContainerTypeFile(vFile);
						
						String classTr = (i % 2 == 0) ? "tr_list_conteudo_impar"
								: "tr_list_conteudo_par";
						String vStri = String.valueOf(i);
						String vFileName = vFile.getName();
						boolean isDirectory = (vContainerType.typeFile == TYPE_FILE.FOLDER) ? true : false;
						
						vBuilder.append("<tr class=\"" + classTr + "\">");
						vBuilder.append("<td  style=\"text-align: center; padding-left: 5px;\">");
						vBuilder.append("<img class=\"icones_list\" src=\""
								+ vContainerType.getImage()
								+ "\">&nbsp;");
						vBuilder.append("</td>");
						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");

						if (!isDirectory)
							vBuilder.append(vFileName);
						else {
							vBuilder.append("<a href=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&path="
									+ vStrPath
									+ vFileName
									+ "/"
									+ "'\" class=\"link_padrao\" onmouseout=\"notip();\" onmouseover=\"tip('"
									+ __helper
											.getMensagemTooltip(TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_DIRETORIO)
									+ "');\">" + vFileName + "</a>");
						}
						vBuilder.append("</td>");

						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");
						Date vDate = new Date(vFile.lastModified());
						vBuilder.append(vDate.toLocaleString());
						vBuilder.append("</td>");

						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");
						if (!isDirectory)
							vBuilder.append(HelperFile.getStrLength(vFile));
						else
							vBuilder.append("-");
						vBuilder.append("</td>");

						vBuilder.append("</tr>");
					}
				}
				vBuilder.append("</tbody>");
				vBuilder.append("</table>");

				vBuilder.append("<br/>");
				vBuilder.append("<br/>");
				vBuilder.append("</form>");
				vBuilder.append("</td>");
				
//				Direita Arquivo ============================================================
				vBuilder.append("<td id=\"td_right\">");
				
//				Informacao Telefone============================================================				
				vContainer.appendData(vBuilder);
				ContainerResponse vContainerInformacaoTelefonePagina = new InformacaoTelefonePagina(__helper).getContainerResponse();
				vContainer.copyContainerResponse(vContainerInformacaoTelefonePagina);
				vBuilder = new StringBuilder();
								
				
				vBuilder.append("</td>");
				
				vContainer.appendData(vBuilder);

			}
		}

		return vContainer;
	}

}
