package app.omegasoftware.wifitransferpro.http.lists;

import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.NanoHTTPD;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class ContainerLists {
	public static NanoHTTPD.Response getHTML(String pPage, HelperHttp pHelper){
		ContainerResponse vContainer = getContainerResponse(pPage, pHelper);
		if(vContainer != null){
			return vContainer.getResponse();
		} else return null;
	}
	
	public static ContainerResponse getContainerResponse(String pPage, HelperHttp pHelper){
		if(pPage != null && pPage.length() > 0 ){
			
			if(pPage.compareTo("arquivo") == 0){
				return new ListsArquivo(pHelper).getContainerResponse();
			}else if(pPage.compareTo("pesquisa") == 0){
				return new ListsPesquisa(pHelper).getContainerResponse();
			}else if(pPage.compareTo("resultado_pesquisa") == 0){
				return new ListsResultadoPesquisa(pHelper).getContainerResponse();
			}else if(pPage.compareTo("imagem") == 0){
				return new ListsImagem(pHelper).getContainerResponse();
			}else if(pPage.compareTo("playlist") == 0){
				return new ListsPlaylist(pHelper).getContainerResponse();
			}else if(pPage.compareTo("playlist_musica") == 0){
				return new ListsPlaylistMusica(pHelper).getContainerResponse();
			}else if(pPage.compareTo("link") == 0){
				return new ListsLink(pHelper).getContainerResponse();
			}
			
			
		}
		return null;
	}
	
	
}
