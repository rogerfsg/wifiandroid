/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.omegasoftware.wifitransferpro.date;



import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import android.app.Activity;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

/**
 * Basic example of using date and time widgets, including
 * {@link android.app.TimePickerDialog} and {@link android.widget.DatePicker}.
 *
 * Also provides a good example of using {@link Activity#onCreateDialog},
 * {@link Activity#onPrepareDialog} and {@link Activity#showDialog} to have the
 * activity automatically save and restore the state of the dialogs.
 */
public class HelperDate{

	// where we display the selected date and time


	// date and time
	private Integer mYear;
	private Integer mMonth;
	private Integer mDay;
	private Integer mHour;
	private Integer mMinute;


	public HelperDate() {

		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mHour = c.get(Calendar.HOUR_OF_DAY);
		mMinute = c.get(Calendar.MINUTE);	
	}


	//	2012-03-22 11:00 || 2012-03-22 || 11:00 
	public HelperDate(String pDateSQL){
		if(pDateSQL != null){
			String vVetorDateTime[] = HelperString.splitWithNoneEmptyToken(pDateSQL, " ");

			String vVetorDate[] = HelperString.splitWithNoneEmptyToken(vVetorDateTime[0], "-");
			if(vVetorDate != null)
				if(vVetorDate.length == 3){
					mYear = HelperInteger.parserInt(vVetorDate[0]);
					mMonth = HelperInteger.parserInt(vVetorDate[1]);
					mDay = HelperInteger.parserInt(vVetorDate[2]);
				}
			String vStrTime = null;
			if(vVetorDateTime.length == 2){
				vStrTime = vVetorDateTime[1];
				
			} else if(pDateSQL.contains(":")){
				//se nao contem o date e contem somente time
				vStrTime = pDateSQL;
			}
			if(vStrTime != null){
				String vVetorTime[] = HelperString.splitWithNoneEmptyToken(vStrTime, ":");
				if(vVetorTime != null){
					if(vVetorTime.length >= 2){
						mHour = HelperInteger.parserInt(vVetorTime[0]);
						mMinute = HelperInteger.parserInt(vVetorTime[1]);
					}
				}
			}
			
		}
	}

	public Integer getYear(){
		return mYear;
	}

	public Integer getMonth(){
		return mMonth;
	}

	public Integer getDay(){
		return mDay;
	}

	public Integer getHour(){
		return mHour;
	}

	public Integer getMinute(){
		return mMinute;
	}

	public static String getHoraFormatadaExibicao(String pTime){
		String vVetorTime[] = HelperString.splitWithNoneEmptyToken(pTime, ":");
		if(vVetorTime == null) return null; 
		else if(vVetorTime.length == 2){
			return vVetorTime[0] + ":" + vVetorTime[1] + ":00"; 
		} else return pTime ;
	}
	
	public static String getHoraFormatadaSQL(String pTime){
		String vVetorTime[] = HelperString.splitWithNoneEmptyToken(pTime, ":");
		if(vVetorTime == null) return null; 
		else if(vVetorTime.length == 2){
			return vVetorTime[0] + ":" + vVetorTime[1] + ":00"; 
		} else return pTime ;
	}

	public static String getDateFormatadaSQL(String pDate){
		String vVetorData[] = HelperString.splitWithNoneEmptyToken(pDate, "-");
		if(vVetorData == null) return null;
		else if(vVetorData.length == 3){
			String vMes = "";
			if(vVetorData[0] != null){
				if(vVetorData[0].length() == 1 )
					vMes = "0" + vVetorData[0];
				else vMes = vVetorData[0];
			}
			String vDia = "";
			if(vVetorData[1] != null){
				if(vVetorData[1].length() == 1 )
					vDia = "0" + vVetorData[1];
				else vDia = vVetorData[1];
			}
			return vVetorData[2]  + "-" + vMes + "-" + vDia; 
		} else return null;
	}
	
	public static String getDateFormatadaParaExibicao(String pDate){
		String vVetorData[] = HelperString.splitWithNoneEmptyToken(pDate, "-");
		if(vVetorData == null) return null;
		else if(vVetorData.length == 3){
			String vMes = "";
			if(vVetorData[1] != null){
				if(vVetorData[1].length() == 1 )
					vMes = "0" + vVetorData[1];
				else vMes = vVetorData[1];
			}
			String vDia = "";
			if(vVetorData[2] != null){
				if(vVetorData[2].length() == 1 )
					vDia = "0" + vVetorData[2];
				else vDia = vVetorData[2];
			}
			return   vDia + "/" + vMes + "/"  + vVetorData[0]; 
		} else return null;
	}
	
	public String getDateAndTimeForFileName(){
		StringBuilder v_date = new StringBuilder();

		v_date.append(mYear).append("_");
		// Month is 0 based so add 1
		v_date.append(mMonth + 1).append("_");
		v_date.append(mDay).append("_");
		v_date.append(pad(mHour)).append("_");
		v_date.append(pad(mMinute));
		return v_date.toString();
	}

	public String getDateAndTimeDisplay(){
		StringBuilder v_date = new StringBuilder();

		v_date.append(mYear).append("-");
		// Month is 0 based so add 1
		v_date.append(mMonth + 1).append("-");
		v_date.append(mDay).append(" ");
		v_date.append(pad(mHour)).append(":");
		v_date.append(pad(mMinute));
		return v_date.toString();
	}

	public String getDateDisplay(){
		StringBuilder v_date = new StringBuilder();

		v_date.append(mYear).append("-");
		// Month is 0 based so add 1
		v_date.append(mMonth + 1).append("-");
		v_date.append(mDay);
		return v_date.toString();
	}

	public String getTimeDisplay(){
		StringBuilder v_date = new StringBuilder();

		v_date.append(pad(mHour)).append(":");
		v_date.append(pad(mMinute));
		return v_date.toString();
	}



	public static ContainerDateHour getStrSomaDiaDataHora(
			Integer data, Integer hora, Integer minuto, 
			Integer dataAdd, Integer horaAdd, Integer minutoAdd){
		if(data == null) data = new Integer(0);
		if(hora == null) hora = new Integer(0);
		if(minuto == null) minuto = new Integer(0);
		if(dataAdd == null) dataAdd = new Integer(0);
		if(horaAdd == null) horaAdd = new Integer(0);
		if(minutoAdd == null) minutoAdd = new Integer(0);

		Integer vSomaMinuto = minuto + minutoAdd;

		if(vSomaMinuto >= 60){
			horaAdd += 1;
			vSomaMinuto -= 60;
		}
		Integer vSomaHora = hora + horaAdd;

		int vSomaData = data + dataAdd;
		if(vSomaHora >= 24 ){
			vSomaHora -= 24;
			vSomaData += 1;
		}
		return new ContainerDateHour(vSomaData, vSomaHora , vSomaMinuto);
	}

	public static ContainerDateHour getStrSomaDataHora(Integer hora, Integer minuto, Integer horaAdd, Integer minutoAdd){


		if(hora == null) hora = new Integer(0);
		if(minuto == null) minuto = new Integer(0);

		if(horaAdd == null) horaAdd = new Integer(0);
		if(minutoAdd == null) minutoAdd = new Integer(0);

		Integer vSomaMinuto = minuto + minutoAdd;

		if(vSomaMinuto >= 60){
			horaAdd += 1;
			vSomaMinuto -= 60;
		}
		Integer vSomaHora = hora + horaAdd;


		return new ContainerDateHour(null, vSomaHora , vSomaMinuto);
	}

	public static String getDataAtualFormatada(){

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date agora = new Date();

		return sdf.format(agora);
	}
	
	



	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
}
