package app.omegasoftware.wifitransferpro.common.TextWatcher;

import android.widget.EditText;

public class CEPTextWatcher extends MaskNumberTextWatcher{

//  30 535-012
	public CEPTextWatcher(EditText pEditText){
		super(pEditText, new int[]{3, 7}, new String[] {" ", "-"}, 8);
	}


}