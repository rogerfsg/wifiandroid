package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.HelperPhone;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class PesquisaAnexoPagina extends InterfaceHTML{

	public PesquisaAnexoPagina(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		String vStrPath = __helper.GET("path");
		if (vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		vStrPath = HelperFile.getNomeSemBarra(vStrPath);
		vStrPath += "/";

		// TODO Auto-generated method stub
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("	<!-- Framework CSS -->");

		vBuilder.append("<table width=\"350\" align=\"top\" class=\"tabela_list\">");

		vBuilder.append("<colgroup>");
		vBuilder.append("<col width=\"80%\" />");
		vBuilder.append("<col width=\"20%\" />");
		vBuilder.append("</colgroup>");
		vBuilder.append("<thead>");

		vBuilder.append("    			<tr class=\"tr_list_titulos\">");
		vBuilder.append("    				<td class=\"td_list_titulos\" colspan=\"2\">");

		vBuilder.append("<img class=\"icones_list\" src=\""
			+ OmegaFileConfiguration
					.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
			+ "icone_pesquisa.png\">&nbsp;");
		
		vBuilder.append(__helper.context.getString(R.string.pesquisa));
		vBuilder.append("    			</td>");
		vBuilder.append("    			</tr>");
	
		vBuilder.append("<tr class=\"tr_list_conteudo_par\">");
		vBuilder.append("    				<td height=\"22\" align=\"right\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
		vBuilder.append(HelperString.ucFirst(context.getString(R.string.nome_arquivo)));
		vBuilder.append("    				</td>");
		vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

		vBuilder.append("<input type=\"text\" maxlength=\"200\" id=\"pesquisa_nome_arquivo\" name=\"pesquisa_nome_arquivo\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\"" + vStrPath+ "\"/>");
		vBuilder.append("    				</td>");
		vBuilder.append("</tr>");
		vBuilder.append("<tr class=\"tr_list_conteudo_impar\">");
		vBuilder.append("    				<td height=\"22\" align=\"right\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
		vBuilder.append(HelperString.ucFirst(context.getString(R.string.extensoes)));
		vBuilder.append("    				</td>");
		vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

		vBuilder.append("<input type=\"text\" maxlength=\"200\" id=\"pesquisa_extensoes\" name=\"pesquisa_extensoes\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\"" + vStrPath+ "\"/>");
		vBuilder.append("    				</td>");
		vBuilder.append("</tr>");
		
		vBuilder.append("</thead>");
		vBuilder.append("</tbody>");
		vBuilder.append("</table>");

		return new ContainerResponse(__helper,new StringBuilder( vBuilder)); 
	}

}
