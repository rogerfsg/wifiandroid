package app.omegasoftware.wifitransferpro.file;

import java.io.File;
import java.io.FileFilter;

public class FilterFileExtension implements FileFilter {
	String[] vVetorExtension;
	//pega arquivos que nao possuem sufixo com "."
	public static String FILE_WITHOUT_EXTENSION = "FILE_WITHOUT_EXTENSION";
	boolean isFileWithoutExtension = false;
	//extensao deve conter o "."


	public  FilterFileExtension(String[] pVetorExtension, boolean pIsFileWithoutExtension){
		vVetorExtension = pVetorExtension;
		isFileWithoutExtension = pIsFileWithoutExtension;
	}
	
	public boolean isExtensionExistent(String pNameFile){
		for (String vExtension : vVetorExtension) {
			if( isFileWithoutExtension){
				if(! pNameFile.contains(".")) return true ;
			}else if(pNameFile.endsWith(vExtension)){
				return true;
			} 
		}
		return false;
	}
	
	public boolean accept(File pFile) {
		
		if(pFile.isDirectory()) return false;
		else {
			String pNameFile = pFile.getName();
			if(pNameFile == null) return false;
			else return isExtensionExistent(pNameFile);
		}
	}

}
