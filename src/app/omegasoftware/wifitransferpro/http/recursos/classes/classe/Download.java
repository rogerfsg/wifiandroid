package app.omegasoftware.wifitransferpro.http.recursos.classes.classe;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.FilesZipper;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.HelperZip;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.NanoHTTPD;

public class Download {
	public String df_path = "";
	public String df_contenttype = "application/octet-stream";
	public String df_contentdisposition = "attachment";
	public String df_filename = "";
	File file;
	File vetorFile[];
	HelperHttp __helper;
	boolean isToZip = true;
	public Download(
			HelperHttp pHelper,
			File pfile,
			String df_contenttype, 
			String df_contentdisposition) {
		__helper = pHelper;
		this.df_path = pfile.getAbsolutePath();
		this.df_filename = pfile.getName();
		this.df_contenttype = df_contenttype;
		this.df_contentdisposition = df_contentdisposition;
		
		file = new File(df_path, df_filename);
	}
	
	public Download(
			HelperHttp pHelper,
			File pfile,
			String df_contenttype, 
			String df_contentdisposition,
			boolean pIsToZip) {
		__helper = pHelper;
		
		this.df_contenttype = df_contenttype;
		this.df_contentdisposition = df_contentdisposition;
		file = pfile;
		this.df_path = pfile.getAbsolutePath();
		this.df_filename = pfile.getName();
		isToZip = pIsToZip;
	}

	public Download(
			HelperHttp pHelper,
			File pVetorFile[],
			String df_filename,
			String df_contenttype, 
			String df_contentdisposition) {
		__helper = pHelper;
		vetorFile = pVetorFile;
		this.df_contenttype = df_contenttype;
		this.df_contentdisposition = df_contentdisposition;
		this.df_filename = df_filename;
	}
	
	public Download(
			HelperHttp pHelper, 
			String df_path, 
			String df_contenttype, 
			String df_contentdisposition, 
			String df_filename,
			boolean pIsToZip) {
		isToZip = pIsToZip;
		__helper = pHelper;
		this.df_path = df_path;
		this.df_contenttype = df_contenttype;
		this.df_contentdisposition = df_contentdisposition;
		this.df_filename = df_filename;
		file = new File(df_path, df_filename);
	}
	
	public Download(
			HelperHttp pHelper, 
			String df_path, 
			String df_contenttype, 
			String df_contentdisposition, 
			String df_filename ) {
		__helper = pHelper;
		this.df_path = df_path;
		this.df_contenttype = df_contenttype;
		this.df_contentdisposition = df_contentdisposition;
		this.df_filename = df_filename;
		file = new File(df_path, df_filename);
	}


	private ContainerResponse df_download_directory_zip(){

		ByteArrayOutputStream vOutputStream = new ByteArrayOutputStream();
		BufferedOutputStream vBuffer = new BufferedOutputStream(vOutputStream);
		if(file.isDirectory()){
			try {
				HelperZip.zipDirectory(file, vBuffer);
				vBuffer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				return null;
			}

			ByteArrayInputStream vInputSream = new ByteArrayInputStream(vOutputStream.toByteArray());
			ContainerResponse vContainer = new ContainerResponse(__helper, NanoHTTPD.MIME_DEFAULT_BINARY, NanoHTTPD.HTTP_OK, vInputSream);
			vContainer.addHeader("Content-type: ", this.df_contenttype);
			vContainer.addHeader("Content-Disposition: ", this.df_contentdisposition+ "; filename=\"" + this.df_filename + ".zip\"");
			vContainer.addHeader("Content-Length: ", String.valueOf(vOutputStream.size()));
			return vContainer;
		} else return null;

	}

	private ContainerResponse df_download_file_zip(){

		ByteArrayOutputStream vOutputStream = new ByteArrayOutputStream();
		BufferedOutputStream vBuffer = new BufferedOutputStream(vOutputStream);
		if(!file.isDirectory()){
			try {
				HelperZip.zipFile(file, vBuffer);
				vBuffer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				return null;
			}

			ByteArrayInputStream vInputSream = new ByteArrayInputStream(vOutputStream.toByteArray());
			ContainerResponse vContainer = new ContainerResponse(__helper, NanoHTTPD.MIME_DEFAULT_BINARY, NanoHTTPD.HTTP_OK, vInputSream);
			vContainer.addHeader("Content-type: ", this.df_contenttype);
			vContainer.addHeader("Content-Disposition: ", this.df_contentdisposition+ "; filename=\"" + this.df_filename + "\"");
			vContainer.addHeader("Content-Length: ", String.valueOf(file.length()));
			return vContainer;
		} else return null;

	}
	
	private ContainerResponse df_download_file(){

		
		if(!file.isDirectory()){
			InputStream vFileInputStream;
			try {
				vFileInputStream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				return null;
			}
			
			ContainerResponse vContainer = new ContainerResponse(__helper, NanoHTTPD.MIME_DEFAULT_BINARY, NanoHTTPD.HTTP_OK, vFileInputStream);
			vContainer.addHeader("Content-type: ", this.df_contenttype);
			vContainer.addHeader("Content-Disposition: ", this.df_contentdisposition+ "; filename=\"" + this.df_filename + "\"");
			vContainer.addHeader("Content-Length: ", String.valueOf(file.length()));
			return vContainer;
		} else return null;

	}
	
	private ContainerResponse df_download_vetor_file_zip(){

		ByteArrayOutputStream vOutputStream = new ByteArrayOutputStream();
		BufferedOutputStream vBuffer = new BufferedOutputStream(vOutputStream); 
		try {
			
			if(vetorFile != null && vetorFile.length > 0 ){
				File vFile = vetorFile[0];
				
				String vPathOfFile = HelperFile.getPathOfFile(vFile.getPath());
				File vDiretorio = new File(vPathOfFile);
				if(vDiretorio.exists())
					HelperZip.zipVetorFile(vetorFile, vDiretorio, vBuffer);
			}
				
			
			vBuffer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
		
		ByteArrayInputStream vInputSream = new ByteArrayInputStream(vOutputStream.toByteArray());
		ContainerResponse vContainer = new ContainerResponse(__helper, NanoHTTPD.MIME_DEFAULT_BINARY, NanoHTTPD.HTTP_OK, vInputSream);
		vContainer.addHeader("Content-type: ", this.df_contenttype);
		vContainer.addHeader("Content-Disposition: ", this.df_contentdisposition+ "; filename=\"" + this.df_filename + ".zip\"");
		vContainer.addHeader("Content-Length: ", String.valueOf(vOutputStream.size()));
		return vContainer;
		
	}
//	
//	private ContainerResponse df_download_vetor_file(){
//
//		ByteArrayOutputStream vOutputStream = new ByteArrayOutputStream();
//		BufferedOutputStream vBuffer = new BufferedOutputStream(vOutputStream); 
//		try {
//			HelperZip.zipVetorFile(vetorFile, vBuffer);
//			vBuffer.close();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			return null;
//		}
//		
//		ByteArrayInputStream vInputSream = new ByteArrayInputStream(vOutputStream.toByteArray());
//		ContainerResponse vContainer = new ContainerResponse(__helper, NanoHTTPD.MIME_DEFAULT_BINARY, NanoHTTPD.HTTP_OK, vInputSream);
//		vContainer.addHeader("Content-type: ", this.df_contenttype);
//		vContainer.addHeader("Content-Disposition: ", this.df_contentdisposition+ "; filename=\"" + this.df_filename + ".zip\"");
//		vContainer.addHeader("Content-Length: ", String.valueOf(vOutputStream.size()));
//		return vContainer;
//		
//	}

	// download file
	public ContainerResponse df_download() {
		if(vetorFile != null && vetorFile.length > 0 )
			return df_download_vetor_file_zip();
		else if(file != null && file.exists()) {
			if(file.isFile()){
				if(isToZip)
					return df_download_file_zip();
				else return df_download_file();
			}
			else return df_download_directory_zip();
		}
		return null;
	}
	//
	//	//download string
	//	public NanoHTTPD.Response ds_download() {
	//
	//		
	//		if(file.exists()) {
	//			InputStream inputFile = null;
	//			try {
	//				inputFile = new FileInputStream(file);
	//			} catch (FileNotFoundException e) {
	//				// TODO Auto-generated catch block
	//				e.printStackTrace();
	//				inputFile = null;
	//			}
	//			if(inputFile != null){
	//				NanoHTTPD.Response vResponse = __helper.nanoHTTPD.factoryResponse( NanoHTTPD.MIME_DEFAULT_BINARY, NanoHTTPD.HTTP_OK, inputFile);
	//				vResponse.addHeader("Content-type: ", this.df_contenttype);
	//				vResponse.addHeader("Content-Disposition: ", this.df_contentdisposition+ "; filename=\"" + this.df_filename + "\"");
	//				vResponse.addHeader("Content-Length: ", String.valueOf(file.length()));
	//				return vResponse;
	//			}
	//		}
	//		return null;
	//
	//	}


}
