package app.omegasoftware.wifitransferpro.service;
import java.io.File;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAODiretorioImagemArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOImagemArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.file.FilenameFilterLikeAndExtension;
import app.omegasoftware.wifitransferpro.file.FilterFileIsDirectory;


public class ServiceCheckFoto extends Service  {

	//Constants
	public  final String TAG = "ServiceCheckFoto";	
	String __vetorExtensao[] = null;
	
	private Timer checkFotoTimer = new Timer();
	boolean __isRunning = false;
	EXTDAOImagemArquivo __objImagemArquivo;
	EXTDAODiretorioImagemArquivo __objDiretorioImagemArquivo;
	DatabaseWifiTransfer db = new DatabaseWifiTransfer(this);
	
	
	ContainerCounterFile __containerCountFile;
	
	File __vetorDiretorioCorrente[];
	private int LIMIT_CYCLE = 1;
	int counterCycle = 0;
	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}

	
	public  boolean isRunning(){
		return __isRunning;
	}
	
	private class MyTimerTask extends TimerTask{
		Service __service;
		public MyTimerTask(Service pService){
			__service = pService;
		}
		@Override
		public void run() {
			if(counterCycle >= LIMIT_CYCLE) {
				checkFotoTimer.cancel();
				__service.stopSelf();
			}else{
				counterCycle  += 1;
			// TODO Auto-generated method stub
			searchFile(new File("/sdcard"), __vetorExtensao);
			}
		}
		
	}
	
	private void setTimer()
	{
		
		//Starts reporter timer
		this.checkFotoTimer.scheduleAtFixedRate(new MyTimerTask(this),
		OmegaConfiguration.SERVICE_CHECK_FOTO_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_CHECK_FOTO_TIMER_INTERVAL);
	}
	@Override
	public void onDestroy(){
		if(checkFotoTimer != null)
			checkFotoTimer.cancel();
		db.close();
		super.onDestroy();
	}

	
	
	public void insertFileInDatabase(String pPath){
		db.openIfNecessary();
		__objImagemArquivo.clearData();
		__objImagemArquivo.setAttrStrValue(EXTDAOImagemArquivo.PATH,pPath);
		__objImagemArquivo.insert(false);
		db.close();
	}
	
	public  void filter(File pDiretorioCorrente, FilenameFilterLikeAndExtension pFilterFile){
		
		if(pDiretorioCorrente.isDirectory()){
			if(__objDiretorioImagemArquivo.checkIfIsNecessaryCheckDirectory(pDiretorioCorrente)){
				File vVetorFileAccept[] = pDiretorioCorrente.listFiles(pFilterFile);
				if(vVetorFileAccept != null && vVetorFileAccept.length > 0 )
					for (File file : vVetorFileAccept) {
						insertFileInDatabase(file.getAbsolutePath());
					}
			}
			__vetorDiretorioCorrente = pDiretorioCorrente.listFiles(new FilterFileIsDirectory(true));
			if(__vetorDiretorioCorrente != null)
				for (File vDiretorio : __vetorDiretorioCorrente) {
					filter(vDiretorio, pFilterFile);
				}	
		}
		
	}
	
	public class ContainerCounterFile{
		int NUMBER_OF_FILE_PER_CYCLE = 50;
		int __counter = 0;
		
		Stack<File> __stackDirectoryRoot;
		Stack<Integer> __stackIteratorDirectoryOfRoot;
		Stack<File> __stackFileInCurrentDirectory;
		FilenameFilterLikeAndExtension __filterFile;
		File __vetorDirectoryOfDirectory[];
		int __iFile = 0;
		Integer __iDirectory = null;
		public ContainerCounterFile(String pPathRootDirectory, FilenameFilterLikeAndExtension pFilterFile){
			File vDirectory = new File(pPathRootDirectory);
			if(vDirectory.isDirectory()){
				__stackDirectoryRoot = new Stack<File>();
				__stackFileInCurrentDirectory = new Stack<File>();
				__stackDirectoryRoot.push(vDirectory);
				__stackIteratorDirectoryOfRoot.push(0);
				__vetorDiretorioCorrente = vDirectory.listFiles(new FilterFileIsDirectory(true));
				__filterFile = pFilterFile;
				File vVetorFile[] = vDirectory.listFiles(__filterFile);
				if(vVetorFile != null){
					for (File file : vVetorFile) {
						__stackFileInCurrentDirectory.push(file);
					}
				}
				__iDirectory = 0;
				__iFile = 0;
			}
		}
		public File nextDirectory(){
			if(hasNextDirectory()){
				__iDirectory ++;
				return __vetorDirectoryOfDirectory[__iDirectory];
			} else{
				//Stack = sdcard/
				//Vetor = f1, f2 
				__vetorDirectoryOfDirectory = null;
				Integer iDirectory = null;
				while(!__stackDirectoryRoot.isEmpty() && 
						( __vetorDirectoryOfDirectory == null || __vetorDirectoryOfDirectory.length == 0 )){
					
					File vNewRoot = __stackDirectoryRoot.peek();
					iDirectory = __stackIteratorDirectoryOfRoot.peek();
					__vetorDirectoryOfDirectory = vNewRoot.listFiles(new FilterFileIsDirectory(true));
					//Se nao existir mais diretorios filhos a serem verificados no atual diretorio pai
					if(__vetorDirectoryOfDirectory == null || 
							__vetorDirectoryOfDirectory.length == 0 || 
							iDirectory == null || 
							__vetorDirectoryOfDirectory.length <= iDirectory ||
							!__objDiretorioImagemArquivo.checkIfIsNecessaryCheckDirectory(vNewRoot)){
						
						__stackDirectoryRoot.pop();
						__stackIteratorDirectoryOfRoot.pop();
						continue;
					} else{
						
							
						__iDirectory = iDirectory;
						__stackIteratorDirectoryOfRoot.pop();
						__stackIteratorDirectoryOfRoot.push(__iDirectory + 1);
						__stackDirectoryRoot.push(__vetorDirectoryOfDirectory[__iDirectory]);
						__stackIteratorDirectoryOfRoot.push(0);
						break;
					}
				}
				if(!( __vetorDirectoryOfDirectory == null || __vetorDirectoryOfDirectory.length == 0 ) && 
						(__iDirectory != null && __iDirectory < __vetorDirectoryOfDirectory.length)){
					return __vetorDirectoryOfDirectory[__iDirectory];
				} else return null;
			}
		}
		public boolean hasNextDirectory(){
			if(__iDirectory + 1< __vetorDirectoryOfDirectory.length){
				return true;
			} else return false;
		}
		public boolean hasNextFile(){
			return !__stackFileInCurrentDirectory.isEmpty();
		}
		
		public File nextFile(){
			if(hasNextFile()) return __stackFileInCurrentDirectory.pop();
			else{
				File vDirectory = nextDirectory();
				File vVetorFile[] = vDirectory.listFiles(__filterFile);
				if(vVetorFile != null){
					for (File file : vVetorFile) {
						__stackFileInCurrentDirectory.push(file);
					}
				}
				if(hasNextFile()) return __stackFileInCurrentDirectory.pop();
				else return null;
			}
		}
	}
	
	
	public  void searchFile(File pDiretorioCorrente, String pVetorExtension[]){
		__isRunning = true;
		db.openIfNecessary();
		FilenameFilterLikeAndExtension vFilterFile = null;
		if(pVetorExtension != null && pVetorExtension.length > 0 ){
			if(pVetorExtension.length == 0 )
				vFilterFile = new FilenameFilterLikeAndExtension(null, pVetorExtension, true);
			else if(pVetorExtension.length > 0 )
				vFilterFile = new FilenameFilterLikeAndExtension(null, pVetorExtension, false);
		}
		
		filter(pDiretorioCorrente, vFilterFile);
		
		__isRunning = false;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		ContainerTypeFile vContainer = ContainerTypeFile.getContainerTypeFile(TYPE_FILE.IMAGE);
		__vetorExtensao = vContainer.getVetorExtensao();
		db = new DatabaseWifiTransfer(this);
		__objImagemArquivo = new EXTDAOImagemArquivo(db);
		__objDiretorioImagemArquivo = new EXTDAODiretorioImagemArquivo(db);
		db.close();
		setTimer();
	}
	
	
}

