package app.omegasoftware.wifitransferpro.database.EXTDAO;

import android.content.Context;
import android.os.Environment;
import app.omegasoftware.wifitransferpro.database.Database;

public class DatabaseWifiTransfer extends Database {
	public static final String DATABASE_PATH = Environment.getDataDirectory()+ "/guidelondon.db";
	public static final String DATABASE_NAME =  "guidelondon.db";
	public static final int DATABASE_VERSION = 1;

	public DatabaseWifiTransfer(Context context){
		super(
			context,
			DATABASE_NAME, 
			DATABASE_VERSION, 
			DATABASE_PATH);
		//populate();
		
		
	}

}
