package app.omegasoftware.wifitransferpro.http.pages;

import java.io.File;

import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Download;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class DownloadArquivo extends InterfaceHTML{

	public DownloadArquivo(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		
		
		if(__helper.GET("path_completo") != null){
			Download vDownloadArquivo = new Download(
					__helper,
					new File(__helper.GET("path_completo")),
					"application/octet-stream",
					"attachment",
					false);
			return vDownloadArquivo.df_download();
		} else {
			Download vDownloadArquivo = new Download(
					__helper,
					__helper.GET(EXTDAOArquivo.PATH),
					"application/octet-stream",
					"attachment",
					__helper.GET(EXTDAOArquivo.NOME),
					false);
			return vDownloadArquivo.df_download();	
		}
	}

}
