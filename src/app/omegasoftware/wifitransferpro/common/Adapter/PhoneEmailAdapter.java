package app.omegasoftware.wifitransferpro.common.Adapter;

import java.util.ArrayList;


import android.content.Context;
import android.graphics.Typeface;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.listener.EmailClickListener;
import app.omegasoftware.wifitransferpro.listener.PhoneClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

public class PhoneEmailAdapter extends BaseAdapter {

	public static final int EMAIL_TYPE = 0;
	public static final int PHONE_TYPE = 1;
	private Typeface customTypeFace;
	
	Context context;
	ArrayList<String> data;
	int type;
	
	public PhoneEmailAdapter(Context context, ArrayList<String> data, int type)
	{
		this.context = context;
		this.data = data;
		this.type = type;
		
		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
		
	}
	public PhoneEmailAdapter(Context context, String data, int type)
	{
		this.context = context;
		this.data = new ArrayList<String>();
		this.data.add(data);
		this.type = type;
		
		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
		
	}

	public int getCount() {
		return this.data.size();
	}

	public String getItem(int position) {
		return this.data.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		String value = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.phone_or_email_button, null);
		
		Button valueButton = (Button) layoutView.findViewById(R.id.email_or_phone_button);
		valueButton.setText(value);
		valueButton.setTypeface(this.customTypeFace);
		
		switch(this.type)
		{
		case PhoneEmailAdapter.EMAIL_TYPE:
			layoutView.setOnClickListener(new EmailClickListener(R.id.email_or_phone_button));
			valueButton.setOnClickListener(new EmailClickListener(R.id.email_or_phone_button));
			break;
		case PhoneEmailAdapter.PHONE_TYPE:
			layoutView.setOnClickListener(new PhoneClickListener(R.id.email_or_phone_button));
			valueButton.setOnClickListener(new PhoneClickListener(R.id.email_or_phone_button));
			break;
		}
		
		return layoutView;
	}
	
}