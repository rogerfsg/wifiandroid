package app.omegasoftware.wifitransferpro.http.pages;

import java.util.ArrayList;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPlaylist;
import app.omegasoftware.wifitransferpro.file.HelperPhone;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Link;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class AnexoAdicionarPlaylistPagina extends InterfaceHTML{

	public AnexoAdicionarPlaylistPagina(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {

		String vFileName = __helper.GET(EXTDAOArquivo.NOME);
		String vStrPath = __helper.GET(EXTDAOArquivo.PATH);
		vFileName = (vFileName == null) ? "" : vFileName;
		vStrPath = (vStrPath == null) ? "" : vStrPath;
		// TODO Auto-generated method stub
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("	<!-- Framework CSS -->");

		vBuilder.append("<table width=\"350\" align=\"top\" class=\"tabela_list\">");

		vBuilder.append("<colgroup>");
		vBuilder.append("<col width=\"65%\" />");
		vBuilder.append("<col width=\"35%\" />");
		vBuilder.append("</colgroup>");
		vBuilder.append("<thead>");

		vBuilder.append("			<tr class=\"tr_list_titulos\">");
		vBuilder.append("				<td class=\"td_list_titulos\" colspan=\"2\">");


		//		Icone adicionar playlist
		Properties vLinkAddPlaylist = new Properties();
		vLinkAddPlaylist
		.put("onmouseover",
				"tip(\'"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_NOVA_PLAYLIST)
						+ "\', this);");
		vLinkAddPlaylist.put("onmouseout", "notip();");
		Link vObjLinkAddPlaylist = new Link(
				150,
				500,
				context.getResources().getString(
						R.string.playlist),
						"popup.php5?tipo=pages&page=NovaPlaylist&"
								+ EXTDAOArquivo.NOME + "=" + vFileName
								+ "&" + EXTDAOArquivo.PATH + "="
								+ vStrPath,
								vLinkAddPlaylist,
								"<img class=\"icones_list\" src=\""
										+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
										+ "icone_adicionar_playlist.png\" onmouseover=\"javascript:tip('"
										+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_NOVA_PLAYLIST)
										+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
										true);

		vBuilder.append(vObjLinkAddPlaylist.montarLink());

		vBuilder.append(__helper.context.getString(R.string.nova_playlist));
		vBuilder.append("		</td>");
		vBuilder.append("	</tr>");
		vBuilder.append("</thread>");

		Database db = new DatabaseWifiTransfer(__helper.context);
		EXTDAOPlaylist vObjPlaylist = new EXTDAOPlaylist(db);
		ArrayList<Table> vListTupla = vObjPlaylist.getListTable();
		if(vListTupla == null || vListTupla.size() == 0 ){
			vBuilder.append("<tr>");
			vBuilder.append("<td>");
			vBuilder.append(HelperHttp.imprimirMensagem(__helper.context.getResources().getString(R.string.list_playlist_vazia), HelperHttp.TYPE_MENSAGEM.MENSAGEM_INFO, ""));
			vBuilder.append("</td>");
			vBuilder.append("</tr>");
		}
		else
			for(int i = 0 ; i < vListTupla.size(); i++){
				String classTr = (i % 2 == 0) ? "tr_list_conteudo_impar"
						: "tr_list_conteudo_par";
				vBuilder.append("<tr class=\"" + classTr + "\">");
				Table vTupla = vListTupla.get(i);
				String vNomePlaylist = vTupla.getStrValueOfAttribute(EXTDAOPlaylist.NOME);
				String vIdPlaylist = vTupla.getStrValueOfAttribute(EXTDAOPlaylist.ID);
				vBuilder.append("<td>");
				vBuilder.append(vNomePlaylist);
				vBuilder.append("</td>");
				vBuilder.append("<td>");

				//			Adicionar musica

				Properties vLinkMusic = new Properties();
				vLinkMusic
				.put("onmouseover",
						"tip(\'"
								+ __helper
								.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_TOCAR_MUSICA)
								+ "\', this);");
				vLinkMusic.put("onmouseout", "notip();");
				Link vObjLinkMusica = new Link(
						400,
						700,
						context.getResources().getString(
								R.string.playlist_play),
								"popup.php5?tipo=pages&page=PopupPlayerList"
										+ "&playlist=" + vIdPlaylist
										+ "&" + EXTDAOArquivo.NOME + "=" + vFileName
										+ "&" + EXTDAOArquivo.PATH + "="
										+ vStrPath,
										vLinkMusic,
										"<img class=\"icones_list\" src=\""
												+ OmegaFileConfiguration
												.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
												+ "music.png\" onmouseover=\"javascript:tip('"
												+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_TOCAR_MUSICA)
												+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
												true);

				vBuilder.append(vObjLinkMusica.montarLink());

				//			vBuilder.append("<img class=\"icones_list\" src=\""
				//			+ OmegaFileConfiguration
				//					.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
				//			+ "music.png\" onmouseover=\"javascript:tip('"
				//			+ __helper
				//					.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_TOCAR_MUSICA)
				//			+ "')\" onclick=\"javascript:location.href='" + "index.php5?tipo=lists&page=playlist_musica&"
				//							+ EXTDAOArquivo.NOME + "=" + vFileName
				//							+ "&" + EXTDAOArquivo.PATH + "=" + vStrPath 
				//							+ "&playlist=" + vIdPlaylist + "'\" onmouseout=\"javascript:notip()\">&nbsp;");

				//			Editar playlist

				vBuilder.append("<img class=\"icones_list\" src=\""
						+ OmegaFileConfiguration
						.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
						+ "icone_editar.png\" onmouseover=\"javascript:tip('"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_EDICAO)
						+ "')\" onclick=\"javascript:location.href='" + "index.php5?tipo=lists&page=playlist&"
						+ EXTDAOArquivo.NOME + "=" + vFileName
						+ "&" + EXTDAOArquivo.PATH + "=" + vStrPath 
						+ "&playlist=" + vIdPlaylist + "'\" onmouseout=\"javascript:notip()\">&nbsp;");

				//			Excluir playlist
				Properties vLinkRemoveTotal = new Properties();
				vLinkRemoveTotal
				.put("onmouseover",
						"tip(\'"
								+ __helper
								.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
								+ "\', this);");
				vLinkRemoveTotal.put("onmouseout", "notip();");
				Link vObjLinkRemoveTotal = new Link(
						180,
						350,
						context.getResources().getString(
								R.string.deletar_arquivo),
								"popup.php5?tipo=pages&page=RemovePlaylist&"
										+ EXTDAOArquivo.NOME + "=" 
										+ "&" + EXTDAOArquivo.PATH + "="
										+ vStrPath + "",
										vLinkRemoveTotal,
										"<img class=\"icones_list\" src=\""
												+ OmegaFileConfiguration
												.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
												+ "icone_excluir.png\" onmouseover=\"javascript:tip('"
												+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
												+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(vObjLinkRemoveTotal.montarLink());

				vBuilder.append("</td>");

				vBuilder.append("</tr>");
			}

		db.close();


		vBuilder.append("</thead>");
		vBuilder.append("</tbody>");
		vBuilder.append("</table>");

		return new ContainerResponse(__helper,new StringBuilder( vBuilder)); 
	}

}
