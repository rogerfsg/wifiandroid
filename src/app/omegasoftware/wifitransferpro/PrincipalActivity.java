package app.omegasoftware.wifitransferpro;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.wifitransferpro.TabletActivities.PaginaInicialActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.activity.OmegaRegularActivity;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.gps.GPSServiceLocationListener;
import app.omegasoftware.wifitransferpro.http.HelperNetwork;
import app.omegasoftware.wifitransferpro.http.HelperWifi;
import app.omegasoftware.wifitransferpro.http.pages.PaginaInicial;
import app.omegasoftware.wifitransferpro.service.ServiceApache;
import app.omegasoftware.wifitransferpro.service.ServiceCheckFoto;


public class PrincipalActivity extends OmegaRegularActivity {

	private static final String TAG = "PrincipalActivity";
	public static boolean isTablet = false;
	
	//	Intent __serviceSynchronizeFastDatabase = null;
	public static Activity __principalActivity = null;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		setContentView(R.layout.splash_mobile);

		new Splasher(this).execute();
		
	}
    
   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
   	super.onActivityResult(requestCode, resultCode, data);
   	finish();
   }

	public Dialog getErrorDialog(int dialogType)
	{

		switch (dialogType) {
		
		default:
			return super.onCreateDialog(dialogType);
		}

	}

	public class Splasher extends AsyncTask<String, Integer, Boolean>
	{
		Activity __activity;
		public Splasher(Activity pActivity){
			__activity = pActivity;
		}
		@Override
		protected Boolean doInBackground(String... params) {
			return loadData();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			initializeComponents();
			
		}

	}

	@Override
	public boolean loadData() {
		DatabaseWifiTransfer db ;
		db = new DatabaseWifiTransfer(this);
		db.close();
		
		try
		{
			Thread.sleep(2000);
			HelperNetwork.getIp(this);
		}
		catch(Exception e)
		{
		}
		
		
		return true;
	}

	
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;
		try{
			switch (id) {

			case OmegaConfiguration.ON_LOAD_DIALOG:
				ProgressDialog vDialogAux = new ProgressDialog(this);
				vDialogAux.setMessage(getResources().getString(R.string.string_loading));
				return vDialogAux;
			default:
				return getErrorDialog(id);
				

			}
		} catch(Exception ex){
			return null;
		}
		

	}
	
	@Override
	public void initializeComponents() {	          
		
		__principalActivity = this;
		
		
		
		Intent v_intent =  new Intent( this, PaginaInicialActivityMobile.class);
		startActivityForResult(v_intent , OmegaConfiguration.STATE_INITIALIZE_PONTO_ELETRONICO);
		
		
	}

}
