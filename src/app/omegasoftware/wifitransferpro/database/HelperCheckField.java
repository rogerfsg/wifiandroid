package app.omegasoftware.wifitransferpro.database;

public class HelperCheckField {

	public static boolean checkPhone(String pToken){
		//(31) 3333 - 3333
		if(pToken.length() < 16) return false;
		else return true;
	}

	public static boolean checkEmail(String pToken){
		
		if(pToken == null) return false;
		else if (pToken.length() == 0 )return false;
		else{
			if(! (pToken.contains("@"))){
				return false;
			}else{
				String vVetorArroba[] = pToken.split("@");
				if(vVetorArroba.length < 2) return false;
				else{
					String vExprPonto = vVetorArroba[1];
					if(vExprPonto == null ) return false;
					else if(vExprPonto.length() == 0 ) return false;
					else{
						
						if(vExprPonto.contains(".")) return true;
						else return false;
						
					}
				}
			}
			
		}
	}

	public static boolean checkCEP(String pToken){
		return true;
	}

	public static boolean checkCPF(String pToken){
		return true;
	}
	
	public static boolean checkRG(String pToken){
		return true;
	}

	public static boolean checkCNPJ(String pToken){
		return true;
	}
}
