package app.omegasoftware.wifitransferpro.common.container;

import android.app.Activity;
import android.os.Bundle;
import android.view.View.OnClickListener;
import app.omegasoftware.wifitransferpro.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;

public class ContainerClassItem extends AbstractContainerClassItem {
	
	public static String TAG = "ContainerClassItem";
	Bundle __bundle;
	
	public ContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag,
			Integer pDrawable,
			boolean pIsComumATodosUsuarios){
		super(pIdStringNome, 
				pClass,
				pTag,
				pDrawable,
				pIsComumATodosUsuarios);
		__bundle = new Bundle();
		__bundle.putString(OmegaConfiguration.SEARCH_FIELD_NOME, pTag);
		
	}
	
	public ContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag,
			Integer pDrawable){
		super(pIdStringNome,
				pClass,
				pTag,
				pDrawable);
		__bundle = new Bundle();
		__bundle.putString(OmegaConfiguration.SEARCH_FIELD_NOME, pTag);
	}
	
	public ContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag,
			Integer pDrawable,
			Bundle pBundle){
		super(pIdStringNome,
				pClass,
				pTag,
				pDrawable);
		
		__bundle = pBundle;
		if(__bundle == null){
			__bundle = new Bundle();
			__bundle.putString(OmegaConfiguration.SEARCH_FIELD_NOME, pTag);
		}else {
			__bundle.putString(OmegaConfiguration.SEARCH_FIELD_NOME, pTag);
		}
		
	}
	
	
	
	@Override
	public OnClickListener getOnClickListener(Activity pActivity) {
		// TODO Auto-generated method stub
		return new MenuOnClickListener(
				(FactoryMenuActivityMobile) pActivity,
				getClassTarget(),
				__bundle);
	}
	
}
