package app.omegasoftware.wifitransferpro.mapa;

import android.graphics.drawable.Drawable;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class VeiculoCustomOverlayItem extends CustomOverlayItem {
	
	Integer __idVeiculoUsuario;
	boolean __isUsuarioPrincipal;
	String __dateTime;
	
	
	public VeiculoCustomOverlayItem(GeoPoint pt, String name, String snippet,
			Drawable marker, String pIdVeiculoUsuario, String pDateTime) {
		super(pt, name, snippet, marker);

		this.marker=marker;
		__idVeiculoUsuario = HelperInteger.parserInteger(pIdVeiculoUsuario);
		__isUsuarioPrincipal = false;
		__dateTime = pDateTime;
	}

	public VeiculoCustomOverlayItem(GeoPoint pt, String name, String snippet,
			Drawable marker, String pIdVeiculoUsuario, String pDateTime, boolean pIsUsuarioPrincipal) {
		super(pt, name, snippet, marker);

		this.marker=marker;
		__idVeiculoUsuario = HelperInteger.parserInteger(pIdVeiculoUsuario);
		__isUsuarioPrincipal = pIsUsuarioPrincipal;
		__dateTime = pDateTime;
	}
	
	public String getDatetime(){
		return __dateTime;
	}

	public int getIdVeiculoUsuario(){
		return __idVeiculoUsuario;
	}

	public boolean isUsuarioPrincipal(){
		return __isUsuarioPrincipal;
	}

}
