package app.omegasoftware.wifitransferpro.primitivetype;

public class HelperDouble {
	
	
	public static Double parserDouble(String pIndex){
		Double index = null;
		
		try{
			if(pIndex == null) return null;
			else if (pIndex.length() == 0 ) return null;
			index = Double.parseDouble(pIndex);
			return index;
		} catch(Exception ex){
			return null;
		}

	}
}
