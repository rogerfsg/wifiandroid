package app.omegasoftware.wifitransferpro.http;

import android.content.Context;
import android.content.SharedPreferences;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;

public class HelperPorta {
	public static int NUMERO_PORTA_INFERIOR = 1025;
	public static int NUMERO_PORTA_SUPERIOR = 65535;
	
	public static String PORTA_DEFAULT = "4321";
	public static String getSavedPorta(Context pContext){
		SharedPreferences vSavedPreferences = pContext.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		
		return vSavedPreferences.getString(
				OmegaConfiguration.SAVED_PORTA_ACESSO, PORTA_DEFAULT);
	}
	
	public static String getStrIntervalo(){
		return String.valueOf(NUMERO_PORTA_INFERIOR) + " .. " + String.valueOf(NUMERO_PORTA_SUPERIOR); 
	}
	
	public static boolean checkValidadeNumeroPorta(String pPorta){
		int vIntPorta = HelperInteger.parserInt(pPorta);
		if(!(vIntPorta >= NUMERO_PORTA_INFERIOR && vIntPorta <= NUMERO_PORTA_SUPERIOR))
			return false;
		else return true;
	}
	
	public static void savePorta(Context pContext, String pPorta){
			if(pPorta == null || pPorta.length() == 0 )
				return;
			
			if(checkValidadeNumeroPorta(pPorta)){
				SharedPreferences vSavedPreferences = pContext.getSharedPreferences(
						OmegaConfiguration.PREFERENCES_NAME, 0);
				SharedPreferences.Editor vEditor = vSavedPreferences.edit();
				vEditor.putString(
						OmegaConfiguration.SAVED_PORTA_ACESSO, pPorta);
				vEditor.commit();	
			}
			
		
	}
}
