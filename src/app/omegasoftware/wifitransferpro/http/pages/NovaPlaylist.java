package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class NovaPlaylist extends InterfaceHTML{

	public NovaPlaylist(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("      <form name=\"form_senha\" id=\"form_senha\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
		vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");

		vBuilder.append("            <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
		vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
		vBuilder.append("           	<input type=\"hidden\" name=\"class\" id=\"class\" value=\"Arquivo\">");
		vBuilder.append("            <input type=\"hidden\" name=\"action\" id=\"action\" value=\"adicionarPlaylist\">");
		
		vBuilder.append("            <input type=\"hidden\" name=\"" + EXTDAOArquivo.NOME+ "1\" id=\"" + EXTDAOArquivo.NOME+ "1\" value=\"" + __helper.GET(EXTDAOArquivo.NOME)+ "\">");
		vBuilder.append("            <input type=\"hidden\" name=\"" + EXTDAOArquivo.PATH+ "1\" id=\"" + EXTDAOArquivo.PATH+ "1\" value=\"" + __helper.GET(EXTDAOArquivo.PATH)+ "\">");

		vBuilder.append("    		<table width=\"350\" align=\"center\" class=\"tabela_popup\">");
		
		vBuilder.append("    			<tr>");
		vBuilder.append("    				<td height=\"11\" align=\"left\" valign=\"middle\" class=\"textos5\" style=\"padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;\" >");
		
		vBuilder.append(HelperString.ucFirst( context.getString(R.string.digite_o_novo_nome)));
		
		vBuilder.append("    				</td>");
		vBuilder.append("    			</tr>");
		vBuilder.append("    			<tr>");
		vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

		vBuilder.append( HelperString.ucFirst(context.getString(R.string.nome)) + ": <input type=\"text\" maxlength=\"200\" id=\"novo_nome\" name=\"novo_nome\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" />");

		vBuilder.append("    					<div style=\"padding: 5px 5px 5px 5px; text-align: right;\" >");
		vBuilder.append("    					<input name=\"txtBotao\" type=\"submit\" value=\"" + context.getString(R.string.ok).toUpperCase() + "\" class=\"botoes_form\" style=\"cursor: pointer;\" />" +
				"</div>");
		vBuilder.append("    				</td>");
		vBuilder.append("    			</tr>");
		vBuilder.append("    		</table>");
		vBuilder.append("    	</form>");

		return new ContainerResponse(__helper, vBuilder);
	}

}
