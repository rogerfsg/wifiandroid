package app.omegasoftware.wifitransferpro.appconfiguration;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import app.omegasoftware.wifitransferpro.common.container.programa.ContainerPrograma;

public class OmegaConfiguration {
//
//	public enum TYPE_PROGRAM {
//		OMEGA_EMPRESA, //12 dolar
//		OMEGA_RASTREADOR, //3 dolar
//		OMEGA_MONITORADOR, //3 dolar
//		OMEGA_RASTREADOR_E_MONITORADOR, //6 dolar
//		OMEGA_PONTO_ELETRONICO, //3 dolar
//		OMEGA_PONTO_ELETRONICO_E_MONITORADOR, //6 dolar 
//		OMEGA_CATOLOGO_ONLINE, //2 dolar
//		OMEGA_CATALOGO_OFFLINE, //free
//		OMEGA_CATALOGO_ONLINE_CLIENTE}; //free
//		
	public static final String PREFERENCES_NAME = "WifiTransferPro";
	public static final boolean IS_FREE_VERSION = false;
	public static ContainerPrograma.TYPE_PROGRAM PROGRAM = ContainerPrograma.TYPE_PROGRAM.OMEGA_BEER_GUIDE; 
	/**
	 * Authtoken type string.
	 */

	public static final int STATE_INITIALIZE_DATABASE = 8 ;
	public static final int STATE_AUTHENTICATION = 9 ;
	public static final int STATE_INITIALIZE_PONTO_ELETRONICO = 10 ;
	public static final int STATE_VERIFICA_OCUPACAO_DO_VEICULO = 111 ;	
	
	private static final String TAG = "OmegaConfiguration";
	public static final String SEARCH_LOG_CATEGORY = "SearchActivityError";

	
	//Omega Server
//	public static final String BASE_URL = "http://10.0.0.101/Ponto%20Eletronico/";
	
	//locahost wifi celular
	//public static final String BASE_URL = "http://192.168.43.8/PontoEletronico/";
	//locahost wifi sofia
//	public static final String BASE_URL = "http://192.168.0.154/pontoeletronico/";
	
	public static final String BASE_URL = "http://android.omegasoftware.com.br/";
	
	public static String URL_REQUEST_CHECK_SERVER = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=isServerOnline";
	
	public static String URL_REQUEST_EMAIL_MELHORIA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=sendMelhoriaToEmail";
	public static String URL_REQUEST_EMAIL_ERROR = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=sendErroToEmail";
	public static String URL_REQUEST_CHECK_SET_DATA_FIM_TAREFA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=setDataDeFimDeExcecucaoDaTarefa";
	public static String URL_REQUEST_CHECK_SET_DATA_INICIO_TAREFA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=setDataDeInicioDeExcecucaoDaTarefa";
	public static String URL_REQUEST_CHECK_VEICULO_USUARIO_TAREFA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=checkVeiculoUsuarioDaTarefa";
	public static String URL_REQUEST_ULTIMA_IMAGEM_USUARIO_FOTO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=exportarUltimaImagemDoUsuario";
	public static String URL_REQUEST_QUERY_INSERT = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveQueryAndReturnIdDatabase";
	public static String URL_REQUEST_HISTORICO_MONITORAMENTO_USUARIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=getListIdUsuarioFotoAndroidDatabase";
	public static String URL_REQUEST_CADASTRO_USUARIO_E_CORPORACAO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveCadastroUsuarioECorporacao";
	public static String URL_REQUEST_CADASTRO_USUARIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveCadastroUsuario";
	public static String URL_REQUEST_LEMBRAR_SENHA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=sendSenhaToEmail";
	public static String URL_REQUEST_ALTERAR_SENHA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveAlterarSenha";
	public static String URL_REQUEST_REMOVE_USUARIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveRemocaoUsuario";
	
	public static String URL_REQUEST_EDICAO_USUARIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveEdicaoUsuario";
	public static String URL_REQUEST_IS_EMAIL_DO_USUARIO_EXISTENTE = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=isEmailDoUsuarioExistente";
	
	public static String URL_REQUEST_IS_CORPORACAO_EXISTENTE = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=isCorporacaoExistente";
	public static String URL_REQUEST_CADASTRO_CORPORACAO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveCadastroCorporacao";
	public static String URL_REQUEST_REMOVE_CORPORACAO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=removeCorporacao";
	public static String URL_REQUEST_IS_USUARIO_ADMINISTRADOR_DA_CORPORACAO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=isAdmnistradorDaCorporacao";
	
	
	
	public static String URL_REQUEST_REMOVE_ALL_CATEGORIA_USUARIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=removeAllPermissaoCategoriaPermissao";
	public static String URL_REQUEST_LISTA_INFORMACAO_SINCRONIZADOR = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=sendListaInformacaoSincronizador";
	public static String URL_REQUEST_RECEIVE_LIST_INSERT_AND_RETURN_ID_DATABASE = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveListInsertAndReturnIdDatabase";
	public static String URL_REQUEST_RECEIVE_LIST_UPDATE= BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveListUpdateDatabase";
	public static String URL_REQUEST_SEND_LIST_QUERY_OF_LIST_ID_SINCRONIZADOR = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=sendListQueryOfListIdSincronizador";
	public static String URL_REQUEST_RECEIVE_LISTA_TUPLA_USUARIO_POSICAO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveListaTuplaUsuarioPosicao";
	public static String URL_REQUEST_RECEIVE_LISTA_TUPLA_USUARIO_FOTO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveListaTuplaUsuarioFoto";
	public static String URL_REQUEST_SEND_ULTIMO_ID_SINCRONIZADOR_ANDROID = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=sendUltimoIdSincronizadorAndroid";
	
	
	public static String URL_REQUEST_INFORMACAO_DO_USUARIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveInformacaoUsuairo";
	public static String URL_REQUEST_GET_LISTA_SERVICO_ATIVO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveListaIdServicoAtivo";
	public static String URL_REQUEST_GET_LISTA_POSICAO_VEICULO_DISPONIVEL = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveTabelaVeiculoPosicaoAndroidDatabase";
	public static String URL_REQUEST_TOTAL_TUPLAS_RASTREAR_ROTA_USUARIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=getTotalTuplasRastrearRotaUsuarioAndroidDatabase";
	public static String URL_REQUEST_RASTREAR_ROTA_USUARIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=rastrearRotaUsuarioAndroidDatabase";
	public static String URL_REQUEST_GET_LISTA_TAREFA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveTabelaTarefaAndroidDatabase";

	public static String URL_REQUEST_GET_LISTA_MENSAGEM_SMS_PESSOA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveListaMensagemSMSPessoa";
	public static String URL_REQUEST_SEND_LISTA_MENSAGEM_SMS_PESSOA_COM_FALHA_DE_ENVIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveListaMensagemSMSPessoaComFalhaDeEnvio";
	public static String URL_REQUEST_GET_LISTA_MENSAGEM_SMS = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveListaPessoaMensagemSMS";

	public static String URL_REQUEST_IS_VEICULO_DISPONIVEL = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=isVeiculoDisponivel";
	public static String URL_REQUEST_DISPONIBILIZA_VEICULO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=disponibilizaVeiculo";
	public static String URL_REQUEST_DISPONIBILIZA_TODO_VEICULO_OCUPADO_PELO_USUARIO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=disponibilizaTodoVeiculoOcupadoPeloUsuario";
	
	public static String URL_REQUEST_DISPONIBILIZA_VEICULO_A_FORCA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=disponibilizaVeiculoPorOutroUsuario";
	public static String URL_REQUEST_VETOR_QUERY = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveVetanorQueryTabelaAndroidDatabase";
	public static String URL_REQUEST_VETOR_QUERY_INSERT_AND_GET_LIST_ID = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveVetorQueryInsertAndReturnListIdTabelaDatabase";
	public static String URL_REQUEST_VETOR_QUERY_SELECT_AND_GET_LIST_ID = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveVetorQuerySelectAndReturnListIdTabelaDatabase";
	
	public static String URL_REQUEST_RECEIVE_LIST_REMOVE_DATABASE = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveListRemoveDatabase";
	
	public static String URL_REQUEST_UPDATE_OPERADORA_PESSOA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=updateOperadoraPessoa";
	public static String URL_REQUEST_UPDATE_OPERADORA_EMPRESA = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=updateOperadoraEmpresa";
	
	public static String URL_REQUEST_LAST_TUPLA_USUARIO_FOTO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveLastTuplaUsuarioFoto";
	
	public static String URL_REQUEST_QUERY = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveTabelaAndroidDatabase";
	public static String URL_REQUEST_FILE_ZIPPER_VETOR_QUERY = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=getListIdOfReceiveZipFileQueryInsert";
	public static String URL_REQUEST_FILE_ZIPPER_QUERY = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveZipFileQuery";
	public static String URL_REQUEST_SEND_FILE_PHOTO = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=receiveFoto";
	public static String URL_REQUEST_RECEIVE_FILE = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=exportarImagemUsuarioFoto";
	public static String URL_REQUEST_SINCRONIZA_TABELA_ANDROID_DATABASE = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=sincronizaTabelaAndroidDatabase";
	public static String URL_REQUEST_SINCRONIZA_TABELA_USUARIO_ANDROID_DATABASE = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=getStrQueryInsertUsuarioMultipleAndroidDatabase";
	public static String URL_REQUEST_SINCRONIZA_TABELA_CORPORACAO_ANDROID_DATABASE = BASE_URL + "adm/actions.php5?class=DatabaseSincronizador&action=sincronizaTabelaCorporacaoAndroidDatabase";
	
	
	public static int NUMERO_TENTATIVAS = 3;
	
		
	
	public static final String DELIMITER_QUERY_UNIQUE_LINHA = "[+!+]";
	public static final String DELIMITER_QUERY_UNIQUE_COLUNA = "[%%jQuery%%]";
	public static final String DELIMITER_QUERY_LINHA = "+!+";
	public static final String DELIMITER_QUERY_COLUNA = "%%jQuery%%";
	
	public static final String CODIGO_DUPLICADA = "-2";
	public static final String CODIGO_INEXISTENTE = "-3";
	public static final String NULL = "null";
		
	//Some DB constants
	public static final String UNEXISTENT_ID_IN_DB = "-9191";
	public static final String MASCULIN_CODE_IN_DB = "1";
	public static final String FEMININ_CODE_IN_DB = "2";
	public static final String COD_TIPO_PRESTADOR_MEDIC = "6";

	public static final int ON_LOAD_DIALOG = -3;
	public static final int SEARCH_NO_RESULTS_DIALOG = -1;
	public static final int FORM_ERROR_DIALOG_INFORMACAO_DIALOG = -4;
	public static final int FORM_ERROR_DIALOG_INFORMACAO_DIALOG_ENDERECO = -5;
	public static final int FORM_ERROR_DIALOG_TELEFONE_OFFLINE = -6;
	public static final int FORM_ERROR_DIALOG_SERVER_OFFLINE = -7;
	public static final int FORM_ERROR_DIALOG_CANT_TRACE_ROUT_TOO_FAR = -8;
	
//mode=walking
	
	//Bar title TypeFace
	public static final String OMEGA_BAR_TITLE_FF = "UnimedDemiBold.ttf";

	//Intent extras
	
	public static final String MEDIC_ID = "MEDIC_ID";
	public static final String FUNCIONARIO_ID = "FUNCIONARIO_ID";

	//FuncionarioTakePictureActivity
	public static final String STATE_ID = "FuncionarioTakePictureActivity_STATE_ID";

	public static final int STATE_READ_QR_CODE = 1 ;
	
	public static final int STATE_FUNCIONARIO_TAKE_PICTURE = 3 ;
	public static final int STATE_FUNCIONARIO_DETAIL = 4 ;
	public static final int STATE_PONTO_ELETRONICO_MANUAL = 5 ;
	
	
	public static final int ACTIVITY_FORM_CONFIRMACAO_CADASTRO_FILTRO = 73 ;
	public static final int ACTIVITY_FORM_CATEGORIA_PERMISSAO = 74 ;
	public static final int ACTIVITY_MAPA_MULTIPLE_MARKER = 75 ;
	public static final int ACTIVITY_LIST_VEICULO_USUARIO_ATIVO = 77 ;
	public static final int ACTIVITY_FORM_PONTO = 78 ;
	public static final int ACTIVITY_FILTER_TRACAR_ROTA = 79;
	public static final int ACTIVITY_CONFIGURACAO = 80;
	
	
	public static final int RESULT_CODE_TRACA_ROTA_CARRO = 90 ;
	public static final int RESULT_CODE_TRACA_ROTA_A_PE = 91 ;
	public static final int RESULT_CODE_TRACA_ROTA_A_TRANSPORTE_PUBLICO = 92 ;
	public static final int RESULT_CODE_PONTO_ORIGEM = 93 ;
	public static final int RESULT_CODE_FOCO_PONTO = 94 ;
	public static final int RESULT_PORTA = 95 ;
	
	//Service Connection Information Service
	public static final String REFRESH_SERVICE_STATUS_SERVER = "REFRESH_SERVICE_STATUS_SERVER";
	
	public static final String REFRESH_SERCH_FILE_END_SEARCH = "REFRESH_SERCH_FILE_END_SEARCH";
	public static final String REFRESH_SERVICE_APACHE_END = "REFRESH_SERVICE_APACHE_END";
	
	public static final String REFRESH_UI_INTENT = "REFRESH_UI_INTENT";
	public static final String REFRESH_SERVICE_RECEIVE_FILE_OF_SERVER = "REFRESH_SERVICE_RECEIVE_FILE_OF_SERVER";
	public static final String REFRESH_ACTIVITY_MONITORAMENTO_REMOTO_USUARIO = "REFRESH_ACTIVITY_MONITORAMENTO_REMOTO_USUARIO";
	
//	SERVICOS SMS
	public static final String REFRESH_ACTIVITY_SEND_ERROR_MENSAGEM_PESSOA_SERVIDOR = "REFRESH_ACTIVITY_SEND_ERROR_MENSAGEM_PESSOA_SERVIDOR";
	public static final String REFRESH_ACTIVITY_RECEIVE_MENSAGEM_SMS_SERVIDOR = "REFRESH_ACTIVITY_RECEIVE_MENSAGEM_SMS_SERVIDOR";
	public static final String REFRESH_ACTIVITY_SEND_SMS = "REFRESH_ACTIVITY_SEND_SMS";
	public static final String REFRESH_ACTIVITY_RECEIVE_SMS = "REFRESH_ACTIVITY_RECEIVE_SMS";
	public static final int STATE_RECEIVE_SMS = 301;
	
	public static final String REFRESH_SERVICE_LAST_USUARIO_FOTO = "REFRESH_SERVICE_LAST_TUPLA_USUARIO_FOTO";
//	VeiculoGPSLocationListener
	public static final String REFRESH_SERVICE_VEICULO_GPS_LOCATION_LISTENER = "REFRESH_SERVICE_VEICULO_GPS_LOCATION_LISTENER";
	
	public static final String REFRESH_SERVICE_RECEIVE_USUARIO_RASTREAMENTO_POSICAO = "REFRESH_SERVICE_RECEIVE_USUARIO_RASTREAMENTO_POSICAO";
	
	
	public static final String REFRESH_SERVICE_STATUS_SERVICO = "REFRESH_SERVICE_STATUS_SERVICO";
	public static final String REFRESH_SERVICE_RECEIVE_SERVICE_STATUS_ATIVO = "REFRESH_SERVICE_RECEIVE_SERVICE_STATUS_ATIVO";
	public static final String REFRESH_SERVICE_RECEIVE_SERVICE_STATUS_INATIVO = "REFRESH_SERVICE_RECEIVE_SERVICE_STATUS_inATIVO";
	
//	Veiculo Map Activity Map
	public static final String REFRESH_PONTO_MARCADO = "REFRESH_PONTO_MARCADO";
	
	public static final String REFRESH_SERVICE_LIST_POSITION_OTHER_CAR = "REFRESH_SERVICE_LIST_POSITION_OTHER_CAR";
	public static final String REFRESH_SERVICE_LIST_POSITION_TAREFA = "REFRESH_SERVICE_LIST_POSITION_TAREFA";
	
	public static final String REFRESH_SERVICE_TRACA_ROTA = "REFRESH_SERVICE_LIST_POSITION_TAREFA";
	
	public static final String SEARCH_FIELD_IP = "SEARCH_FIELD_IP";
	
	
	public static final String SEARCH_FIELD_VETOR_NOME = "SEARCH_FIELD_VETOR_NOME";
	public static final String SEARCH_FIELD_VETOR_EXTENSAO = "SEARCH_FIELD_VETOR_EXTENSAO";
	public static final String SEARCH_FIELD_PATH = "SEARCH_FIELD_PATH";
	
	
	public static final String SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO = "SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO";
	public static final String SEARCH_FIELD_NOME_TIPO_DOCUMENTO = "SEARCH_FIELD_NOME_TIPO_DOCUMENTO";
	public static final String SEARCH_FIELD_NOME_TIPO_EMPRESA = "SEARCH_FIELD_NOME_TIPO_EMPRESA";
	
	
	public static final String REFRESH_SERVICE_MY_POSITION_OTHER_CAR = "REFRESH_SERVICE_MY_POSITION_OTHER_CAR";
	public static final String SEARCH_FIELD_IS_ATIVO = "SEARCH_FIELD_IS_ATIVO";
	public static final String SEARCH_FIELD_TOTAL_TUPLAS_BAIXADAS = "SEARCH_FIELD_TOTAL_TUPLAS_BAIXADAS";
	public static final String SEARCH_LIST_LATITUDE ="SEARCH_LIST_LATITUDE";
	public static final String SEARCH_LIST_LONGITUDE = "SEARCH_LIST_LONGITUDE";
	public static final int STATE_RASTREAR_CARRO = 6 ;
	public static final int STATE_RASTREAR_TAREFA = 66 ;
	public static final int STATE_TRACAR_ROTA = 68 ;
	public static final int STATE_RECARREGAR_ADAPTER = 69 ;
	public static final int ACTIVITY_FILTER_VEICULO_USUARIO = 667 ;
	
	
	public static final String SEARCH_FIELD_IS_FLOAT = "SEARCH_FIELD_IS_FLOAT";
	public static final String SEARCH_FIELD_SERVICO = "SEARCH_FIELD_SERVICO";
	
//	Timing Configuration
	public static final long SERVICE_TIRA_FOTO_DELAY = 60000;
	public static final long SERVICE_TIRA_FOTO_INITIAL_DELAY = 60000;
	
	public static final long INITIAL_INFORMATION_PONTO_ELETRONICO= 5000;
	public static final long INFORMATION_PONTO_ELETRONICO_TIMER = 1000;	
	
	public static final long SERVICE_RECEIVE_USUARIO_POSICAO_TIMER_INTERVAL = 2000;
	public static final long SERVICE_RECEIVE_USUARIO_POSICAO_INITIAL_DELAY = 50000;
//	30 seg
	public static final long SERVICE_TIRA_FOTO_TIMER_INTERVAL = 30000;
	
	public static final long LOAD_LIST_TUPLA_INITIAL_DELAY = 30000;
//	30 seg
	public static final long LOAD_LIST_TUPLA_TIMER_INTERVAL = 5000;
	
	public static final long LOAD_TELA_MONITORAMENTO_INITIAL_DELAY = 3000;
//	30 seg
	public static final long LOAD_TELA_MONITORAMENTO_TIMER_INTERVAL = 30000;

	public static final long LOAD_IMAGE_VIEW_OFFLINE_INITIAL_DELAY = LOAD_LIST_TUPLA_TIMER_INTERVAL;
	public static final long LOAD_IMAGE_VIEW_OFFLINE_TIMER_INTERVAL = LOAD_LIST_TUPLA_TIMER_INTERVAL;
	
	public static final long LOAD_IMAGE_VIEW_ONLINE_INITIAL_DELAY = 50000;
	public static final long LOAD_IMAGE_VIEW_ONLINE_TIMER_INTERVAL = 30000;
	
	public static final long SERVICE_POSITION_TAREFA_INITIAL_DELAY = 1000;
//	60 seg
	public static final long SERVICE_POSITION_TAREFA_TIMER_INTERVAL = 60000;
	public static final long SERVICE_POSITION_OTHER_CAR_INITIAL_DELAY = 2000;
	
	public static final long SERVICE_SEND_PONTO_MARCADO_TIMER_INTERVAL = 60000;
	public static final long SERVICE_SEND_PONTO_MARCADO_INITIAL_DELAY = 2000;
	
	public static final long SERVICE_CHECK_FOTO_TIMER_INTERVAL = 60000;
	public static final long SERVICE_CHECK_FOTO_INITIAL_DELAY = 2000;
	
	public static final long SERVICE_SYNCHRONIZE_FAST_DATABASE_TIMER_INTERVAL = 100000;
	public static final long SERVICE_SYNCHRONIZE_FAST_DATABASE_INITIAL_DELAY = 20000;
//	5 seg
	public static final long SERVICE_POSITION_OTHER_CAR_TIMER_INTERVAL = 5000;
	
	public static final long SERVICE_SEND_FILE_TO_SERVER_INITIAL_DELAY = 40000;
	public static final long SERVICE_SEND_FILE_TO_SERVER_TIMER_INTERVAL = 50000;
	
	public static final long SERVICE_SEND_MENSAGE_USUARIO_INITIAL_DELAY = 70000;
	public static final long SERVICE_SEND_MENSAGE_USUARIO_TIMER_INTERVAL = 500000;
	
	public static final long SERVICE_POSITION_MY_CAR_INITIAL_DELAY = 2000;
	public static final long SERVICE_POSITION_MY_CAR_TIMER_INTERVAL = 50000;
	public static final long SERVICE_CHECK_LIST_ATIVE_SERVICE_OF_USER_INITIAL_DELAY= 2000;
	public static final long SERVICE_CHECK_LIST_ATIVE_SERVICE_OF_USER_INTERVAL = 60000;
	
	public static final long SERVICE_CHECK_SERVER_ONLINE = 1000;
//	10 seg - Checa se esta online
	public static final long SERVICE_CHECK_SERVER_ONLINE_INTERVAL = 60000;
	
//	SMS SERVICES
	public static final long SEND_ERROR_SMS_MENSAGEM_PESSOA_SERVIDOR_INITIAL_DELAY = 30000;
	public static final long SEND_ERROR_SMS_MENSAGEM_PESSOA_SERVIDOR_TIMER_INTERVAL = 30000;
	
	public static final long RECEIVE_MENSAGEM_SMS_SERVIDOR_INITIAL_DELAY = 30000;
	public static final long RECEIVE_MENSAGEM_SMS_SERVIDOR_TIMER_INTERVAL = 30000;
	//Form empresa
	public static final int ACTIVITY_FORM_EMPRESA_FUNCIONARIO = 4 ;
	public static final int ACTIVITY_FORM_FUNCIONARIO = 5 ;
	public static final int ACTIVITY_FORM_ENDERECO = 6 ;
	public static final int ACTIVITY_FORM_TECLADO_TELEFONE = 306 ;
	public static final int ACTIVITY_FORM_TECLADO_CELULAR = 307 ;
	public static final int ACTIVITY_FORM_TECLADO_CELULAR_SMS = 308 ;
	public static final int ACTIVITY_FORM_TECLADO_TELEFONE_1 = 309 ;
	public static final int ACTIVITY_FORM_TECLADO_TELEFONE_2 = 310 ;
	public static final int ACTIVITY_FORM_TECLADO_FAX = 311 ;
	public static final int ACTIVITY_FORM_TECLADO_PORTA = 312 ;

	
	public static final String SEARCH_FIELD_TECLADO_TELEFONE_ORIGEM_CHAMADO = "SEARCH_FIELD_TECLADO_TELEFONE_ORIGEM_CHAMADO" ;
	
	public static final int ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO = 7 ;
	public static final int ACTIVITY_FORM_ADD_LAYOUT_ENDERECO = 16 ;
	public static final int ACTIVITY_FORM_EDIT = 201 ;
	public static final int ACTIVITY_FORM_EDIT_DELETE = 210 ;
	public static final int ACTIVITY_FORM_EDIT_EDIT = 211 ;
	public static final int ACTIVITY_SETTING_SAIR = 202 ;
	public static final int ACTIVITY_SETTING_AJUDA = 203 ;
	public static final int ACTIVITY_LIST_MAP = 223 ;
	
	//Visibilities
	public static final int VISIBILITY_HIDDEN = 8;
	public static final int VISIBILITY_VISIBLE = 0;
	public static final int VISIBILITY_INVISIBLE = 4;
	

	//Order mode
	public static final short ORDER_BY_MODE_A_Z = 1;
	public static final short ORDER_BY_MODE_Z_A = 2;
	public static final short ORDER_BY_MODE_NEAR_ME = 3;
	public static final short DEFAULT_ORDER_BY_MODE = ORDER_BY_MODE_NEAR_ME;

	//Search modes
	public static final short SEARCH_MODE_EMPRESA = 1;
	public static final short SEARCH_MODE_USUARIO = 7;
	public static final short SEARCH_MODE_ENTIDADE = 3;
	public static final short SEARCH_MODE_MAP_ENTIDADE = 4;
	
	public static final short SEARCH_MODE_VEICULO = 4;
	public static final short SEARCH_MODE_VEICULO_USUARIO = 5;
	public static final short SEARCH_MODE_VEICULO_USUARIO_ATIVO = 6;
	public static final short SEARCH_MODE_ESTADO = 10;
	public static final short SEARCH_MODE_PAIS = 13;
	public static final short SEARCH_MODE_CIDADE = 11;
	public static final short SEARCH_MODE_BAIRRO = 12;
	public static final short SEARCH_MODE_CORPORACAO = 17;
	public static final short SEARCH_MODE_PROFISSAO = 18;
	public static final short SEARCH_MODE_TIPO_DOCUMENTO = 19;
	public static final short SEARCH_MODE_TIPO_EMPRESA = 20;
	public static final short SEARCH_MODE_MODELO = 21;
	public static final short DEFAULT_SEARCH_MODE = SEARCH_MODE_ENTIDADE;


	//Tipo local values
	public static final short TIPO_LOCAL_CLINICAS = 1;
	public static final short TIPO_LOCAL_HOSPITAIS = 2;

	//Seu plano options
	public static final int UNIFACIL_PLANO_ID_IN_DB = 0;

	//Cadastro modes
	public static final short FORM_MODE_EMPRESA = 4;
	public static final short FORM_MODE_FUNCIONARIO = 5;
	public static final short DEFAULT_FORM_MODE = FORM_MODE_FUNCIONARIO;

	//Search field names
	
	
	
	
	public static final String SEARCH_FIELD_LAYOUT = "SEARCH_FIELD_LAYOUT";
	
	public static final String SEARCH_FIELD_NOME_PROFISSAO = "SEARCH_FIELD_NOME_PROFISSAO";
	public static final String SEARCH_FIELD_IS_MINHA_PROFISSAO = "SEARCH_FIELD_IS_MINHA_PROFISSAO";
	
	public static final String SEARCH_FIELD_IDENTIFICADOR = "SEARCH_FIELD_IDENTIFICADOR";
	public static final String SEARCH_FIELD_NOME = "SEARCH_FIELD_NOME";
	public static final String SEARCH_FIELD_ID_TIPO_ENTIDADE = "SEARCH_FIELD_ID_TIPO_ENTIDADE";
	
	public static final String SEARCH_FIELD_NOME_TIPO_ENTIDADE = "SEARCH_FIELD_NOME_TIPO_ENTIDADE";
	public static final String SEARCH_FIELD_CPF = "SEARCH_FIELD_CIDADE";
	public static final String SEARCH_FIELD_RG = "SEARCH_FIELD_BAIRRO";
	public static final String SEARCH_FIELD_SEXO = "SEARCH_FIELD_SEXO_FUNCIONARIO";
	public static final String SEARCH_FIELD_TELEFONE = "SEARCH_FIELD_TELEFONE";
	public static final String SEARCH_FIELD_NUMERO_TELEFONE = "SEARCH_FIELD_NUMERO_TELEFONE";
	public static final String SEARCH_FIELD_CELULAR = "SEARCH_FIELD_CELULAR";
	
	public static final int LIMIT_TUPLA_RECEIVE_FROM_SERVER_PHP = 10;
	
	public static final String SEARCH_FIELD_ID_ESTADO = "SEARCH_FIELD_ID_ESTADO";
	public static final String SEARCH_FIELD_ID_PAIS = "SEARCH_FIELD_ID_PAIS";
	
	
	/**
	 * 
	 */
	public static final String SEARCH_FIELD_NOME_CIDADE = "SEARCH_FIELD_NOME_CIDADE";
	public static final String SEARCH_FIELD_NOME_BAIRRO = "SEARCH_FIELD_NOME_BAIRRO";
	public static final String SEARCH_FIELD_NOME_ESTADO = "SEARCH_FIELD_NOME_ESTADO";
	public static final String SEARCH_FIELD_NOME_PAIS = "SEARCH_FIELD_NOME_PAIS";

	
	//Detail Tarefa
	public static final String SEARCH_FIELD_NOME_TAREFA = "SEARCH_FIELD_NOME_TAREFA";
	
	public static final String SEARCH_FIELD_LATITUDE_ORIGEM = "SEARCH_FIELD_LATITUDE_ORIGEM";
	public static final String SEARCH_FIELD_LONGITUDE_ORIGEM = "SEARCH_FIELD_LONGITUDE_ORIGEM";
	
	public static final String SEARCH_FIELD_LATITUDE_DESTINO = "SEARCH_FIELD_LATITUDE_DESTINO";
	public static final String SEARCH_FIELD_LONGITUDE_DESTINO = "SEARCH_FIELD_LONGITUDE_DESTINO";
	
	public static final String SEARCH_FIELD_ENDERECO_ORIGEM = "SEARCH_FIELD_ENDERECO_ORIGEM";
	public static final String SEARCH_FIELD_ENDERECO_DESTINO = "SEARCH_FIELD_ENDERECO_DESTINO";
		
	public static final String SEARCH_FIELD_DATETIME_CADASTRO = "SEARCH_FIELD_DATETIME_CADASTRO";
	public static final String SEARCH_FIELD_DATE_ABERTURA = "SEARCH_FIELD_DATE_ABERTURA";
	
	public static final String SEARCH_FIELD_DATETIME_INICIO_PROGRAMADO_TAREFA = "SEARCH_FIELD_DATETIME_INICIO_PROGRAMADO_TAREFA";
	
	public static final String SEARCH_FIELD_DATA_INICIO_TAREFA = "SEARCH_FIELD_DATA_INICIO_TAREFA";
	public static final String SEARCH_FIELD_HORA_INICIO_TAREFA = "SEARCH_FIELD_HORA_INICIO_TAREFA";
	public static final String SEARCH_FIELD_DATA_FIM_TAREFA = "SEARCH_FIELD_DATA_FIM_TAREFA";
	public static final String SEARCH_FIELD_HORA_FIM_TAREFA = "SEARCH_FIELD_HORA_FIM_TAREFA";
	
	
	
	public static final String SEARCH_FIELD_ENDERECO_LAYOUT = "SEARCH_FIELD_ENDERECO_LAYOUT";
	public static final String SEARCH_FIELD_DATETIME_INICIO_TAREFA = "SEARCH_FIELD_DATETIME_INICIO_TAREFA";
	public static final String SEARCH_FIELD_DATETIME_FIM_TAREFA = "SEARCH_FIELD_DATETIME_FIM_TAREFA";
	
	public static final String SEARCH_FIELD_LATITUDE_PROPRIA = "SEARCH_FIELD_LATITUDE_PROPRIA";
	public static final String SEARCH_FIELD_LONGITUDE_PROPRIA = "SEARCH_FIELD_LONGITUDE_PROPRIA";
	
	
	//Funcionario Take Picture
	public static final String SEARCH_FUNCIONARIO_ID = "SEARCH_FUNCIONARIO_ID";
	public static final String SEARCH_FIELD_IS_ENTRADA = "SEARCH_FIELD_IS_ENTRADA";
	public static final String SEARCH_NEXT_ACTIVITY = "SEARCH_NEXT_ACTIVITY";
	
	
	public static final String SEARCH_LIST_MAP_ICONE = "SEARCH_LIST_MAP_ICONE";
	
	//Map fied layout
	public static final String MAP_FIELD_ID = "MAP_FIELD_ID";
	public static final String MAP_ADDRESS = "MAP_ADDRESS";
	public static final String MAP_GEOPOINT_LATITUDE = "MAP_GEOPOINT_LATITUDE";
	public static final String MAP_GEOPOINT_LONGITUDE = "MAP_GEOPOINT_LONGITUDE";
	
	public static final String MAP_TYPE_LOCALIZATION = "MAP_TYPE_LOCALIZATION"; 
	
	//Search field names
	
	
	public static final String SEARCH_FIELD_ID = "SEARCH_FIELD_ID";
	
	public static final String SEARCH_FIELD_TAG = "SEARCH_FIELD_TAG";
	public static final String SEARCH_FIELD_NOME_TABELA = "SEARCH_FIELD_NOME_TABELA";
	public static final String SEARCH_FIELD_TABELA = "SEARCH_FIELD_TABELA";
	public static final String SEARCH_FIELD_CLASS_LIST = "SEARCH_FIELD_CLASS_LIST";
	public static final String SEARCH_FIELD_CLASS_DETAIL = "SEARCH_FIELD_CLASS_DETAIL";
	public static final String SEARCH_FIELD_IS_REMOVE_PERMITED = "SEARCH_FIELD_IS_REMOVE_PERMITED";
	public static final String SEARCH_FIELD_IS_EDIT_PERMITED = "SEARCH_FIELD_IS_EDIT_PERMITED";
	
	public static final String SEARCH_FIELD_CLASS_CADASTRO = "SEARCH_FIELD_CLASS_CADASTRO";
	public static final String SEARCH_FIELD_TAG_CADASTRO = "SEARCH_FIELD_TAG_CADASTRO";
	public static final String SEARCH_FIELD_CLASS_FILTRO = "SEARCH_FIELD_CLASS_FILTRO";
	public static final String SEARCH_FIELD_TAG_FILTRO = "SEARCH_FIELD_TAG_FILTRO";
	
	public static final String SEARCH_FIELD_MESSAGE = "SEARCH_FIELD_MESSAGE";
	public static final String SEARCH_FIELD_ENTIDADE = "SEARCH_FIELD_ENTIDADE";
	public static final String SEARCH_FIELD_IS_MASCULINO = "SEARCH_FIELD_IS_MASCULINO";
	public static final int STATE_FORM_CONFIRMACAO_SAIR_CADASTRO = 222;
	public static final int STATE_FORM_CONFIRMACAO_DELETAR_REGISTRO = 232;
	public static final int STATE_FORM_CONFIRMACAO = 214;
	public static final int STATE_FORM_CONFIRMACAO_LOGOUT = 242;
	public static final int STATE_FORM_CONFIRMACAO_LEMBRAR_SENHA = 220;
	public static final int STATE_FORM_CONFIRMACAO_PRIMEIRO_LOGIN = 339;
	public static final int STATE_FORM_CONFIRMACAO_PONTO = 340;
	public static final int STATE_FORM_MARCA_PONTO = 341;
	
	public static final int STATE_FORM_CONFIRMACAO_CADASTRO_GENERICO = 340;
	public static final int STATE_FORM_CONFIRMACAO_FILTRO_GENERICO = 341;
	
	public static final int STATE_FORM_CONFIRMACAO_CADASTRO_OU_FILTRO_EMPRESA = 342;
	public static final int STATE_FORM_CADASTRA_EMPRESA = 343;
	public static final int STATE_SEND_FOTO = 238;
	public static final int STATE_FORM_CONFIRMACAO_REMOVER_CORPORACAO = 215;
	public static final int STATE_FORM_CONFIRMACAO_LIMPAR_DADOS = 216;
	public static final String SEARCH_FIELD_IS_CONFIRMADO = "SEARCH_FIELD_IS_CONFIRMADO";
	
	
	public static final String SEARCH_FIELD_TYPE_REMOVE = "SEARCH_FIELD_TYPE_REMOVE";
	public static final String SEARCH_FIELD_IS_EDIT = "SEARCH_FIELD_IS_EDIT";
	public static final String SEARCH_FIELD_IS_MODIFICATED = "SEARCH_FIELD_IS_MODIFICATED";
	
	public static final String SEARCH_FIELD_VETOR_ID = "SEARCH_FIELD_VETOR_ID";
	public static final String SEARCH_FIELD_VETOR_FOTO_USUARIO_ID = "SEARCH_FIELD_VETOR_FOTO_USUARIO_ID";
	public static final String SEARCH_FIELD_VETOR_FOTO_USUARIO_DATA_HORA = "SEARCH_FIELD_VETOR_FOTO_USUARIO_DATA_HORA";
	public static final String SEARCH_FIELD_IS_CAMERA_INTERNA = "SEARCH_FIELD_IS_CAMERA_INTERNA";
	public static final String SEARCH_FIELD_PLANO = "SEARCH_FIELD_PLANO";
	
	public static final String SEARCH_FIELD_ID_PROFISSAO = "SEARCH_FIELD_PROFISSAO";
	public static final String SEARCH_FIELD_NOME_FUNCIONARIO = "SEARCH_FIELD_NOME_FUNCIONARIO";
	public static final String SEARCH_FIELD_SEXO_FUNCIONARIO = "SEARCH_FIELD_SEXO_FUNCIONARIO";
	public static final String SEARCH_FIELD_IS_CONSUMIDOR = "SEARCH_FIELD_IS_CONSUMIDOR";
	public static final String SEARCH_FIELD_SETOR = "SEARCH_FIELD_SETOR";
	public static final String SEARCH_FIELD_ID_TIPO_EMPRESA = "SEARCH_FIELD_ID_TIPO_EMPRESA";
	public static final String SEARCH_FIELD_NOME_EMPRESA = "SEARCH_FIELD_NOME_EMPRESA";
	public static final String SEARCH_FIELD_LIST_USUARIO_VEICULO = "SEARCH_FIELD_LIST_USUARIO_VEICULO";

	
	public static final String SEARCH_FIELD_VELOCIDADE = "SEARCH_FIELD_VELOCIDADE";
	public static final String SEARCH_FIELD_DATE_TIME = "SEARCH_FIELD_DATE_TIME";
	public static final String SEARCH_FIELD_LONGITUDE = "SEARCH_FIELD_LONGITUDE";
	
	public static final String SEARCH_FIELD_LATITUDE = "SEARCH_FIELD_LATITUDE";
	public static final String SEARCH_FIELD_RESULT_CODE_TIPO_ROTA = "SEARCH_FIELD_RESULT_CODE_TIPO_ROTA";
	
	
	
	public static final String SEARCH_FIELD_IS_EMPTY = "SEARCH_FIELD_IS_EMPTY";
	public static final String SEARCH_FIELD_IS_SMS_ENVIADO_COM_SUCESSO = "SEARCH_FIELD_IS_SMS_ENVIADO_COM_SUCESSO";
	
//	MapaFixedListMarker
//	public static final String SEARCH_FIELD_FOCO_LONGITUDE = "SEARCH_FIELD_FOCO_LONGITUDE";
//	public static final String SEARCH_FIELD_FOCO_LATITUDE = "SEARCH_FIELD_FOCO_LATITUDE";
//	public static final String SEARCH_FIELD_FOCO_MARKER = "SEARCH_FIELD_FOCO_MARKER";
	public static final String SEARCH_FIELD_LIST_LONGITUDE = "SEARCH_FIELD_LIST_LONGITUDE";
	public static final String SEARCH_FIELD_LIST_LATITUDE = "SEARCH_FIELD_LIST_LATITUDE";
	public static final String SEARCH_FIELD_LIST_MARKER = "SEARCH_FIELD_LIST_MARKER";
	public static final String SEARCH_FIELD_MARKER = "SEARCH_FIELD_MARKER";
	public static final String SEARCH_FIELD_LIST_LONGITUDE_ROTA = "SEARCH_FIELD_LIST_LONGITUDE_ROTA";
	public static final String SEARCH_FIELD_LIST_LATITUDE_ROTA = "SEARCH_FIELD_LIST_LATITUDE_ROTA";
	public static final String SEARCH_FIELD_LIST_COLOR_ROTA = "SEARCH_FIELD_LIST_COLOR_ROTA";
	public static final String SEARCH_FIELD_INDEX_FOCO = "SEARCH_FIELD_INDEX_FOCO";
	
	public static final String SEARCH_FIELD_NOME_CORPORACAO = "SEARCH_FIELD_NOME_CORPORACAO";
	public static final String SEARCH_FIELD_IS_MINHA_CORPORACAO = "SEARCH_FIELD_IS_MINHA_CORPORACAO";
	
	public static final String SEARCH_FIELD_STATUS_SERVER = "SEARCH_FIELD_STATUS_SERVER";
	
	
	public static final String SEARCH_MODE = "SEARCH_MODE";
	public static final String FORM_MODE = "FORM_MODE";
	public static final String ORDER_BY_MODE = "ORDER_BY_MODE";
	public static final String SEARCH_FIELD_PRESTADOR_ENDERECO = "SEARCH_FIELD_PRESTADOR_ENDERECO";

	public static final String SEARCH_FIELD_USUARIO = "SEARCH_FIELD_USUARIO";
	public static final String SEARCH_FIELD_VEICULO = "SEARCH_FIELD_VEICULO";
	public static final String SEARCH_FIELD_VEICULO_USUARIO = "SEARCH_FIELD_FUNCIONARIO_VEICULO";
	public static final String SEARCH_FIELD_IS_DISPLAY_ROTA = "SEARCH_FIELD_IS_DISPLAY_ROTA";
	public static final String SEARCH_FIELD_IS_TO_FOCO = "SEARCH_FIELD_IS_TO_FOCO";
	public static final String SEARCH_FIELD_TYPE_MONITORAMENTO = "SEARCH_FIELD_TYPE_MONITORAMENTO";
	public static final String SEARCH_FIELD_TAREFA = "SEARCH_FIELD_TAREFA";
//	public static final String SEARCH_FIELD_IS_TAREFA_ABERTA = "SEARCH_FIELD_IS_TAREFA_ABERTA";
	public static final String SEARCH_FIELD_TYPE_TAREFA = "SEARCH_FIELD_TYPE_TAREFA_ONLINE";
	
	public static final String SEARCH_FIELD_STATUS = "SEARCH_FIELD_STATUS";
	public static final String SEARCH_FIELD_IS_ADM = "SEARCH_FIELD_IS_ADM";
	
	public static final String SEARCH_FIELD_EMAIL = "SEARCH_FIELD_EMAIL";
	public static final String SEARCH_FIELD_CATEGORIA_PERMISSAO = "SEARCH_FIELD_CATEGORIA_PERMISSAO";
	public static final String SEARCH_FIELD_NOME_USUARIO = "SEARCH_FIELD_NOME_USUARIO";
	public static final String SEARCH_FIELD_NOME_MODELO = "SEARCH_FIELD_NOME_MODELO";
	public static final String SEARCH_FIELD_ANO_MODELO = "SEARCH_FIELD_ANO_MODELO";
	
	public static final String SEARCH_FIELD_PLACA = "SEARCH_FIELD_PLACA";
	
	public static final String SEARCH_ENDERECO_ORIGEM = "SEARCH_ENDERECO_ORIGEM";
	public static final String SEARCH_DATA_CADASTRO_TAREFA = "SEARCH_DATA_CADASTRO_TAREFA";
	public static final String SEARCH_NOME_TAREFA = "SEARCH_NOME_TAREFA";

	
	//Filter Tarefa
	
	public static final String SEARCH_FIELD_DESTINO_LOGRADOURO = "SEARCH_FIELD_DESTINO_LOGRADOURO";
	public static final String SEARCH_FIELD_DESTINO_ID_BAIRRO = "SEARCH_FIELD_DESTINO_ID_BAIRRO";
	public static final String SEARCH_FIELD_DESTINO_ID_CIDADE = "SEARCH_FIELD_DESTINO_ID_CIDADE";
	public static final String SEARCH_FIELD_DESTINO_NUMERO = "SEARCH_FIELD_DESTINO_NUMERO";
	public static final String SEARCH_FIELD_DESTINO_COMPLEMENTO = "SEARCH_FIELD_DESTINO_COMPLEMENTO";
	
	public static final String SEARCH_FIELD_ORIGEM_LOGRADOURO = "SEARCH_FIELD_ORIGEM_LOGRADOURO";
	public static final String SEARCH_FIELD_ORIGEM_ID_BAIRRO = "SEARCH_FIELD_ORIGEM_ID_BAIRRO";
	public static final String SEARCH_FIELD_ORIGEM_ID_CIDADE = "SEARCH_FIELD_ORIGEM_ID_CIDADE";
	public static final String SEARCH_FIELD_ORIGEM_NUMERO = "SEARCH_FIELD_ORIGEM_NUMERO";
	public static final String SEARCH_FIELD_ORIGEM_COMPLEMENTO = "SEARCH_FIELD_ORIGEM_COMPLEMENTO";
	
	public static final String SEARCH_FIELD_IS_PARCEIRO = "SEARCH_FIELD_IS_PARCEIRO";
	public static final String SEARCH_FIELD_IS_CLIENTE = "SEARCH_FIELD_IS_CLIENTE";
	public static final String SEARCH_FIELD_IS_FORNECEDOR = "SEARCH_FIELD_IS_FORNECEDOR";
	
	
	public static final String SEARCH_FIELD_ID_EMPRESA = "SEARCH_FIELD_ID_EMPRESA";
	public static final String SEARCH_FIELD_NOME_PESSOA = "SEARCH_FIELD_NOME_PESSOA";
	public static final String SEARCH_FIELD_ID_PESSOA = "SEARCH_FIELD_ID_PESSOA";
	
	public static final String SEARCH_FIELD_LOGRADOURO = "SEARCH_FIELD_LOGRADOURO";
	public static final String SEARCH_FIELD_ID_BAIRRO = "SEARCH_FIELD_ID_BAIRRO";
	public static final String SEARCH_FIELD_ID_CIDADE = "SEARCH_FIELD_ID_CIDADE";
	public static final String SEARCH_FIELD_NUMERO = "SEARCH_FIELD_NUMERO";
	public static final String SEARCH_FIELD_COMPLEMENTO = "SEARCH_FIELD_COMPLEMENTO";
	
	public static final String SEARCH_FIELD_ORIGEM_ENDERECO = "SEARCH_FIELD_ORIGEM_ENDERECO";
	public static final String SEARCH_FIELD_DESTINO_ENDERECO = "SEARCH_FIELD_DESTINO_ENDERECO";
	public static final String SEARCH_FIELD_INICIO_DATA_PROGRAMADA = "SEARCH_FIELD_INICIO_DATA_PROGRAMADA";
	public static final String SEARCH_FIELD_INICIO_HORA_PROGRAMADA = "SEARCH_FIELD_INICIO_HORA_PROGRAMADA";
	public static final String SEARCH_FIELD_DATA_EXIBIR = "SEARCH_FIELD_DATA_EXIBIR";
	public static final String SEARCH_FIELD_INICIO_DATA = "SEARCH_FIELD_INICIO_DATA";
	public static final String SEARCH_FIELD_INICIO_HORA = "SEARCH_FIELD_INICIO_HORA";
	public static final String SEARCH_FIELD_FIM_DATA = "SEARCH_FIELD_FIM_DATA";
	public static final String SEARCH_FIELD_FIM_HORA = "SEARCH_FIELD_FIM_HORA";
	public static final String SEARCH_FIELD_VELOCIDADE_MINIMA = "SEARCH_FIELD_VELOCIDADE_MINIMA";
	public static final String SEARCH_FIELD_SOMENTE_COM_FOTO = "SEARCH_FIELD_SOMENTE_COM_FOTO";
	public static final String SEARCH_FIELD_DATA_CADASTRO = "SEARCH_FIELD_DATA_CADASTRO";
	public static final String SEARCH_FIELD_HORA_CADASTRO = "SEARCH_FIELD_HORA_CADASTRO";
	public static final String SEARCH_FIELD_DESCRICAO = "SEARCH_FIELD_DESCRICAO";
	
	//Camera configuration
	public static final Bitmap.CompressFormat PICTURE_COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;
	public static final int PICTURE_COMPRESS_QUALITY = 20;

	//File storage configuration
	public static final String BASE_DIRECTORY_PATH = "";
	public static final String LOG_FILES_EXTENSION = ".txt";
	public static final String ZIP_FILES_EXTENSION = ".zip";
	public static final String PICTURE_FILES_EXTENSION = ".jpeg";
	//Saved Preferences
	
	
	public static final String SAVED_SINCRONIZACAO_AUTOMATICA = "SAVED_SINCRONIZACAO_AUTOMATICA";
	public static final String SAVED_ADDRESS = "SAVED_ADDRESS";
	public static final String SAVED_PAIS = "SAVED_PAIS";
	public static final String SAVED_ESTADO = "SAVED_ESTADO";
	public static final String SAVED_CIDADE = "SAVED_CIDADE";
	public static final String SAVED_NOME_MODELO = "SAVED_MODELO";
	
	
	
	public static final String FORM_PAIS = "FORM_PAIS";
	public static final String FORM_ESTADO = "FORM_ESTADO";
	public static final String FORM_CIDADE = "FORM_CIDADE";
	
	
//	public static final String SAVED_SYNCH = "SAVED_SYNCH";
	
	
	public static final String SAVED_EMAIL = "SAVED_EMAIL";
	public static final String SAVED_PASSWORD = "SAVED_PASSWORD";
	public static final String SAVED_CORPORACAO = "SAVED_CORPORACAO";
	public static final String SAVED_IS_ADM = "SAVED_IS_ADM";
	public static final String DATABASE_CREATED = "DatabaseWasCreated";
	public static final String SAVED_PORTA_ACESSO = "SAVED_PORTA_ACESSO";

	public static boolean isTablet(Context p_context) {		
		try { 
			// Compute screen size
			DisplayMetrics dm = p_context.getResources().getDisplayMetrics(); 
			float screenWidth  = dm.widthPixels / dm.xdpi; 
			float screenHeight = dm.heightPixels / dm.ydpi; 
			double size = Math.sqrt(Math.pow(screenWidth, 2) + Math.pow(screenHeight, 2));
			//If screen size > 6", device is a tablet
			return size >= 6;
		} catch(Exception e) { 
			//Log.e(TAG, "Failed to compute screen size", e); 
			return false;
		} 
	}

}
