package app.omegasoftware.wifitransferpro.mapa;

import android.graphics.drawable.Drawable;
import app.omegasoftware.wifitransferpro.common.ItemList.AppointmentPlaceItemList;

import com.google.android.maps.GeoPoint;

public class TarefaCustomOverlayItem extends CustomOverlayItem {

	//caso seja direcionada a tarefa a alguem, o endereco de destino deve ser preenchido
	Integer __idTarefa = -1;
	
	Integer __idVeiculoUsuario = -1;

	//Pode ser nulo
	GeoPoint __ptOrigem;
	GeoPoint __ptDestino;
	GeoPoint __ptProprio;
	
	String __origemEndereco;
	String __origemComplemento;
	
	String __destinoComplemento;	
	String __destinoEndereco;

	CustomOverlayItem __customOverlayDestino = null;
	
	String __datetimeCadastro;
	String __dateAberturaTarefa;
	String __datetimeInicioMarcadoDaTarefa;
	String __datetimeInicioTarefa;
	String __datetimeFimTarefa;

	//Pode ser nulo
	String __descricao;

	public TarefaCustomOverlayItem(
			Integer pIdTarefa,
			GeoPoint ptProprio,
			GeoPoint ptOrigem,
			String pOrigemEndereco,
			String pOrigemComplemento,
			String pDestinoEndereco,
			String pDestinoComplemento,
			String name, 
			String snippet,
			Drawable markerOrigem,
			Drawable markerDestino,
			Integer pIdVeiculoUsuario,
			String pDatetimeCadastro,
			String pDateAberturaTarefa,
			String pDatetimeInicioMarcadoDaTarefa,
			String pDatetimeInicioTarefa,
			String pDatetimeFimTarefa,
			String pDescricao) {
		super(ptOrigem, name, snippet, markerOrigem);
		
		__dateAberturaTarefa = pDateAberturaTarefa;
		__datetimeInicioMarcadoDaTarefa = pDatetimeInicioMarcadoDaTarefa;
		this.marker=markerOrigem;
		
		__origemComplemento = pOrigemComplemento;
		__origemEndereco = pOrigemEndereco;
		
		__destinoEndereco = pDestinoEndereco; 
		__destinoComplemento = pDestinoComplemento;
		
		__ptProprio = ptProprio;
		
		if(__destinoEndereco != null)
			__ptDestino = HelperMapa.getGeoPointDoEndereco(__destinoEndereco);
		
		__idTarefa = pIdTarefa;
		__idVeiculoUsuario = pIdVeiculoUsuario;
		__datetimeCadastro = pDatetimeCadastro;
		__descricao = pDescricao;
		__datetimeInicioTarefa = pDatetimeInicioTarefa;
		__datetimeFimTarefa = pDatetimeFimTarefa;
		if(__ptDestino != null)
			__customOverlayDestino = new CustomOverlayItem(
					__ptDestino, 
					"Destino da Tarefa " + String.valueOf(this.__idTarefa), 
					__destinoEndereco, 
					markerDestino);
			
		
	}	
	
	public CustomOverlayItem getDestinoCustomOverlayItem(){
		
		return __customOverlayDestino;
	}
	
	
	public String getOrigemComplemento(){
		return __origemComplemento;
	}
	
	public String getDestinoComplemento(){
		return __destinoComplemento;
	}
	public String getDateAberturaTarefa(){
		return __dateAberturaTarefa ;	
	}
	
	public String getDatetimeInicioTarefa(){
		return __datetimeInicioTarefa;
	}
	
	public void setDatetimeInicioTarefa(String pDatetimeInicioTarefa){
		__datetimeInicioTarefa = pDatetimeInicioTarefa;
	}
	
	public void setDatetimeFimTarefa(String pDatetimeFimTarefa){
		__datetimeFimTarefa = pDatetimeFimTarefa;
	}
	
	public String getDatetimeFimTarefa(){
		return __datetimeFimTarefa;
	}
	
	public String getDatetimeInicioMarcadoDaTarefa(){
		return __datetimeInicioMarcadoDaTarefa;
	}
	
	public GeoPoint getGeoPointOrigem(){
		return __ptOrigem;
	}
	
	public GeoPoint getGeoPointDestino(){
		return __ptDestino;
	}
	
	public GeoPoint getGeoPointProprio(){
		return __ptProprio;
	}

	public String getDatetimeCadastro(){
		return __datetimeCadastro;
	}

	public String getEnderecoDestino(){
		return __destinoEndereco;
	}

	public String getEnderecoOrigem(){
		return __origemEndereco;
	}

	public Integer getIdVeiculoUsuario(){
		return __idVeiculoUsuario;
	}

	public String getDescricao(){
		return this.__descricao;
	}
	
	public Integer getIdTarefa(){
		return this.__idTarefa;
	}

}
