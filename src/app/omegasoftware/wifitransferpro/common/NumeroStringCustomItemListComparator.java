package app.omegasoftware.wifitransferpro.common;

import java.util.Comparator;

import app.omegasoftware.wifitransferpro.common.ItemList.CustomItemList;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;



public class NumeroStringCustomItemListComparator implements Comparator<Object> {
	private boolean isAsc;
	private String nameField;
	public NumeroStringCustomItemListComparator(boolean p_isAsc, String pNameField)
	{
		this.isAsc = p_isAsc;
		this.nameField = pNameField;
	}
	
	
    public int compare(Object o1, Object o2)
    {
        return compare((CustomItemList)o1, (CustomItemList)o2);
    }
    
    public int compare(CustomItemList o1, CustomItemList o2)
    {
    	try{
    	CustomItemList v_custom1 =  ((CustomItemList)o1);
    	CustomItemList v_custom2 =  ((CustomItemList)o2);
    	
    	if(v_custom1 != null && v_custom2 != null){
    		
    		String vNomeUm = v_custom1.getConteudoContainerItem(this.nameField);
    		String vNomeDois = v_custom2.getConteudoContainerItem(this.nameField);
    		Integer vValor1 = HelperInteger.parserInteger(vNomeUm);
    		Integer vValor2 = HelperInteger.parserInteger(vNomeDois);
    		if(vValor1 != null && vValor2 != null)	{
	    		if(isAsc){
	            	if(vValor1 >= vValor2)
	    				return 1;
	    			else return -1;
	    		}
	            else{
	            	if(vValor1 <= vValor2)
    					return 1;
    				else return -1;
	            }
    		} 
    	} 
    	} catch(Exception ex){
    		
    	}
    	return 0;
    }

}
