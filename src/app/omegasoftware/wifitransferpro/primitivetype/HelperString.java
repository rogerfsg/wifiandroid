package app.omegasoftware.wifitransferpro.primitivetype;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.Set;

import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;



public class HelperString {


	  private static final String UPPERCASE_ASCII =
	    "AEIOU" // grave
	    + "AEIOUY" // acute
	    + "AEIOUY" // circumflex
	    + "AON" // tilde
	    + "AEIOUY" // umlaut
	    + "A" // ring
	    + "C" // cedilla
	    + "OU" // double acute
	    ;

	  private static final String UPPERCASE_UNICODE =
	    "\u00C0\u00C8\u00CC\u00D2\u00D9"
	    + "\u00C1\u00C9\u00CD\u00D3\u00DA\u00DD"
	    + "\u00C2\u00CA\u00CE\u00D4\u00DB\u0176"
	    + "\u00C3\u00D5\u00D1"
	    + "\u00C4\u00CB\u00CF\u00D6\u00DC\u0178"
	    + "\u00C5"
	    + "\u00C7"
	    + "\u0150\u0170"
	    ;

	  public static String getWordWithoutAccent(String word){
			 if(word == null || word.length() == 0) return null;
			 else {
				    word = word.replaceAll("[����]","e");
				    word = word.replaceAll("[��]","u");
				    word = word.replaceAll("[��]","i");
				    word = word.replaceAll("[��]","a");
				    word = word.replaceAll("�","o");

				    word = word.replaceAll("[����]","E");
				    word = word.replaceAll("[��]","U");
				    word = word.replaceAll("[��]","I");
				    word = word.replaceAll("[��]","A");
				    word = word.replaceAll("�","O");
				    return word;
			 }
		}
	  
	public static String getRandomName(String pPrefixo)
	{
		if(pPrefixo == null) return HelperString.generateRandomString();
		else return pPrefixo + HelperString.generateRandomString() ;
	}	
	  public static String valueOfBooleanSQL(boolean pValue){
		  if(pValue) return "1";
		  else return "0";
	  }
	  public static String getStrSeparateByDelimiterColumnDatabase(Integer pVetorToken[]){
			String vLine =""; 
			for (Integer vToken : pVetorToken) {
				if(vToken == null) continue ;
				else if(vLine.length() > 0 ) vLine += OmegaConfiguration.DELIMITER_QUERY_COLUNA + String.valueOf( vToken);
				else vLine += String.valueOf( vToken); 
				 
			}
			return vLine;
		}
	  
	  
	  
	  public static String getStrSeparateByDelimiterColumn(String pVetorToken[], String pDelimiter){
			String vLine =""; 
			for (String vToken : pVetorToken) {
				if(vToken == null) continue;
				else if (vToken.length() == 0 ) continue;
				else if(vLine.length() > 0 ) vLine += pDelimiter + vToken;
				else vLine += vToken; 
				 
			}
			if(vLine.length() == 0 ) return null;
			else return vLine;
		}
	  
		public static String getStrSeparateByDelimiterColumnDatabase(String pVetorToken[]){
			String vLine =""; 
			for (String vToken : pVetorToken) {
		
				if(vLine.length() > 0 ) vLine += OmegaConfiguration.DELIMITER_QUERY_COLUNA + vToken;
				else vLine += vToken; 
				 
			}
			return vLine;
		}
		
		public static String getStrSeparateByDelimiterLineDatabase(String pVetorToken[]){
			String vLine =""; 
			for (String vToken : pVetorToken) {
				if(vLine.length() > 0 ) vLine += OmegaConfiguration.DELIMITER_QUERY_LINHA + vToken;
				else vLine += vToken;
			}
			return vLine;
		}

		
		public static String getIndexTokenInHashMap(LinkedHashMap<String, String> pVetorToken, String pId){
			Set<String> vSet = pVetorToken.keySet();
			for (String vId : vSet) {
				if(vId.compareTo(pId) == 0 ){
					return vId;
				}
			}
			return null;
		  }
		
		public static ArrayList<String> getArrayListOfContent(LinkedHashMap<String, String> pVetorToken){
			Set<String> vSet = pVetorToken.keySet();
			ArrayList<String> vList = new ArrayList<String>(); 
			for (String vId : vSet) {
				vList.add(pVetorToken.get(vId));
				
			}
			return vList;
		  }
		
		public static String getContentInHashMap(LinkedHashMap<String, String> pVetorToken, String pId){
			Set<String> vSet = pVetorToken.keySet();
			for (String vId : vSet) {
				if(vId.compareTo(pId) == 0 ){
					return pVetorToken.get(vId);
				}
			}
			return null;
		  }
		
	  public static int getIndexTokenInVector(String[] pVetorToken, String pToken){
		  if(pVetorToken == null) return -1;
		  else if (pToken == null) return -1;
		  int i = 0 ;
		  for (String vToken : pVetorToken) {
			  if(vToken != null) 
				  if(vToken.compareTo(pToken) == 0 ) return i;
			i += 1;
		}
		  return -1;
	  }
	  
	  public static int getIndexTokenInVectorCaseInsensitive(String[] pVetorToken, String pToken){
		  if(pVetorToken == null) return -1;
		  else if (pToken == null) return -1;
		  
		  String vTokenCompare = pToken.toLowerCase();
		  for ( int i = 0 ; i < pVetorToken.length; i++) {
			  String vToken = pVetorToken[i];
			  if(vToken != null) {
				  String vTokenCompareAux = vToken.toLowerCase();
				  if(vTokenCompareAux.compareTo(vTokenCompare) == 0 ) return i;
			  }
			
		}
		  return -1;
	  }
	  
	  public static String toUpperCaseSansAccent(String txt) {
	       if (txt == null) {
	          return null;
	       }
	       String txtUpper = txt.toUpperCase();
	       StringBuilder sb = new StringBuilder();
	       int n = txtUpper.length();
	       for (int i = 0; i < n; i++) {
	          char c = txtUpper.charAt(i);
	          int pos = UPPERCASE_UNICODE.indexOf(c);
	          if (pos > -1){
	            sb.append(UPPERCASE_ASCII.charAt(pos));
	          }
	          else {
	            sb.append(c);
	          }
	       }
	       return sb.toString();
	  }	
	  
	public static String generateRandomString()
	{
		Random random = new Random();
		String hash = Long.toHexString(random.nextLong());
		return hash;
	}
	
	public static String[] splitWithNoneEmptyToken(String pToken, String pDelimiter){
		if(pToken == null) return null;
		else if(pToken.length() == 0 ) return null;
		else{
			
			String vVetorToken[] = pToken.split(pDelimiter);
			ArrayList<String> vList = new ArrayList<String>();
			for (String vTokenAux : vVetorToken) {
				if(vTokenAux == null) continue;
				else if(vTokenAux.length() > 0 ){
					vList.add(vTokenAux);
				}
			}
			
			String vVetorTokenReturn[] = new String[vList.size()] ;
			vList.toArray((String[]) vVetorTokenReturn);
			return vVetorTokenReturn;
		}
	}

	public static String checkIfIsNull(String pToken){
		
		if(pToken == null ) return null;
		else if(pToken.length() == 0 ) return null;
		else{
			if(pToken.compareTo("NULL") == 0 || pToken.compareTo("null") == 0) return null;
			else return pToken;
		}
	}
	
	public static String getStrAndCheckIfIsNull(String pToken){
		if(pToken == null) return "null";
		else if (pToken.length() == 0  ) return "null";
		else return pToken;
	}
	  public static String retiraEspacoDoInicioEFim(String pToken){
		  if(pToken!= null){
				if(pToken.length() > 1 ){
					int i = 0 ;
					for(i = 0 ; i < pToken.length(); i ++){
						char vChar = pToken.charAt(i);
						if(vChar != ' '){
							break;
						}
					}
					int j = pToken.length() - 1;
					for(j = pToken.length() - 1 ; j >= 0; j --){
						char vChar = pToken.charAt(j);
						if(vChar != ' '){
							break;
						}
					}
					if(i < pToken.length() )
						return pToken.substring(i, j + 1);					
					else return pToken;
				}
		  }
		  return pToken;
	  }
	  
	  public static String ucFirst(String pToken){
			if(pToken!= null){
				
				if(pToken.length() > 1 ){
					String vTokenSemEspacoNoInicioEFim = retiraEspacoDoInicioEFim(pToken);	
					String vNewToken = vTokenSemEspacoNoInicioEFim.substring(0, 1).toUpperCase() + 
							vTokenSemEspacoNoInicioEFim.substring(1, vTokenSemEspacoNoInicioEFim.length() ).toLowerCase() ; 
					return vNewToken;
					 
				} else{
					return pToken.toUpperCase();
				}
				
			}
			return null;
		}
		
	  public static String ucFirstForEachToken(String pToken){
		  if(pToken == null) return null;
		  String vVetorToken[] = pToken.split(" ");
		  String vNewToken = "";
		  for (String vToken : vVetorToken) {
			  if(vNewToken.length() > 0 )
			    vNewToken += " " + HelperString.ucFirst(vToken);
			  else vNewToken += HelperString.ucFirst(vToken);
			    	
		}
		  return vNewToken;
	  }
	
}
