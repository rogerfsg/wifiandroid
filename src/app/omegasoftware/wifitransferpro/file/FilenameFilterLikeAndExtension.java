package app.omegasoftware.wifitransferpro.file;

import java.io.File;
import java.io.FilenameFilter;

public class FilenameFilterLikeAndExtension  implements FilenameFilter{
	String vetorNameFile[];
	String vetorExtension[];
	boolean isFileWithoutExtensionValid;
	public FilenameFilterLikeAndExtension(String pVetorNameFile[], String pVetorExtension[], boolean pIsFileWithoutExtensionValid){
		vetorNameFile = pVetorNameFile;
		vetorExtension = pVetorExtension;
		isFileWithoutExtensionValid = pIsFileWithoutExtensionValid;
	}
	
	public boolean accept(File dir, String filename) {
		// TODO Auto-generated method stub
		if(vetorNameFile != null)
		for (String vToken : vetorNameFile) {
			if(filename.contains(vToken)) return true;
		}
		if(vetorExtension != null)
		for (String vExtension : vetorExtension) {
			if( isFileWithoutExtensionValid){
				if(! filename.contains(".")) return true ;
			}else if(filename.endsWith(vExtension)){
				return true;
			} 
		}
		return false;
	}

}
