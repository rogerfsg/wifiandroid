package app.omegasoftware.wifitransferpro.http.lists;

import java.io.File;
import java.util.Date;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPesquisa;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPesquisaArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.pages.InformacaoTelefonePagina;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM_TOOLTIP;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class ListsResultadoPesquisa extends InterfaceHTML {

	public ListsResultadoPesquisa(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ContainerResponse getContainerResponse() {
		String vStrPath = __helper.GET("path");
		String vIdPesquisa = __helper.GET("pesquisa");
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		vStrPath = HelperFile.getNomeSemBarra(vStrPath);
		vStrPath += "/";
		StringBuilder vBuilder = new StringBuilder();
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		if (vStrPath != null && vStrPath.length() > 0) {
			File vDirectory = new File(vStrPath);
			if (vDirectory.isDirectory()) {

				
				vBuilder.append("<td id=\"td_left\" >");
				vBuilder.append("      <form name=\"form_arquivo\" id=\"form_arquivo\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
				vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");
				vBuilder.append("            <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
				vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
				vBuilder.append("           	<input type=\"hidden\" name=\"class\" id=\"class\" value=\""
						+ EXTDAOArquivo.NAME + "\">");
				vBuilder.append("            <input type=\"hidden\" name=\"action\" id=\"action\" value=\"pesquisaArquivo\">");
				vBuilder.append("            <input type=\"hidden\" name=\"funcao\" id=\"funcao\" value=\"\">");
								
				
				
				vBuilder.append("            <input type=\"hidden\" name=\""
						+ EXTDAOArquivo.NOME + "1\" id=\"" + EXTDAOArquivo.NOME
						+ "1\" value=\"" + __helper.GET(EXTDAOArquivo.NOME)
						+ "\">");
				vBuilder.append("            <input type=\"hidden\" name=\""
						+ EXTDAOArquivo.PATH + "1\" id=\"" + EXTDAOArquivo.PATH
						+ "1\" value=\"" + vStrPath + "\">");
//		TABELA CABECALHO =================================================================================================================
				vBuilder.append("    		<table width=\"350\" align=\"top\" class=\"tabela_list\">");
				
				
				vBuilder.append("<colgroup>");
				vBuilder.append("<col width=\"10%\" />");
				vBuilder.append("<col width=\"60%\" />");
				vBuilder.append("<col width=\"30%\" />");
				vBuilder.append("</colgroup>");
				vBuilder.append("<thead>");

//		FIM DO CABECALHO ========================================================================================================================
				vBuilder.append("</thead>");
				vBuilder.append("    		</table>");

				
//		TABELA ==================================================================================================================================
				vBuilder.append("<table class=\"tabela_list\">");
				vBuilder.append("<colgroup>");
				
				vBuilder.append("<col width=\"10%\" />");
				vBuilder.append("<col width=\"50%\" />");
				vBuilder.append("<col width=\"30%\" />");
				vBuilder.append("<col width=\"10%\" />");
				
				vBuilder.append("</colgroup>");

				vBuilder.append("<thead>");
				vBuilder.append("<tr class=\"tr_list_titulos\">");
				
				vBuilder.append("<script>");
				vBuilder.append("function marcarTodosOsCheckBoxesCujoIdContenha(isChecked, conteudoId){");
				vBuilder.append("    var inputs = document.getElementsByTagName(\"input\");");
				vBuilder.append("   for(var i=0; i < inputs.length; i++){");
				vBuilder.append("       if(inputs[i].type == \"checkbox\" && inputs[i].id.indexOf(conteudoId) != -1){");
				vBuilder.append("           inputs[i].checked = isChecked;");
				vBuilder.append("       }");
				vBuilder.append("   }");
				vBuilder.append("}");
				vBuilder.append("</script>");
				
//				NOME ======================================================================================
				vBuilder.append("<td class=\"td_list_titulos\">");
//				DIRETORIO ACIMA ICON ================================================================================================================================
						
						vBuilder.append(" <img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "up_folder.png\" "
								+ "onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_DIRETORIO_ACIMA)
								+ "')\" "
								+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&"
								+ EXTDAOArquivo.PATH + "="
								+ HelperHttp.getStrPathUp(vStrPath) 
								+  "'\""
								+ "onmouseout=\"javascript:notip()\">&nbsp;");
//				RAIZ ICON ================================================================================================================================
				
						vBuilder.append("<img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "home_folder.png\" "
								+ "onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_RAIZ)
								+ "')\" "
								+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&"
								+ EXTDAOArquivo.PATH + "="
								+ HelperHttp.getPathRoot()
								+ "\""
								+ "onmouseout=\"javascript:notip()\">&nbsp;");
				
				vBuilder.append("</td>");
				vBuilder.append("<td class=\"td_list_titulos\">");
				
				vBuilder.append(HelperString.ucFirst(__helper.context.getResources().getString(
								R.string.arquivo_nome)));
						
				vBuilder.append("</td>");
//				=========================================================================================
				vBuilder.append("<td class=\"td_list_titulos\">");
				vBuilder.append(HelperString.ucFirst( __helper.context.getResources().getString(
								R.string.arquivo_data)));
				
				vBuilder.append("</td>");
				vBuilder.append("<td class=\"td_list_titulos\">");
				vBuilder.append(HelperString.ucFirst(__helper.context.getResources().getString(
								R.string.arquivo_tamanho)));
				vBuilder.append("</td>");
				vBuilder.append("</tr>");
				vBuilder.append("</thead>");
				vBuilder.append("<tbody>");
				
				Database db = new DatabaseWifiTransfer(__helper.context);
				EXTDAOPesquisaArquivo vObjPesquisaArquivo = new EXTDAOPesquisaArquivo(db);
				File vVetorFile[] =vObjPesquisaArquivo.getListFileDaPesquisa(vIdPesquisa);
				
				EXTDAOPesquisa vObjPesquisa = new EXTDAOPesquisa(db);
				vObjPesquisa.setAttrStrValue(EXTDAOPesquisa.ID, vIdPesquisa);
				String vStrIsRunning = vObjPesquisa.getStrValueOfAttribute(EXTDAOPesquisa.IS_RUNNING_BOOLEAN);
				boolean vIsRunning = false;
				vIsRunning = (vStrIsRunning == null || vStrIsRunning.compareTo("0") == 0 ) ? false : true;
				db.close();
				
				if (vVetorFile == null || vVetorFile.length == 0) {
					vBuilder.append("<tr class=\"tr_list_conteudo_impar\">");
					vBuilder.append("<td  colspan=\"5\">");
					vBuilder.append(HelperHttp.imprimirMensagem(
							HelperString.ucFirst(context.getResources().getString(
									R.string.pesquisa_sem_resultado)),
							HelperHttp.TYPE_MENSAGEM.MENSAGEM_OK, null));
					vBuilder.append("</td>");
					vBuilder.append("</tr>");
				} else {
					 
					for (int i = 0; i < vVetorFile.length; i++) {
						File vFile = vVetorFile[i];
						ContainerTypeFile vContainerType = ContainerTypeFile
								.getContainerTypeFile(vFile);
						
						String classTr = (i % 2 == 0) ? "tr_list_conteudo_impar"
								: "tr_list_conteudo_par";
						String vStri = String.valueOf(i);
						String vFileName = vFile.getName();
						boolean isDirectory = (vContainerType.typeFile == TYPE_FILE.FOLDER) ? true : false;
						
						vBuilder.append("<tr class=\"" + classTr + "\">");
						vBuilder.append("<td  style=\"text-align: center; padding-left: 5px;\">");
						vBuilder.append("<img class=\"icones_list\" src=\""
								+ vContainerType.getImage()
								+ "\">&nbsp;");
						vBuilder.append("</td>");
						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");

						if (!isDirectory)
							vBuilder.append(vFileName);
						else {
							vBuilder.append("<a href=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&path="
									+ vStrPath
									+ vFileName
									+ "/"
									+ "'\" class=\"link_padrao\" onmouseout=\"notip();\" onmouseover=\"tip('"
									+ __helper
											.getMensagemTooltip(TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_DIRETORIO)
									+ "');\">" + vFileName + "</a>");
						}
						vBuilder.append("</td>");

						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");
						Date vDate = new Date(vFile.lastModified());
						vBuilder.append(vDate.toLocaleString());
						vBuilder.append("</td>");

						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");
						if (!isDirectory)
							vBuilder.append(HelperFile.getStrLength(vFile));
						else
							vBuilder.append("-");
						vBuilder.append("</td>");

						vBuilder.append("</tr>");
					}
				}
				vBuilder.append("</tbody>");
				vBuilder.append("</table>");

				vBuilder.append("<br/>");
				vBuilder.append("<br/>");
				vBuilder.append("</form>");
				vBuilder.append("</td>");
				
//				Direita Arquivo ============================================================
				vBuilder.append("<td id=\"td_right\">");
				
//				Informacao Telefone============================================================				
				vContainer.appendData(vBuilder);
				ContainerResponse vContainerInformacaoTelefonePagina = new InformacaoTelefonePagina(__helper).getContainerResponse();
				vContainer.copyContainerResponse(vContainerInformacaoTelefonePagina);
				vBuilder = new StringBuilder();
								
				
				vBuilder.append("</td>");
				
				vContainer.appendData(vBuilder);

			}
		}

		return vContainer;
	}

}
