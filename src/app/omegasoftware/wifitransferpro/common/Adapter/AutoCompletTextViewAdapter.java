package app.omegasoftware.wifitransferpro.common.Adapter;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class AutoCompletTextViewAdapter {

	public AutoCompletTextViewAdapter(
			Activity pActivity,
			int idEditText, 
			int idLayout, 
			String[] pListOption){
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				pActivity,
                android.R.layout.simple_dropdown_item_1line, 
                pListOption);
        AutoCompleteTextView textView = (AutoCompleteTextView)
                pActivity.findViewById(idEditText);
        textView.setAdapter(adapter);
	}
	
	
}
