package app.omegasoftware.wifitransferpro.gps;


import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import app.omegasoftware.wifitransferpro.mapa.HelperMapa;

import com.google.android.maps.GeoPoint;

public class GPSServiceLocationListener implements LocationListener {

	private static final String TAG = "GPSServiceLocationListener";

	protected static GeoPoint geoPoint;
	static protected LocationManager locationManager;
	static protected Context __context = null;
	private static GPSServiceLocationListener singletonGPSServiceLocationListener;
	
	protected GPSServiceLocationListener(Context pContext){

		locationManager = (LocationManager) pContext.getSystemService(Context.LOCATION_SERVICE);
		
		__context = pContext;
		loadLocationManager(this);		
	}
	
	
	public static GPSServiceLocationListener construct(Context pContext){
		if(singletonGPSServiceLocationListener == null) singletonGPSServiceLocationListener = new GPSServiceLocationListener(pContext);
		
		if(!isAtive()){
			loadLocationManager(singletonGPSServiceLocationListener);
		}
		return  singletonGPSServiceLocationListener;
		
	}
	
	public static boolean isAtive(){
		if(geoPoint == null){
			return false;
		} else return true;
	}

	public static void loadLocationManager(LocationListener pLocationListener){
		try{
		locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, 
				GPSConfiguration.MIN_TIME_LOCATION_UPDATE, 
				GPSConfiguration.MIN_DISTANCE_LOCATION_UPDATE,
				pLocationListener);
		if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			try{
				locationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true);
			} catch(Exception ex){

			}
		}		//		
		Location location = locationManager.getLastKnownLocation( LocationManager.GPS_PROVIDER);
		
		geoPoint = HelperMapa.getGeoPoint(location);
		} catch(Exception ex){
			int i = 0;
			i += 1;
		}
	}
	
	public Double getLongitudeDouble(){
		Integer longitude = getLongitude();
		if(longitude != null) {
			double vValor = longitude / 1000000;
			return vValor;
		}
		else return null;
	}
	
	public Double getLatitudeDouble(){
		Integer latitude = getLatitude();
		if(latitude != null) {
			double vValor = latitude / 1000000;
			return vValor;
		}
		else return null;
	}

	public Integer getLatitude(){
		
		if(geoPoint != null) return geoPoint.getLatitudeE6();
		else {
			return null;
		}
	}

	public Integer getLongitude(){
		if(geoPoint != null) return geoPoint.getLongitudeE6();
		else {
			return null;
		}
		
	}

	public GeoPoint getGeoPoint(){

		return geoPoint;
	}

	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		int i = 0 ;
	}

	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		int i = 0 ;
	}

	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		int i = 0 ;
	}

	public void onLocationChanged(Location location) {
		//Log.d(TAG,"onLocationChanged(): Location changed");
		if(location != null){
			
			geoPoint = HelperMapa.getGeoPoint(location);
			
			//			this.vEXTDAOVeiculoPosicao.setAttrStrValue(EXTDAOVeiculoPosicao.VEICULO_ID_INT, );
		}


	}


	public String getLocationPrint(String p_delimitador){
		if(geoPoint == null){
			return null;
		}
		else return String.valueOf(this.geoPoint.getLatitudeE6()/1000000) + p_delimitador + String.valueOf(this.geoPoint.getLongitudeE6()/1000000);
	}

}
