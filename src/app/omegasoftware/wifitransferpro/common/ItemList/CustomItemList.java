package app.omegasoftware.wifitransferpro.common.ItemList;

import android.app.LauncherActivity.ListItem;

public abstract class CustomItemList extends ListItem {
	
	
	private ContainerItem[] __vetorContainerItem;
	private String __id;
	public CustomItemList(String pId, ContainerItem pContainerItem){
		__vetorContainerItem = new ContainerItem[1];
		__vetorContainerItem[0] = pContainerItem;
		__id = pId;
	}
	
	public CustomItemList(String pId, ContainerItem[] pVetorContainerItem){
		__vetorContainerItem = pVetorContainerItem;
		__id = pId;
	}

	public String getId(){
		return __id;
	}
	
	
	public ContainerItem[] getVetorContainerItem(){
		return __vetorContainerItem;
	}

	public ContainerItem getContainerItem(String p_nome){
		for (ContainerItem v_containerItem : __vetorContainerItem) {
			if(v_containerItem.isEqual(p_nome)){
				return v_containerItem;
			}
		}
		return null;
	}
	
	public String getConteudoContainerItem(String p_nome){
		for (ContainerItem v_containerItem : __vetorContainerItem) {
			if(v_containerItem.isEqual(p_nome)){
				return v_containerItem.getConteudo();
			}
		}
		return null;
	}
		
}
