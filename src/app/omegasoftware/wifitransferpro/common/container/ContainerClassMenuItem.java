package app.omegasoftware.wifitransferpro.common.container;

import android.app.Activity;
import android.view.View.OnClickListener;
import app.omegasoftware.wifitransferpro.TabletActivities.FactoryMenuActivityMobile;

public class ContainerClassMenuItem extends AbstractContainerClassItem{
	
	public static String TAG = "ContainerClassMenuItem";
	
	public ContainerClassMenuItem(
			int pIdStringNome,
			String pTag,
			Integer pDrawable){
		super(pIdStringNome,
				FactoryMenuActivityMobile.class,
				pTag, 
				pDrawable);
	}
	
	public ContainerClassMenuItem(
			int pIdStringNome,
			String pTag,
			Integer pDrawable, 
			boolean pIsComumATodosUsuarios){
		super(pIdStringNome,
				FactoryMenuActivityMobile.class,
				pTag, 
				pDrawable,
				pIsComumATodosUsuarios);
	}

	@Override
	public OnClickListener getOnClickListener(Activity pActivity) {
		// TODO Auto-generated method stub
		return new MenuOnClickListener(
				(FactoryMenuActivityMobile) pActivity,
				getClassTarget(),
				__tag);
	}


}
