package app.omegasoftware.wifitransferpro.listener;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SendEmailClickListener implements OnClickListener {

	private TextView textview = null;
	String __titulo;
	public SendEmailClickListener(String pTitulo, TextView pTextview){
		textview = pTextview;
		__titulo = pTitulo;
	}
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		
		String emailAddress = (String) textview.getText();
		
		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		intent.putExtra(android.content.Intent.EXTRA_SUBJECT,  __titulo);
		intent.putExtra(android.content.Intent.EXTRA_TEXT,  emailAddress );
		intent.setType("text/plain");
		v.getContext().startActivity(intent);
		
	}
	
	
}
