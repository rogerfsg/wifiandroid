package app.omegasoftware.wifitransferpro.database.DAO;

import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.GenericDAO;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public abstract class  DAOPlaylist extends GenericDAO{

	public DAOPlaylist(String p_name, Database p_database) {
		super(p_name, p_database);
		// TODO Auto-generated constructor stub
	}

	
	public void setByPost(HelperHttp pHelper, String pNumReg){
		setAttrStrValue(EXTDAOArquivo.ID, formatarDados( pHelper.POST("id" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.NOME, formatarDados(pHelper.POST("nome" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.PATH, formatarDados(pHelper.POST("path" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.TIPO_ARQUIVO_ID_INT, formatarDados(pHelper.POST("tipo_arquivo_id_INT" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.TAMANHO_BYTES_INT, formatarDados(pHelper.POST("tamanho_bytes_INT" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.IS_DIRETORIO_BOOLEAN, formatarDados(pHelper.POST("is_diretorio_BOOLEAN" + pNumReg)));
	}
	
	public void setByGet(HelperHttp pHelper, String pNumReg){
		setAttrStrValue(EXTDAOArquivo.ID, formatarDados( pHelper.GET("id" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.NOME, formatarDados(pHelper.GET("nome" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.PATH, formatarDados(pHelper.GET("path" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.TIPO_ARQUIVO_ID_INT, formatarDados(pHelper.GET("tipo_arquivo_id_INT" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.TAMANHO_BYTES_INT, formatarDados(pHelper.GET("tamanho_bytes_INT" + pNumReg)));
		setAttrStrValue(EXTDAOArquivo.IS_DIRETORIO_BOOLEAN, formatarDados(pHelper.GET("is_diretorio_BOOLEAN" + pNumReg)));
	}
			
	@Override
	public ContainerResponse actionAdd(HelperHttp pHelper) {
		// TODO Auto-generated method stub
		return null;
	}
	public ContainerResponse actionEdit(HelperHttp pHelper){
		return null;
	}
	@Override
	public ContainerResponse actionRemove(HelperHttp pHelper){
		return null;
	}
	
	public ContainerResponse callUserFunc(HelperHttp pHelper, String pNomeFuncao){
		return null;
	}
	
}
