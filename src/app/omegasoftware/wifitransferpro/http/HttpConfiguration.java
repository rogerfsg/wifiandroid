package app.omegasoftware.wifitransferpro.http;

import app.omegasoftware.wifitransferpro.http.lists.ListsArquivo;
import app.omegasoftware.wifitransferpro.http.pages.PaginaInicial;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class HttpConfiguration {
	public static String TITULO_PAGINA = "WIFI FILE";
	public static NanoHTTPD.Response getHTMLPaginaInicialPadrao(HelperHttp pHelper){
		return new ListsArquivo(pHelper).getHTML();
	}
	
	public static ContainerResponse getContainerResponseHTMLPaginaInicialPadrao(HelperHttp pHelper){
		return new ListsArquivo(pHelper).getContainerResponse();
	}
}
