package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Javascript;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Music;

public class MusicPagina extends InterfaceHTML{

	public MusicPagina(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
//		String vStrReturn =  Javascript.importarBibliotecaJavascriptMusicPlayer();
		if(__helper.GET("path_completo") != null){
			String vStrReturn = Javascript.importarBibliotecaJavascriptMusicPlayer(__helper.GET("path_completo"));
			return new ContainerResponse(__helper, new StringBuilder(vStrReturn));
		}else{
			String vStrReturn = Javascript.importarBibliotecaJavascriptMusicPlayer(HelperFile.getNomeSemBarra( __helper.GET(EXTDAOArquivo.PATH)) , __helper.GET(EXTDAOArquivo.NOME));
			return new ContainerResponse(__helper, new StringBuilder(vStrReturn));	
		}
		
		 
	}

}
