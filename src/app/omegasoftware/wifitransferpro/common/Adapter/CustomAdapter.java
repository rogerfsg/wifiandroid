
package app.omegasoftware.wifitransferpro.common.Adapter;



import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.NameCustomItemListComparator;
import app.omegasoftware.wifitransferpro.common.ItemList.CustomItemList;
import app.omegasoftware.wifitransferpro.listener.DetailButtonClickListener;
import app.omegasoftware.wifitransferpro.listener.GestureOnTouchListener;
import app.omegasoftware.wifitransferpro.listener.SlideSimpleOnGestureListener;


public abstract class CustomAdapter extends BaseAdapter {
	protected ArrayList<CustomItemList> list = new ArrayList<CustomItemList>();

	Context context;
	boolean isAsc;
	boolean isToSort = true;
	String nameOfItemListToOrder;
	protected ArrayList<GestureDetector> listSlideGestureDetector = new ArrayList<GestureDetector>();

	protected CustomAdapter(Context context, boolean pIsAsc, String pNameOfItemListToOrder)
	{
		this.context = context;
		this.isAsc = pIsAsc;
		this.list = loadData(pIsAsc);
		nameOfItemListToOrder = pNameOfItemListToOrder;
	}
	protected CustomAdapter(Context context, boolean pIsAsc, boolean pLoadList, boolean pIsToSort, String pNameOfItemListToOrder)
	{
		this.context = context;
		this.isAsc = pIsAsc;
		isToSort = pIsToSort;
		nameOfItemListToOrder = pNameOfItemListToOrder;

		if(pLoadList)
			this.list = loadData(pIsAsc);

	}
	protected CustomAdapter(Context context, boolean pIsAsc, boolean pLoadList, String pNameOfItemListToOrder)
	{
		this.context = context;
		this.isAsc = pIsAsc;
		nameOfItemListToOrder = pNameOfItemListToOrder;
		if(pLoadList)
			this.list = loadData(pIsAsc);	
	}

	public long getSize(){
		if(list == null) return 0;
		return list.size();
	}

	public boolean isAsc(){
		return isAsc;
	}


	public Context getContext(){
		return context;
	}

	protected void CreateTextView(int pRLayoutId, String pText, View pView)
	{
		CreateTextView(pRLayoutId, pText, pView, false);
	}



	protected void CreateLinearLayout(int pRLayoutId, View pView, OnTouchListener pOnClickListener)
	{
		LinearLayout vLinearLayout = (LinearLayout)pView.findViewById(pRLayoutId);
		vLinearLayout.setClickable(true);
		vLinearLayout.setFocusable(true);
		vLinearLayout.setOnTouchListener(pOnClickListener);
	}

	protected void CreateLinearLayout(int pRLayoutId, View pView, OnClickListener pOnClickListener)
	{
		LinearLayout vLinearLayout = (LinearLayout)pView.findViewById(pRLayoutId);
		vLinearLayout.setClickable(true);
		vLinearLayout.setFocusable(true);
		vLinearLayout.setOnClickListener(pOnClickListener);
	}

	protected TextView CreateTextView(int pRLayoutId, String pText, View pView, boolean pAppend)
	{
		if(pText == null || pView == null) return null;
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		if (vTextView == null) return null;
		if(pAppend)
			vTextView.setText(new StringBuilder(vTextView.getText() + pText));
		else
			vTextView.setText(pText);
		return vTextView;
	}
	protected void CreateTextView(int pRLayoutId, String pText, View pView, boolean pAppend, int pColor)
	{
		if(pText == null || pView == null) return;
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		if (vTextView == null) return;
		if(pAppend)
			vTextView.setText(new StringBuilder(vTextView.getText() + pText));
		else
			vTextView.setText(pText);			
		vTextView.setTextColor(pColor);
	}

	protected void CreateDetailButton(Activity pActivity, View pView, int pRLayoutId, String pIdCustomItemList, short p_searchMode)
	{
		Button vButton = (Button) pView.findViewById(pRLayoutId);
		vButton.setClickable(true);
		vButton.setFocusable(true);	   
		//		if(DoutorLaserActivity.isTablet){
		//			
		//			vDetailButtonClickListenerTablet =  new DetailButtonClickListenerTablet(this.__activity, pIdCustomItemList, p_searchMode);
		//			vButton.setOnClickListener(vDetailButtonClickListenerTablet);
		//			CreateLinearLayout(R.id.linearLayoutfuncionariodetails1, pView, vDetailButtonClickListenerTablet);
		//		} else {
		//		vButton.setOnTouchListener(new DetailButtonClickListener(this.context, pView, pIdCustomItemList, p_searchMode));
		CreateLinearLayout(R.id.linearLayoutfuncionariodetails1, pView, new DetailButtonClickListener(pActivity, pIdCustomItemList, p_searchMode));
		SlideSimpleOnGestureListener vSlideListener = new SlideSimpleOnGestureListener(pActivity,  pView, pIdCustomItemList, p_searchMode);
		GestureDetector vGestor = new  GestureDetector(context, vSlideListener);
		GestureOnTouchListener vListener = new GestureOnTouchListener(vGestor);
		listSlideGestureDetector.add(vGestor);
		pView.setOnTouchListener(vListener); 
		//		}

	}

	protected void CreateDetailButton(Activity pActivity,String pIdCustomItemList, View pView, short p_searchMode) {
		CreateDetailButton(pActivity, pView, R.id.custom_button_detail, pIdCustomItemList, p_searchMode);		
	}

	public int getCount() {		
		if(this.list == null) return 0;
		else return this.list.size();
	}

	public CustomItemList getItem(int pPosition) {
		return this.list.get(pPosition);
	}

	public void initalizeListCustomItemList(){
		this.list = loadData(isAsc);
	}



	public ArrayList<CustomItemList> loadData(boolean p_isAsc )
	{

		try {

			ArrayList<Integer> vListId = getListIdBySearchParameters(p_isAsc);

			ArrayList<CustomItemList> vRetorno = getListCustomItemList( vListId);


			if(isToSort)
				Collections.sort(vRetorno, new NameCustomItemListComparator(p_isAsc, nameOfItemListToOrder));

			return vRetorno;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}

		return null;
	}


	public ArrayList<CustomItemList> getListCustomItemList(
			ArrayList<Integer> pArrayListLong) throws Exception {

		ArrayList<CustomItemList> vArrayFuncionarioItemList = new ArrayList<CustomItemList>();


		for (int iLong : pArrayListLong) {
			try{
				CustomItemList v_item = getCustomItemListOfTable(iLong);
				if(v_item != null)
					vArrayFuncionarioItemList.add(v_item);
			}catch (Exception e) {
				// TODO Auto-generated catch block
				if(e != null)
					Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, "Error ");
			}
			//				vObjTable.clearData();
		}
		return vArrayFuncionarioItemList;	



	}

	public long getItemId(int pPosition) {		
		CustomItemList vCustom = this.list.get(pPosition);
		String v_id = vCustom.getId();
		try{
			if(v_id != null)
				return Long.parseLong(v_id);
			else return -1;
		} catch(Exception ex){
			return -1;
		}

	}

	protected abstract ArrayList<Integer> getListIdBySearchParameters(boolean pIsAsc);

	public abstract CustomItemList getCustomItemListOfTable(Integer pId);

	public void SortList(boolean pAsc) {
		Collections.sort(list, new NameCustomItemListComparator(pAsc, nameOfItemListToOrder));
		this.notifyDataSetChanged();
	}

	public  abstract View getView(int position, View convertView, ViewGroup parent) ;

}

