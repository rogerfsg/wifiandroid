package app.omegasoftware.wifitransferpro.http;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import org.apache.http.conn.util.InetAddressUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class HelperNetwork {
	public static String TAG = "HelperNetwork";
	
	public static String getIp(Context pContext){
		try {
			String ipteste = getIPAddress(true);
			return ipteste;
		} catch (Exception ex) {
			
		}
		return null;
	}
	/**
     * Get IP address from first non-localhost interface
     * @param ipv4  true=return ipv4, false=return ipv6
     * @return  address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr); 
                        if (useIPv4) {
                            if (isIPv4) 
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                return delim<0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }
	
	public static String getStatus(Context pContext){
		StringBuilder textStatus = new StringBuilder();
		try {
			for (NetworkInterface intf : Collections.list(NetworkInterface.getNetworkInterfaces())) {
				for (InetAddress addr : Collections.list(intf.getInetAddresses())) {
					if (!addr.isLoopbackAddress()){
						textStatus.append("\n\n IP Address: " + addr.getHostAddress() );
						textStatus.append("\n" + addr.getHostName() );
						textStatus.append("\n" + addr.getCanonicalHostName() );
						textStatus.append("\n\n" + intf.toString() );
						textStatus.append("\n\n" + intf.getName() );
						
					} 
				}
			}
		} catch (Exception ex) {
			textStatus.append("\n\n Error getting IP address: " + ex.getLocalizedMessage() );
		}


		ConnectivityManager connectivity = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo allInfo[] = connectivity.getAllNetworkInfo();
		NetworkInfo mobileInfo = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		textStatus.append("\n\n TypeName: " + mobileInfo.getTypeName());
		textStatus.append("\n State: " + mobileInfo.getState());
		textStatus.append("\n Subtype: " + mobileInfo.getSubtype());
		textStatus.append("\n SubtypeName: " + mobileInfo.getSubtypeName());
		textStatus.append("\n Type: " + mobileInfo.getType());
		textStatus.append("\n ConnectedOrConnecting: " + mobileInfo.isConnectedOrConnecting());
		textStatus.append("\n DetailedState: " + mobileInfo.getDetailedState());
		textStatus.append("\n ExtraInfo: " + mobileInfo.getExtraInfo());
		textStatus.append("\n Reason: " + mobileInfo.getReason());
		textStatus.append("\n Failover: " + mobileInfo.isFailover());
		textStatus.append("\n Roaming: " + mobileInfo.isRoaming()); 

		textStatus.append("\n\n 0: " + allInfo[0].toString());
		textStatus.append("\n\n 1: " + allInfo[1].toString());
		textStatus.append("\n\n 2: " + allInfo[2].toString());
		Log.d(TAG, textStatus.toString());
		return textStatus.toString();
	}
}