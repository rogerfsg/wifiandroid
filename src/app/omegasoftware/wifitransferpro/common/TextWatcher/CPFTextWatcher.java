package app.omegasoftware.wifitransferpro.common.TextWatcher;

import android.widget.EditText;

public class CPFTextWatcher extends MaskNumberTextWatcher{

//  082.473.671-79
	public CPFTextWatcher(EditText pEditText){
		super(pEditText, new int[]{3, 7, 11}, new String[] {".", ".", "-"}, 11);
	}


}