package app.omegasoftware.wifitransferpro.TabletActivities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.activity.OmegaRegularActivity;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.http.HelperPorta;
import app.omegasoftware.wifitransferpro.listener.TecladoPortaOnClickListener;

public class ConfiguracaoActivityMobile extends OmegaRegularActivity {
	public static String TAG = "AboutActivityMobile";
	private Button portaButton;
	private Button restaurarDadosButton;
	private TecladoPortaOnClickListener portaOnClickListener;
	private ContainerPorta containerPorta;
	private class ContainerPorta{
		String initialPorta ;
		
		public ContainerPorta(Context pContext){
			initialPorta = HelperPorta.getSavedPorta(pContext);
		}
		public boolean isPortaChanged(){
			String vText = portaButton.getText().toString();
			if(vText.compareTo(initialPorta) == 0 ) return false;
			else return true;
		}
	}
	
	boolean __isPortaChanged = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.configuracao_activity_mobile_layout);
		
		containerPorta = new ContainerPorta(this);
		
		this.portaButton = (Button) findViewById(R.id.porta_button);
		portaOnClickListener = new TecladoPortaOnClickListener(this, portaButton, OmegaConfiguration.ACTIVITY_FORM_TECLADO_TELEFONE_1);
		portaButton.setOnClickListener(portaOnClickListener);
		
		this.restaurarDadosButton = (Button) findViewById(R.id.restaurar_dados_button);
		restaurarDadosButton.setOnClickListener(new RestaurarDadosButtonListener(this));
		
		initializeComponents();
		applyTypeFace();
	}
	
	public void onBackPressed(){
		if(containerPorta.isPortaChanged()){
			setResult(OmegaConfiguration.RESULT_PORTA);
			finish();
		}
		super.onBackPressed();
	}
	
	private void applyTypeFace(){
		
		Typeface  customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		((TextView) findViewById(R.id.porta_textview)).setTypeface(customTypeFace);
		
	}
	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_FORM_TECLADO_PORTA:
			portaOnClickListener.onActivityResult(intent);
			break;
		}
	}
	
	@Override
	public boolean loadData() {
		//
		// TODO Auto-generated method stub
		return true;
	}
	
	
	@Override
	public void initializeComponents() {
		String vPorta = HelperPorta.getSavedPorta(this);
		if(vPorta != null){
			portaButton.setText(vPorta);
			portaButton.setTextSize(20);
		}
		// TODO Auto-generated method stub
		
	}
	
	private class RestaurarDadosButtonListener implements OnClickListener
	{
		Activity __activity;
		public RestaurarDadosButtonListener(Activity pActivity){
			__activity = pActivity;
		}

		public void onClick(View v) {
			new RestaurarDadosButtonLoader(__activity).execute();
		}
	}	
	
	public class RestaurarDadosButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		
		Integer dialogId = null;
		Activity __activity;
		public RestaurarDadosButtonLoader(Activity pActivity){
			__activity = pActivity;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				OmegaFileConfiguration.createHierarquiaArquivo(__activity, true);
				return true;
				
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}finally{
				
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);
			if(dialogId != null)
				showDialog(dialogId);
		}
	}
	
}
