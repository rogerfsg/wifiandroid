package app.omegasoftware.wifitransferpro.listener;

import android.app.Activity;

import android.view.View;
import android.view.View.OnClickListener;

public class CloseButtonActivityClickListenerTablet implements OnClickListener {

	
	Activity __activity ;
	
	public CloseButtonActivityClickListenerTablet(Activity p_activity )
	{
		
		__activity = p_activity;
		
	}
	
	public void onClick(View v) {
	
		__activity.finish();
		
	}
	
	
	
}
