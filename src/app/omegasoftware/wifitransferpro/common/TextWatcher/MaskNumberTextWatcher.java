package app.omegasoftware.wifitransferpro.common.TextWatcher;

import android.widget.EditText;



public class MaskNumberTextWatcher extends InterfaceMaskTextWatcher{
	//	(31) 3333 - 3333
	//123456789A
	//  0,
	
		public MaskNumberTextWatcher(
				EditText pEditText,
				int pVetorPosicaoInicioToken[],
				String pVetorString[], 
				int pMaxSize){
			super(pEditText, pVetorPosicaoInicioToken, pVetorString, pMaxSize, true);
		}

		public MaskNumberTextWatcher(
				EditText pEditText,
				int pMaxSize){
			super(pEditText, null, null, pMaxSize, true);
		}



		public String getValidToken(String pToken){
			
			String vNewToken = "";
			for (char vLetra : pToken.toCharArray()) {
			
				if(isNumero(vLetra)){
					vNewToken += vLetra;
				}
			}

			if(__maxSize > 0 ){
				if(vNewToken.length() > __maxSize){
					return vNewToken.substring(0, __maxSize);
				} else return vNewToken;
			} else return vNewToken;
		}

	


}