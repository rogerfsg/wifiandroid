package app.omegasoftware.wifitransferpro.listener;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.TabletActivities.TecladoPortaActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.http.HelperPorta;

public class TecladoPortaOnClickListener implements OnClickListener{
	Button __button;
	int __origemChamado;
	Activity __activity;
	Handler __telefoneHandler;
	public TecladoPortaOnClickListener(Activity pActivity, Button pButton, int pOrigemChamado){
		__origemChamado = pOrigemChamado;
		__button = pButton;
		__activity = pActivity;
		__telefoneHandler = new ButtonTelefoneHandler(pActivity);
	}
	public void clearComponent(){
		
		updateTelefone("", true);
	}
	
	public boolean isNumeroSet(){
		String vButtonText = __button.getText().toString();
		if(vButtonText != "" && !vButtonText.startsWith("DIGITE")) return true;
		else return false;
	}
	public Handler getHandler(){
		return __telefoneHandler;
	}
	public Button getButton(){
		return __button;
	}
	public int getOrigemChamado(){
		return __origemChamado;
	}

	public void updateTelefone(String pNumero){
		updateTelefone(pNumero, false);
	}
	public void updateTelefone(String pNumero, boolean pIsToForce){
		if((pNumero != null && pNumero.length() > 0 && !pNumero.startsWith("DIGITE")) || pIsToForce){
			Bundle vParameter = new Bundle();
			vParameter.putString(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE, pNumero);
			Message vMessage = new Message();
			vMessage.what = ButtonTelefoneHandler.TAG_TELEFONE;
			vMessage.setData(vParameter);
			__telefoneHandler.sendMessage(vMessage);
		}
	}
	
	public void onActivityResult(Intent pIntent){
		if(pIntent != null){
			Bundle vParameter = pIntent.getExtras();
			if(vParameter != null){
				Message vMessage = new Message();
				vMessage.what = ButtonTelefoneHandler.TAG_TELEFONE;
				vMessage.setData(vParameter);
				__telefoneHandler.sendMessage(vMessage);
				
			}
		}
	}

	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String vConteudo = __button.getText().toString();
		Intent vIntent = new Intent(__activity, TecladoPortaActivityMobile.class);
		
		vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE, vConteudo);

		__activity.startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_FORM_TECLADO_PORTA );
	}

	public class ButtonTelefoneHandler extends Handler{
		public static final int TAG_TELEFONE = 0;
		Activity __activity;
		public ButtonTelefoneHandler(Activity pActivity){
			__activity = pActivity;
		}
		@Override
        public void handleMessage(Message msg)
        {
			switch (msg.what) {
			
			
			case TAG_TELEFONE:
				Bundle vParameter = msg.getData();
				String vNumeroTelefone = null;
				if(vParameter != null){
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE)){
						vNumeroTelefone = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE);
						if(vNumeroTelefone != null && vNumeroTelefone.length() > 0 ){
							__button.setText(vNumeroTelefone);
							HelperPorta.savePorta(__activity, vNumeroTelefone);
							__button.setTextSize(20);
						}
					}
				}
				
				if(vNumeroTelefone == null || vNumeroTelefone.length() == 0){
					vNumeroTelefone = __activity.getResources().getString(R.string.digite_porta);
					__button.setText(vNumeroTelefone);
					__button.setTextSize(13);
				}
				
				__button.postInvalidate();
				break;
			

			default:
				break;
			}
			
        }
	}
}
