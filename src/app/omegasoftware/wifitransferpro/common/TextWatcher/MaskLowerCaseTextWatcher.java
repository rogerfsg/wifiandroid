package app.omegasoftware.wifitransferpro.common.TextWatcher;

import android.widget.EditText;



public class MaskLowerCaseTextWatcher extends InterfaceMaskTextWatcher{
	//	(31) 3333 - 3333
	//123456789A
	//  0,
	
		public MaskLowerCaseTextWatcher(EditText pEditText){
			super(pEditText, false);
		}
		
		public MaskLowerCaseTextWatcher(EditText pEditText, int pMaxSize){
			super(pEditText, false, pMaxSize);
		}

		public String getValidToken(String pToken){
			
			String vNewToken = "";
			if(pToken != null){
				vNewToken = pToken.toLowerCase();
			}
			return vNewToken;
		}

}