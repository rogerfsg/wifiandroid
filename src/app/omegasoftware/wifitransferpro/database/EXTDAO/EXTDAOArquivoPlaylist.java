package app.omegasoftware.wifitransferpro.database.EXTDAO;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;

public class EXTDAOArquivoPlaylist extends Table{

	public static final String NAME = "arquivo_playlist";
	public static String ID = "id";
	public static String PLAYLIST_ID_INT = "playlist_id_INT";
	public static String PATH = "path";
	
	
	
	public static String IS_DIRETORIO_BOOLEAN = "is_parser_check_BOOLEAN";
	public EXTDAOArquivoPlaylist(Database p_database) {
		super(NAME, p_database);

		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));
		super.addAttribute(new Attribute(PLAYLIST_ID_INT, EXTDAOPlaylist.NAME, EXTDAOPlaylist.ID,  "arquivo da playlist", true, Attribute.SQL_TYPE.LONG, true, null, false, null, 11, Attribute.TYPE_RELATION_FK.CASCADE, Attribute.TYPE_RELATION_FK.CASCADE));
		super.addAttribute(new Attribute(PATH, "path", true, Attribute.SQL_TYPE.TEXT, false, null, false, null, 255));
		setUniqueKey(new String[]{PLAYLIST_ID_INT, PATH});
		
	
		
	}
	@Override
	public void populate() {
		
		

	}

	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAOArquivoPlaylist(this.getDatabase());
	}

	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId, String pOldId) {
		
	}
	
	
	
	public ArrayList<File> getListFile(String pIdPlaylist){
		
		HashMap<String, String> vHashIdPorPath = getListPathArquivoDaPesquisa(pIdPlaylist);
		if(vHashIdPorPath == null) return null;
		ArrayList<File> vListFile = new ArrayList<File>();
		Set<String> vKeySetId = vHashIdPorPath.keySet();
		Iterator<String> vIterator = vKeySetId.iterator();
		while(vIterator.hasNext()){
			String vId = vIterator.next();
			String vPath = vHashIdPorPath.get(vId);
			File vFile=  new File(vPath);
			if(vFile.exists()){
				vListFile.add(vFile);
			}
			else{
				remove(vId, false);
			}
		}
		return vListFile;
	}
	
	public HashMap<String, File> getHashIdPorFile(String pIdPlaylist){
		HashMap<String, File> vHash = new HashMap<String, File>(); 
		HashMap<String, String> vHashIdPorPath = getListPathArquivoDaPesquisa(pIdPlaylist);
		if(vHashIdPorPath == null) return null;
		
		Set<String> vKeySetId = vHashIdPorPath.keySet();
		Iterator<String> vIterator = vKeySetId.iterator();
		while(vIterator.hasNext()){
			String vId = vIterator.next();
			String vPath = vHashIdPorPath.get(vId);
			File vFile=  new File(vPath);
			if(vFile.exists()){
				vHash.put(vId, vFile);
			} else{
				remove(vId, false);
			}
		}
		return vHash;
	}
	
	public HashMap<String, String> getListPathArquivoDaPesquisa(String pIdPlaylist){		
		
		try{
			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
			
			String vQuery = "SELECT pa." + EXTDAOPesquisaArquivo.ID + ", pa." + EXTDAOPesquisaArquivo.PATH + " " +
					" FROM " + EXTDAOArquivoPlaylist.NAME + " AS pa " + 
					" WHERE pa." + EXTDAOArquivoPlaylist.PLAYLIST_ID_INT + " = ?" ;
			HashMap<String, String> vHashIdPorPath = new HashMap<String, String>(); 

			Cursor vCursor = v_SQLiteDatabase.rawQuery(vQuery, 
					new String[]{
					pIdPlaylist});

			
			ArrayList<String> vListStrId = new ArrayList<String>();
			if (vCursor.moveToFirst()) {
				do {
					String vToken = vCursor.getString(0);
					String vPath = vCursor.getString(1);
					vHashIdPorPath.put(vToken, vPath);
					vListStrId.add(vToken);
											
				} while (vCursor.moveToNext());
			}

			if (vCursor != null && !vCursor.isClosed()) {	    	  
				vCursor.close();
			}
			return vHashIdPorPath;
		}catch(Exception ex){
			int i = 0 ;
			i += 1;
		}
		return null;
	}
	
}
