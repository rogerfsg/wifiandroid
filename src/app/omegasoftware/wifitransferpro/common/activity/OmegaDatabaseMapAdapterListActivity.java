package app.omegasoftware.wifitransferpro.common.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.TabletActivities.AboutActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.InformationDialog;
import app.omegasoftware.wifitransferpro.common.Adapter.DatabaseCustomAdapter;


public abstract class OmegaDatabaseMapAdapterListActivity extends ListActivity {

	private String TAG = "OmegaDatabaseMapAdapterListActivity";

	private ToggleButton azOrderByButton;
	private ToggleButton zaOrderByButton;
	private ToggleButton nearMeOrderByButton;


	protected HandlerNotifyChange __handlerNotify ;

	//Order by mode
	private short currentOrderByMode = OmegaConfiguration.DEFAULT_ORDER_BY_MODE;

	protected abstract void loadCommomParameters(Bundle pParameters);
	protected abstract void buildAdapter (boolean pIsAsc);

	protected DatabaseCustomAdapter adapter;

	String __nomeTag = null;
	Class<?> __classList = null;
	Class<?> __classDetail = null;
	protected ArrayList<String> __listIdPrestadorEnderecoDialog = new ArrayList<String>();
	protected Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_map_activity_mobile_layout);
		if(getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TAG))
			__nomeTag = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_TAG);

		__classList = (Class<?>) getIntent().getSerializableExtra(OmegaConfiguration.SEARCH_FIELD_CLASS_LIST);
		__classDetail = (Class<?>) getIntent().getSerializableExtra(OmegaConfiguration.SEARCH_FIELD_CLASS_DETAIL);

		__handlerNotify = new HandlerNotifyChange();
		try{
			new CustomDataLoader().execute();
		}
		catch(Exception e)
		{
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}

	}

	public void setTag(String p_tag)
	{
		this.TAG = p_tag;
	}

	public void addIdPrestadorEnderecoDialog(String p_idPrestadorEndereco){
		__listIdPrestadorEnderecoDialog.add(p_idPrestadorEndereco);
	}
	public void clearPrestadorEnderecoDialog(){
		__listIdPrestadorEnderecoDialog.clear();
	}	
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;
	}

	protected void loadParameters()	
	{
		Bundle vParameters = getIntent().getExtras();
		if(vParameters != null)
		{
			loadCommomParameters(vParameters);			
		}
	}
	public void performExecute(){
		new CustomDataLoader().execute();
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;


		switch (item.getItemId()) {
		case R.id.menu_favorito:
			
			break;

		case R.id.menu_sobre:
			intent = new Intent(this, AboutActivityMobile.class);
			break;

		default:
			return super.onOptionsItemSelected(item);


		}
		if(intent != null){
			startActivity(intent);	
		}
		return true;

	}

	@Override
	public void finish(){


		super.finish();

	}
	protected Boolean getSavedDialogInformacao()
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		if(vSavedPreferences.contains(__nomeTag + "_dialog_informacao"))
			return vSavedPreferences.getBoolean(__nomeTag + "_dialog_informacao", false);
		else return false;
	}

	//Save Cidade ID in the preferences
	protected void saveDialogInformacao()
	{

		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putBoolean(__nomeTag + "_dialog_informacao", true);
		vEditor.commit();

	}

	boolean __isInformacaoDialogShow = false;
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.unimedbh_dialog_error_textview);
		Button vOkButton = (Button) vDialog.findViewById(R.id.unimedbh_dialog_ok_button);

		try{
			switch (id) {
			case OmegaConfiguration.ON_LOAD_DIALOG:
				ProgressDialog vDialogAux = new ProgressDialog(this);

				vDialogAux.setMessage(getResources().getString(R.string.string_loading));
				return vDialogAux;
			case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
				vTextView.setText(getResources().getString(R.string.dialog_noresult_alert));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
						Intent vIntent = getIntent();
						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
						finish();
					}
				});
				break;
			case OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG:
				if(__isInformacaoDialogShow) return null;
				else __isInformacaoDialogShow = true;
				InformationDialog vInformation = new InformationDialog(this, vDialog, __nomeTag + "_dialog_informacao", OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG);
				return vInformation.getDialogInformation(new int[]{R.string.informacao_uso_list_detalhes,
						R.string.informacao_uso_list_gerenciamento});


			default:
				//			vDialog = this.getErrorDialog(id);
				return null;
			}
		} catch(Exception ex){
			return null;
		}
		return vDialog;

	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent pIntent) {
		
		try{
			switch (requestCode) {
			case OmegaConfiguration.ACTIVITY_LIST_MAP:
				switch (resultCode) {
				case OmegaConfiguration.RESULT_CODE_TRACA_ROTA_CARRO:
					Intent v_intent = getIntent();
					v_intent.putExtras(pIntent.getExtras());
					setResult(resultCode, v_intent);
					finish();
					break;
				case OmegaConfiguration.RESULT_CODE_TRACA_ROTA_A_PE:
					Intent v_intent2 = getIntent();
					v_intent2.putExtras(pIntent.getExtras());
					setResult(resultCode, v_intent2);
					finish();
					break;
				default:
					break;
				}
				break;
			default:
				boolean vIsModificated = true;
				if(pIntent != null){
					Bundle pParameter = pIntent.getExtras();

					if(pParameter != null)
						if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
							vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
					if(vIsModificated)
						performExecute();

					break;
				}


				break;

			}
		}catch(Exception ex){
			Log.e( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
		}

	}



	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean>
	{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				return loadData();
			}
			catch(Exception e)
			{
				Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());			
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			if(result)
			{
				initializeComponents();
			}

			try
			{
				dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);	
			}
			catch(Exception e)
			{
			}
			showDialogNoResultIfNecessary();

		}		
	}

	private void showDialogNoResultIfNecessary()
	{
		if(adapter!= null)
			if(adapter.getCount() <= 0)
			{
				showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
			}
	}


	public class AdapterChangeListener implements OnCheckedChangeListener 
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			boolean vIsAsc = azOrderByButton.isChecked(); 
			buildAdapter(vIsAsc);
		}
	}

	private void setOrderByMode(short p_orderByMode){
		this.currentOrderByMode = p_orderByMode;

	}



	public void initializeComponents() {

		this.customTypeFace = Typeface.createFromAsset(this.getAssets(),"trebucbd.ttf");

		((ToggleButton) findViewById(R.id.toogle_a_z_button)).setTypeface(this.customTypeFace);
		((ToggleButton) findViewById(R.id.toogle_z_a_button)).setTypeface(this.customTypeFace);
		((ToggleButton) findViewById(R.id.toogle_near_me_button)).setTypeface(this.customTypeFace);

		//Attach toogle_a_z_button to ZAOrderByTypeChangeListener()
		this.zaOrderByButton = (ToggleButton) findViewById(R.id.toogle_z_a_button);
		this.zaOrderByButton.setOnCheckedChangeListener(new ZAOrderByTypeChangeListener());

		nearMeOrderByButton = (ToggleButton) findViewById(R.id.toogle_near_me_button);
		this.nearMeOrderByButton.setOnCheckedChangeListener(new NearMeOrderByTypeChangeListener());
		//Attach toogle_z_a_button to AZOrderByTypeChangeListener()
		this.azOrderByButton = (ToggleButton) findViewById(R.id.toogle_a_z_button);
		this.azOrderByButton.setOnCheckedChangeListener(new AZOrderByTypeChangeListener());

		//Sets default search mode
		switch (this.currentOrderByMode) {
		case OmegaConfiguration.ORDER_BY_MODE_A_Z:
			this.azOrderByButton.setChecked(true);
			this.zaOrderByButton.setChecked(false);
			this.nearMeOrderByButton.setChecked(false);
			break;
		case OmegaConfiguration.ORDER_BY_MODE_NEAR_ME:
			this.nearMeOrderByButton.setChecked(true);
			this.zaOrderByButton.setChecked(false);
			this.azOrderByButton.setChecked(false);
			break;
		case OmegaConfiguration.ORDER_BY_MODE_Z_A:
			this.zaOrderByButton.setChecked(true);
			this.azOrderByButton.setChecked(false);
			this.nearMeOrderByButton.setChecked(false);
			break;
		}

		setListAdapter(adapter);
	}





	public boolean loadData() {
		loadParameters();		
		buildAdapter();		

		return (adapter!=null?true:false);
	}

	private void buildAdapter ()
	{
		buildAdapter(true);	
	}

	public class NearMeOrderByTypeChangeListener implements OnCheckedChangeListener 
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			if(isChecked)
			{
				azOrderByButton.setChecked(false);
				azOrderByButton.setTextColor(R.color.dark_gray);
				zaOrderByButton.setChecked(false);
				zaOrderByButton.setTextColor(R.color.dark_gray);

				nearMeOrderByButton.setTextColor(R.color.white);
				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_NEAR_ME);
				ListUpdate(true, true);

			}
		}
	}

	public class ZAOrderByTypeChangeListener implements OnCheckedChangeListener 
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			if(isChecked)
			{
				azOrderByButton.setChecked(false);
				azOrderByButton.setTextColor(R.color.dark_gray);
				nearMeOrderByButton.setChecked(false);
				nearMeOrderByButton.setTextColor(R.color.dark_gray);

				zaOrderByButton.setTextColor(R.color.white);
				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_Z_A);
				ListUpdate(false, false);

			}
		}
	}
	public class AZOrderByTypeChangeListener implements OnCheckedChangeListener 
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			if(isChecked)
			{
				nearMeOrderByButton.setChecked(false);
				nearMeOrderByButton.setTextColor(R.color.dark_gray);
				zaOrderByButton.setChecked(false);
				zaOrderByButton.setTextColor(R.color.dark_gray);

				azOrderByButton.setTextColor(R.color.white);
				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_A_Z);
				ListUpdate(false, true);
			}
		}

	}



	private void MyNotify(){
		try{
			adapter.notifyDataSetChanged();	
		}catch(Exception ex){
			int i = 0;
			i += 1;
		}

	}

	public class HandlerNotifyChange extends Handler{

		@Override
		public void handleMessage(Message msg)
		{	
			MyNotify();
		}
	}


	public class LoaderSort extends AsyncTask<String, Integer, Boolean>
	{

		boolean __isAsc;

		boolean __isNearMe;
		Activity __activity;
		public LoaderSort(Activity pActivity, boolean pIsAsc, boolean pIsNearMe){
			__isAsc = pIsAsc;

			__isNearMe = pIsNearMe;
			__activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try{
				__activity.showDialog(OmegaConfiguration.ON_LOAD_DIALOG);	
			}catch(Exception ex){

			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				if(__isNearMe){
					try{
						adapter.SortListNearMe(true);
					} catch(Exception ex){

					}

				}else {
					adapter.SortList(__isAsc);
				}


				__handlerNotify.sendEmptyMessage(0);
			}
			catch(Exception e)
			{
				int i = 0;
				i += 1;
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				__activity.dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);
			} catch(Exception ex){

			}
		}
	}


	private void ListUpdate(boolean pIsNearMe, Boolean pAsc)
	{
		if(adapter.getCount() == 0)
		{
			showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);

		} else {

			new LoaderSort(this, pAsc, pIsNearMe).execute();
		}
	}

}
