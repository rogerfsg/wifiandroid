package app.omegasoftware.wifitransferpro.mapa.container;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import app.omegasoftware.wifitransferpro.R;

import com.google.android.maps.GeoPoint;

public class ContainerMarcadorMapa {
	private int __index ;
	
	public GeoPoint __geoPoint = null;
	
	final public String __tituloBalao ;
	final public String __textoBalao ;
	final public Drawable __icone;
	
	private boolean __isInitialized = false;
	
	public ContainerMarcadorMapa(
			GeoPoint pGeoPoint,
			String pTituloBalao, 
			String pTextoBalao, 
			Activity pActivity){
		
		
		__geoPoint= pGeoPoint;
		__textoBalao = pTextoBalao;
		__tituloBalao = pTituloBalao;
		__icone = pActivity.getResources().getDrawable(R.drawable.marcador);
	}
	public ContainerMarcadorMapa(
			GeoPoint pGeoPoint,
			String pTituloBalao, 
			String pTextoBalao, 
			Drawable pIcone){
		
		
		__geoPoint= pGeoPoint;
		__textoBalao = pTextoBalao;
		__tituloBalao = pTituloBalao;
		__icone = pIcone;
	}
	
	public void setGeoPoint(GeoPoint pGeoPoint){
		__geoPoint = pGeoPoint;
	}
	
	public void setIndex(Integer pIndex){
		__index = pIndex;
	}
	
	public int getIndex(){
		return __index;
	}
	
	public void setInitialized(){
		__isInitialized = true;
	}
	
	public boolean isInitialized(){
		return __isInitialized;
	}
	
}
