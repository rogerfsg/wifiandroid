package app.omegasoftware.wifitransferpro.common.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;


import android.app.Activity;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Table;
import android.os.Handler;
import android.os.Message;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class TableDependentAutoCompleteTextView {
	
	LinkedHashMap<String, String> __hashIdByDefinition;
	
	String __attributeName = Table.NOME_UNIVERSAL;
	String __attributeDependentName ;
	
	Table __table;
	
	public TableDependentAutoCompleteTextView(
			Table pTable, 
			String pStrAttributeNameDependent){
		
		__attributeDependentName = pStrAttributeNameDependent;
		__table = pTable;
		
	}
	
	public boolean isEmpty(){
		if(__hashIdByDefinition == null) return true;
		else if(__hashIdByDefinition.size() == 0 ) return true;
		else return false;
	}
	
	public LinkedHashMap<String, String> getHashIdByDefinition(String pValueAttributeDependent){
		__table.setAttrStrValue(__attributeDependentName, pValueAttributeDependent);
		return __table.getHashMapIdByAttributeValue(true, __attributeName);
	}
	
	public String[] getVetorStrId(String pValueAttributeDependent){
		
		__hashIdByDefinition = getHashIdByDefinition(pValueAttributeDependent);
		if(__hashIdByDefinition == null) return new String[]{};
		Set<String> vListKey = __hashIdByDefinition.keySet();
		ArrayList<String> vListId = new ArrayList<String>(); 
		for (String vKey : vListKey) {
			String vContent = __hashIdByDefinition.get(vKey);
			vListId.add(vContent);
		}
		String vVetorId[] = new String[vListId.size()];
		vListId.toArray(vVetorId);
		return vVetorId;
	}
	

	public String getIdOfTokenIfExist(String pToken){
		if(__hashIdByDefinition == null){
			
			return null;
		}
		Set<String> vListKey = __hashIdByDefinition.keySet();
		for (String vKey : vListKey) {
			String vContent = __hashIdByDefinition.get(vKey);
			if(pToken.compareTo(vContent) == 0 ) return vKey;
			
		}
		return null;
	}

	
	public String getTokenOfId(String pId){
		if(__hashIdByDefinition == null) return null;
		if(__hashIdByDefinition.containsKey(pId))
			return __hashIdByDefinition.get(pId);
		else return null;
	}
	
	
	//Envia o objeto Table com todos os atributos que compoe a unicidade da tupla
		public String addTupleIfNecessay(Table pTable){
			String vValueAttribute = pTable.getStrValueOfAttribute(__attributeName);
			
			String vId = getIdOfTokenIfExist(vValueAttribute);
			if(vId != null) return vId;
			else {
				pTable.formatToSQLite();
				Long vNewId = pTable.insert();
				if(vNewId >= 0 ) return vNewId.toString(); 
				else{
					pTable.setAttrStrValue(Table.ID_UNIVERSAL, null);
					Table vTupla = pTable.getFirstTupla();
					if(vTupla != null) return vTupla.getStrValueOfAttribute(Table.ID_UNIVERSAL);
					else return null;
				}
			}
		}
		
}
