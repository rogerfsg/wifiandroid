package app.omegasoftware.wifitransferpro.mapa;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import android.graphics.drawable.Drawable;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class MapaItemizedOverlay extends ItemizedOverlay<OverlayItem> {

	
	private List<OverlayItem> listOverlay = new ArrayList<OverlayItem>();
	
	public List<Overlay> listMapOverlay;
	
	
	public MapaItemizedOverlay(Drawable defaultMarker, List<Overlay> pListMapOverlay) {
		super(boundCenterBottom(defaultMarker));
		this.listMapOverlay = pListMapOverlay;
	}
	
//retorna o indice adicionado
	public void addOverlay(OverlayItem overlay) {
//		Id do mListOverlay pelo id do listMapOverlay
		
		this.listOverlay.add(overlay);
		this.listMapOverlay.add(this);
		
	    populate();
	}
	
//	TODO conferir hash
	public void remove(Integer pIndexOverlay){
	
		listOverlay.remove(pIndexOverlay);
		this.listMapOverlay.remove(pIndexOverlay);
	
	}
	
	public void limparOverlay(){
		this.listOverlay.clear();
		
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		if(this.listOverlay.size() > i)
			return this.listOverlay.get(i);
		else return null;
	}

	@Override
	public int size() {
		return this.listOverlay.size();
	}
	
}

