package app.omegasoftware.wifitransferpro.common.TextWatcher;

import android.widget.EditText;


public class CNPJTextWatcher extends MaskNumberTextWatcher{
	//	13.265.729/0001-31
	public CNPJTextWatcher(EditText pEditText){
		super(pEditText, new int[]{3, 7, 11, 16}, new String[] {".", ". ", "/", "-"}, 14);
	}


}