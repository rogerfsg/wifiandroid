package app.omegasoftware.wifitransferpro.common;

import java.util.Comparator;

import app.omegasoftware.wifitransferpro.common.ItemList.CustomItemList;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;



public class IntegerListComparator implements Comparator<Object> {
	private boolean isAsc;
	
	public IntegerListComparator(boolean p_isAsc)
	{
		this.isAsc = p_isAsc;
	}
	
    public int compare(Object o1, Object o2)
    {
        return compare((Integer)o1, (Integer)o2);
    }
    
    public int compare(Integer o1, Integer o2)
    {
    	
    	
    	if(o1 != null && o2 != null){
    		
			if(isAsc)
            	if(o1 > o2 ) return 1;
            	else if(o1 == o2) return 0;
            	else return -1;
            else
            	if(o1 < o2 ) return 1;
            	else if(o1 == o2) return 0;
            	else return -1;
    		
    	} 
    	
    	return 0;
    }

}
