package app.omegasoftware.wifitransferpro.database.EXTDAO;

import android.content.Context;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;

public class EXTDAOLink extends Table{

	public static final String NAME = "link";
	
	public static String ID = "id";
	public static String NOME = "nome";
	public static String URL = "url";
	public static String PATH_IMAGEM = "path_imagem";
	
	
	public EXTDAOLink(Database p_database) {
		super(NAME, p_database);

		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));
		super.addAttribute(new Attribute(NOME, "nome", true, Attribute.SQL_TYPE.TEXT, true, null, false, null, 100));
		super.addAttribute(new Attribute(URL, "url", true, Attribute.SQL_TYPE.TEXT, true, null, false, null, 255));
//		link para a imagem no site/OmegaWikiTransfer/
		super.addAttribute(new Attribute(PATH_IMAGEM, "link_imagem", true, Attribute.SQL_TYPE.TEXT, true, null, false, null, 255));
	}
	
	public static TYPE_ICONE getTypeOfURL(String pURL){
		if(pURL != null && pURL.length() > 0 ){
			if(pURL.contains("tipo=lists&page=playlist"))
				return TYPE_ICONE.MUSICA;
			else if(pURL.contains("tipo=lists&page=arquivo"))
				return TYPE_ICONE.EXPLORER;
			else if(pURL.contains("tipo=lists&page=imagem"))
				return TYPE_ICONE.IMAGENS;
			else if(pURL.contains("tipo=lists&page=playlist_musica"))
				return TYPE_ICONE.MUSICA;
			
			else return TYPE_ICONE.EXPLORER;
		}
		return TYPE_ICONE.EXPLORER;
	}
	
	public static enum TYPE_ICONE{
		EXPLORER,
		IMAGENS,
		MUSICA
	}

	public static String getPathIcone(TYPE_ICONE pType){
		switch (pType) {
		case EXPLORER:
			return "adm/imgs/especifico/topo_folders.png";
		case IMAGENS:
			return "adm/imgs/especifico/topo_pictures.png";
		case MUSICA:
			return "adm/imgs/especifico/topo_music.png";


		default:
			return "";
		}
	}

	
	@Override
	public void populate() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAOLink(getDatabase());
	}
	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId,
			String pOldId) {
		// TODO Auto-generated method stub
		
	}

}
