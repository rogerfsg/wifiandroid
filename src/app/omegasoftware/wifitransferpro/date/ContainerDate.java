package app.omegasoftware.wifitransferpro.date;

import java.util.Calendar;


public class ContainerDate {
	public Integer __mes=null;
	public Integer __dia=null;
	public Integer __ano=null;
	
	
	public ContainerDate(){

        final Calendar c = Calendar.getInstance();
        __ano = c.get(Calendar.YEAR);
        __mes = c.get(Calendar.MONTH);
        __dia = c.get(Calendar.DAY_OF_MONTH);
	
	}
	
	public ContainerDate(int ano, int mes, int dia){

        
        __ano = ano;
        __mes = mes;
        __dia = dia;
	
	}
	
	

	
	public Integer getAno(){
		return __ano;
	}
	
	public Integer getMes(){
		return __mes;
	}
	
	public Integer getDia(){
		return __dia;
	}
	
	public void setAno(Integer pAno){
		__ano = pAno;
	}
	
	public void setMes(Integer pMes){
		__mes = pMes;
	}
	
	public void setDia(Integer pDia){
		__dia = pDia;
	}
}
