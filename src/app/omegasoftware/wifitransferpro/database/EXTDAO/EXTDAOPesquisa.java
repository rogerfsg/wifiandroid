package app.omegasoftware.wifitransferpro.database.EXTDAO;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.database.DAO.DAOArquivo;
import app.omegasoftware.wifitransferpro.file.FilenameFilterLikeAndExtension;
import app.omegasoftware.wifitransferpro.file.FilesUnzipper;
import app.omegasoftware.wifitransferpro.file.FilesZipper;
import app.omegasoftware.wifitransferpro.file.FilterFileIsDirectory;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.HelperZip;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.NanoHTTPD;
import app.omegasoftware.wifitransferpro.http.pages.ContainerMultiple;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM;

public class EXTDAOPesquisa extends Table{

	public static final String NAME = "pesquisa";
	public static String ID = "id";
	public static String IS_RUNNING_BOOLEAN = "is_running_BOOLEAN";
	public static String PATH_DIRETORIO = "path_diretorio";
	public static String VETOR_NOME = "vetor_nome";
	public static String VETOR_EXTENSAO = "vetor_extensao";
	
	public EXTDAOPesquisa(Database p_database) {
		super(NAME, p_database);

		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));
		
		super.addAttribute(new Attribute(IS_RUNNING_BOOLEAN, "est� rodando", true, Attribute.SQL_TYPE.LONG, true, null, false, null, 1));
		super.addAttribute(new Attribute(PATH_DIRETORIO, "path_diretorio", true, Attribute.SQL_TYPE.TEXT, true, null, false, null, 255));
		super.addAttribute(new Attribute(VETOR_NOME, "vetor_nome", true, Attribute.SQL_TYPE.TEXT, true, null, false, null, 255));
		super.addAttribute(new Attribute(VETOR_EXTENSAO, "vetor_extensao", true, Attribute.SQL_TYPE.TEXT, true, null, false, null, 255));


	}
	
	

	
	@Override
	public void populate() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAOPesquisa(getDatabase());
	}
	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId,
			String pOldId) {
		// TODO Auto-generated method stub
		
	}

}
