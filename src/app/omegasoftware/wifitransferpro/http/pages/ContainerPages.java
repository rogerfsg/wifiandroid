package app.omegasoftware.wifitransferpro.http.pages;

import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.NanoHTTPD;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class ContainerPages {
	public static NanoHTTPD.Response getHTML(String pPage, HelperHttp pHelper){
		ContainerResponse vContainer = getContainerResponse(pPage, pHelper);
		if(vContainer != null) return vContainer.getResponse();
		else return null;
	}
	
	public static ContainerResponse getContainerResponse(String pPage, HelperHttp pHelper){
		if(pPage != null && pPage.length() > 0 ){
			
			if(pPage.compareTo("renomear") == 0){
				return new Renomear(pHelper).getContainerResponse();
			} else if(pPage.compareTo("RemoveArquivo") == 0){
				return new RemoveArquivo(pHelper).getContainerResponse();
			} else if(pPage.compareTo("DownloadArquivo") == 0){
				return new DownloadArquivo(pHelper).getContainerResponse();
			} else if(pPage.compareTo("ImageArquivo") == 0){
				return new ImageArquivo(pHelper).getContainerResponse();
			} else if(pPage.compareTo("ListsArquivoMove") == 0){
				return new ListsArquivoMove(pHelper).getContainerResponse();
			} else if(pPage.compareTo("ListsArquivoCopy") == 0){
				return new ListsArquivoCopy(pHelper).getContainerResponse();
			} else if(pPage.compareTo("ZipArquivo") == 0){
				return new ZipArquivo(pHelper).getContainerResponse();
			}else if(pPage.compareTo("ListsArquivoUnzip") == 0){
				return new ListsArquivoUnzip(pHelper).getContainerResponse();
			}else if(pPage.compareTo("CriaDiretorio") == 0){
				return new CriaDiretorio(pHelper).getContainerResponse();
			} else if(pPage.compareTo("Upload") == 0){
				return new Upload(pHelper).getContainerResponse();
			} else if(pPage.compareTo("MusicPagina") == 0){
				return new MusicPagina(pHelper).getContainerResponse();
			} else if(pPage.compareTo("PlayerMusic") == 0){
				return new PlayerMusic(pHelper).getContainerResponse();
			} else if(pPage.compareTo("MultipleDownloadArquivo") == 0){
				return new MultipleDownloadArquivo(pHelper).getContainerResponse();
			} else if(pPage.compareTo("TipoMultipleDownloadArquivo") == 0){
				return new TipoMultipleDownload(pHelper).getContainerResponse();
			} else if(pPage.compareTo("NovaPlaylist") == 0){
				return new NovaPlaylist(pHelper).getContainerResponse();
			} else if(pPage.compareTo("PlayerPlaylist") == 0){
				return new PlayerPlaylist(pHelper).getContainerResponse();
			} else if(pPage.compareTo("PopupPlayerList") == 0){
				return new PopupPlayerList(pHelper).getContainerResponse();
			} else if(pPage.compareTo("RemovePlaylistArquivo") == 0){
				return new RemovePlaylistArquivo(pHelper).getContainerResponse();
			} else if(pPage.compareTo("RemovePlaylist") == 0){
				return new RemovePlaylist(pHelper).getContainerResponse();
			} else if(pPage.compareTo("AdicionaArquivoPlaylist") == 0){
				return new AdicionaArquivoPlaylist(pHelper).getContainerResponse();
			} else if(pPage.compareTo("MultipleRemovePlaylistArquivo") == 0){
				return new MultipleRemovePlaylistArquivo(pHelper).getContainerResponse();
			} else if(pPage.compareTo("AdicionarLinkPagina") == 0){
				return new AdicionarLinkPagina(pHelper).getContainerResponse();
			}else if(pPage.compareTo("UploadPagina") == 0){
				return new UploadPagina(pHelper).getContainerResponse();
			}
			
						
			
		}
		return null;
	}
}
