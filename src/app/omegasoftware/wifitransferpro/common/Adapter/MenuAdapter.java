package app.omegasoftware.wifitransferpro.common.Adapter;

import java.util.ArrayList;
import java.util.Collections;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.IntegerListComparator;
import app.omegasoftware.wifitransferpro.common.ItemList.CustomItemList;
import app.omegasoftware.wifitransferpro.common.ItemList.MenuItemList;
import app.omegasoftware.wifitransferpro.common.container.AbstractContainerClassItem;
import app.omegasoftware.wifitransferpro.common.container.ContainerMenuOption;
import app.omegasoftware.wifitransferpro.common.container.programa.ContainerPrograma;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOTipoArquivo;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;

public class MenuAdapter extends CustomAdapter{

	private static final int ID_TEXT_VIEW_IMAGEVIEW = R.id.menu_imageview;
	private static final int ID_TEXT_VIEW_TEXTVIEW = R.id.menu_textview;
	DatabaseWifiTransfer db = null;

	public String nomeTagAtual;
	public String idPermissaoAtual;
	FactoryMenuActivityMobile activity;
	
	public MenuAdapter(
			FactoryMenuActivityMobile pActivity,
			String pNomeTagAtual)
	{
		super(pActivity, false, false, false, null);
		activity = pActivity;
		nomeTagAtual =pNomeTagAtual;
		db = new DatabaseWifiTransfer(this.context);
		
		super.initalizeListCustomItemList();
		db.close();
	}
	@Override
	public void finalize(){
		try{

			db.close();
			try {
				super.finalize();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}catch(Exception ex){

		}
	}

	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			MenuItemList vMenuItemList = (MenuItemList)this.getItem(pPosition);
			if(vMenuItemList == null) return null;
			LayoutInflater vLayoutInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.menu_item_list_layout, null);
			
			
			if((pPosition % 4) == 0)
			{
//				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundResource(Color.CYAN);
//				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundColor(activity.getResources().getColor(R.color.red_england));
				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundResource(R.drawable.menu_red_england_button);
			}
			else if((pPosition % 4) == 2)
			{
//				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundResource(Color.CYAN);
				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundResource(R.drawable.menu_blue_england_button);
//				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundColor(activity.getResources().getColor(R.color.blue_england));
			}
			else
			{
				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundResource(R.drawable.menu_white_england_button);
//				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundColor(Color.LT);
				
			}

			TextView vTextViewTitle = CreateTextView(
					ID_TEXT_VIEW_TEXTVIEW, 
					vMenuItemList.getConteudoContainerItem(MenuItemList.TEXTVIEW), 
					vLayoutView,
					false);
			if(vTextViewTitle != null && ((pPosition % 4) == 0 || (pPosition % 4) == 2))
			vTextViewTitle.setTextColor(Color.WHITE);

			Integer vIdDrawable = HelperInteger.parserInt(vMenuItemList.getConteudoContainerItem(MenuItemList.DRAWABLE));
			if(vIdDrawable == null) return null;
			ImageView vImageView = (ImageView) vLayoutView.findViewById(ID_TEXT_VIEW_IMAGEVIEW);
			Drawable vDrawable = context.getResources().getDrawable(vIdDrawable);
			vImageView.setImageDrawable(vDrawable);

			OnClickListener vOnClick = vMenuItemList.getClickListener();

			if(vOnClick != null){
				vLayoutView.setOnClickListener(vOnClick);
				vImageView.setOnClickListener(vOnClick);
			}
			else {
				int i = 0 ; 
				i += 1;
				i += 1;
			}
			return vLayoutView;
		} catch (Exception exc) {
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
//					"MenuAdapter: getView()");
		}
		return null;
	}

	@Override
	protected ArrayList<Integer> getListIdBySearchParameters(boolean pIsAsc) {

		// TODO Auto-generated method stub

		ArrayList<Integer> vListRetorno = new ArrayList<Integer>(); 
		try {
			EXTDAOTipoArquivo vObjTipoEntidade = new EXTDAOTipoArquivo(db);
			AbstractContainerClassItem[] vVetor = ContainerPrograma.getVetorContainerClassItem();
			for(int i = 0 ; i < vVetor.length ; i ++){
				AbstractContainerClassItem vContainer = vVetor[i];
				String vTag = vContainer.getTag();
				vObjTipoEntidade.setAttrStrValue(EXTDAOTipoArquivo.NOME, vTag);
				Table vTupla = vObjTipoEntidade.getFirstTupla();
				if(vTupla != null){
					String vIdTipoEntidade = vTupla.getStrValueOfAttribute(EXTDAOTipoArquivo.ID);
					vListRetorno.add(HelperInteger.parserInt(vIdTipoEntidade) );
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			db.close();
		}
		
		return vListRetorno;
	}



	@Override
	public CustomItemList getCustomItemListOfTable(Integer pId) {

		EXTDAOTipoArquivo vEXTDAOTipoEntidade = new EXTDAOTipoArquivo(db);
		String vStrId = String.valueOf(pId);
		if(vStrId == null) return null;
		vEXTDAOTipoEntidade.setAttrStrValue(EXTDAOTipoArquivo.ID, vStrId);
		if(vEXTDAOTipoEntidade.select()){
			String vTag = vEXTDAOTipoEntidade.getStrValueOfAttribute(EXTDAOTipoArquivo.NOME);
			
	//		String vNome = vEXTDAOPermissao.getStrValueOfAttribute(EXTDAOPermissao.NOME);
			
			AbstractContainerClassItem vContainer = ContainerMenuOption.getContainerItem(vTag);
			if(vContainer == null) return null;
			String vNome = vContainer.getNome(context);
			
			Integer vIdDrawable = null;
			vIdDrawable = vContainer.getDrawableId();
			if(vIdDrawable == null) return null;
			String vStrDrawable = String.valueOf(vIdDrawable);
			MenuItemList vItemList = new MenuItemList(
					vStrId, 
					vStrDrawable, 
					vNome,
					vContainer.getOnClickListener(activity));
	
			return vItemList;
		} else return null;
	}
}

