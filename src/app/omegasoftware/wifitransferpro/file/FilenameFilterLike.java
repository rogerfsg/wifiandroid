package app.omegasoftware.wifitransferpro.file;

import java.io.File;
import java.io.FilenameFilter;

public class FilenameFilterLike  implements FilenameFilter{
	String vetorNameFile[];
	String vetorExtension[];
	boolean isFileWithoutExtensionValid;
	public FilenameFilterLike(String pVetorNameFile[], String pVetorExtension[], boolean pIsFileWithoutExtensionValid){
		vetorNameFile = pVetorNameFile;
		vetorExtension = pVetorExtension;
		isFileWithoutExtensionValid = pIsFileWithoutExtensionValid;
	}
	
	public boolean accept(File dir, String filename) {
		// TODO Auto-generated method stub
		for (String vToken : vetorNameFile) {
			if(filename.contains(vToken)) return true;
		}
		for (String vExtension : vetorExtension) {
			if( isFileWithoutExtensionValid){
				if(! filename.contains(".")) return true ;
			}else if(filename.endsWith(vExtension)){
				return true;
			} 
		}
		return false;
	}

}
