package app.omegasoftware.wifitransferpro.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.widget.ImageView;

public class OmegaImageView extends ImageView{
	 private boolean makeRequest;
	    public OmegaImageView(Context context){
	        super(context);makeRequest=true;
	    }
	    public OmegaImageView(Context context, AttributeSet attrs) {
	        this(context, attrs, 0);
	    }
	    public OmegaImageView(Context context, AttributeSet attrs, int defStyle) {
	         super(context, attrs, defStyle);
	         makeRequest=true;
	    }
	    public void setImageBitmapNoRequestLayout(Bitmap bitmap) {
	        makeRequest = false;
	        setImageBitmap(bitmap);
	        makeRequest = true;
	        
	    }
	    
	    @Override protected void onAnimationEnd(){
	    	super.onAnimationEnd();
	    	
	    }
	    
	    @Override public void onFinishTemporaryDetach(){
	    	super.onFinishTemporaryDetach();
	    	
	    }
	    
	    
	    @Override protected void onCreateContextMenu(ContextMenu menu){
	    	super.onCreateContextMenu(menu);
	    	
	    	
	    }
	    
	    @Override public int[] onCreateDrawableState(int extraSpace){
	    	return super.onCreateDrawableState(extraSpace);
	    }
	    
	    @Override protected void onDraw(Canvas canvas){
	    	super.onDraw(canvas);
	    	
	    	
	    	
	    }
	    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
	    	super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    }
	    @Override protected void onFinishInflate(){
	    	
	    	super.onFinishInflate();
	    }
	    @Override protected void onDetachedFromWindow(){
	    	
	    	
	    	super.onDetachedFromWindow();
	    }
	    @Override protected void onAttachedToWindow(){
	    	super.onAttachedToWindow();
	    }
	    @Override protected void onAnimationStart(){
	    	
	    	super.onAnimationStart();
	    }
	    @Override protected void drawableStateChanged(){
	    	boolean vIsActivated = isDrawingCacheEnabled();
	    	
	    	boolean vIsShow = isShown();
	    	super.drawableStateChanged();
	    }
	    @Override public void requestLayout(){
//	        if(makeRequest)super.requestLayout();
	    	super.requestLayout();
	    }
	

}
