package app.omegasoftware.wifitransferpro.common.container;

import java.util.ArrayList;

import app.omegasoftware.wifitransferpro.common.container.programa.ContainerPrograma;
import app.omegasoftware.wifitransferpro.database.Database;




public class ContainerMenuOption{


	public static ArrayList<Integer> getListContainerItemComumATodosUsuarios(Database db, String pIdTagPai){
		AbstractContainerClassItem vVetorContainerClassItemOmegaEmpresa[] = ContainerPrograma.getVetorContainerClassItem();
		ArrayList<Integer> vList = new ArrayList<Integer>(); 
		for (AbstractContainerClassItem containerClassItem : vVetorContainerClassItemOmegaEmpresa) {
			if(containerClassItem.isComumATodosUsuarios()){
				String vTag = containerClassItem.getTag();
				
				
			}
			
		}
		return vList;
	}
	
	public static boolean isContainerItemExistent(String pTag){
		AbstractContainerClassItem vContainer = getContainerItem(pTag);
		if(vContainer == null) return false;
		else return true;
	}
	
	public static AbstractContainerClassItem getContainerItem(String pTag){
		if(pTag == null) return null;
		AbstractContainerClassItem vVetorContainerClassItemOmegaEmpresa[] = ContainerPrograma.getVetorContainerClassItem();
		for (AbstractContainerClassItem containerClassItem : vVetorContainerClassItemOmegaEmpresa) {
			if(containerClassItem.isEqual(pTag)) return containerClassItem;
		}
		return null;
	}
	
	public static ArrayList<AbstractContainerClassItem> getListContainerItem(ArrayList<String> pListTag){
		ArrayList<AbstractContainerClassItem> vList = new ArrayList<AbstractContainerClassItem>();
		for (String vToken : pListTag) {
			AbstractContainerClassItem vContainer = getContainerItem(vToken);
			if(vContainer != null) vList.add(vContainer);
		}
		
		return vList;
	}
	
}
