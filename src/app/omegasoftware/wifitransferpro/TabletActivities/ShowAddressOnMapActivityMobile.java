package app.omegasoftware.wifitransferpro.TabletActivities;

import java.util.ArrayList;


import android.app.Dialog;
import android.app.ProgressDialog;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.activity.OmegaMapActivity;
import app.omegasoftware.wifitransferpro.date.HelperDate;
import app.omegasoftware.wifitransferpro.mapa.HelperMapa;
import app.omegasoftware.wifitransferpro.mapa.container.ContainerMarcadorMapa;
import android.os.Bundle;

import com.google.android.maps.GeoPoint;

public class ShowAddressOnMapActivityMobile extends OmegaMapActivity {

	public static String TAG = "ShowAddressOnMapActivityMobile";
	
	ArrayList<String> __listEndereco;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.map_show_address_layout_mobile);

		new CustomDataLoader(this).execute();

		Bundle vParameters = getIntent().getExtras();

		__listEndereco = vParameters.getStringArrayList(OmegaConfiguration.MAP_ADDRESS);
		
	}


	/**
	 * Creates loading dialog
	 * 
	 */
	@Override
	protected Dialog onCreateDialog(int id) {

		ProgressDialog dialog = null;

		switch (id) {
		case OmegaConfiguration.ON_LOAD_DIALOG:
			dialog = new ProgressDialog(this);
			dialog.setMessage(getResources().getString(R.string.string_loading));
			break;

		default:
			break;
		}

		return dialog;

	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public boolean loadData() {


		return true;

	}

	@Override
	public void initializeComponents(){

		if(__listEndereco != null)
			if(__listEndereco.size() > 0 ){
				String v_endereco = __listEndereco.get(0);
				GeoPoint v_geo = HelperMapa.getGeoPointDoEndereco(v_endereco);
				if(v_geo != null){
					loadPrincipalContainerMarcadorMapa(
							new ContainerMarcadorMapa(
									v_geo, 
									HelperDate.getDataAtualFormatada(), 
									v_endereco, 
									this));
					
					if(__listEndereco.size() > 1 ){	
						for(int i = 1 ; i < __listEndereco.size(); i ++){
							v_endereco = __listEndereco.get(i);
							v_geo = HelperMapa.getGeoPointDoEndereco(v_endereco);
							loadPrincipalContainerMarcadorMapa(
									new ContainerMarcadorMapa(
											v_geo, 
											HelperDate.getDataAtualFormatada(), 
											v_endereco, 
											this));
						}
					}
				}
			}

	}


}
