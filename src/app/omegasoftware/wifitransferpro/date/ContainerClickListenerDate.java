package app.omegasoftware.wifitransferpro.date;

import java.util.Calendar;
import java.util.Date;


import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.listener.DateClickListener;

public class ContainerClickListenerDate {
	public Integer __mes=null;
	public Integer __dia=null;
	public Integer __ano=null;
	private Button __button;
	DateClickListener __clickListener = null;
	
	public ContainerClickListenerDate(Button pButton, DateClickListener vListener){

        final Calendar c = Calendar.getInstance();
        __ano = c.get(Calendar.YEAR);
        __mes = c.get(Calendar.MONTH);
        __dia = c.get(Calendar.DAY_OF_MONTH);
        __button = pButton;
        __clickListener = vListener;
		
	}
	
	public void setDateOfView(Integer pAno, Integer pMes, Integer pDia){
		if(pAno != null && pMes != null && pDia != null){
		__ano = pAno;
		__mes = pMes;
		__dia = pDia;
		
		__clickListener.setDateOfView(pAno, pMes, pDia);
		}
	}
	
	public Date getDate(){
		return new Date(__ano,__mes,__dia);
	}
	
	public TextView getButton(){
		return __button;
	}
	public void setTextView(){
		__button.setText(new StringBuilder()
        // Month is 0 based so add 1
        .append(__mes + 1).append("-")
        .append(__dia).append("-")
        .append(__ano));
	}
	
	public Integer getAno(){
		return __ano;
	}
	
	public Integer getMes(){
		return __mes;
	}
	
	public Integer getDia(){
		return __dia;
	}
	
	public void setAno(Integer pAno){
		__ano = pAno;
	}
	
	public void setMes(Integer pMes){
		__mes = pMes;
	}
	
	public void setDia(Integer pDia){
		__dia = pDia;
	}
}
