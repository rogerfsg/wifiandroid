package app.omegasoftware.wifitransferpro.common;

import java.util.Comparator;

import app.omegasoftware.wifitransferpro.common.ItemList.CustomItemList;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;



public class NameCustomItemListComparator implements Comparator<Object> {
	private boolean isAsc;
	private String nameField;
	public NameCustomItemListComparator(boolean p_isAsc, String pNameField)
	{
		this.isAsc = p_isAsc;
		this.nameField = pNameField;
	}
	
	
    public int compare(Object o1, Object o2)
    {
        return compare((CustomItemList)o1, (CustomItemList)o2);
    }
    
    public int compare(CustomItemList o1, CustomItemList o2)
    {
    	CustomItemList v_custom1 =  ((CustomItemList)o1);
    	CustomItemList v_custom2 =  ((CustomItemList)o2);
    	
    	if(v_custom1 != null && v_custom2 != null){
    		
    		String vNomeUm = v_custom1.getConteudoContainerItem(this.nameField);
    		String vNomeDois = v_custom2.getConteudoContainerItem(this.nameField);
    		
    		if(v_custom1 != null && v_custom2 != null)	{
	    		if(isAsc)
	            	return vNomeUm.compareTo(vNomeDois);
	            else
	            	return -(vNomeUm.compareTo(vNomeDois));
    		}
    	} 
    	
    	return 0;
    }

}
