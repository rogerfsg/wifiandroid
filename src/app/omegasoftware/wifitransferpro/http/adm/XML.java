package app.omegasoftware.wifitransferpro.http.adm;

import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class XML extends InterfaceHTML{

	public XML(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		
		if(__helper.GET("tipo") != null && __helper.GET("page") != null){
			
			vContainer.copyContainerResponse(__helper.getContainerResponsePagina(__helper.GET("tipo"), __helper.GET("page")));
		}
		
		return vContainer;
	}

}
