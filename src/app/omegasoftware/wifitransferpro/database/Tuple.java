package app.omegasoftware.wifitransferpro.database;

import java.util.Enumeration;
import java.util.Hashtable;



public class Tuple {
	private Hashtable<String, String> __hashStrAttributeByStrValue;
	public Tuple(){
		__hashStrAttributeByStrValue = new Hashtable<String, String>();
	}
	
	public boolean isVazia(){
		if(__hashStrAttributeByStrValue.size() == 0){
			return true;
		} else return false;
	}
	public boolean addAttrValue(String p_attrName, String p_strValue){
		if(__hashStrAttributeByStrValue.contains(p_attrName)){
			return false;
		}else{
			__hashStrAttributeByStrValue.put(p_attrName, p_strValue);
			return true;
		}
	}
	
	public String getStrValue(String p_attrName){
		Enumeration<String> v_listKey = __hashStrAttributeByStrValue.keys();
		while(v_listKey.hasMoreElements()){
			String v_strKey = v_listKey.nextElement();
			if(v_strKey.compareTo(p_attrName) == 0)
				return __hashStrAttributeByStrValue.get(p_attrName);
		}
				
		return null;
		
	}
	
	public Enumeration<String> getEnumarationStrKeyNameAttr(){
		return __hashStrAttributeByStrValue.keys();
	}
	
	public String[] getVectorStrKeyNameAttr(){
	
	Enumeration<String> __enumarationKeyStrNameAttr = __hashStrAttributeByStrValue.keys();
	
	int v_count = 0;
    while(__enumarationKeyStrNameAttr.hasMoreElements()){
  	  __enumarationKeyStrNameAttr.nextElement();
  	  v_count += 1;
    }
    __enumarationKeyStrNameAttr = __hashStrAttributeByStrValue.keys();
    String[] v_vetor = new String[v_count];
    int v_index = 0;
    while(__enumarationKeyStrNameAttr.hasMoreElements()){
    	  String v_strNameAttr = __enumarationKeyStrNameAttr.nextElement();
    	  v_vetor[v_index] = v_strNameAttr;
    	  v_index += 1;
      }
    return v_vetor;
}
	
}
