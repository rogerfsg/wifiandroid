package app.omegasoftware.wifitransferpro.database.EXTDAO;



import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Context;
import app.omegasoftware.wifitransferpro.common.container.AbstractContainerClassItem;
import app.omegasoftware.wifitransferpro.common.container.ContainerMenuOption;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;



public class EXTDAOTipoArquivo extends Table  {
	
	public static final String NAME = "tipo_arquivo";
	
	public static String ID = "id";
	public static String NOME = "nome";
	
	public EXTDAOTipoArquivo(Database p_database) {
		super(NAME, p_database);
		
		// TODO Auto-generated constructor stub
		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));
		super.addAttribute(new Attribute(NOME, "nome", true, Attribute.SQL_TYPE.TEXT, false, null, false, null, 50));
		setUniqueKey(new String[]{NOME});
	}
	
	public static LinkedHashMap<String, String> getAllTipoEntidade(Context pContext, Database db)
	{
		EXTDAOTipoArquivo vObj = new EXTDAOTipoArquivo(db);
		ArrayList<Table> vListTupla = vObj.getListTable();
		LinkedHashMap<String, String> vHash = new LinkedHashMap<String, String>();
		if(vListTupla != null && vListTupla.size() > 0 ){
			for (Table vTupla : vListTupla) {
				 String vNomeTipoEntidade = vTupla.getStrValueOfAttribute(EXTDAOTipoArquivo.NOME);
				 AbstractContainerClassItem vContainer = ContainerMenuOption.getContainerItem(vNomeTipoEntidade);
				 if(vContainer != null){
					 String vId = vTupla.getStrValueOfAttribute(EXTDAOTipoArquivo.ID);
					 vHash.put(vId, vContainer.getNome(pContext));
				 }
			}
		}
		return vObj.getHashMapIdByDefinition(true);
	}
	
	@Override
	public void populate() {
		// TODO Auto-generated method stub

	}

	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAOTipoArquivo(this.getDatabase());
		
	}
	
	public LinkedHashMap<String, String> getAllCidades()
	{
		
		this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		for(Table iEntry : this.getListTable(new String[]{EXTDAOTipoArquivo.NOME}))
		{
			EXTDAOTipoArquivo cidade = (EXTDAOTipoArquivo) iEntry;
			String v_strIdCidade = cidade.getAttribute(EXTDAOTipoArquivo.ID).getStrValue();
			String v_strNomeCidade = cidade.getAttribute(EXTDAOTipoArquivo.NOME).getStrValue();
			 
			if(v_strNomeCidade != null){
				String v_strNomeCidadeModificado = v_strNomeCidade.toUpperCase();
				hashTable.put(v_strIdCidade, v_strNomeCidadeModificado);	
			}
			
		}
		
		return hashTable;
		
	}

	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId, String pOldId) {
		// TODO Auto-generated method stub
		
	}
	    
}
