package app.omegasoftware.wifitransferpro.http.pages;

import java.io.File;
import java.util.ArrayList;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Download;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM;

public class MultipleDownloadArquivo extends InterfaceHTML{

	public MultipleDownloadArquivo(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		// TODO Auto-generated method stub
		String vNovoPath = HelperFile.getNomeSemBarra(__helper.GET("novo_path"));
		String vPath = HelperFile.getNomeSemBarra(__helper.GET("path"));
		vPath = HelperFile.getNomeSemBarra(vPath);
		String vNome = HelperFile.getNomeSemBarra(__helper.GET(EXTDAOArquivo.NOME));
		
		String vIsUnique = __helper.POST("is_unique");
		if(vIsUnique!= null && vIsUnique.compareTo("1") == 0){
			StringBuilder vBuilder = new StringBuilder();
			vBuilder.append("<script language=\"javascript\">");
			ContainerMultiple vContainerMultiple = new ContainerMultiple();
			vContainerMultiple.setByPost(__helper);
			
			String vVetorNomeArquivoToMove[] = vContainerMultiple.getVetorCheckBox();
			if(vVetorNomeArquivoToMove != null && vVetorNomeArquivoToMove.length > 0 )
				for(int i = 0; i < vVetorNomeArquivoToMove.length; i++){
					if(vVetorNomeArquivoToMove[i] != null && vVetorNomeArquivoToMove[i].length() > 0){
						vBuilder.append("window.open('download.php5?tipo=pages&page=DownloadArquivo&nome=" +vVetorNomeArquivoToMove[i] + "&path=" + vPath + "', '_blank');");
					}	
				}
			vBuilder.append("</script>");
			
			return new ContainerResponse(__helper, vBuilder);


			
		}else{
			ContainerMultiple vContainerMultiple = new ContainerMultiple();
			vContainerMultiple.setByPost(__helper);
			ArrayList<File> vListFile = new ArrayList<File>(); 
			String vVetorNomeArquivoToMove[] = vContainerMultiple.getVetorCheckBox();
			if(vVetorNomeArquivoToMove != null && vVetorNomeArquivoToMove.length > 0 )
				for(int i = 0; i < vVetorNomeArquivoToMove.length; i++){
					if(vVetorNomeArquivoToMove[i] != null && vVetorNomeArquivoToMove[i].length() > 0)
						vListFile.add(new File(vPath, vVetorNomeArquivoToMove[i]));
				}
			
			File[] vVetorFile = new File[vListFile.size()];
			vListFile.toArray(vVetorFile);
			
			Download vDownloadArquivo = new Download(
					__helper,
					vVetorFile,
					vNome,
					"application/octet-stream",
					"attachment");
			return vDownloadArquivo.df_download();
		}
	}
	

}
