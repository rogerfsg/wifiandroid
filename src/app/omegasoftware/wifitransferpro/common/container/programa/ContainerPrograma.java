package app.omegasoftware.wifitransferpro.common.container.programa;

import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.common.container.AbstractContainerClassItem;
import app.omegasoftware.wifitransferpro.database.Database;

public class ContainerPrograma {

	public static String PONTO_ELETRONICO = "ServicePontoEletronicoActivityMobile";
	public static String RASTREAMENTO = "ServiceRastrearActivityMobile";
	public static String CADASTROS = "ServiceCadastroActivityMobile";
	public static String CADASTRO_GERAL = "ServiceCadastroGeralActivityMobile";
	public static String CATALOGO_GERAL = "ServiceCatalogoGeralActivityMobile";
	public static String CATALOGOS = "ServiceCatalogoActivityMobile";
	public static String MONITORAMENTO_REMOTO = "ServiceMonitoramentoRemotoActivityMobile";
	public static String SINCRONIZADOR_DADOS = "ServiceSynchronizeActivityMobile";
	public static String PERMISSOES = "ServicePermissaoActivityMobile";
	public static String CONFIGURACAO = "ConfigurationActivityMobile";
	public static String MONITORAR = "ServiceMonitoramentoRemotoActivityMobile";
	public static String SERVICO_ONLINE = "ServiceOnlineActivityMobile";

	public enum TYPE_PROGRAM {
		OMEGA_COMPLETE_GUIDE, 
		OMEGA_BEER_GUIDE, //12 dolar
		}; //free

		public enum TYPE_SERVICE_ID { SERVICE_MY_POSITION, SERVICE_TIRA_FOTO_INTERNA, SERVICE_TIRA_FOTO_EXTERNA};
		
		private static AbstractContainerClassItem __vetorContainerClassItemCatalogoOnline[] = {
			
		};
		
		private static AbstractContainerClassItem __vetorContainerClassItemBeerGuide[] = {
			
		};

		
		public static boolean isEmptyServicoPermitido(){
			int vVetorIdServico[] = getVetorIdServicoPermissaoOnline();
			if(vVetorIdServico == null) return true;
			else if (vVetorIdServico.length == 0 ) return true;
			else return false;
		}

		public static boolean isServicoPermitido(int pId){
			int [] vVetorIdServicoInterno = getVetorIdServicoInterno();
			int [] vVetorIdServicoPermitido = getVetorIdServicoPermissaoOnline();
			boolean vValidade = false;
			if(vVetorIdServicoPermitido != null && vVetorIdServicoPermitido.length > 0) 
				vValidade = true;
			
			if ( vVetorIdServicoInterno != null && vVetorIdServicoInterno.length > 0 && !vValidade) 
				vValidade = true;
			if(vValidade){
				if(vVetorIdServicoPermitido != null)
					for (int id : vVetorIdServicoPermitido) {
						if(id == pId) return true;
					}
				if(vVetorIdServicoInterno != null)
					for (int id : vVetorIdServicoInterno) {
						if(id == pId) return true;
					}
				return false;
			} else return false;
		}
		public static boolean isCategoriaPermissaoPermitidaParaUsuarioLogado(Database db, String pTagClassPermissao){
			if(pTagClassPermissao == null || pTagClassPermissao.length() == 0 ) return false;
			return true;

		}
		public static AbstractContainerClassItem[] getVetorContainerClassItem(){
			switch (OmegaConfiguration.PROGRAM) {
			case OMEGA_COMPLETE_GUIDE:
				return __vetorContainerClassItemCatalogoOnline;
			case OMEGA_BEER_GUIDE:
				return __vetorContainerClassItemBeerGuide;
			default:
				return null;
			}
			
			
			
		}

		public static String getIdPrograma(){
			switch (OmegaConfiguration.PROGRAM) {
			case OMEGA_COMPLETE_GUIDE:
				return "1";
			case OMEGA_BEER_GUIDE:
				return "9";
			default:
				return null;
			}
		}

		public static int[] getVetorIdServicoPermissaoOnline(){
			switch (OmegaConfiguration.PROGRAM) {
			
			default:
				return null;
			}
		}
		
		public static int[] getVetorIdServicoInterno(){
			switch (OmegaConfiguration.PROGRAM) {
			
			default:
				return null;
			}
		}
		

}
