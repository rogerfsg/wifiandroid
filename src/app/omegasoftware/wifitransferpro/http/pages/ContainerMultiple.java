package app.omegasoftware.wifitransferpro.http.pages;

import java.util.ArrayList;

import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;

public class ContainerMultiple {

	public String vetorCheckBox[];
	public String vPrefixoNomeCheckbox = "checkbox_arquivo";
	public ContainerMultiple(String pPrefixoNomeCheckbox){
		vPrefixoNomeCheckbox = pPrefixoNomeCheckbox;
	}
	
	public ContainerMultiple(){

	}

	public String[] getVetorCheckBox(){
		return vetorCheckBox;
	}

	public void setByPost(HelperHttp pHelper){
		
		ArrayList<String> vListCheckBox = new ArrayList<String>();
		int i = 0 ;
		while(true){
			String vCheckBoxValue = pHelper.POST(vPrefixoNomeCheckbox + "_"+ String.valueOf(i));
			if(vCheckBoxValue == null) break;
			else vListCheckBox.add(vCheckBoxValue);
			i += 1;
		}
		vetorCheckBox = new String[vListCheckBox.size()];
		vListCheckBox.toArray(vetorCheckBox);
	}

	public static String getHTML(){
		//		Prefixo do nome do checkbox no POST 'checkbox_arquivo'
		String vPrefixoNomeCheckbox = "checkbox_arquivo";
		String vReturn ="";

		vReturn += "<script>";
		vReturn += "jQuery(document).ready(function(){";
		
		vReturn += "	 parent.parent.jQuery('input[id^=\"" + vPrefixoNomeCheckbox +"\"]:checked').each(function(index, value){";
		
		vReturn += "        var nomeArquivo = jQuery(this).val();";

		vReturn += "        jQuery('#form_list_arquivo').append('<input type=\"hidden\" name=\"" + vPrefixoNomeCheckbox + "_' + index + '\" value=\"' + nomeArquivo + '\" />');";

		vReturn += "    });";

		vReturn += "});";
		
		vReturn += "</script>";

		return vReturn;
	}
	
	public static String getHTML(String pPrefixoCheckBox){
		//		Prefixo do nome do checkbox no POST 'checkbox_arquivo'
		String vReturn ="";

		vReturn += "<script>";
		vReturn += "jQuery(document).ready(function(){";
		
		vReturn += "	 parent.parent.jQuery('input[id^=\"" + pPrefixoCheckBox +"\"]:checked').each(function(index, value){";
		
		vReturn += "        var nomeArquivo = jQuery(this).val();";

		vReturn += "        jQuery('#form_list_arquivo').append('<input type=\"hidden\" name=\"" + pPrefixoCheckBox + "_' + index + '\" value=\"' + nomeArquivo + '\" />');";

		vReturn += "    });";

		vReturn += "});";
		
		vReturn += "</script>";

		return vReturn;
	}
}
