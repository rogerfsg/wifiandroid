package app.omegasoftware.wifitransferpro.database.EXTDAO;

import java.io.File;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;

public class EXTDAODiretorioImagemArquivo extends Table{

	public static final String NAME = "diretorio_imagem_arquivo";
	public static String ID = "id";
	public static String PATH = "path";
	public static String LAST_MODIFIED_DATE_INT = "last_modified_date_INT";
	
	public EXTDAODiretorioImagemArquivo(Database p_database) {
		super(NAME, p_database);

		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));		
		super.addAttribute(new Attribute(PATH, "path", true, Attribute.SQL_TYPE.TEXT, false, null, false, null, 255));
		super.addAttribute(new Attribute(LAST_MODIFIED_DATE_INT, "last modified date", true, Attribute.SQL_TYPE.LONG, false, null, false, null, 11));
		
		setUniqueKey(new String[] {PATH});
	}
	
	public boolean checkIfIsNecessaryCheckDirectory(File pDirectory){
		try{
			SQLiteDatabase v_SQLiteDatabase= __database.getSQLiteDatabase();
			
			String vQuery = "SELECT pa." + EXTDAODiretorioImagemArquivo.ID + ", " + EXTDAODiretorioImagemArquivo.LAST_MODIFIED_DATE_INT +
					" FROM " + EXTDAODiretorioImagemArquivo.NAME + " AS pa " +
					" WHERE pa." + EXTDAODiretorioImagemArquivo.PATH + "= ?" +
					" LIMIT 0,1 ";
			String vPath = pDirectory.getAbsolutePath();
			Cursor vCursor = v_SQLiteDatabase.rawQuery(vQuery, 
					new String[]{
					vPath});
			try{

			

			String vStrLastModifiedStored = null;
			long vLastModifiedFile = pDirectory.lastModified();
			if (vCursor.moveToFirst()) {
				
					vStrLastModifiedStored = vCursor.getString(1);
					Long vLastModifiedStored = null;
					
					if(vStrLastModifiedStored != null){
						try{
							vLastModifiedStored = Long.parseLong(vStrLastModifiedStored);
						} catch(Exception ex){
							vLastModifiedStored = null;
						}
					} else {
//						Se a data ainda nao foi marcada
						String vId = vCursor.getString(0);
						this.clearData();
						this.setAttrStrValue(EXTDAODiretorioImagemArquivo.ID, vId);
						if(this.select()){
							this.setAttrStrValue(EXTDAODiretorioImagemArquivo.LAST_MODIFIED_DATE_INT, String.valueOf(vLastModifiedFile));
							this.update(false);
							return true;
						}
					}
					if(vLastModifiedStored == null) return false;
					else{
						
						if(vLastModifiedFile > vLastModifiedStored) return true;
						else return false;
					}
			} else{
//				Se o diretorio nunca foi verificado
				this.clearData();
				this.setAttrStrValue(EXTDAODiretorioImagemArquivo.LAST_MODIFIED_DATE_INT, String.valueOf(vLastModifiedFile));
				this.setAttrStrValue(EXTDAODiretorioImagemArquivo.PATH, vPath);
				long vLastId = this.insert(false);
				return true;
			}
			
			} catch(Exception ex){
				
			} finally{
				if (vCursor != null && !vCursor.isClosed()) {	    	  
					vCursor.close();
				}	
			}
			return false;
		}catch(Exception ex){

		}
		return false;
	}

	@Override
	public void populate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAODiretorioImagemArquivo(getDatabase());
	}

	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId,
			String pOldId) {
		// TODO Auto-generated method stub
		
	}

}
