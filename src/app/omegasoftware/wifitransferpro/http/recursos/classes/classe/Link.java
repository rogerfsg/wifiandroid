package app.omegasoftware.wifitransferpro.http.recursos.classes.classe;

import java.util.Enumeration;
import java.util.Properties;

public class Link {
	public static final int LARGURA_PADRAO_GREYBOX = 1000;
	public static final int ALTURA_PADRAO_GREYBOX= 600;

		public String url;
		public int larguraGreyBox;
		public int alturaGreyBox;
		public String tituloGreyBox;
		public String cssClass;
		public String label;
		public String onclick;
		public Properties demaisAtributos;
	    public boolean organograma = false;
	    public int numeroNiveisPai;
	    boolean isSetAltura;
		public Link(int pAlturaGreyBox, int pLarguraGreyBox, String pTituloGreyBox, String pUrl, Properties pDemaisAtributos, String pLabel, boolean pIsSetAltura){

			this.url = pUrl;
			this.larguraGreyBox = pLarguraGreyBox;
			this.alturaGreyBox = pAlturaGreyBox;
			this.tituloGreyBox = pTituloGreyBox;
			this.cssClass = "link_padrao";
			this.label = pLabel;
			this.onclick = "";
			this.demaisAtributos = pDemaisAtributos;
			this.numeroNiveisPai = 1;
			this.isSetAltura = pIsSetAltura;
		}
	    
		public Link(int pAlturaGreyBox, int pLarguraGreyBox, String pTituloGreyBox, String pUrl, Properties pDemaisAtributos, String pLabel){

			this.url = pUrl;
			this.larguraGreyBox = pLarguraGreyBox;
			this.alturaGreyBox = pAlturaGreyBox;
			this.tituloGreyBox = pTituloGreyBox;
			this.cssClass = "link_padrao";
			this.label = pLabel;
			this.onclick = "";
			this.demaisAtributos = pDemaisAtributos;
			this.numeroNiveisPai = 1;
		}
	    
		public Link(){

			this.url = "javascript:void(0);";
			this.larguraGreyBox = LARGURA_PADRAO_GREYBOX;
			this.alturaGreyBox = ALTURA_PADRAO_GREYBOX;
			this.tituloGreyBox = "";
			this.cssClass = "link_padrao";
			this.label = "";
			this.onclick = "";
			this.demaisAtributos = new Properties();
			this.numeroNiveisPai = 1;
		}

		public ContainerGreyBox montarStringGreyBox(){

			String strRetorno = "";
			ContainerGreyBox vContainer = null;
			if(this.larguraGreyBox > 0 && this.alturaGreyBox > 0 && this.url.length() > 0 ){

			    if(this.organograma){

    				strRetorno = "onclick=\"gerarLinkOrganograma(this);\"";
    				vContainer = new ContainerGreyBox(strRetorno, null);
    				demaisAtributos.put("rel", this.url);
    				demaisAtributos.put("title", this.tituloGreyBox);

			    }
			    else{

			    	vContainer =  getLinkParaGreyBox(this.tituloGreyBox, this.url, this.larguraGreyBox, this.alturaGreyBox, null, this.numeroNiveisPai);

			    }

			    this.url = "javascript:void(0);";

			}

			return vContainer;

		}
		static int contadorDeIdsGreyBox = 0;
		public class ContainerGreyBox{
			
			public String linkParaGreyBox;
			public StringBuilder comandoJavaScript;
			
			public ContainerGreyBox(String pLinkParaGreyBox, StringBuilder pComandoJavaScript){
				if(pLinkParaGreyBox != null)
					linkParaGreyBox = pLinkParaGreyBox;
				else  linkParaGreyBox = "";
				
				if(pComandoJavaScript != null)
					comandoJavaScript = pComandoJavaScript;
				else  comandoJavaScript = new StringBuilder();
				 
			}
			
		}
		
		private ContainerGreyBox getLinkParaGreyBox(String titulo, String url, Integer largura, Integer altura, Integer id , Integer parent) {
			
			if(largura == null )
				largura = LARGURA_PADRAO_GREYBOX;
			if(altura == null)
				altura = ALTURA_PADRAO_GREYBOX;
			
			StringBuilder vBuilderParent = null;
			
	        String retorno = "";
			int idGB = 0;

	        if (id == null) {

	            idGB = contadorDeIdsGreyBox++;

	            retorno = " id=\"link_greybox_" + String.valueOf(idGB)+ "\" ";
	        }

	        if (parent != null) {
	        	vBuilderParent = new StringBuilder();
	            for (int i = 0; i < parent; i++) {

	            	vBuilderParent.append( "parent.");
	            }
	        }
	        StringBuilder vBuilder = new StringBuilder();
	        vBuilder.append("<script type=\"text/javascript\">");
	        vBuilder.append( "jQuery(function(){");
	        vBuilder.append( "jQuery(\"#link_greybox_" + String.valueOf(idGB)+ "\").click(function(){");
	        if(vBuilderParent != null)
	        	vBuilder.append(vBuilderParent);
	        vBuilder.append("carregarConteudoDialog('" + url + "');");
	        if(vBuilderParent != null)
	        	vBuilder.append(vBuilderParent);
	        vBuilder.append("jQuery(\"#div_dialog\").dialog(\"option\", \"width\", " + largura+ ");");
//	        if(isSetAltura){
	        	if(vBuilderParent != null)
	        		vBuilder.append(vBuilderParent);
	        	vBuilder.append("jQuery(\"#div_dialog\").dialog(\"option\", \"height\", " + altura+ ");");
//	        }
	        if(vBuilderParent != null)
	        	vBuilder.append(vBuilderParent);
        	vBuilder.append("jQuery(\"#div_dialog\").dialog(\"option\", \"title\", \"" + titulo+ "\");");
        	if(vBuilderParent != null)
        		vBuilder.append(vBuilderParent);
        	vBuilder.append("jQuery(\"#div_dialog\").dialog(\"open\");");
        	vBuilder.append("});");
        	vBuilder.append("});");
        	vBuilder.append("</script>");

    		ContainerGreyBox vContainer = new ContainerGreyBox(retorno, vBuilder);
    		
	        return vContainer;
	    }

		public String montarStringClass(){

			String strRetorno = "class=\"" + this.cssClass+ "\"";

			return strRetorno;

		}

		public StringBuilder montarStringOnClick(){
			
			String string = "";
			String complemento = "";
			if(this.onclick.length() > 0){

			   string = "{this.onclick};";

			}
			else{
			    if(complemento.length() > 0){
			        string = "javascript: ";
			    }
			    else{
			        return null;
			    }
			}

			return new StringBuilder( "onclick=\"" + string + " " + complemento + "\"");
		}

		public StringBuilder montarStringDemaisAtributos(){
			StringBuilder vBuilder = null;
			if(demaisAtributos != null && demaisAtributos.size() > 0 )
				vBuilder = new StringBuilder();
			
			Enumeration<Object> vListKey = demaisAtributos.keys();
			while(vListKey.hasMoreElements()){
				Object vKey = vListKey.nextElement();
				String vStrKey = (String) vKey;
				String vToken = (String)demaisAtributos.get(vKey);
				vBuilder.append(vStrKey + "=\"" + vToken + "\" ");
			}
			return vBuilder;
		}

		public StringBuilder montarLink(){
			
			ContainerGreyBox vContainer = this.montarStringGreyBox();
			StringBuilder vBuilder = new StringBuilder(); 
			if(vContainer != null && vContainer.comandoJavaScript != null)
				vBuilder.append(vContainer.comandoJavaScript);
			
			vBuilder.append("<a ");
			if(vContainer != null && vContainer.linkParaGreyBox != null)
				vBuilder.append(vContainer.linkParaGreyBox);
			vBuilder.append(" href=\"");
			if(url != null)
				vBuilder.append(this.url );
			vBuilder.append("\" ");
			
			StringBuilder vBuilderMontarStringOnClick = this.montarStringOnClick();
			if(vBuilderMontarStringOnClick != null)
				vBuilder.append(vBuilderMontarStringOnClick);
			vBuilder.append(" ");
			
			String vStringClass = montarStringClass();
			if(vStringClass != null)
				vBuilder.append(vStringClass);
			
			vBuilder.append(" ");
			
			StringBuilder vBuilderMontarDemaisAtributos = this.montarStringDemaisAtributos();
			if(vBuilderMontarDemaisAtributos != null)
				vBuilder.append(vBuilderMontarDemaisAtributos);
			vBuilder.append(vBuilderMontarDemaisAtributos);
			vBuilder.append(">");
			vBuilder.append(label);
			vBuilder.append("</a>");
			
			return vBuilder;

		}
}
