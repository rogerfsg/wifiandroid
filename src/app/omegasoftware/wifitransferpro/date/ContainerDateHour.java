package app.omegasoftware.wifitransferpro.date;

public class ContainerDateHour {
	public Integer __tempoHora=null;
	public Integer __tempoMinuto=null;
	public Integer __tempoDia=null;

	public ContainerDateHour(Integer pTempoDia, Integer pTempoHora, Integer pTempoMinuto){
		__tempoHora = pTempoHora;
		__tempoMinuto = pTempoMinuto;
		__tempoDia = pTempoDia;
	}
}
