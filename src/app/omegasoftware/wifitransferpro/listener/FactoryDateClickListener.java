/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.omegasoftware.wifitransferpro.listener;



import java.util.ArrayList;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.date.ContainerClickListenerDate;
import android.widget.Button;
import android.widget.DatePicker;

/**
 * Basic example of using date and time widgets, including
 * {@link android.app.TimePickerDialog} and {@link android.widget.DatePicker}.
 *
 * Also provides a good example of using {@link Activity#onCreateDialog},
 * {@link Activity#onPrepareDialog} and {@link Activity#showDialog} to have the
 * activity automatically save and restore the state of the dialogs.
 */
public class FactoryDateClickListener {


    Activity __activity;
    ArrayList<ContainerClickListenerDate> __listContainerDate = new ArrayList<ContainerClickListenerDate>();
    ContainerClickListenerDate __lastContainerDate = null;
    static public final int DATE_DIALOG_ID = 109999;
    
    public static boolean isDateButtonSet(Context pContext, Button pButtonDate){
    	String vStrSelecionarData = pContext.getResources().getString(R.string.form_selecionar_data);
    	String vStrButtonDate = pButtonDate.getText().toString();
    	if(vStrButtonDate.equals("")) return false;
    	if(vStrButtonDate.equals(vStrSelecionarData)) return false;
    	else return true;
    }
    
    public FactoryDateClickListener(Activity pActivity){
    	__activity = pActivity;    
    
        
    }
    
    public ContainerClickListenerDate setDateClickListener(Button pButton){
    	
    	DateClickListener vListener = new DateClickListener(
    			__activity, 
    			__listContainerDate.size(), 
    			this);
    	ContainerClickListenerDate vContainer = new ContainerClickListenerDate(pButton, vListener);
    	__listContainerDate.add(vContainer);
    	pButton.setOnClickListener(vListener);
    	return vContainer;
    }
    
    
    protected void setLastContainerDate(int pIndexContainerDate){
    	__lastContainerDate = __listContainerDate.get(pIndexContainerDate);
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
    
            case DATE_DIALOG_ID:
            	 
                return new DatePickerDialog(__activity,
                            mDateSetListener,
                            __lastContainerDate.getAno(), 
                            __lastContainerDate.getMes(), 
                            __lastContainerDate.getDia());
        }
        return null;
    }

    
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
           
            case DATE_DIALOG_ID:
            	
                ((DatePickerDialog) dialog).updateDate(
                		__lastContainerDate.getAno(), 
                		__lastContainerDate.getMes(), 
                		__lastContainerDate.getDia());
                break;
        }
    }    

    public void setDateOfView(int year, int month, int day){
    	__lastContainerDate.setAno(year);
    	__lastContainerDate.setMes(month);
    	__lastContainerDate.setDia(day);
    	__lastContainerDate.setTextView();
    }
    
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear,
                        int dayOfMonth) {
                	setDateOfView(year, monthOfYear, dayOfMonth);
                    
                }
            };
}
