package app.omegasoftware.wifitransferpro.database;

import java.util.Properties;

import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public abstract class GenericDAO extends Table{
	
	
	public GenericDAO(String p_name, Database p_database) {
		super(p_name, p_database);
		// TODO Auto-generated constructor stub
	}
	public abstract ContainerResponse actionAdd(HelperHttp pHelper);
	
	public abstract ContainerResponse actionEdit(HelperHttp pHelper);
	
	public abstract ContainerResponse actionRemove(HelperHttp pHelper);
	
	public abstract void setByPost(HelperHttp pHelper, String pNumReg);
	
	public abstract void setByGet(HelperHttp pHelper, String pNumReg);
	
	public static String formatarDados(String pToken){
		if(pToken == null || pToken.length() == 0 )
			return null;
		else return pToken.replace("\'", "");
	}
	
	
	public abstract ContainerResponse callUserFunc(HelperHttp pHelper, String pNomeFuncao);
	
	
	public String campoArquivo( Properties objArgumento) {

        String nome = (String) objArgumento.get("nome") + (String) objArgumento.get("numeroDoRegistro");
        String label = (String) objArgumento.get("label");
        String largura = (String) objArgumento.get("largura");
        String obrigatorio = (String) objArgumento.get("obrigatorio");
        String id = (String) objArgumento.get("id") + (String) objArgumento.get("numeroDoRegistro");
        String classeCss = (String) objArgumento.get("classeCss");
        String classeCssFocus = (String) objArgumento.get("classeCssFocus");
        Boolean readOnly = (Boolean) objArgumento.get("readOnly");
        Boolean disabled = (Boolean) objArgumento.get("disabled");        

        String strFocus = "onfocus=\"this.className='" + classeCssFocus + "'\" onblur=\"this.className='" + classeCss + "'\"";
        
        String strReadOnly= "";
        if (readOnly != null && readOnly) {

            strReadOnly = "readOnly=\"readOnly\"";
        }
        String strDisabled = "";
        if (disabled != null && disabled) {

            strDisabled = "disabled=\"disabled\"";
        }

        String strRetorno = "<input type=\"file\" name=\"" + nome + "\" id=\"" + id + "\" class=\"" + classeCss + "\" style=\"width: " + largura + "px;\" " + strFocus + " " + strDisabled + " " + strReadOnly + "/>";

        return strRetorno;
    }

}
