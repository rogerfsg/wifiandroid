package app.omegasoftware.wifitransferpro.service;
import java.io.File;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPesquisa;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPesquisaArquivo;
import app.omegasoftware.wifitransferpro.file.FilenameFilterLikeAndExtension;
import app.omegasoftware.wifitransferpro.file.FilterFileIsDirectory;
import app.omegasoftware.wifitransferpro.http.IndexServer;


public class ServiceSearchFile extends Service  {

	//Constants
	public  final String TAG = "ServiceSearchFile";

	
	String __vetorNome[] = null;
	String __vetorExtensao[] = null;
	String __path = null;
	String __idPesquisa = null;
	boolean __isRunning = false;
	DatabaseWifiTransfer db = new DatabaseWifiTransfer(this);
	StopSearchBroadcastReceiver __serviceReceiver;
	EXTDAOPesquisaArquivo __objPesquisaArquivo;
	IndexServer __indexServer;
	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}

	
	public  boolean isRunning(){
		return __isRunning;
	}
	
	
	@Override
	public void onDestroy(){
		if(__serviceReceiver != null){
			unregisterReceiver(this.__serviceReceiver);
			__serviceReceiver = null;
		}
		db.close();
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		Bundle vParameter = intent.getExtras();
		
		if (this.__serviceReceiver == null){
			__serviceReceiver = new StopSearchBroadcastReceiver(this);
			IntentFilter serviceIntent = new IntentFilter(OmegaConfiguration.REFRESH_SERCH_FILE_END_SEARCH);
			registerReceiver(__serviceReceiver, serviceIntent);
		}
		if(vParameter != null){
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_VETOR_NOME))
				__vetorNome = vParameter.getStringArray(OmegaConfiguration.SEARCH_FIELD_VETOR_NOME);
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_VETOR_EXTENSAO))
				__vetorExtensao = vParameter.getStringArray(OmegaConfiguration.SEARCH_FIELD_VETOR_EXTENSAO);
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_PATH))
				__path = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_PATH);
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID))
				__idPesquisa = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
			
			db = new DatabaseWifiTransfer(this);
			db.close();
		}
		if(__path != null){
			File vDiretorio = new File(__path);
			if(vDiretorio.exists() && vDiretorio.isDirectory()){
				searchFile(vDiretorio, __vetorNome, __vetorExtensao);	
			}	
		}
		
		return(START_NOT_STICKY);
	}
	
	public void insertFileInDatabase(String pPath){
		db.openIfNecessary();
		__objPesquisaArquivo.clearData();
		__objPesquisaArquivo.setAttrStrValue(EXTDAOPesquisaArquivo.PESQUISA_ID_INT, __idPesquisa);
		__objPesquisaArquivo.setAttrStrValue(EXTDAOPesquisaArquivo.PESQUISA_ID_INT,pPath);
		__objPesquisaArquivo.insert(false);
		db.close();
	}
	
	
	public  void filter(File pDiretorioCorrente, FilenameFilterLikeAndExtension pFilterFile){

		if(pDiretorioCorrente.isDirectory()){
			File vVetorFileAccept[] = pDiretorioCorrente.listFiles(pFilterFile);
			if(vVetorFileAccept != null && vVetorFileAccept.length > 0 )
				for (File file : vVetorFileAccept) {
					insertFileInDatabase(file.getAbsolutePath());
				}
			
			for (File vDiretorio : pDiretorioCorrente.listFiles(new FilterFileIsDirectory(true))) {
				filter(vDiretorio, pFilterFile);
			}	
		}
		
	}
	private void sendBroadcastEndService()
	{
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERCH_FILE_END_SEARCH);
		
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, __idPesquisa);
		
		sendBroadcast(updateUIIntent);
	}
	
	public  void searchFile(File pDiretorioCorrente, String pVetorNome[], String pVetorExtension[]){
		__isRunning = true;
		FilenameFilterLikeAndExtension vFilterFile = null;
		if(pVetorExtension.length > 0 || pVetorNome.length > 0 ){
			if(pVetorExtension.length == 0 )
				vFilterFile = new FilenameFilterLikeAndExtension(pVetorNome, pVetorExtension, true);
			else if(pVetorExtension.length > 0 )
				vFilterFile = new FilenameFilterLikeAndExtension(pVetorNome, pVetorExtension, false);
		}
		
		filter(pDiretorioCorrente, vFilterFile);
		sendBroadcastEndService();
		__isRunning = false;
	}
	@Override
	public void onCreate() {
		super.onCreate();
		
	}
	
	public class StopSearchBroadcastReceiver extends BroadcastReceiver{
		

		public static final String TAG = "StopSearchBroadcastReceiver";
		ServiceSearchFile service;
		public StopSearchBroadcastReceiver(ServiceSearchFile pService){
			service = pService;
		}
		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(OmegaConfiguration.REFRESH_SERCH_FILE_END_SEARCH)) {
				Bundle vParameter = intent.getExtras();
				if(vParameter != null){
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID)){
						String vIdPesquisa = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
						if(vIdPesquisa != null && vIdPesquisa.compareTo(__idPesquisa) == 0 ){
							Database db = new DatabaseWifiTransfer(service);
							
							EXTDAOPesquisa vObjPesquisa = new EXTDAOPesquisa(db);
							vObjPesquisa.setAttrStrValue(EXTDAOPesquisa.ID, vIdPesquisa);
							vObjPesquisa.select();
							vObjPesquisa.setAttrStrValue(EXTDAOPesquisa.IS_RUNNING_BOOLEAN, "0");
							vObjPesquisa.update();
							service.stopSelf();
						}
					}
					
				}
					
			}
		}
		
	}

}

