package app.omegasoftware.wifitransferpro.http;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

public class HelperWifi {
	public static String TAG = "HelperWifi";
	public static String IP = "";

	
	
	//	public static String getIpAddr(Context pContext) {
	//		WifiManager wifiManager = (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);
	//		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
	//		int ip = wifiInfo.getIpAddress();
	//
	//		if(ip == 0)
	//			return null;
	//		else{
	//			String ipString = String.format(
	//					"%d.%d.%d.%d",
	//					(ip & 0xff),
	//					(ip >> 8 & 0xff),
	//					(ip >> 16 & 0xff),
	//					(ip >> 24 & 0xff));
	//			return ipString;	
	//		}
	//	}

	public static String getIpAddr(Context pContext) {

		WifiManager myWifiManager = (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);

		WifiInfo myWifiInfo = myWifiManager.getConnectionInfo();
		int myIp = myWifiInfo.getIpAddress();

		int intMyIp3 = myIp/0x1000000;
		int intMyIp3mod = myIp%0x1000000;

		int intMyIp2 = intMyIp3mod/0x10000;
		int intMyIp2mod = intMyIp3mod%0x10000;

		int intMyIp1 = intMyIp2mod/0x100;
		int intMyIp0 = intMyIp2mod%0x100;

		if(myIp == 0)
			return null;
		else{
			String ipConfig = String.valueOf(intMyIp0)
					+ "." + String.valueOf(intMyIp1)
					+ "." + String.valueOf(intMyIp2)
					+ "." + String.valueOf(intMyIp3);
			IP = ipConfig;
			Log.d(TAG, "Ip: " + ipConfig);
			return ipConfig;
		}
	}


}
