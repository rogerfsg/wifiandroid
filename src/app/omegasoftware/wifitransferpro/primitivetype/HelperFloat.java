package app.omegasoftware.wifitransferpro.primitivetype;

public class HelperFloat {
	
	
	public static Float parserFloat(String pIndex){
		Float index = null;
		
		try{
			if(pIndex == null) return null;
			else if (pIndex.length() == 0 ) return null;
			index = Float.parseFloat(pIndex);
			return index;
		} catch(Exception ex){
			return null;
		}

	}
}
