package app.omegasoftware.wifitransferpro.database;

import android.database.sqlite.SQLiteStatement;

public class Attribute{
	public enum SQL_TYPE { TEXT, LONG, DOUBLE};
	public enum TYPE_RELATION_FK { CASCADE, SET_NULL, NOT_ACTION }; 
	public TYPE_RELATION_FK __onUpdade = TYPE_RELATION_FK.SET_NULL;
	public TYPE_RELATION_FK __onDelete = TYPE_RELATION_FK.SET_NULL;
	public String __name;
	public SQL_TYPE __sqlType;
	public boolean __isNull;
	public String __strDefault;
	public boolean __isPrimaryKey;
	public String __strValue = null;
	public String __strLabel;
	public boolean __isMasculino ;
	public int __size;
	public String __tableFK;
	public String __attributeFK;
	public boolean __isAutoIncrement = false;
	public Attribute __atributoNormalizado = null; 
//	public boolean __isNormalizado = false;
	
	public Attribute( String p_name,
			String p_strLabel,
			boolean p_isMasculino,
			SQL_TYPE p_sqlType,
			boolean p_isNull,
			String p_strDefault,
			boolean p_isPrimaryKey,
			String p_strValue,
			int p_size, 
			boolean p_isAutoIncrement 
			){
		this.__isMasculino = p_isMasculino;
		this.__name = p_name;
		this.__sqlType = p_sqlType;
		this.__isNull =p_isNull;
		this.__strDefault = p_strDefault;
		this.__isPrimaryKey = p_isPrimaryKey;
		this.__strValue = p_strValue;  
		this.__strLabel = p_strLabel;
		if(p_isPrimaryKey)
			this.__isAutoIncrement = p_isAutoIncrement;
		__size = p_size;
	}
	
	private Attribute( String p_name,
			String p_strLabel,
			boolean p_isMasculino,
			SQL_TYPE p_sqlType,
			boolean p_isNull,
			String p_strDefault,
			boolean p_isPrimaryKey,
			String p_strValue,
			int p_size, 
			boolean p_isAutoIncrement,
			boolean p_isNormalizado,
			Attribute pAtributoNormalizado
			){
		this.__isMasculino = p_isMasculino;
		this.__name = p_name;
		this.__sqlType = p_sqlType;
		this.__isNull =p_isNull;
		this.__strDefault = p_strDefault;
		this.__isPrimaryKey = p_isPrimaryKey;
		this.__strValue = p_strValue;  
		this.__strLabel = p_strLabel;
		if(p_isPrimaryKey)
			this.__isAutoIncrement = p_isAutoIncrement;
		__size = p_size;
		__atributoNormalizado = pAtributoNormalizado;
//		__isNormalizado = p_isNormalizado;
	}
	
	
	
	public Attribute getNewAtributoNormalizado(Attribute pAtributoNormalizado){
		return new Attribute( 
				__name + "_normalizado", 
				__strLabel,
				__isMasculino,
				__sqlType,
				__isNull, 
				__strDefault, 
				__isPrimaryKey, 
				__strValue, 
				__size, 
				__isAutoIncrement,
				true,
				pAtributoNormalizado);
	}

	public Attribute( String p_name,
			String p_strLabel,
			boolean p_isMasculino,
			SQL_TYPE p_sqlType,
			boolean p_isNull,
			String p_strDefault,
			boolean p_isPrimaryKey,
			String p_strValue,
			int p_size 
			){
		this.__isMasculino = p_isMasculino;
		this.__name = p_name;
		this.__sqlType = p_sqlType;
		this.__isNull =p_isNull;
		this.__strDefault = p_strDefault;
		this.__isPrimaryKey = p_isPrimaryKey;
		this.__strValue = p_strValue;  
		this.__strLabel = p_strLabel;
		this.__isAutoIncrement = false;
		__size = p_size;
	}

	public Attribute( String p_name,
			String p_tableFK,
			String p_attributeFK,
			String p_strLabel,
			boolean p_isMasculino,
			SQL_TYPE p_sqlType,
			boolean p_isNull,
			String p_strDefault,
			boolean p_isPrimaryKey,
			String p_strValue, 
			int p_size,
			TYPE_RELATION_FK p_onUpdate ,
			TYPE_RELATION_FK p_onDelete ){
		this.__onUpdade = p_onUpdate;
		this.__onDelete = p_onDelete;
		this.__tableFK = p_tableFK;
		this.__attributeFK = p_attributeFK;
		this.__isMasculino = p_isMasculino;
		this.__name = p_name;
		this.__sqlType = p_sqlType;
		this.__isNull =p_isNull;
		this.__strDefault = p_strDefault;
		this.__isPrimaryKey = p_isPrimaryKey;

		if(p_strValue != null)
			if(p_strValue.length() > 0 )
				this.__strValue = p_strValue;

		this.__strLabel = p_strLabel;
		this.__isAutoIncrement = false;
		__size = p_size;
	}

	public Attribute( String p_name,
			String p_tableFK,
			String p_attributeFK,
			String p_strLabel,
			boolean p_isMasculino,
			SQL_TYPE p_sqlType,
			boolean p_isNull,
			String p_strDefault,
			boolean p_isPrimaryKey,
			String p_strValue, 
			int p_size){

		this.__tableFK = p_tableFK;
		this.__attributeFK = p_attributeFK;
		this.__isMasculino = p_isMasculino;
		this.__name = p_name;
		this.__sqlType = p_sqlType;
		this.__isNull =p_isNull;
		this.__strDefault = p_strDefault;
		this.__isPrimaryKey = p_isPrimaryKey;

		if(p_strValue != null)
			if(p_strValue.length() > 0 )
				this.__strValue = p_strValue;

		this.__strLabel = p_strLabel;
		this.__isAutoIncrement = false;
		__size = p_size;
	}
	
	public static String getNomeAtributoNormalizado(String pNome){
		return pNome + "_normalizado";
	}
	

	public boolean isNormalizado(){
		if(__atributoNormalizado == null) return false;
		else return true;
	}
	
	public Attribute getAttributeNormalizado(){
		return __atributoNormalizado;
	}
	
	public boolean isEmpty(){
		if(this.__strValue == null){
			return true;
		} else return false;
	}
	public boolean hasDefault(){
		if(this.__strDefault == null){
			return false;
		}else if(this.__strDefault.length() == 0 ){
			return false;
		} else return true;
	}
	public String getStrLabel(){
		return this.__strLabel;
	}

	public boolean isAutoIncrement(){
		return this.__isAutoIncrement;
	}

	public String getStrTableFK(){
		if(this.__tableFK == null){
			return null;
		}else if(this.__tableFK.length() == 0){
			return null;
		}else return this.__tableFK; 

	}

	public String getStrAttributeFK(){
		if(this.__attributeFK == null){
			return null;
		}else if(this.__attributeFK.length() == 0){
			return null;
		}else return this.__attributeFK; 

	}

	public boolean isFK(){
		if(this.__attributeFK == null){
			return false;
		}else if(this.__attributeFK.length() == 0){
			return false;
		}else return true;
	}


	public boolean bind(SQLiteStatement p_insertStmt, int p_index){
		//nao precisa de dar bind


		if(__strValue == null){
			p_insertStmt.bindNull(p_index);
			return false;
		}else{
			switch (__sqlType) {
			case TEXT:
				p_insertStmt.bindString(p_index, __strValue);				
				break;
			case LONG:

				try{
					Long value = Long.parseLong(__strValue);
					p_insertStmt.bindLong(p_index, value);	
				}catch (Exception e) {
					// TODO: handle exception

					p_insertStmt.bindNull(p_index);
				}


				break;
			case DOUBLE:
				try{
					Double value = Double.parseDouble(__strValue);
					p_insertStmt.bindDouble(p_index, value);	
				}catch (Exception e) {
					// TODO: handle exception
					p_insertStmt.bindNull(p_index);
				}

				break;
			default:

				break;
			}	
		}

		return true;
	}


	public String getName(){
		return __name;
	}

	public boolean isNull(){
		return __isNull;
	}

	public String getStrDefault(){
		return __strDefault;
	}

	public void clear(){
		__strValue = null;
	}

	public boolean isPrimaryKey(){
		return __isPrimaryKey;
	}

	public TYPE_RELATION_FK getOnUpdate(){
		return __onUpdade;
	}
	public String getStrOnDelete(){
		switch (__onDelete) {
		case CASCADE:
			return "CASCADE";
		case NOT_ACTION:
			return "NO ACTION";
		case SET_NULL:
			return "SET NULL";
		default:
			return null;
		}
	}
	public String getStrOnUpdate(){
		switch (__onUpdade) {
		case CASCADE:
			return "CASCADE";
		case NOT_ACTION:
			return "NO ACTION";
		case SET_NULL:
			return "SET NULL";
		default:
			return null;
		}
	}

	public TYPE_RELATION_FK getOnRemove(){
		return __onDelete;
	}

	public String getStrValue(){
		return __strValue;
	}
	public void setStrValue(String p_strValue){
		__strValue = p_strValue;
	}

	public SQL_TYPE getSQLType(){
		return __sqlType;
	}

	public String getStrSQLDeclaration(){
		String v_strType = this.getStrSQLType();
		if(this.__size > 0 ){
			return v_strType + "(" + String.valueOf(__size) +") ";
		} else return v_strType + " ";
	}

	public String getStrSQLType(){
		switch (__sqlType) {
		case LONG:
			return "INTEGER";
		case TEXT:
			return "TEXT";
		case DOUBLE:
			return "DOUBLE";
		default:
			return null;
		}
	}
}
