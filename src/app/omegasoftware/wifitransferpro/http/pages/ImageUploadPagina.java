package app.omegasoftware.wifitransferpro.http.pages;
//package app.omegasoftware.riodejaneirobeer.http.pages;
//
//import app.omegasoftware.riodejaneirobeer.file.OmegaFileConfiguration;
//import app.omegasoftware.riodejaneirobeer.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
//import app.omegasoftware.riodejaneirobeer.http.ContainerResponse;
//import app.omegasoftware.riodejaneirobeer.http.InterfaceHTML;
//import app.omegasoftware.riodejaneirobeer.http.recursos.classes.classe.HelperHttp;
//import app.omegasoftware.riodejaneirobeer.http.recursos.classes.classe.Javascript;
//
//public class ImageUploadPagina extends InterfaceHTML{
//
//	public ImageUploadPagina(HelperHttp pHelperHTTP) {
//		super(pHelperHTTP);
//		// TODO Auto-generated constructor stub
//	}
//
//	@Override
//	public ContainerResponse getContainerResponse() {
//		String vRelativePathUploader = OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.LIBS) + "multiple_uploader/";
//		
//		// TODO Auto-generated method stub
//		String vStrReturn = "";
//		vStrReturn += "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\"";
//		vStrReturn += "	\"http://www.w3.org/TR/html4/strict.dtd\">";
//		vStrReturn += "<html lang=\"en\" xml:lang=\"en\">";
//		vStrReturn += "<head>";
//		vStrReturn += "	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";
//		vStrReturn += "	<title>Queued Photo Uploader - Standalone Showcase from digitarald.de</title>";
//
//		vStrReturn += "	<meta name=\"author\" content=\"Harald Kirschner, digitarald.de\" />";
//		vStrReturn += "	<meta name=\"copyright\" content=\"Copyright 2009 Harald Kirschner\" />";
//
//		vStrReturn += "	<!-- Framework CSS -->";
//		
//		
//		vStrReturn += "<!--[if lte IE 7]>";
//		vStrReturn += "<![endif]-->";
//
//
//		vStrReturn += "	<script type=\"text/javascript\" src=\"" + OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.LIBS) + "mootools/mootools.js\"></script>";
//
//		vStrReturn += Javascript.importarBibliotecaJavascriptUpload();
//		
//		vStrReturn += "	<script type=\"text/javascript\" src=\"http://github.com/mootools/mootools-more/raw/master/Source/Core/Lang.js\"></script>";
//
//		vStrReturn += "	<!-- See script.js -->";
//		vStrReturn += "	<script type=\"text/javascript\">";
//		
//
//		vStrReturn += "		/**";
//		vStrReturn += " * FancyUpload Showcase";
//		vStrReturn += " *";
//		vStrReturn += " * @license		MIT License";
//		vStrReturn += " * @author		Harald Kirschner <mail [at] digitarald [dot] de>";
//		vStrReturn += " * @copyright	Authors";
//		vStrReturn += " */";
//
//		vStrReturn += "window.addEvent('domready', function() {";
//
//
//		vStrReturn += "	";
//		vStrReturn += "	var up = new FancyUpload2(jQuery('demo-status'), jQuery('demo-list'), {";
//
//		vStrReturn += "		verbose: true,";
//		vStrReturn += "		";
//
//		vStrReturn += "		url: jQuery('form-demo').action,";
//		vStrReturn += "		";
//		
//		vStrReturn += "		path: '" +vRelativePathUploader + "source/Swiff.Uploader.swf',";
//		vStrReturn += "		";
//
//		vStrReturn += "		typeFilter: {";
//		vStrReturn += "			'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'";
//		vStrReturn += "		},";
//		vStrReturn += "		";
//
//		vStrReturn += "		target: 'demo-browse',";
//		vStrReturn += "		";
//
//		vStrReturn += "		onLoad: function() {";
//		vStrReturn += "			jQuery('demo-status').removeClass('hide');";
//		vStrReturn += "			jQuery('demo-fallback').destroy();";
//		vStrReturn += "			";
//
//		vStrReturn += "			this.target.addEvents({";
//		vStrReturn += "				click: function() {";
//		vStrReturn += "					return false;";
//		vStrReturn += "				},";
//		vStrReturn += "				mouseenter: function() {";
//		vStrReturn += "					this.addClass('hover');";
//		vStrReturn += "				},";
//		vStrReturn += "				mouseleave: function() {";
//		vStrReturn += "					this.removeClass('hover');";
//		vStrReturn += "					this.blur();";
//		vStrReturn += "				},";
//		vStrReturn += "				mousedown: function() {";
//		vStrReturn += "					this.focus();";
//		vStrReturn += "				}";
//		vStrReturn += "			});";
//
//
//		vStrReturn += "			";
//		vStrReturn += "			jQuery('demo-clear').addEvent('click', function() {";
//		vStrReturn += "				up.remove();";
//		vStrReturn += "				return false;";
//		vStrReturn += "			});";
//
//		vStrReturn += "			jQuery('demo-upload').addEvent('click', function() {";
//		vStrReturn += "				up.start();";
//		vStrReturn += "				return false;";
//		vStrReturn += "			});";
//		vStrReturn += "		},";
//		vStrReturn += "		";
//
//		vStrReturn += "		";
//		vStrReturn += "		/**";
//		vStrReturn += "		 * Is called when files were not added, \"files\" is an array of invalid File classes.";
//		vStrReturn += "		 * ";
//		vStrReturn += "		 * This example creates a list of error elements directly in the file list, which";
//		vStrReturn += "		 * hide on click.";
//		vStrReturn += "		 */ ";
//		vStrReturn += "		onSelectFail: function(files) {";
//		vStrReturn += "			files.each(function(file) {";
//		vStrReturn += "				new Element('li', {";
//		vStrReturn += "					'class': 'validation-error',";
//		vStrReturn += "					html: file.validationErrorMessage || file.validationError,";
//		vStrReturn += "					title: MooTools.lang.get('FancyUpload', 'removeTitle'),";
//		vStrReturn += "					events: {";
//		vStrReturn += "						click: function() {";
//		vStrReturn += "							this.destroy();";
//		vStrReturn += "						}";
//		vStrReturn += "					}";
//		vStrReturn += "				}).inject(this.list, 'top');";
//		vStrReturn += "			}, this);";
//		vStrReturn += "		},";
//		vStrReturn += "		";
//		vStrReturn += "		/**";
//		vStrReturn += "		 * This one was directly in FancyUpload2 before, the event makes it";
//		vStrReturn += "		 * easier for you, to add your own response handling (you probably want";
//		vStrReturn += "		 * to send something else than JSON or different items).";
//		vStrReturn += "		 */";
//		vStrReturn += "		onFileSuccess: function(file, response) {";
//		vStrReturn += "			var json = new Hash(JSON.decode(response, true) || {});";
//		vStrReturn += "			";
//		vStrReturn += "			if (json.get('status') == '1') {";
//		vStrReturn += "				file.element.addClass('file-success');";
//		vStrReturn += "				file.info.set('html', '<strong>Image was uploaded:</strong> ' + json.get('width') + ' x ' + json.get('height') + 'px, <em>' + json.get('mime') + '</em>)');";
//		vStrReturn += "			} else {";
//		vStrReturn += "				file.element.addClass('file-failed');";
//		vStrReturn += "				file.info.set('html', '<strong>An error occured:</strong> ' + (json.get('error') ? (json.get('error') + ' #' + json.get('code')) : response));";
//		vStrReturn += "			}";
//		vStrReturn += "		},";
//		vStrReturn += "		";
//		vStrReturn += "		/**";
//		vStrReturn += "		 * onFail is called when the Flash movie got bashed by some browser plugin";
//		vStrReturn += "		 * like Adblock or Flashblock.";
//		vStrReturn += "		 */";
//		vStrReturn += "		onFail: function(error) {";
//		vStrReturn += "			switch (error) {";
//		vStrReturn += "				case 'hidden':";
//		vStrReturn += "					alert('To enable the embedded uploader, unblock it in your browser and refresh (see Adblock).');";
//		vStrReturn += "					break;";
//		vStrReturn += "				case 'blocked':";
//		vStrReturn += "					alert('To enable the embedded uploader, enable the blocked Flash movie (see Flashblock).');";
//		vStrReturn += "					break;";
//		vStrReturn += "				case 'empty':";
//		vStrReturn += "					alert('A required file was not found, please be patient and we fix this.');";
//		vStrReturn += "					break;";
//		vStrReturn += "				case 'flash':";
//		vStrReturn += "					alert('To enable the embedded uploader, install the latest Adobe Flash plugin.')";
//		vStrReturn += "			}";
//		vStrReturn += "		}";
//		vStrReturn += "		";
//		vStrReturn += "	});";
//		vStrReturn += "	";
//		vStrReturn += "});";
//		
//		vStrReturn += "	</script>";
//
//
//
//		vStrReturn += "	<!-- See style.css -->";
//		vStrReturn += "	<style type=\"text/css\">";
//		vStrReturn += "		/**";
//		vStrReturn += " * FancyUpload Showcase";
//		vStrReturn += " *";
//		vStrReturn += " * @license		MIT License";
//		vStrReturn += " * @author		Harald Kirschner <mail [at] digitarald [dot] de>";
//		vStrReturn += " * @copyright	Authors";
//		vStrReturn += " */";
//
//		vStrReturn += "/* CSS vs. Adblock tabs */";
//		vStrReturn += ".swiff-uploader-box a {";
//		vStrReturn += "	display: none !important;";
//		vStrReturn += "}";
//
//		vStrReturn += "/* .hover simulates the flash interactions */";
//		vStrReturn += "a:hover, a.hover {";
//		vStrReturn += "	color: red;";
//		vStrReturn += "}";
//
//		vStrReturn += "#demo-status {";
//		vStrReturn += "	padding: 10px 15px;";
//		vStrReturn += "	width: 420px;";
//		vStrReturn += "	border: 1px solid #eee;";
//		vStrReturn += "}";
//
//		vStrReturn += "#demo-status .progress {";
//		vStrReturn += "	background: url(" +vRelativePathUploader + "assets/progress-bar/progress.gif) no-repeat;";
//		vStrReturn += "	background-position: +50% 0;";
//		vStrReturn += "	margin-right: 0.5em;";
//		vStrReturn += "	vertical-align: middle;";
//		vStrReturn += "}";
//
//		vStrReturn += "#demo-status .progress-text {";
//		vStrReturn += "	font-size: 0.9em;";
//		vStrReturn += "	font-weight: bold;";
//		vStrReturn += "}";
//
//		vStrReturn += "#demo-list {";
//		vStrReturn += "	list-style: none;";
//		vStrReturn += "	width: 450px;";
//		vStrReturn += "	margin: 0;";
//		vStrReturn += "}";
//
//		vStrReturn += "#demo-list li.validation-error {";
//		vStrReturn += "	padding-left: 44px;";
//		vStrReturn += "	display: block;";
//		vStrReturn += "	clear: left;";
//		vStrReturn += "	line-height: 40px;";
//		vStrReturn += "	color: #8a1f11;";
//		vStrReturn += "	cursor: pointer;";
//		vStrReturn += "	border-bottom: 1px solid #fbc2c4;";
//		vStrReturn += "	background: #fbe3e4 url(assets/failed.png) no-repeat 4px 4px;";
//		vStrReturn += "}";
//
//		vStrReturn += "#demo-list li.file {";
//		vStrReturn += "	border-bottom: 1px solid #eee;";
//		vStrReturn += "	background: url(assets/file.png) no-repeat 4px 4px;";
//		vStrReturn += "	overflow: auto;";
//		vStrReturn += "}";
//		vStrReturn += "#demo-list li.file.file-uploading {";
//		vStrReturn += "	background-image: url(assets/uploading.png);";
//		vStrReturn += "	background-color: #D9DDE9;";
//		vStrReturn += "}";
//		vStrReturn += "#demo-list li.file.file-success {";
//		vStrReturn += "	background-image: url(assets/success.png);";
//		vStrReturn += "}";
//		vStrReturn += "#demo-list li.file.file-failed {";
//		vStrReturn += "	background-image: url(assets/failed.png);";
//		vStrReturn += "}";
//
//		vStrReturn += "#demo-list li.file .file-name {";
//		vStrReturn += "	font-size: 1.2em;";
//		vStrReturn += "	margin-left: 44px;";
//		vStrReturn += "	display: block;";
//		vStrReturn += "	clear: left;";
//		vStrReturn += "	line-height: 40px;";
//		vStrReturn += "	height: 40px;";
//		vStrReturn += "	font-weight: bold;";
//		vStrReturn += "}";
//		vStrReturn += "#demo-list li.file .file-size {";
//		vStrReturn += "	font-size: 0.9em;";
//		vStrReturn += "	line-height: 18px;";
//		vStrReturn += "	float: right;";
//		vStrReturn += "	margin-top: 2px;";
//		vStrReturn += "	margin-right: 6px;";
//		vStrReturn += "}";
//		vStrReturn += "#demo-list li.file .file-info {";
//		vStrReturn += "	display: block;";
//		vStrReturn += "	margin-left: 44px;";
//		vStrReturn += "	font-size: 0.9em;";
//		vStrReturn += "	line-height: 20px;";
//		vStrReturn += "	clear";
//		vStrReturn += "}";
//		vStrReturn += "#demo-list li.file .file-remove {";
//		vStrReturn += "	clear: right;";
//		vStrReturn += "	float: right;";
//		vStrReturn += "	line-height: 18px;";
//		vStrReturn += "	margin-right: 6px;";
//		vStrReturn += "}	</style>";
//
//
//		vStrReturn += "</head>";
//		vStrReturn += "<body>";
//
//		vStrReturn += "	<div class=\"container\">";
//
//		vStrReturn += "		<h1><a href=\"http://digitarald.de/project/fancyupload/\">FancyUpload</a> Standalone Showcase from <a href=\"http://digitarald.de/\">digitarald.de</a></h1>";
//
//		vStrReturn += "		<h2>Queued Photo Uploader</h2>";
//		vStrReturn += "		<!-- See index.html -->";
//		vStrReturn += "		<div>";
//		
//		vStrReturn += "			<form action=\"index.php5?pages=page&tipo=Upload\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-demo\">";
//
//		vStrReturn += "	<fieldset id=\"demo-fallback\">";
//		vStrReturn += "		<legend>File Upload</legend>";
//		vStrReturn += "		<p>";
//		vStrReturn += "			This form is just an example fallback for the unobtrusive behaviour of FancyUpload.";
//		vStrReturn += "			If this part is not changed, something must be wrong with your code.";
//		vStrReturn += "		</p>";
//		vStrReturn += "		<label for=\"demo-photoupload\">";
//		vStrReturn += "			Upload a Photo:";
//		vStrReturn += "			<input type=\"file\" name=\"Filedata\" />";
//		vStrReturn += "		</label>";
//		vStrReturn += "	</fieldset>";
//
//		vStrReturn += "	<div id=\"demo-status\" class=\"hide\">";
//		vStrReturn += "		<p>";
//		vStrReturn += "			<a href=\"#\" id=\"demo-browse\">Browse Files</a> |";
//		vStrReturn += "			<a href=\"#\" id=\"demo-clear\">Clear List</a> |";
//		vStrReturn += "			<a href=\"#\" id=\"demo-upload\">Start Upload</a>";
//		vStrReturn += "		</p>";
//		vStrReturn += "		<div>";
//		vStrReturn += "			<strong class=\"overall-title\"></strong><br />";
//		vStrReturn += "			<img src=\"" + vRelativePathUploader+ "assets/progress-bar/bar.gif\" class=\"progress overall-progress\" />";
//		vStrReturn += "		</div>";
//		vStrReturn += "		<div>";
//		vStrReturn += "			<strong class=\"current-title\"></strong><br />";
//		vStrReturn += "			<img src=\"" + vRelativePathUploader+ "assets/progress-bar/bar.gif\" class=\"progress current-progress\" />";
//		vStrReturn += "		</div>";
//		vStrReturn += "		<div class=\"current-text\"></div>";
//		vStrReturn += "	</div>";
//
//		vStrReturn += "	<ul id=\"demo-list\"></ul>";
//
//		vStrReturn += "</form>		</div>";
//
//
//		vStrReturn += "	</div>";
//
//		vStrReturn += "	<div class=\"container quiet\" style=\"line-height: 5em;\">";
//		vStrReturn += "		� 2008-2009 by <a href=\"http://digitarald.de/\">Harald Kirschner</a> and available under <a href=\"http://www.opensource.org/licenses/mit-license.php\">The MIT License</a>";
//		vStrReturn += "	</div>";
//
//		vStrReturn += "</body>";
//		vStrReturn += "</html>";
//
//		return new ContainerResponse(__helper, vStrReturn); 
//	}
//
//}
