package app.omegasoftware.wifitransferpro.primitivetype;

public class HelperInteger {
	public static int parserInt(String pIndex){
		int index = -1;
		try{
			if(pIndex == null) return -1;
			else if (pIndex.length() == 0 ) return -1;
			index = Integer.parseInt(pIndex);
			return index;
		} catch(Exception ex){
			return -1;
		}

	}
	public static Integer getIndexOfContent(Integer vVetor[] , Integer pValue){
		if(vVetor == null) return null;
		int i = 0 ;
		for (Integer vValueOfVetor : vVetor) {
			
			if(vValueOfVetor != null) {
				if(vValueOfVetor == pValue) return i;
			}
			i += 1;
		}
		return null;
	}
	
	
	public static Integer parserInteger(String pIndex){
		
		Integer index = -1;
		try{
			if(pIndex == null) return null;
			else if (pIndex.length() == 0 ) return null;
			index = Integer.parseInt(pIndex);
			return index;
		} catch(Exception ex){
			return null;
		}

	}
}
