package app.omegasoftware.wifitransferpro.http.lists;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivoPlaylist;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.pages.AnexoAdicionarPlaylistPagina;
import app.omegasoftware.wifitransferpro.http.pages.InformacaoTelefonePagina;
import app.omegasoftware.wifitransferpro.http.pages.PlayerPlaylist;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Javascript;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM_TOOLTIP;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class ListsPlaylistMusica extends InterfaceHTML {

	public ListsPlaylistMusica(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		Database db = new DatabaseWifiTransfer(__helper.context);
		EXTDAOArquivoPlaylist vObjPlaylistMusica = new EXTDAOArquivoPlaylist(db);
		
		String vPlayList = __helper.GET("playlist");
		
		if(vPlayList == null || vPlayList.length() == 0 ){
			Table vTupla = vObjPlaylistMusica.getFirstTupla();
			if(vTupla != null)
				vPlayList = vTupla.getStrValueOfAttribute(EXTDAOArquivoPlaylist.ID);
		}	
		
		String vStrPath = __helper.GET("path");
		
		if (vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		vStrPath = HelperFile.getNomeSemBarra(vStrPath);
		vStrPath += "/";
		StringBuilder vBuilder = new StringBuilder();
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		if (vStrPath != null && vStrPath.length() > 0) {
			File vDirectory = new File(vStrPath);
			if (vDirectory.isDirectory()) {

				
				
				vBuilder.append("<td id=\"td_left\" >");
				
//				Anexo Playlist Telefone  ==========================================================
						vContainer.appendData(vBuilder);
						ContainerResponse vContainerAnexoAdicionarPlaylistPagina = new AnexoAdicionarPlaylistPagina(__helper).getContainerResponse();
						vContainer.copyContainerResponse(vContainerAnexoAdicionarPlaylistPagina);
						vBuilder = new StringBuilder();
				
				
//				Direita Arquivo ============================================================
				vBuilder.append("<td id=\"td_right\">");
//				vBuilder.append(Javascript.importarBibliotecaJavascriptPlaylistPlayer(vPlayList));
				vContainer.appendData(vBuilder);
				ContainerResponse vContainerInformacaoTelefonePagina = new InformacaoTelefonePagina(__helper).getContainerResponse();
				vContainer.copyContainerResponse(vContainerInformacaoTelefonePagina);
				vBuilder = new StringBuilder();
				
				vBuilder.append("</td>");
//				
				
				vBuilder.append("</td>");
				
				vContainer.appendData(vBuilder);

			}
		}

		return vContainer;
	}

}
