package app.omegasoftware.wifitransferpro.http.lists;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivoPlaylist;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOPlaylist;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.pages.AnexoAdicionarPlaylistPagina;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Link;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM_TOOLTIP;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class ListsPlaylist extends InterfaceHTML {

	//	PLAYLIST contem toda a lista de musicas para a montagem
	//	1 . Contem um filtro no cabecalho para filtragem das musicas, 
	//		1.1	Explorer que so exibe as musicas e diretorios

	StringBuilder vBuilder = new StringBuilder();
	String vStrPath ;
	String vPlaylist ;
	File vDirectory;
	ContainerResponse vContainer;
	public ListsPlaylist(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		vStrPath = __helper.GET("path");
		vPlaylist = __helper.GET("playlist");

		if (vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		vStrPath = HelperFile.getNomeSemBarra(vStrPath);
		vStrPath += "/";

		vContainer = new ContainerResponse(__helper, new StringBuilder());
		if (vStrPath != null && vStrPath.length() > 0) {
			vDirectory = new File(vStrPath);
			if (vDirectory.isDirectory()) {

				vBuilder.append("<script>");
				vBuilder.append("function marcarTodosOsCheckBoxesCujoIdContenha(isChecked, conteudoId){");
				vBuilder.append("    var inputs = document.getElementsByTagName(\"input\");");
				vBuilder.append("   for(var i=0; i < inputs.length; i++){");
				vBuilder.append("       if(inputs[i].type == \"checkbox\" && inputs[i].id.indexOf(conteudoId) != -1){");
				vBuilder.append("           inputs[i].checked = isChecked;");
				vBuilder.append("       }");
				vBuilder.append("   }");
				vBuilder.append("}");
				vBuilder.append("</script>");
				
				

				vBuilder.append("<td id=\"td_left_playlist_explorer\" >");

				procedurePrimeiraTabela();

				vBuilder.append("</td>");

				vBuilder.append("<td id=\"td_left_playlist_music\">");

				procedureSegundaTabela();

				vBuilder.append("</td>");

				//				Direita Arquivo ============================================================
				vBuilder.append("<td id=\"td_right\">");

				procedureTerceiraTabela();


				vBuilder.append("</td>");

				vContainer.appendData(vBuilder);

			}
		}

		return vContainer;
	}

	private void procedurePrimeiraTabela(){
		vBuilder.append("      <form name=\"form_arquivo\" id=\"form_arquivo\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
		vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");
		vBuilder.append("			<input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
		vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
		vBuilder.append("           	<input type=\"hidden\" name=\"class\" id=\"class\" value=\""
				+ EXTDAOArquivo.NAME + "\">");
		vBuilder.append("            <input type=\"hidden\" name=\"action\" id=\"action\" value=\"pesquisaArquivo\">");
		vBuilder.append("            <input type=\"hidden\" name=\"funcao\" id=\"funcao\" value=\"\">");
		vBuilder.append("            <input type=\"hidden\" name=\"novo_nome\" id=\"novo_nome\" value=\"\">");
		vBuilder.append("            <input type=\"hidden\" name=\"novo_path\" id=\"novo_path\" value=\"\">");				
		vBuilder.append("            <input type=\"hidden\" name=\""
				+ EXTDAOArquivo.NOME + "1\" id=\"" + EXTDAOArquivo.NOME
				+ "1\" value=\"" + __helper.GET(EXTDAOArquivo.NOME)
				+ "\">");
		vBuilder.append("            <input type=\"hidden\" name=\""
				+ EXTDAOArquivo.PATH + "1\" id=\"" + EXTDAOArquivo.PATH
				+ "1\" value=\"" + vStrPath + "\">");

		//		TABELA CABECALHO =================================================================================================================
		vBuilder.append("	<table width=\"350\" align=\"top\" class=\"tabela_list\">");

		vBuilder.append("		<colgroup>");
		vBuilder.append("			<col width=\"10%\" />");
		vBuilder.append("			<col width=\"60%\" />");
		vBuilder.append("			<col width=\"30%\" />");
		vBuilder.append("		</colgroup>");
		vBuilder.append("		<thead>");

		//		DIRETORIO DE BUSCA DO CABECALHO ==============================================================================================================
		vBuilder.append("		<tr class=\"tr_list_conteudo\">");

		vBuilder.append("    		<td height=\"22\" align=\"right\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
		vBuilder.append(HelperString.ucFirst(context.getString(R.string.path)));
		vBuilder.append("    		</td>");
		vBuilder.append("    		<td  height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

		vBuilder.append("			<input type=\"text\" maxlength=\"200\" id=\""
				+ EXTDAOArquivo.PATH+ "1\" "
				+ "name=\""+ EXTDAOArquivo.PATH+ "1\" "
				+ "style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\""
				+ vStrPath + "\"/>");

		vBuilder.append("    		</td>");
		
//		BOTAO DE BUSCA DO ARQUIVO ==============================================================================================================
		
		vBuilder.append("    		<td  colspan =\"2\" height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
		vBuilder.append("<input name=\"txtBotao\" type=\"button\" value=\""
				+ context.getString(R.string.ok).toUpperCase()
				+ "\" " +
				"onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=playlist&"
				+ EXTDAOArquivo.PATH + "="
				+ "' + document.getElementById('" + EXTDAOArquivo.PATH + "1').value + '"
				+  "\"" +
				"class=\"botoes_form\" style=\"cursor: pointer;\" />");
		vBuilder.append("    		</td>");
		
		vBuilder.append("		</tr>");

//		LISTA DE BOTOS DO PATH CORRENTE =================================================================================================		

		vBuilder.append("    	<tr>");
		vBuilder.append("    		<td height=\"11\" colspan=\"5\" align=\"left\" valign=\"middle\" class=\"textos5\" style=\"padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;\" >");

		String vVetorPath[] = vStrPath.split("[/]");
		String vPathDiretorio = "";
		if(vVetorPath == null || vVetorPath.length == 0 )
			vBuilder.append("<input class=\"botoes_form\" value=\"/"
					+ "\" type=\"button\" onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&playlist=" + vPlaylist
					+ EXTDAOArquivo.PATH + "=" + vPathDiretorio
					+ "'\">");
		else{

			for(int i = 0 ;  i < vVetorPath.length; i++){
				if(i == 0 ){
					vBuilder.append("<input class=\"botoes_form\" value=\"/"
							+ "\" type=\"button\" onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&playlist=" + vPlaylist
							+ EXTDAOArquivo.PATH + "=" + vPathDiretorio
							+ "'\">");

				}
				String vDiretorio = vVetorPath[i];

				if (vDiretorio == null || vDiretorio.length() == 0)
					continue;
				vPathDiretorio += "/" + vDiretorio;
				vBuilder.append("<input class=\"botoes_form\" value=\""
						+ vDiretorio
						+ "\" type=\"button\" onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&playlist=" + vPlaylist
						+ EXTDAOArquivo.PATH + "=" + vPathDiretorio
						+ "'\">");
			}
		}
		vBuilder.append("    			</td>");
//		ADICIONAR MUSICA PLAYLIST ICON ================================================================================================================================
		vBuilder.append("    	<tr>");
		vBuilder.append("		</thead>");
		vBuilder.append("  	</table>");
		
		vBuilder.append("	<table width=\"350\" align=\"top\" class=\"tabela_list\">");

		vBuilder.append("		<colgroup>");
		vBuilder.append("			<col width=\"5%\" />");
		vBuilder.append("			<col width=\"10%\" />");
		vBuilder.append("			<col width=\"35%\" />");
		vBuilder.append("			<col width=\"25%\" />");
		vBuilder.append("			<col width=\"15%\" />");
		vBuilder.append("			<col width=\"10%\" />");
		vBuilder.append("		</colgroup>");
		vBuilder.append("		<thead>");

		//		MULTIPLE ADICIONA MUSICA A PLAYLIST CABECALHO ==============================================================================================================
		vBuilder.append("		<tr class=\"tr_list_titulos\">");
		vBuilder.append("			<td class=\"td_list_titulos_arredondados\" colspan=\"3\" >");
		
		
		Properties vLinkMultiploAdicionaMusicaPlaylist = new Properties();
		vLinkMultiploAdicionaMusicaPlaylist
		.put("onmouseover",
				"tip(\'"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ADICIONAR_MUSICA_PLAYLIST)
						+ "\', this);");
		vLinkMultiploAdicionaMusicaPlaylist.put("onmouseout", "notip();");
		Link vObjLinkMultiploAdicionaMusicaPlaylist = new Link(
				100,
				500,
				context.getResources().getString(
						R.string.playlist_play),
						"popup.php5?tipo=pages&page=AdicionaArquivoPlaylist"
								+ "&" + EXTDAOArquivo.PATH + "=" + vStrPath
								+ "&playlist=" + vPlaylist,
								vLinkMultiploAdicionaMusicaPlaylist,
								"<img class=\"icones_list\" src=\""
										+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
										+ "icone_add.png\" onmouseover=\"javascript:tip('"
										+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ADICIONAR_MUSICA_PLAYLIST)
										+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
										true);
		vBuilder.append(vObjLinkMultiploAdicionaMusicaPlaylist.montarLink());
//		
//		vBuilder.append("			<img class=\"icones_list\" " +
//				"src=\"" +  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
//				+ "icone_add.png\""
//				+ " onclick=\""
//				+ "javascript:document.location.href='"
//				+"popup.php5?tipo=pages&page=AdicionaArquivoPlaylist"
//				+ "&playlist=" + vPlaylist
//				+ "&" + EXTDAOArquivo.PATH + "="
//				+ vStrPath+ "'\">&nbsp;");
		
		vBuilder.append(HelperString.ucFirst(context
				.getString(R.string.adicionar_musica_da_playlist)));
		
		
		vBuilder.append("    			</td>");
		
		vBuilder.append("    		</tr>");
		vBuilder.append("		</thead>");
		vBuilder.append("  	</table>");

		//		TABELA ==================================================================================================================================
		vBuilder.append("			<table class=\"tabela_list\">");
		vBuilder.append("				<colgroup>");
		vBuilder.append("					<col width=\"5%\" />");
		vBuilder.append("					<col width=\"13%\" />");
		vBuilder.append("					<col width=\"32%\" />");
		vBuilder.append("					<col width=\"25%\" />");
		vBuilder.append("					<col width=\"12%\" />");
		vBuilder.append("					<col width=\"13%\" />");

		vBuilder.append("				</colgroup>");

		vBuilder.append("				<thead>");
		vBuilder.append("					<tr class=\"tr_list_titulos\">");

		vBuilder.append("<td class=\"td_list_titulos\">"
				+ "<input id=\"vetor_checkbox_arquivo"
				+ "\" type=\"checkbox\" name=\"vetor_checkbox_playlist\" onclick=\"marcarTodosOsCheckBoxesCujoIdContenha(this.checked, 'checkbox_arquivo')\">"
				+ "</td>");

		//				NOME ======================================================================================
		vBuilder.append("					<td class=\"td_list_titulos\">");
		//				DIRETORIO ACIMA ICON ================================================================================================================================

		vBuilder.append(" 						<img class=\"icones_list\" src=\""
				+ OmegaFileConfiguration
				.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
				+ "up_folder.png\" "
				+ "onmouseover=\"javascript:tip('"
				+ __helper
				.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_DIRETORIO_ACIMA)
				+ "')\" "
				+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&playlist=" + vPlaylist
				+ EXTDAOArquivo.PATH + "="
				+ HelperHttp.getStrPathUp(vStrPath) 
				+  "'\""
				+ "onmouseout=\"javascript:notip()\">&nbsp;");
		//				RAIZ ICON ================================================================================================================================

		vBuilder.append("						<img class=\"icones_list\" src=\""
				+ OmegaFileConfiguration
				.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
				+ "home_folder.png\" "
				+ "onmouseover=\"javascript:tip('"
				+ __helper
				.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_RAIZ)
				+ "')\" "
				+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=pesquisa&playlist=" + vPlaylist
				+ EXTDAOArquivo.PATH + "="
				+ HelperHttp.getPathRoot()
				+ "\""
				+ "onmouseout=\"javascript:notip()\">&nbsp;");

		vBuilder.append("						</td>");
		vBuilder.append("						<td class=\"td_list_titulos\">");

		vBuilder.append(HelperString.ucFirst(__helper.context.getResources().getString(
				R.string.arquivo_nome)));

		vBuilder.append("						</td>");

		//				=========================================================================================
		vBuilder.append("						<td class=\"td_list_titulos\">");
		vBuilder.append(HelperString.ucFirst( __helper.context.getResources().getString(
				R.string.arquivo_data)));

		vBuilder.append("						</td>");
		vBuilder.append("						<td class=\"td_list_titulos\">");
		vBuilder.append(HelperString.ucFirst(__helper.context.getResources().getString(
				R.string.arquivo_tamanho)));
		vBuilder.append("						</td>");
		vBuilder.append("						<td class=\"td_list_titulos\">");
		vBuilder.append(HelperString.ucFirst(__helper.context.getResources().getString(
				R.string.arquivo_acoes)));
		vBuilder.append("						</td>");
		vBuilder.append("					</tr>");
		vBuilder.append("				</thead>");
		vBuilder.append("				<tbody>");


		File vVetorFile[] = vDirectory.listFiles();
		if (vVetorFile == null || vVetorFile.length == 0) {
			vBuilder.append("		<tr class=\"tr_list_conteudo_impar\">");
			vBuilder.append("			<td  colspan=\"6\">");
			vBuilder.append(HelperHttp.imprimirMensagem(
					HelperString.ucFirst(context.getResources().getString(
							R.string.diretorio_sem_arquivo)),
							HelperHttp.TYPE_MENSAGEM.MENSAGEM_OK, null));
			vBuilder.append("			</td>");
			vBuilder.append("		</tr>");
		} else {
			boolean vHasFileOrDirectoryInFolder = false;
			Properties vLinkAdicionaMusicaPlaylist = new Properties();
			vLinkMultiploAdicionaMusicaPlaylist
			.put("onmouseover",
					"tip(\'"
							+ __helper
							.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ADICIONAR_MUSICA_PLAYLIST)
							+ "\', this);");
			
			for (int i = 0; i < vVetorFile.length; i++) {
				File vFile = vVetorFile[i];
				ContainerTypeFile vContainerType = ContainerTypeFile
						.getContainerTypeFile(vFile);
				if(vContainerType.typeFile != TYPE_FILE.MUSIC && vContainerType.typeFile != TYPE_FILE.FOLDER) 
					continue;
				else if (!vHasFileOrDirectoryInFolder)
					vHasFileOrDirectoryInFolder = true;
				String classTr = (i % 2 == 0) ? "tr_list_conteudo_impar"
						: "tr_list_conteudo_par";
				String vStri = String.valueOf(i);
				String vFileName = vFile.getName();
				boolean isDirectory = (vContainerType.typeFile == TYPE_FILE.FOLDER) ? true : false;

				vBuilder.append("		<tr class=\"" + classTr + "\">");
				vBuilder.append("			<td  class=\"td_list_conteudo\">");
				if(vContainerType.typeFile == TYPE_FILE.MUSIC ){
					vBuilder.append("<input id=\"checkbox_arquivo_"
							+ vFileName
							+ "\" type=\"checkbox\" name=\"checkbox_arquivo_" 
							+ vFileName 
							+ "\" value=\"" + vFileName + "\" >");
				}
				vBuilder.append("			</td>");

				vBuilder.append("			<td class=\"td_list_conteudo\">");
				vBuilder.append("				<img class=\"icones_list\" src=\""
						+ vContainerType.getImage()
						+ "\">&nbsp;");
				vBuilder.append("			</td>");
				vBuilder.append("			<td   class=\"td_list_conteudo\">");

				if (!isDirectory)
					vBuilder.append(vFileName);
				else {
					vBuilder.append("				<a href=\"javascript:document.location.href='index.php5?tipo=lists&page=playlist&playlist=" + vPlaylist +
							"&path="+ vStrPath
							+ vFileName
							+ "/"
							+ "'\" class=\"link_padrao\" onmouseout=\"notip();\" onmouseover=\"tip('"
							+ __helper
							.getMensagemTooltip(TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_DIRETORIO)
							+ "');\">" + vFileName + "</a>");
				}
				vBuilder.append("			</td>");

				vBuilder.append("			<td   class=\"td_list_conteudo\">");
				Date vDate = new Date(vFile.lastModified());
				vBuilder.append(vDate.toLocaleString());
				vBuilder.append("			</td>");

				vBuilder.append("			<td   class=\"td_list_conteudo\">");
				if (!isDirectory)
					vBuilder.append(HelperFile.getStrLength(vFile));
				else
					vBuilder.append("-");
				vBuilder.append("			</td>");

				vBuilder.append("			<td  class=\"td_list_conteudo\">");
				if(vContainerType.typeFile == TYPE_FILE.MUSIC ){
					Properties vLinkMusic = new Properties();
					vLinkMusic
					.put("onmouseover",
							"tip(\'"
									+ __helper
									.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_TOCAR_MUSICA)
									+ "\', this);");
					vLinkMusic.put("onmouseout", "notip();");
					Link vObjLinkMusica = new Link(
							100,
							300,
							context.getResources().getString(
									R.string.music),
									"image.php5?tipo=pages&page=MusicPagina&"
											+ EXTDAOArquivo.NOME + "=" + vFileName
											+ "&" + EXTDAOArquivo.PATH + "="
											+ vStrPath,
											vLinkMusic,
											"<img class=\"icones_list\" src=\""
													+ OmegaFileConfiguration
													.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
													+ "music.png\" onmouseover=\"javascript:tip('"
													+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_TOCAR_MUSICA)
													+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
													true);
					vBuilder.append(vObjLinkMusica.montarLink());
					
//					Link vObjLinkAdicionaMusicaPlaylist = new Link(
//							100,
//							500,
//							context.getResources().getString(
//									R.string.playlist_play),
//									"popup.php5?class=arquivo&action=adicionarMusicaNaPlaylist"
//											+ "&playlist=" + vPlaylist
//											+"&" + EXTDAOArquivo.NOME + "=" + vFileName
//											+ "&" + EXTDAOArquivo.PATH + "=" + vStrPath,
//											vLinkAdicionaMusicaPlaylist,
//											"<img class=\"icones_list\" src=\""
//													+ OmegaFileConfiguration
//													.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
//													+ "icone_add.png\" onmouseover=\"javascript:tip('"
//													+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ADICIONAR_MUSICA_PLAYLIST)
//													+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
//													true);
//					vBuilder.append(vObjLinkAdicionaMusicaPlaylist.montarLink());
					
//					vBuilder.append("			<img class=\"icones_list\" " +
//							"src=\"" +  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
//							+ "icone_add.png\""
//							+ " onclick=\""
//							+ "javascript:document.location.href='"
//							+"popup.php5?class=arquivo&action=adicionarMusicaNaPlaylist"
//							+ "&playlist=" + vPlaylist
//							+"&" + EXTDAOArquivo.NOME + "=" + vFileName
//							+ "&" + EXTDAOArquivo.PATH + "="
//							+ vStrPath+ "'\">&nbsp;");
				}

				vBuilder.append("			</td>");

				vBuilder.append("		</tr>");
			}
			if(!vHasFileOrDirectoryInFolder){
				vBuilder.append("<tr class=\"tr_list_conteudo_impar\">");
				vBuilder.append("<td  colspan=\"5\">");
				vBuilder.append(HelperHttp.imprimirMensagem(
						HelperString.ucFirst(context.getResources().getString(
								R.string.diretorio_sem_arquivo)),
								HelperHttp.TYPE_MENSAGEM.MENSAGEM_OK, null));
				vBuilder.append("</td>");
				vBuilder.append("</tr>");
			}
		}
		vBuilder.append("		</tbody>");
		vBuilder.append("	</table>");

		vBuilder.append("</form>");

	}

	private void procedureSegundaTabela(){
		Database db = new DatabaseWifiTransfer(__helper.context);
		EXTDAOPlaylist vObjPlaylist = new EXTDAOPlaylist(db);
		EXTDAOArquivoPlaylist vObjArquivoPlaylist = new EXTDAOArquivoPlaylist(db);
		vObjPlaylist.setAttrStrValue(EXTDAOPlaylist.ID, vPlaylist);
		
		String vNomePlaylist = "";
		if(vObjPlaylist.select()){
			vNomePlaylist = vObjPlaylist.getStrValueOfAttribute(EXTDAOPlaylist.NOME); 
		} else {
			vNomePlaylist = __helper.context.getResources().getString(R.string.nenhuma_playlist_selecionada);
		}
		//		TABELA CABECALHO =================================================================================================================
		vBuilder.append("    		<table width=\"350\" align=\"top\" class=\"tabela_list\">");
		vBuilder.append("<colgroup>");
		vBuilder.append("<col width=\"100%\" />");
		vBuilder.append("</colgroup>");
		vBuilder.append("<thead>");
		vBuilder.append("<tr class=\"tr_list_titulos\">");
		vBuilder.append("    				<td class=\"td_list_titulos\" >");
		vBuilder.append(vNomePlaylist);
		vBuilder.append("    				</td>");
		vBuilder.append("    				</tr>");
		//		EXCLUIR ICON ================================================================================================================================
		
		vBuilder.append("<tr class=\"tr_list_titulos\">");
		vBuilder.append("    				<td class=\"td_list_titulos\" >");
		
		Properties vLinkRemoveTotal = new Properties();
		vLinkRemoveTotal
		.put("onmouseover",
				"tip(\'"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
						+ "\', this);");
		vLinkRemoveTotal.put("onmouseout", "notip();");
		Link vObjLinkRemoveTotal = new Link(
				180,
				350,
				context.getResources().getString(
						R.string.deletar_arquivo),
						"popup.php5?tipo=pages&page=MultipleRemovePlaylistArquivo&"
								+ EXTDAOArquivo.PATH + "=" + vStrPath + "",
								vLinkRemoveTotal,
								"<img class=\"icones_list\" src=\""
										+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
										+ "icone_excluir.png\" onmouseover=\"javascript:tip('"
										+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
										+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
		vBuilder.append(vObjLinkRemoveTotal.montarLink());
		vBuilder.append(HelperString.ucFirst(context
				.getString(R.string.deletar_musica_da_playlist)));
		
		vBuilder.append("    			</td>");
		vBuilder.append("    			</tr>");
		vBuilder.append("</thead>");
		vBuilder.append("    		</table>");

		vBuilder.append("<table class=\"tabela_list\">");
		vBuilder.append("<colgroup>");

		vBuilder.append("<col width=\"10%\" />");
		vBuilder.append("<col width=\"80%\" />");
		vBuilder.append("<col width=\"20%\" />");

		vBuilder.append("</colgroup>");

		vBuilder.append("<thead>");
		vBuilder.append("<tr class=\"tr_list_titulos\">");

		vBuilder.append("<td class=\"td_list_titulos\">"
				+ "<input id=\"vetor_checkbox_playlist"
				+ "\" type=\"checkbox\" name=\"vetor_checkbox_musica\" onclick=\"marcarTodosOsCheckBoxesCujoIdContenha(this.checked, 'checkbox_playlist')\">"
				+ "</td>");

		vBuilder.append("<td class=\"td_list_titulos\">");


		vBuilder.append(HelperString.ucFirst(__helper.context.getResources().getString(
				R.string.arquivo_nome)));

		vBuilder.append("</td>");
		//		=========================================================================================

		vBuilder.append("<td class=\"td_list_titulos\">");
		vBuilder.append(HelperString.ucFirst(__helper.context.getResources().getString(
				R.string.arquivo_acoes)));
		vBuilder.append("</td>");
		vBuilder.append("</tr>");
		vBuilder.append("</thead>");
		vBuilder.append("<tbody>");
		
		HashMap<String, File> vHashIdArquivoPlaylistPorFile = vObjArquivoPlaylist.getHashIdPorFile(vPlaylist);

		if(vHashIdArquivoPlaylistPorFile != null && vHashIdArquivoPlaylistPorFile.size() > 0 ){
			Set<String> vSetIdArquivoPlaylist = vHashIdArquivoPlaylistPorFile.keySet();
			Iterator<String> vIterator = vSetIdArquivoPlaylist.iterator();
			while(vIterator.hasNext()){
				String vIdArquivoPlaylist = vIterator.next();
				File vFileMusica = vHashIdArquivoPlaylistPorFile.get(vIdArquivoPlaylist);
				String vFilePath = vFileMusica.getAbsolutePath();
				String vFileName = HelperFile.getNameOfFileInPath(vFilePath);
				vBuilder.append("<tr class=\"td_list_conteudo\">");
				vBuilder.append("<td class=\"td_list_conteudo\">");
				vBuilder.append("<input id=\"checkbox_playlist"
						+ vFilePath
						+ "\" type=\"checkbox\" name=\"checkbox_playlist" 
						+ vFilePath 
						+ "\" value=\"" + vIdArquivoPlaylist + "\" >");
				vBuilder.append("</td>");

				vBuilder.append("<td class=\"td_list_conteudo\">");
				vBuilder.append(vFileName);
				vBuilder.append("</td>");
				vBuilder.append("<td class=\"td_list_conteudo\">");
//				TOCAR MUSICA
				Properties vLinkMusica = new Properties();
				vLinkMusica
						.put("onmouseover",
								"tip(\'"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_TOCAR_MUSICA)
										+ "\', this);");
				vLinkMusica.put("onmouseout", "notip();");
				Link vObjLinkMusica = new Link(
						200,
						500,
						context.getResources().getString(
								R.string.music),
						"image.php5?tipo=pages&page=MusicPagina&"
							+ "path_completo=" + vFilePath,
						vLinkMusica,
						"<img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "music.png\" onmouseover=\"javascript:tip('"
								+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_TOCAR_MUSICA)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
						true);
				vBuilder.append(vObjLinkMusica.montarLink());
				
				
//				Excluir playlist
				Properties vLinkRemove = new Properties();
				vLinkRemove
						.put("onmouseover",
								"tip(\'"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
										+ "\', this);");
				vLinkRemove.put("onmouseout", "notip();");
				Link vObjLinkRemove = new Link(
						180,
						350,
						context.getResources().getString(
								R.string.deletar_arquivo),
						"popup.php5?tipo=pages&page=RemovePlaylistArquivo&"
								+ "&" + EXTDAOArquivo.PATH + "=" + vStrPath
								+ "&arquivo_playlist=" + vIdArquivoPlaylist
								+ "&playlist=" + vPlaylist,
								vLinkRemove,
						"<img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "icone_excluir.png\" onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(vObjLinkRemove.montarLink());
				
				
				vBuilder.append("</td>");
				vBuilder.append("</tr>");
			}
			
		} else {
			vBuilder.append("<tr class=\"tr_list_conteudo_impar\">");
			vBuilder.append("<td  colspan=\"3\">");
			vBuilder.append(HelperHttp.imprimirMensagem(
					HelperString.ucFirst(context.getResources().getString(
							R.string.playlist_vazia)),
							HelperHttp.TYPE_MENSAGEM.MENSAGEM_OK, null));
			vBuilder.append("</td>");
			vBuilder.append("</tr>");
		}
		db.close();
		vBuilder.append("</tbody>");
		vBuilder.append("</table>");


		vBuilder.append("<br/>");
		vBuilder.append("<br/>");
	}

	private void procedureTerceiraTabela(){

		//		Anexo Playlist Telefone============================================================
		vContainer.appendData(vBuilder);
		ContainerResponse vContainerAnexoAdicionarPlaylistPagina = new AnexoAdicionarPlaylistPagina(__helper).getContainerResponse();
		vContainer.copyContainerResponse(vContainerAnexoAdicionarPlaylistPagina);
		vBuilder = new StringBuilder();
	}

}
