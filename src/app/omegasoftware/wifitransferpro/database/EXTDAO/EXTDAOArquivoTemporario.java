package app.omegasoftware.wifitransferpro.database.EXTDAO;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import app.omegasoftware.wifitransferpro.database.Attribute;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;

public class EXTDAOArquivoTemporario extends Table{

	public static final String NAME = "arquivo_temporario";
	
	public static String ID = "id";
	public static String NOME = "nome";
	
	public static String PATH = "path";
	
	
	public EXTDAOArquivoTemporario(Database p_database) {
		super(NAME, p_database);

		super.addAttribute(new Attribute(ID, "id", true, Attribute.SQL_TYPE.LONG, false, null, true, null, 11, true));
//		link para a imagem no site/OmegaWikiTransfer/
		super.addAttribute(new Attribute(PATH, "path", true, Attribute.SQL_TYPE.TEXT, true, null, false, null, 255));
	}
	
	public static File createArquivoTemporario(Context pContext, String pFileName){
		
		String tmpdir = System.getProperty("java.io.tmpdir");
		File temp = null;
		try {
			temp = File.createTempFile(pFileName, "", new File(tmpdir));
			DatabaseWifiTransfer db = new DatabaseWifiTransfer(pContext);
			EXTDAOArquivoTemporario vObjArquivoTemporario = new EXTDAOArquivoTemporario(db);
			vObjArquivoTemporario.setAttrStrValue(EXTDAOArquivoTemporario.PATH, temp.getAbsolutePath());
			vObjArquivoTemporario.insert(false);
			db.close();
		} catch (Exception e) { // Catch exception if any
		}
		
		
		return temp;
	}
	
	public static void deleteAllArquivoTemporario(Context pContext){
		DatabaseWifiTransfer db = new DatabaseWifiTransfer(pContext);
		EXTDAOArquivoTemporario vObjArquivoTemporario = new EXTDAOArquivoTemporario(db);
		ArrayList<Table> vList = vObjArquivoTemporario.getListTable();
		if(vList != null && vList.size() > 0 ){
			for (Table vTupla : vList) {
				String vPath = vTupla.getStrValueOfAttribute(EXTDAOArquivoTemporario.PATH);
				if(vPath != null && vPath.length() > 0 ){
					File vFile = new File(vPath);
					if(vFile.exists())
						vFile.delete();
					vTupla.remove(false);	
				}
			}
		}
		db.close();
	}
	
	@Override
	public void populate() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Table factory() {
		// TODO Auto-generated method stub
		return new EXTDAOArquivoTemporario(getDatabase());
	}
	@Override
	public void procedureAfterSynchInsert(Context pContext, String pNewId,
			String pOldId) {
		// TODO Auto-generated method stub
		
	}

}
