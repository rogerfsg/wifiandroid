package app.omegasoftware.wifitransferpro.http.lists;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.ContainerTypeOrderFile;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.file.ContainerTypeOrderFile.TYPE_ORDER;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.pages.InformacaoTelefonePagina;
import app.omegasoftware.wifitransferpro.http.pages.PesquisaAnexoPagina;
import app.omegasoftware.wifitransferpro.http.pages.UploadPagina;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Link;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM_TOOLTIP;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class ListsArquivo extends InterfaceHTML {

	public ListsArquivo(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		String vStrPath = __helper.GET("path");
		if (vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		vStrPath = HelperFile.getNomeSemBarra(vStrPath);
		vStrPath += "/";
		StringBuilder vBuilder = new StringBuilder();
		ContainerResponse vContainer = new ContainerResponse(__helper, new StringBuilder());
		if (vStrPath != null && vStrPath.length() > 0) {
			File vDirectory = new File(vStrPath);
			if (vDirectory.isDirectory()) {

				ContainerTypeOrderFile vContainerOrder = new ContainerTypeOrderFile();
				vContainerOrder.setByGet(__helper);
				vBuilder.append("<td id=\"td_left\" >");
				vBuilder.append("      <form name=\"form_arquivo\" id=\"form_arquivo\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
				vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");
				vBuilder.append("            <input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
				vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
				vBuilder.append("           	<input type=\"hidden\" name=\"class\" id=\"class\" value=\""
						+ EXTDAOArquivo.NAME + "\">");
				vBuilder.append("            <input type=\"hidden\" name=\"action\" id=\"action\" value=\"arquivo\">");
				vBuilder.append("            <input type=\"hidden\" name=\"funcao\" id=\"funcao\" value=\"\">");
				vBuilder.append("            <input type=\"hidden\" name=\"novo_nome\" id=\"novo_nome\" value=\"\">");
				vBuilder.append("            <input type=\"hidden\" name=\"novo_path\" id=\"novo_path\" value=\"\">");				
				
				
				vBuilder.append("            <input type=\"hidden\" name=\""
						+ EXTDAOArquivo.NOME + "1\" id=\"" + EXTDAOArquivo.NOME
						+ "1\" value=\"" + __helper.GET(EXTDAOArquivo.NOME)
						+ "\">");
				vBuilder.append("            <input type=\"hidden\" name=\""
						+ EXTDAOArquivo.PATH + "1\" id=\"" + EXTDAOArquivo.PATH
						+ "1\" value=\"" + vStrPath + "\">");
//		TABELA CABECALHO =================================================================================================================
				vBuilder.append("    		<table width=\"350\" align=\"top\" class=\"tabela_list\">");
				String vLinkOrder = vContainerOrder.getLinkHTML(null, null);
				String vAppendLinkOrder = ((vLinkOrder.length()) > 0 ? ("&" + vLinkOrder) : "");
				
				vBuilder.append("<colgroup>");
				vBuilder.append("<col width=\"20%\" />");
				vBuilder.append("<col width=\"20%\" />");
				vBuilder.append("<col width=\"20%\" />");
				vBuilder.append("<col width=\"20%\" />");
				vBuilder.append("<col width=\"20%\" />");
				vBuilder.append("</colgroup>");
				vBuilder.append("<thead>");

				
				
//		PATH DO CABECALHO =================================================================================================================
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td colspan=\"5\" height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; adding-left: 5px; color: #333333; \" >");
				vBuilder.append(HelperString.ucFirst(context
						.getString(R.string.path)) + ":");
				
				
				vBuilder.append("<input type=\"text\" maxlength=\"200\" id=\""
						+ EXTDAOArquivo.PATH
						+ "1\" name=\""
						+ EXTDAOArquivo.PATH
						+ "1\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\""
						+ vStrPath + "\"/>");
		
				vBuilder.append("<input name=\"txtBotao\" type=\"button\" value=\""
						+ context.getString(R.string.ok).toUpperCase()
						+ "\" " +
						"onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&"
						+ EXTDAOArquivo.PATH + "="
						+ "' + document.getElementById('" + EXTDAOArquivo.PATH + "1').value + '"
						+  "\"" +
						"class=\"botoes_form\" style=\"cursor: pointer;\" />");
				vBuilder.append("    				</td>");
				vBuilder.append("    			</tr>");
//		LISTA DE BOTOS DO PATH CORRENTE =================================================================================================		
				
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td height=\"11\" colspan=\"5\" align=\"left\" valign=\"middle\" class=\"textos5\" style=\"padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;\" >");
				String vVetorPath[] = vStrPath.split("[/]");
				String vPathDiretorio = "";
				if(vVetorPath == null || vVetorPath.length == 0 )
					vBuilder.append("<input class=\"botoes_form\" value=\"/"
							+ "\" type=\"button\" onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&"
							+ EXTDAOArquivo.PATH + "=" + vPathDiretorio
							+ "'\">");
				else{
					
					for(int i = 0 ;  i < vVetorPath.length; i++){
						if(i == 0 ){
							vBuilder.append("<input class=\"botoes_path\" value=\"/"
									+ "\" type=\"button\" onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&"
									+ EXTDAOArquivo.PATH + "=" + vPathDiretorio
									+ "'\">");
							
						}
						String vDiretorio = vVetorPath[i];
						
						if (vDiretorio == null || vDiretorio.length() == 0)
							continue;
						vPathDiretorio += "/" + vDiretorio;
						vBuilder.append("<input class=\"botoes_path\" value=\""
								+ vDiretorio
								+ "\" type=\"button\" onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&"
								+ EXTDAOArquivo.PATH + "=" + vPathDiretorio
								+ "'\">");
					}
				}
				vBuilder.append("    			</td>");
				vBuilder.append("    			</tr>");
				vBuilder.append("    			<tr class=\"tr_list_titulos\">");
				
//		ZIP ICON ================================================================================================================================
				vBuilder.append("    				<td class=\"td_list_titulos_arredondados\" >");
				
				Properties vLinkZipTotal = new Properties();
				vLinkZipTotal.put(
						"onmouseover",
						"tip(\'"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ZIP_ARQUIVO)
								+ "\', this);");
				vLinkZipTotal.put("onmouseout", "notip();");
				
				Link vObjLinkZipTotal = new Link(
						200,
						400,
						context.getResources().getString(
								R.string.zipar_arquivo),
						"popup.php5?tipo=pages&page=ZipArquivo&next_action=multipleZipArquivo&is_multiple=true&"
								+ EXTDAOArquivo.PATH + "="
								+ vStrPath,
						vLinkZipTotal,
						"<img class=\"imagem_menu_acoes\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "zip_folder.png\" onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ZIP_ARQUIVO)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(vObjLinkZipTotal.montarLink());
				vBuilder.append(HelperString.ucFirst(context
						.getString(R.string.zipar_arquivo)));
				
				vBuilder.append("    			</td>");
//		COPY ICON ================================================================================================================================
				vBuilder.append("    				<td class=\"td_list_titulos_arredondados\" >");
				Properties vLinkCopyTotal = new Properties();
				vLinkCopyTotal
						.put("onmouseover",
								"tip(\'"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_COPY_ARQUIVO)
										+ "\', this);");
				vLinkCopyTotal.put("onmouseout", "notip();");
				Link vObjLinkCopyTotal = new Link(
						1000,
						700,
						context.getResources().getString(
								R.string.copiar_arquivo),
						"popup.php5?tipo=pages&page=ListsArquivoCopy&next_action=multipleCopiarArquivo&is_multiple=true&"
								+ EXTDAOArquivo.NOME + "="  
								+ "&" + EXTDAOArquivo.PATH + "="
								+ vStrPath + "",
						vLinkCopyTotal,
						"<img class=\"imagem_menu_acoes\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "copy_folder.png\" onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_COPY_ARQUIVO)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(vObjLinkCopyTotal.montarLink());
				vBuilder.append(HelperString.ucFirst(context
						.getString(R.string.copiar_arquivo)));
				vBuilder.append("    			</td>");
//		MOVER ICON ================================================================================================================================
				vBuilder.append("    				<td class=\"td_list_titulos_arredondados\" >");
				Properties vLinkMoveTotal = new Properties();
				vLinkMoveTotal
						.put("onmouseover",
								"tip(\'"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_MOVER_ARQUIVO)
										+ "\', this);");
				vLinkMoveTotal.put("onmouseout", "notip();");
				Link vObjLinkMoveTotal = new Link(
						1000,
						700,
						context.getResources().getString(
								R.string.mover_arquivo),
						"popup.php5?tipo=pages&page=ListsArquivoMove&next_action=multipleMoverArquivo&is_multiple=true&"
								+ EXTDAOArquivo.NOME + "="  
								+ "&" + EXTDAOArquivo.PATH + "="
								+ vStrPath + "",
						vLinkMoveTotal,
						"<img class=\"imagem_menu_acoes\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "move_folder.png\" onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_MOVER_ARQUIVO)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(vObjLinkMoveTotal.montarLink());
				vBuilder.append(HelperString.ucFirst(context
						.getString(R.string.mover_arquivo)));
				vBuilder.append("    			</td>");
//		DOWNLOAD ICON ================================================================================================================================
				vBuilder.append("    				<td class=\"td_list_titulos_arredondados\" >");
				
				Properties vLinkDownloadTotal = new Properties();
				vLinkDownloadTotal
						.put("onmouseover",
								"tip(\'"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_DOWNLOAD)
										+ "\', this);");
				vLinkDownloadTotal.put("onmouseout", "notip();");
				Link vObjLinkDownloadTotal = new Link(
						250,
						600,
						context.getResources().getString(
								R.string.download),
						"popup.php5?tipo=pages&page=TipoMultipleDownloadArquivo&"
								+ EXTDAOArquivo.NOME + "=" 
								+ "&" + EXTDAOArquivo.PATH + "="
								+ vStrPath + "",
						vLinkDownloadTotal,
						"<img class=\"imagem_menu_acoes\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "icone_download.png\" onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_DOWNLOAD)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(vObjLinkDownloadTotal.montarLink());
				vBuilder.append(HelperString.ucFirst(context
						.getString(R.string.download_arquivo)));
				vBuilder.append("    			</td>");
//		EXCLUIR ICON ================================================================================================================================
				vBuilder.append("    				<td class=\"td_list_titulos_arredondados\" >");
				Properties vLinkRemoveTotal = new Properties();
				vLinkRemoveTotal
						.put("onmouseover",
								"tip(\'"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
										+ "\', this);");
				vLinkRemoveTotal.put("onmouseout", "notip();");
				Link vObjLinkRemoveTotal = new Link(
						180,
						350,
						context.getResources().getString(
								R.string.deletar_arquivo),
						"popup.php5?tipo=pages&page=RemoveArquivo&"
								+ EXTDAOArquivo.NOME + "=" 
								+ "&" + EXTDAOArquivo.PATH + "="
								+ vStrPath + ""
								+"&next_action=multipleRemoverArquivo",
						vLinkRemoveTotal,
						"<img class=\"imagem_menu_acoes\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "icone_excluir.png\" onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(vObjLinkRemoveTotal.montarLink());
				vBuilder.append(HelperString.ucFirst(context
						.getString(R.string.deletar_arquivo)));
				


				

				
				vBuilder.append("    			</tr>");
				vBuilder.append("</thead>");
				vBuilder.append("    		</table>");

				
//		TABELA ==================================================================================================================================
				vBuilder.append("<table class=\"tabela_list\">");
				vBuilder.append("<colgroup>");
				vBuilder.append("<col width=\"2%\" />");
				vBuilder.append("<col width=\"10%\" />");
				vBuilder.append("<col width=\"30%\" />");
				vBuilder.append("<col width=\"18%\" />");
				vBuilder.append("<col width=\"15%\" />");
				vBuilder.append("<col width=\"20%\" />");
				vBuilder.append("</colgroup>");

				vBuilder.append("<thead>");
				vBuilder.append("<tr class=\"tr_list_titulos\">");
				
				vBuilder.append("<script>");
				vBuilder.append("function marcarTodosOsCheckBoxesCujoIdContenha(isChecked, conteudoId){");
				vBuilder.append("    var inputs = document.getElementsByTagName(\"input\");");
				vBuilder.append("   for(var i=0; i < inputs.length; i++){");
				vBuilder.append("       if(inputs[i].type == \"checkbox\" && inputs[i].id.indexOf(conteudoId) != -1){");
				vBuilder.append("           inputs[i].checked = isChecked;");
				vBuilder.append("       }");
				vBuilder.append("   }");
				vBuilder.append("}");
				vBuilder.append("</script>");
				
				vBuilder.append("<td class=\"td_list_titulos\">"
						+ "<input id=\"vetor_checkbox_arquivo"
						+ "\" type=\"checkbox\" name=\"vetor_checkbox_arquivo\" onclick=\"marcarTodosOsCheckBoxesCujoIdContenha(this.checked, 'checkbox_arquivo')\">"
						+ "</td>");
//				NOME ======================================================================================
				vBuilder.append("<td class=\"td_list_titulos\">");
//				DIRETORIO ACIMA ICON ================================================================================================================================
						
						vBuilder.append(" <img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "up_folder.png\" "
								+ "onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_DIRETORIO_ACIMA)
								+ "')\" "
								+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&"
								+ EXTDAOArquivo.PATH + "="
								+ HelperHttp.getStrPathUp(vStrPath) 
								+ vAppendLinkOrder + "'\""
								+ "onmouseout=\"javascript:notip()\">&nbsp;");
//				RAIZ ICON ================================================================================================================================
				
						vBuilder.append("<img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "home_folder.png\" "
								+ "onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_RAIZ)
								+ "')\" "
								+ "onclick=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&"
								+ EXTDAOArquivo.PATH + "="
								+ HelperHttp.getPathRoot()
								+ vAppendLinkOrder + "\""
								+ "onmouseout=\"javascript:notip()\">&nbsp;");
				
						Properties vLinkCriaDiretorio = new Properties();
						vLinkCriaDiretorio
								.put("onmouseover",
										"tip(\'"
												+ __helper
														.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_CRIA_DIRETORIO)
												+ "\', this);");
						vLinkCriaDiretorio.put("onmouseout", "notip();");
						Link vObjLinkCriaDiretorio = new Link(
								200,
								400,
								context.getResources().getString(R.string.criar_diretorio),
								"popup.php5?tipo=pages&page=CriaDiretorio&"
										+ EXTDAOArquivo.PATH + "=" + vStrPath + "",
								vLinkCriaDiretorio,
								"<img class=\"icones_list\" src=\""
										+ OmegaFileConfiguration
												.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
										+ "add_folder.png\" onmouseover=\"javascript:tip('"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_CRIA_DIRETORIO)
										+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
						
						vBuilder.append(vObjLinkCriaDiretorio.montarLink());
						
				vBuilder.append(vContainerOrder.getImageHTML(TYPE_ORDER.TYPE, "lists", "arquivo", EXTDAOArquivo.PATH + "=" + vStrPath));
				
				vBuilder.append("</td>");
				vBuilder.append("<td class=\"td_list_titulos\">");
				
				vBuilder.append(vContainerOrder.getButtonTitulo(
						__helper.context,
						HelperString.ucFirst(__helper.context.getResources().getString(
								R.string.arquivo_nome)), "lists", "arquivo",
						EXTDAOArquivo.PATH + "=" + vStrPath, TYPE_ORDER.NAME));
				vBuilder.append(vContainerOrder.getImageHTML(TYPE_ORDER.NAME, "lists", "arquivo", EXTDAOArquivo.PATH + "=" + vStrPath));
				
						
				vBuilder.append("</td>");
//				=========================================================================================
				vBuilder.append("<td class=\"td_list_titulos\">");
				vBuilder.append(vContainerOrder.getButtonTitulo(
						__helper.context,
						HelperString.ucFirst( __helper.context.getResources().getString(
								R.string.arquivo_data)), "lists", "arquivo",
						EXTDAOArquivo.PATH + "=" + vStrPath, TYPE_ORDER.DATE));
				vBuilder.append(vContainerOrder.getImageHTML(TYPE_ORDER.DATE, "lists", "arquivo", EXTDAOArquivo.PATH + "=" + vStrPath));
				
				vBuilder.append("</td>");
				vBuilder.append("<td class=\"td_list_titulos\">");
				vBuilder.append(vContainerOrder.getButtonTitulo(
						__helper.context,
						HelperString.ucFirst(__helper.context.getResources().getString(
								R.string.arquivo_tamanho)), "lists", "arquivo",
						EXTDAOArquivo.PATH + "=" + vStrPath, TYPE_ORDER.SIZE));
				vBuilder.append(vContainerOrder.getImageHTML(TYPE_ORDER.SIZE, "lists", "arquivo", EXTDAOArquivo.PATH + "=" + vStrPath));
				vBuilder.append("</td>");
				vBuilder.append("<td class=\"td_list_titulos\">"
						+ HelperString.ucFirst(context.getResources().getString(
								R.string.arquivo_acoes)) + "</td>");

				vBuilder.append("</tr>");
				vBuilder.append("</thead>");
				vBuilder.append("<tbody>");
				
				File vVetorFile[] = vContainerOrder
						.getListFileOfDirectory(vDirectory);
				if (vVetorFile == null || vVetorFile.length == 0) {
					vBuilder.append("<tr class=\"tr_list_conteudo_impar\">");
					vBuilder.append("<td  colspan=\"5\">");
					vBuilder.append(HelperHttp.imprimirMensagem(
							HelperString.ucFirst(context.getResources().getString(
									R.string.diretorio_sem_arquivo)),
							HelperHttp.TYPE_MENSAGEM.MENSAGEM_OK, null));
					vBuilder.append("</td>");
					vBuilder.append("</tr>");
				} else {
					 
					for (int i = 0; i < vVetorFile.length; i++) {
						File vFile = vVetorFile[i];
						ContainerTypeFile vContainerType = ContainerTypeFile
								.getContainerTypeFile(vFile);
						
						String classTr = (i % 2 == 0) ? "tr_list_conteudo_impar"
								: "tr_list_conteudo_par";
						String vStri = String.valueOf(i);
						String vFileName = vFile.getName();
						boolean isDirectory = (vContainerType.typeFile == TYPE_FILE.FOLDER) ? true : false;
						
						vBuilder.append("<tr class=\"" + classTr + "\">");
						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");
						vBuilder.append("<input id=\"checkbox_arquivo_"
								+ vStri
								+ "\" type=\"checkbox\" name=\"checkbox_arquivo_" 
								+ vStri 
								+ "\" value=\"" + vFileName + "\" >");
						vBuilder.append("</td>");
						vBuilder.append("<td  style=\"text-align: center; padding-left: 5px;\">");
						vBuilder.append("<img class=\"icones_list\" src=\""
								+ vContainerType.getImage()
								+ "\">&nbsp;");
						vBuilder.append("</td>");
						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");

						if (!isDirectory)
							vBuilder.append(vFileName);
						else {
							vBuilder.append("<a href=\"javascript:document.location.href='index.php5?tipo=lists&page=arquivo&path="
									+ vStrPath
									+ vFileName
									+ "/"
									+ "'\" class=\"link_padrao\" onmouseout=\"notip();\" onmouseover=\"tip('"
									+ __helper
											.getMensagemTooltip(TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_DIRETORIO)
									+ "');\">" + vFileName + "</a>");
						}
						vBuilder.append("</td>");

						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");
						long vLastModified = vFile.lastModified();
						if(vLastModified == 0 )
							vBuilder.append("-");
						else{
							Date vDate = new Date();
							vBuilder.append(vDate.toLocaleString());
							vBuilder.append("</td>");	
						}
						

						vBuilder.append("<td  style=\"text-align: left; padding-left: 5px;\">");
						if (!isDirectory)
							vBuilder.append(HelperFile.getStrLength(vFile));
						else
							vBuilder.append("-");
						vBuilder.append("</td>");
						vBuilder.append("<td  style=\"text-align: center;\">");

						
						
						if (vContainerType.typeFile == TYPE_FILE.IMAGE) {
							Properties vLinkImage = new Properties();
							vLinkImage
									.put("onmouseover",
											"tip(\'"
													+ __helper
															.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_VISUALIZAR_IMAGEM)
													+ "\', this);");
							vLinkImage.put("onmouseout", "notip();");
							Link vObjLinkImage = new Link(
									600,
									600,
									context.getResources().getString(
											R.string.imagem),
									"image.php5?tipo=pages&page=ImageArquivo&"
										+ EXTDAOArquivo.NOME + "=" + vFileName
										+ "&" + EXTDAOArquivo.PATH + "="
										+ vStrPath,
									vLinkImage,
									"<img class=\"icones_list\" src=\""
											+ OmegaFileConfiguration
													.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
											+ "imagem.png\" onmouseover=\"javascript:tip('"
											+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_VISUALIZAR_IMAGEM)
											+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
									true);
							vBuilder.append(vObjLinkImage.montarLink());	
						} else if (vContainerType.typeFile == TYPE_FILE.MUSIC){
							Properties vLinkMusica = new Properties();
							vLinkMusica
									.put("onmouseover",
											"tip(\'"
													+ __helper
															.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_TOCAR_MUSICA)
													+ "\', this);");
							vLinkMusica.put("onmouseout", "notip();");
							Link vObjLinkMusica = new Link(
									100,
									500,
									context.getResources().getString(
											R.string.music),
									"image.php5?tipo=pages&page=MusicPagina&"
										+ EXTDAOArquivo.NOME + "=" + vFileName
										+ "&" + EXTDAOArquivo.PATH + "="
										+ vStrPath,
									vLinkMusica,
									"<img class=\"icones_list\" src=\""
											+ OmegaFileConfiguration
													.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
											+ "music.png\" onmouseover=\"javascript:tip('"
											+ __helper.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_TOCAR_MUSICA)
											+ "')\" onmouseout=\"javascript:notip()\">&nbsp;",
									true);
							vBuilder.append(vObjLinkMusica.montarLink());
						}
						
						Properties vLinkRenomear = new Properties();
						vLinkRenomear.put(
								"onmouseover",
								"tip(\'"
										+ context.getResources().getString(
												R.string.tooltip_renomear)
										+ "\');");
						vLinkRenomear.put("onmouseout", "notip();");
						Link vObjLinkRenomear = new Link(
								200,
								500,
								context.getResources().getString(
										R.string.renomear),
								"popup.php5?tipo=pages&page=renomear&navegacao=false&"
										+ EXTDAOArquivo.NOME + "=" + vFileName
										+ "&" + EXTDAOArquivo.PATH + "="
										+ vStrPath,
								vLinkRenomear,
								"<img class=\"icones_list\" src=\""
										+ OmegaFileConfiguration
												.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
										+ "icone_editar.png\" onmouseover=\"javascript:tip('"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_EDICAO)
										+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
						vBuilder.append(vObjLinkRenomear.montarLink());

						
						if (vContainerType.typeFile != TYPE_FILE.ZIP) {
							Properties vLinkZip = new Properties();
							vLinkZip.put(
									"onmouseover",
									"tip(\'"
											+ __helper
													.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ZIP_ARQUIVO)
											+ "\', this);");
							vLinkZip.put("onmouseout", "notip();");
							Link vObjLinkZip = new Link(
									300,
									500,
									context.getResources().getString(
											R.string.zipar_arquivo),
									"popup.php5?tipo=pages&page=ZipArquivo&next_action=zipArquivo&"
											+ EXTDAOArquivo.NOME + "="
											+ vFileName + "&"
											+ EXTDAOArquivo.PATH + "="
											+ vStrPath + "",
									vLinkZip,
									"<img class=\"icones_list\" src=\""
											+ OmegaFileConfiguration
													.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
											+ "zip_folder.png\" onmouseover=\"javascript:tip('"
											+ __helper
													.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ZIP_ARQUIVO)
											+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
							vBuilder.append(vObjLinkZip.montarLink());
						} else {
							Properties vLinkUnzip = new Properties();
							vLinkUnzip.put(
									"onmouseover",
									"tip(\'"
											+ __helper
													.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_UNZIP_ARQUIVO)
											+ "\', this);");
							vLinkUnzip.put("onmouseout", "notip();");
							Link vObjLinkZip = new Link(
									1000,
									700,
									context.getResources().getString(
											R.string.extrair_arquivo),
									"popup.php5?tipo=pages&page=ListsArquivoUnzip&"
											+ EXTDAOArquivo.NOME + "="
											+ vFileName + "&"
											+ EXTDAOArquivo.PATH + "="
											+ vStrPath + "",
									vLinkUnzip,
									"<img class=\"icones_list\" src=\""
											+ OmegaFileConfiguration
													.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
											+ "unzip_folder.png\" onmouseover=\"javascript:tip('"
											+ __helper
													.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_UNZIP_ARQUIVO)
											+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
							vBuilder.append(vObjLinkZip.montarLink());
						}

						Properties vLinkCopy = new Properties();
						vLinkCopy
								.put("onmouseover",
										"tip(\'"
												+ __helper
														.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_COPY_ARQUIVO)
												+ "\', this);");
						vLinkCopy.put("onmouseout", "notip();");
						Link vObjLinkCopy = new Link(
								1000,
								700,
								context.getResources().getString(
										R.string.copiar_arquivo),
								"popup.php5?tipo=pages&page=ListsArquivoCopy&next_action=copiarArquivo&"
										+ EXTDAOArquivo.NOME + "=" + vFileName
										+ "&" + EXTDAOArquivo.PATH + "="
										+ vStrPath + "",
								vLinkCopy,
								"<img class=\"icones_list\" src=\""
										+ OmegaFileConfiguration
												.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
										+ "copy_folder.png\" onmouseover=\"javascript:tip('"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_COPY_ARQUIVO)
										+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
						vBuilder.append(vObjLinkCopy.montarLink());
						
						Properties vLinkMove = new Properties();
						vLinkMove
								.put("onmouseover",
										"tip(\'"
												+ __helper
														.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_MOVER_ARQUIVO)
												+ "\', this);");
						vLinkMove.put("onmouseout", "notip();");
						Link vObjLinkMove = new Link(
								1000,
								700,
								context.getResources().getString(
										R.string.mover_arquivo),
								"popup.php5?tipo=pages&page=ListsArquivoMove&next_action=moverArquivo&"
										+ EXTDAOArquivo.NOME + "=" + vFileName
										+ "&" + EXTDAOArquivo.PATH + "="
										+ vStrPath + "",
								vLinkMove,
								"<img class=\"icones_list\" src=\""
										+ OmegaFileConfiguration
												.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
										+ "move_folder.png\" onmouseover=\"javascript:tip('"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_MOVER_ARQUIVO)
										+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
						vBuilder.append(vObjLinkMove.montarLink());
						
						vBuilder.append("<img class=\"icones_list\" " +
								"src=\"" +  OmegaFileConfiguration.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "icone_download.png\""
								+ " onclick=\""
								+ "javascript:document.location.href='"
								+"download.php5?tipo=pages&page=DownloadArquivo&"
								+ EXTDAOArquivo.NOME + "=" + vFileName
								+ "&" + EXTDAOArquivo.PATH + "="
								+ vStrPath+ "'\">&nbsp;");

						vBuilder.append("<img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "icone_excluir.png\" onclick=\"javascript:confirmarExclusao('actions.php5?class="
								+ EXTDAOArquivo.NAME
								+ "&action=remove&nome1="
								+ vFileName
								+ "&path1="
								+ vStrPath
								+ "','"
								+ __helper
										.getMensagemTooltip(TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
								+ "')\" onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");

						vBuilder.append("</td>");

						vBuilder.append("</tr>");
					}
				}
				vBuilder.append("</tbody>");
				vBuilder.append("</table>");

				vBuilder.append("<br/>");
				vBuilder.append("<br/>");
				vBuilder.append("</form>");
				vBuilder.append("</td>");
				
//				Direita Arquivo ============================================================
				vBuilder.append("<td id=\"td_right\">");
				
//				Informacao Telefone Arquivo ============================================================				
				vContainer.appendData(vBuilder);
				ContainerResponse vContainerInformacaoTelefonePagina = new InformacaoTelefonePagina(__helper).getContainerResponse();
				vContainer.copyContainerResponse(vContainerInformacaoTelefonePagina);
				vBuilder = new StringBuilder();
				

//				Uploar Arquivo ============================================================				
//				vContainer.appendData(vBuilder);
//				ContainerResponse vContainerPesquisaPagina = new PesquisaAnexoPagina(__helper).getContainerResponse();
//				vContainer.copyContainerResponse(vContainerPesquisaPagina);
//				vBuilder = new StringBuilder();
				

//				Upload Telefone ============================================================
				vContainer.appendData(vBuilder);
				ContainerResponse vContainerUploadPagina = new UploadPagina(__helper).getContainerResponse();
				vContainer.copyContainerResponse(vContainerUploadPagina);
				vBuilder = new StringBuilder();
				
				
				
				vBuilder.append("</td>");
				
				vContainer.appendData(vBuilder);

			}
		}

		return vContainer;
	}

}
