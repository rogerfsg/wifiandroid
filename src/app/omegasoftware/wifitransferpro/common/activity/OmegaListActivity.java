package app.omegasoftware.wifitransferpro.common.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.TabletActivities.AboutActivityMobile;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;

public abstract class OmegaListActivity extends ListActivity {

	private String TAG = "OmegaListActivity";

	String __nomeTag = null;
	
	
	public abstract boolean loadData();
	public abstract void initializeComponents();
	
	protected ArrayList<String> __listIdPrestadorEnderecoDialog = new ArrayList<String>();
	
	public void setTag(String p_tag)
	{
		this.TAG = p_tag;
	}
	
	public void addIdPrestadorEnderecoDialog(String p_idPrestadorEndereco){
		__listIdPrestadorEnderecoDialog.add(p_idPrestadorEndereco);
	}
	public void clearPrestadorEnderecoDialog(){
		__listIdPrestadorEnderecoDialog.clear();
	}	
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.mobile_menu, menu);
	    return true;
	}
	
	@Override
	public void finish(){
		
		super.finish();
		
	}

	public void performExecute(){
		new CustomDataLoader(this).execute();
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		Intent intent = null;
		
			switch (item.getItemId()) {
			case R.id.menu_favorito:
				
				break;
			case R.id.menu_sobre:
				intent = new Intent(this, AboutActivityMobile.class);
				break;

			default:
				return super.onOptionsItemSelected(item);

			}
		
		if(intent != null){
			startActivity(intent);	
		}
		return true;
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if(requestCode == OmegaConfiguration.ACTIVITY_FORM_EDIT){

			try{
				boolean vIsModificated = true;
				Bundle pParameter = intent.getExtras();
				
				if(pParameter != null)
					if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
						vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
				if(vIsModificated)
					performExecute();

			}catch(Exception ex){
				Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
			}
		} 
	}
	
	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity __activity;
		public CustomDataLoader(Activity pActivity){
			__activity = pActivity;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			try{
				return loadData();
			}
			catch(Exception e)
			{
				Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());			
			}
			return false;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			if(result)
			{
				initializeComponents();
			}
			
			try
			{
				dismissDialog(OmegaConfiguration.ON_LOAD_DIALOG);	
			}
			catch(Exception e)
			{
			}
			
		}
		
	}
	
}
