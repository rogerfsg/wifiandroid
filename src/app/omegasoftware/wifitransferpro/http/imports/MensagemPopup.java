package app.omegasoftware.wifitransferpro.http.imports;

import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;

public class MensagemPopup extends InterfaceHTML{

	public MensagemPopup(HelperHttp pHelperHTTP) {
		super(pHelperHTTP);
		// TODO Auto-generated constructor stub
	}
 
	@Override
	public ContainerResponse getContainerResponse() {
		StringBuilder vBuilder = new StringBuilder();
		vBuilder.append("<center> " );
		if(__helper.GET("msgSucesso") != null){

			vBuilder.append(HelperHttp.imprimirMensagem(HelperHttp.nl2br(__helper.GET("msgSucesso")), HelperHttp.TYPE_MENSAGEM.MENSAGEM_OK, null));
			vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(
					HelperHttp.COMANDO_FECHAR_DIALOG_ATUALIZANDO, 
					HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, 
					false));
		}
		else if(__helper.GET("msgErro") != null){
			vBuilder.append(HelperHttp.imprimirMensagem(HelperHttp.nl2br(__helper.GET("msgErro")), HelperHttp.TYPE_MENSAGEM.MENSAGEM_ERRO, null));
			vBuilder.append(HelperHttp.imprimirComandoJavascriptComTimer(
					HelperHttp.COMANDO_FECHAR_DIALOG, 
					HelperHttp.TEMPO_PADRAO_FECHAR_DIALOG, 
					false));
		}

		vBuilder.append("</center>");
		return new ContainerResponse( __helper, vBuilder);
	}

}
