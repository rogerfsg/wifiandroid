package app.omegasoftware.wifitransferpro.gps;

public class GPSConfiguration {

	//Intents configuration
	public static final String REFRESH_UI_INTENT = "android.omegasoftware.gps.REFRESH_UI_INTENT";
	public static final String TAKE_PICTURE_INTENT = "android.omegasoftware.gps.TAKE_PICTURE_INTENT";

	//Service state intent extras configuration
	public static final String SS_GPS_STATE = "SS_GPS_STATE";
	public static final String SS_GPS_LOCATION_LATITUDE = "SS_GPS_LOCATION_LATITUDE";
	public static final String SS_GPS_LOCATION_LONGITUDE = "SS_GPS_LOCATION_LONGITUDE";


	//GPS configuration
	public static final int MIN_TIME_LOCATION_UPDATE = 100; //milliseconds
	public static final int MIN_DISTANCE_LOCATION_UPDATE = 5; //meters

	//Timing configurations
	public static final long INITIAL_DELAY = 2000;
	public static final long REPORTER_TIMER_INTERVAL = 30000;
	public static final long UI_UPDATE_TIMER_INTERVAL = 10000;
	public static final long PICTURE_TAKER_TIMER_INTERVAL = 120000;

	//Ponto Eletronico Take Picture
	//Timing configurations
	public static final long INITIAL_TAKE_PICTURE_TIMER = 3000;
	public static final long TAKE_PICTURE_TIMER = 1000;
	



}
