package app.omegasoftware.wifitransferpro.http;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;


public class ContainerResponse{
	public StringBuilder data;
	public InputStream input;
	private Properties header = new Properties();
	public String mimeType = NanoHTTPD.MIME_HTML;
	public String status = NanoHTTPD.HTTP_OK;
	public HelperHttp helper;
	public ContainerResponse(HelperHttp pHelper, Properties pHeader, StringBuilder pContent){
		header = pHeader;
		data = pContent;
		helper = pHelper;
	}

	public ContainerResponse(HelperHttp pHelper, StringBuilder pContent){
		header = new Properties();
		data = pContent;
		helper = pHelper;
	}

	public ContainerResponse(HelperHttp pHelper, String pMimeType, String pStatus, StringBuilder pData){
		mimeType = pMimeType;
		status = pStatus;
		data = pData;
		helper = pHelper;
	}

	public ContainerResponse(HelperHttp pHelper, String pMimeType, String pStatus, ByteArrayInputStream pStream){
		mimeType = pMimeType;
		status = pStatus;
		input = pStream;
		helper = pHelper;
	}
	
	public ContainerResponse(HelperHttp pHelper, String pMimeType, String pStatus, InputStream pStream){
		mimeType = pMimeType;
		status = pStatus;
		input = pStream;
		helper = pHelper;
	}

	public ContainerResponse(HelperHttp pHelper, Properties pHeader, ByteArrayInputStream pInput){
		header = pHeader;
		input = pInput;
		helper = pHelper;
	}

	public void mergeContainerResponse(ContainerResponse pContainer){
		appendData(pContainer.data);
	}

	public void addHeader(String pKey, String pContent){
		header.put(pKey, pContent);
	}

	public void appendData(StringBuilder pContent){
		data = data.append( pContent);
	}

	public void copyContainerResponse(ContainerResponse pContainer){
		if(pContainer == null) return;
		copyHeader(pContainer.header);
		if(pContainer.data != null)
			appendData(pContainer.data);
		else input = pContainer.input;
		
		mimeType = pContainer.mimeType;
		status = pContainer.status;
	}

	public void copyHeader(Properties pHeader){
		if(pHeader == null) return;
		Enumeration vKeysOrigem = pHeader.keys();

		while(vKeysOrigem.hasMoreElements()){
			Object vKeyOrigem = vKeysOrigem.nextElement();
			String vStrKey = (String) vKeyOrigem;
			String vStrContent = (String) pHeader.get(vKeyOrigem);
			
			header.put(vStrKey,vStrContent );
		}
	}

	public NanoHTTPD.Response getResponse(){

		NanoHTTPD.Response vResponse = null;
		if (input != null) 
			vResponse = helper.nanoHTTPD.factoryResponse(status, mimeType, input);
		else if(data != null)
			vResponse = helper.nanoHTTPD.factoryResponse(status, mimeType, data.toString());
		else vResponse = helper.nanoHTTPD.factoryResponse(status, mimeType, "");

		Enumeration vKeysOrigem = header.keys();
		
		while(vKeysOrigem.hasMoreElements()){
			try{
				Object vKeyOrigem = vKeysOrigem.nextElement();
				String vStrKey = (String) vKeyOrigem;
				String vStrContent = (String) header.get(vKeyOrigem);
				vResponse.addHeader(vStrKey, vStrContent);	
			} catch(Exception ex){
				
			}
			
		}
		return vResponse;
	}
}