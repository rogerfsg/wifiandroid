package app.omegasoftware.wifitransferpro.TabletActivities;


import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.common.activity.OmegaRegularActivity;
import app.omegasoftware.wifitransferpro.listener.BrowserOpenerListener;

public class ProblemaConexaoActivityMobile extends OmegaRegularActivity {
	public static String TAG = "AjudaActivityMobile";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.problema_conexao_activity_mobile_layout);
		
		initializeComponents();
		applyTypeFace();
	}
	
	private void applyTypeFace(){
		
		Typeface  customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		((TextView) findViewById(R.id.passo_1_textview)).setTypeface(customTypeFace);
		((TextView) findViewById(R.id.passo_2_textview)).setTypeface(customTypeFace);
		((TextView) findViewById(R.id.passo_3_textview)).setTypeface(customTypeFace);
		((TextView) findViewById(R.id.passo_4_textview)).setTypeface(customTypeFace);
		((TextView) findViewById(R.id.passo_5_textview)).setTypeface(customTypeFace);
		((TextView) findViewById(R.id.passo_6_textview)).setTypeface(customTypeFace);
		
	
		
	}
	
	
	@Override
	public boolean loadData() {
		//
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void initializeComponents() {
		
		// TODO Auto-generated method stub
		
	}
	
}
