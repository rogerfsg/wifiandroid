package app.omegasoftware.wifitransferpro.common.TextWatcher;


import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public abstract class InterfaceMaskTextWatcher implements TextWatcher{
	//	(31) 3333 - 3333
	//123456789A
	//  0,

	private enum TYPE  {
		INICIO, 
		FORMATANDO, 
		NOVO_CONTEUDO};

		//posicao considerando q o token vai ser colocado, logo se for no 0, ele sera
		//colocado antes do primeiro digito
		protected int __vetorPosicaoInicioToken[] ;
		protected String __vetorString[] ;
		String __conteudoBeforeModicacao;
		int __indexSelection;
		protected TYPE __type = TYPE.INICIO ;
		protected String __novoConteudo;
		protected int __maxSize ;
		protected boolean __isJustNumber = true;
		private EditText __editText;
		public InterfaceMaskTextWatcher(
				EditText pEditText,
				int pVetorPosicaoInicioToken[],
				String pVetorString[], 
				int pMaxSize, 
				boolean pIsJustNumber){
			__vetorPosicaoInicioToken = pVetorPosicaoInicioToken;
			__vetorString = pVetorString;
			__maxSize = pMaxSize;
			__isJustNumber = pIsJustNumber;
			__editText = pEditText;
			__editText.addTextChangedListener(this);
		}

		public InterfaceMaskTextWatcher(EditText pEditText, boolean pIsJustNumber){
			__vetorPosicaoInicioToken = null;
			__vetorString = null;
			__maxSize = -1;
			__isJustNumber = pIsJustNumber;
			__editText = pEditText;
			__editText.addTextChangedListener(this);
		}
		
		public InterfaceMaskTextWatcher(EditText pEditText, boolean pIsJustNumber, int pMaxSize){
			__vetorPosicaoInicioToken = null;
			__vetorString = null;
			__maxSize = pMaxSize;
			__isJustNumber = pIsJustNumber;
			__editText = pEditText;
			__editText.addTextChangedListener(this);
		}

		public String getTokenDaPosicao(int pPosicaoDesejada){
			int i = 0 ;
			if(this.__vetorPosicaoInicioToken == null) return null;
			for (int vPosicao : this.__vetorPosicaoInicioToken) {
				String vTokenDemarcada = __vetorString[i];

				int vPosicaoFinal = vPosicao + vTokenDemarcada.length();

				if(pPosicaoDesejada >= vPosicao &&  
						pPosicaoDesejada <= vPosicaoFinal) return vTokenDemarcada;

				i += 1;
			}
			return null;

		}


		public abstract String getValidToken(String pToken);

		public String appendMask(String pToken){
			if(this.__vetorPosicaoInicioToken == null && !__isJustNumber) return pToken;
			int i = -1 ;
			String vNewToken = "";
			for (char vLetra : pToken.toCharArray()) {
				i += 1;
				String vToken = this.getTokenDaPosicao(i);
				//se a posicao eh correspondente a uma mascara,
				//entao eh inserida no conteudo
				if(vToken != null){
					vNewToken += vToken;
					i += vToken.length();
					vNewToken += vLetra;
				} else if(isNumero(vLetra) && __isJustNumber){
					vNewToken += vLetra;
				} else vNewToken += vLetra; 
			}
			return vNewToken;
		}

		public boolean isNumero(char vUltimaLetra){
			if(vUltimaLetra >= 48 && 
					vUltimaLetra <= 57 ){
				return true;
			} else return false;
		}

		public boolean isLetraMaiuscula(char vUltimaLetra){
			if(vUltimaLetra >= 65 && 
					vUltimaLetra <= 90 ){
				return true;
			} else return false;
		}

		public boolean isLetraMinuscula(char vUltimaLetra){
			if(vUltimaLetra >= 97 && 
					vUltimaLetra <= 122 ){
				return true;
			} else return false;
		}

		public boolean isLetra(char vUltimaLetra){
			if(vUltimaLetra >= 65 && 
					vUltimaLetra <= 90 ){
				return true;
			} else if(vUltimaLetra >= 97 && 
					vUltimaLetra <= 122 ){
				return true;
			} else return false;
		}
		

		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			while(true){
				if(__type == TYPE.INICIO ){
					__conteudoBeforeModicacao = s.toString();
					String vConteudoValido = getValidToken(__conteudoBeforeModicacao);
					String vConteudoComMascara = appendMask(vConteudoValido);
					__novoConteudo = vConteudoComMascara;
					__type = TYPE.FORMATANDO;
					
					s.clear();
					break;

				}  else if(__type == TYPE.FORMATANDO ){
					__type = TYPE.NOVO_CONTEUDO;
					String vBackupNovoConteudo = __novoConteudo ;
					__novoConteudo = "";
					
					if(vBackupNovoConteudo == null || vBackupNovoConteudo.length() == 0){ 
						__type = TYPE.INICIO;
						continue;
					}
					else {
						s.append(vBackupNovoConteudo);
						break;
					}
					
					
				} else if(__type == TYPE.NOVO_CONTEUDO){
					String vBackupNovoConteudo = s.toString();
					if(__conteudoBeforeModicacao.length() > vBackupNovoConteudo.length()){
						__indexSelection -= __conteudoBeforeModicacao.length() - vBackupNovoConteudo.length();
					} else if(__conteudoBeforeModicacao.length() < vBackupNovoConteudo.length()){
						__indexSelection += __conteudoBeforeModicacao.length() - vBackupNovoConteudo.length();
					}
					__conteudoBeforeModicacao = "";
					__editText.setSelection(__indexSelection);
					__type = TYPE.INICIO;
					break;
				}
			}
		}
		
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			
		}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(__type == TYPE.INICIO ){
				__indexSelection = start;
				if (count > 0 ){
					__indexSelection += count;
				}
					
			} else if(__type == TYPE.FORMATANDO && __conteudoBeforeModicacao.length() == 0 ){
				__indexSelection = start;
				if (count > 0 ){
					__indexSelection += count;
				}
			}
		}


}