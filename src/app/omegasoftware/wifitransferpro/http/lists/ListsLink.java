package app.omegasoftware.wifitransferpro.http.lists;

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.Database;
import app.omegasoftware.wifitransferpro.database.Table;
import app.omegasoftware.wifitransferpro.database.EXTDAO.DatabaseWifiTransfer;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOLink;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Link;

public class ListsLink extends InterfaceHTML {

	//	PLAYLIST contem toda a lista de musicas para a montagem
	//	1 . Contem um filtro no cabecalho para filtragem das musicas, 
	//		1.1	Explorer que so exibe as musicas e diretorios

	StringBuilder vBuilder = new StringBuilder();
	String vStrPath ;
	String vPlaylist ;
	File vDirectory;
	ContainerResponse vContainer;
	public ListsLink(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContainerResponse getContainerResponse() {
		vContainer = new ContainerResponse(__helper, new StringBuilder());
		vBuilder.append("<td  >");

		procedurePrimeiraTabela();

		vBuilder.append("</td>");

		vContainer.appendData(vBuilder);

		return vContainer;
	}

	private void procedurePrimeiraTabela(){

		// TODO Auto-generated method stub

		vBuilder.append("	<!-- Framework CSS -->");

		vBuilder.append("<table class=\"tabela_list\">");
		vBuilder.append("<tbody>");

		Database db = new DatabaseWifiTransfer(__helper.context);
		EXTDAOLink vObjLink = new EXTDAOLink(db);
		ArrayList<Table> vListTupla = vObjLink.getListTable();
		vBuilder.append("<tr class=\"tr_list_conteudo_par\">");
		vBuilder.append("<td  class=\"td_list_conteudo\">");
		vBuilder.append("<div  class=\"div_galeria\">");

		if(vListTupla != null)
			for(int i = 0 ; i < vListTupla.size(); i++){


				Table vTupla = vListTupla.get(i);
				String vURLLink = vTupla.getStrValueOfAttribute(EXTDAOLink.URL);
				String vImagemLink = vTupla.getStrValueOfAttribute(EXTDAOLink.PATH_IMAGEM);
				String vNomeLink = vTupla.getStrValueOfAttribute(EXTDAOLink.NOME);
				String vIdLink = vTupla.getStrValueOfAttribute(EXTDAOLink.ID);

				vBuilder.append("<div  class=\"div_imagem_link\">");

				vBuilder.append("<img class=\"imagem_link\" src=\""
						+ vImagemLink + "\" onmouseover=\"javascript:tip('"
						+ __helper
						.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.TOOLTIP_ACESSAR_LINK)
						+ "')\" onclick=\"javascript:location.href='" + vURLLink + "'\" onmouseout=\"javascript:notip()\">&nbsp;");

				vBuilder.append("</br>");

				vBuilder.append(vNomeLink);

				vBuilder.append("</br>");
				
//				Excluir playlist
				Properties vLinkRemove = new Properties();
				vLinkRemove
						.put("onmouseover",
								"tip(\'"
										+ __helper
												.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
										+ "\', this);");
				vLinkRemove.put("onmouseout", "notip();");
				Link vObjLinkRemove = new Link(
						180,
						350,
						context.getResources().getString(
								R.string.deletar_arquivo),
						"actions.php5?class=" + EXTDAOArquivo.NAME + "&action=removeLink"
								+ "&link=" + vIdLink,
								vLinkRemove,
						"<img class=\"icones_list\" src=\""
								+ OmegaFileConfiguration
										.getPathTypeRelativePath(TYPE_RELATIVE_PATH.IMGS)
								+ "icone_excluir.png\" onmouseover=\"javascript:tip('"
								+ __helper
										.getMensagemTooltip(HelperHttp.TYPE_MENSAGEM_TOOLTIP.MENSAGEM_EXCLUSAO)
								+ "')\" onmouseout=\"javascript:notip()\">&nbsp;");
				vBuilder.append(vObjLinkRemove.montarLink());
				
				vBuilder.append("</br>");
				
				vBuilder.append("</div>");

			}

		db.close();

		vBuilder.append("</div>");
		vBuilder.append("</td>");
		vBuilder.append("</tr>");

		vBuilder.append("</tbody>");
		vBuilder.append("</table>");

	}


}
