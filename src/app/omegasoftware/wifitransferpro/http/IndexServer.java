package app.omegasoftware.wifitransferpro.http;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import android.content.Context;
import app.omegasoftware.wifitransferpro.PrincipalActivity;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.http.adm.Actions;
import app.omegasoftware.wifitransferpro.http.adm.Download;
import app.omegasoftware.wifitransferpro.http.adm.Image;
import app.omegasoftware.wifitransferpro.http.adm.Index;
import app.omegasoftware.wifitransferpro.http.adm.Popup;
import app.omegasoftware.wifitransferpro.http.adm.XML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.primitivetype.HelperInteger;

/**
 * An example of subclassing NanoHTTPD to make a custom HTTP server.
 */
public class IndexServer extends NanoHTTPD
{
	public static String PAG_HTML = "index.php5";
	Context context= PrincipalActivity.__principalActivity;
	
	
	
	public static String getRootAddress(String pIp){
		
		return "http://" + pIp + ":" + HelperPorta.getSavedPorta(PrincipalActivity.__principalActivity) + "/";
	}
	
	public static String getRootAddress(){
		
		return "http://" + HelperWifi.IP + ":" + HelperPorta.getSavedPorta(PrincipalActivity.__principalActivity) + "/";
	}
	
	public IndexServer() throws IOException
	{
		super(HelperInteger.parserInt( HelperPorta.getSavedPorta(PrincipalActivity.__principalActivity)), 
				OmegaFileConfiguration.getFileIndexSite(PrincipalActivity.__principalActivity) );

	}

	/**
	 * Basic constructor.
	 */
	public Response factoryResponse( String status, String mimeType, InputStream data )
	{
		return new Response(status, mimeType, data);
	}

	
	/**
	 * Convenience method that makes an InputStream out of
	 * given text.
	 */
	public Response factoryResponse( String txt )
	{
		return new Response(NanoHTTPD.HTTP_OK, NanoHTTPD.MIME_HTML, txt);
	}
	/**
	 * Convenience method that makes an InputStream out of
	 * given text.
	 */
	public Response factoryResponse( String status, String mimeType, String txt )
	{
		return new Response(status, mimeType, txt);
	}
	
	public Response serve( String uri, String method, Properties header, Properties parms, Properties files )
	{
		System.out.println( method + " '" + uri + "' " );
		HelperHttp vHelper = new HelperHttp(context, this, uri, method, header, parms, files);
		if(uri.compareTo("/") == 0 || uri.compareTo("/" + PAG_HTML) == 0 )
			return (new Index(vHelper)).getHTML();
		else if(uri.compareTo("/popup.php5" ) == 0 )
			return (new Popup(vHelper)).getHTML();
		else if(uri.compareTo("/download.php5" ) == 0 )
			return (new Download(vHelper)).getHTML();
		else if(uri.compareTo("/image.php5" ) == 0 )
			return (new Image(vHelper)).getHTML();
		else if(uri.compareTo("/actions.php5" ) == 0 )
			return (new Actions(vHelper)).getHTML();
		else if(uri.compareTo("/xml.php5" ) == 0 )
			return (new XML(vHelper)).getHTML();
		else 
			return super.serve(uri, method, header, parms, files);
		
	}


	public static void main( String[] args )
	{
		try
		{
			new IndexServer();
		}
		catch( IOException ioe )
		{
			System.err.println( "Couldn't start server:\n" + ioe );
			System.exit( -1 );
		}
		System.out.println( "Listening on port 8080. Hit Enter to stop.\n" );
		try { System.in.read(); } catch( Throwable t ) {};
	}
}