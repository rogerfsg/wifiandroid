package app.omegasoftware.wifitransferpro.listener;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnLongClickListener;
import app.omegasoftware.wifitransferpro.appconfiguration.OmegaConfiguration;

public class EditCustomOnLongClickListener implements OnLongClickListener{
	
		View __parentView;
		String __id;
		Class<?> __classFormEdit;
		Class<?> __classFormActionEdit;
		Activity __activity;
		String __nomeTabela;
		boolean __isEdit; 
		boolean __isRemove;
		
		public EditCustomOnLongClickListener(
				Activity pActivity, 
				String pNomeTabela,
				View pParentView, 
				String pId, 
				Class<?> pClassFormEdit,
				Class<?> pClassFormActionEdit,
				boolean pIsEdit, 
				boolean pIsRemove){
			__nomeTabela = pNomeTabela;
			__parentView = pParentView;
			__id = pId;
			__classFormEdit = pClassFormEdit;
			__classFormActionEdit = pClassFormActionEdit;
			__activity = pActivity;
			__isEdit = pIsEdit;
			__isRemove = pIsRemove;
		}
		
		
		public boolean onLongClick(View arg0) {
			
			Intent intent;
			
			intent = new Intent(__activity, __classFormEdit);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, __id);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_TABELA, __nomeTabela);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_CLASS_LIST, __classFormActionEdit);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_REMOVE_PERMITED, __isRemove);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_EDIT_PERMITED, __isEdit);
			
			
			__activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EDIT);
			
			// TODO Auto-generated method stub
			return false;
		}
		

}
