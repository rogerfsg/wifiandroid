package app.omegasoftware.wifitransferpro.http.pages;

import java.io.File;
import java.util.Properties;

import app.omegasoftware.wifitransferpro.R;
import app.omegasoftware.wifitransferpro.database.EXTDAO.EXTDAOArquivo;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile;
import app.omegasoftware.wifitransferpro.file.HelperFile;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration;
import app.omegasoftware.wifitransferpro.file.ContainerTypeFile.TYPE_FILE;
import app.omegasoftware.wifitransferpro.file.OmegaFileConfiguration.TYPE_RELATIVE_PATH;
import app.omegasoftware.wifitransferpro.http.ContainerResponse;
import app.omegasoftware.wifitransferpro.http.InterfaceHTML;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.Link;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM;
import app.omegasoftware.wifitransferpro.http.recursos.classes.classe.HelperHttp.TYPE_MENSAGEM_TOOLTIP;
import app.omegasoftware.wifitransferpro.primitivetype.HelperString;

public class ListsArquivoCopy extends InterfaceHTML{

	public ListsArquivoCopy(HelperHttp pHelper) {
		super(pHelper);
		// TODO Auto-generated constructor stub
	}


	@Override
	public ContainerResponse getContainerResponse() {
		String vStrPath = __helper.GET("novo_path") != null ? __helper.GET("novo_path") : __helper.GET(EXTDAOArquivo.PATH);
		if(vStrPath == null)
			vStrPath = HelperHttp.getPathRoot();
		StringBuilder vBuilder= new StringBuilder();
		if(vStrPath != null && vStrPath.length() > 0 ){
			File vFileDiretorioCorrente = new File(vStrPath);
			if(vFileDiretorioCorrente.isDirectory()){
				String vStrAction = HelperFile.getNomeSemBarra(__helper.GET("next_action"));
				
				vBuilder.append("      <form name=\"form_list_arquivo\" id=\"form_list_arquivo\" action=\"actions.php5\" method=\"POST\" enctype=\"multipart/form-data ");
				vBuilder.append("    		  onsubmit=\"return validarCampos();\" >");
				vBuilder.append("			<input type=\"hidden\" name=\"junk\" id=\"junk\" value=\"junk\">");
				vBuilder.append("			<input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"1\">");
				vBuilder.append("			<input type=\"hidden\" name=\"class\" id=\"class\" value=\"Arquivo\">");
				vBuilder.append("			<input type=\"hidden\" name=\"action\" id=\"action\" value=\"" + vStrAction + "\">");
				
				if(__helper.GET("is_multiple") != null)
					vBuilder.append(ContainerMultiple.getHTML());
				
				String valueNome = (__helper.GET(EXTDAOArquivo.NOME) == null) ? "" : __helper.GET(EXTDAOArquivo.NOME) ;
				String valuePath = (__helper.GET(EXTDAOArquivo.PATH) == null) ? "" : __helper.GET(EXTDAOArquivo.PATH) ;
				vBuilder.append("            <input type=\"hidden\" name=\"" + EXTDAOArquivo.NOME+ "1\" id=\"" + EXTDAOArquivo.NOME+ "1\" value=\"" + valueNome + "\">");
				vBuilder.append("            <input type=\"hidden\" name=\"" + EXTDAOArquivo.PATH+ "1\" id=\"" + EXTDAOArquivo.PATH+ "1\" value=\"" + valuePath+ "\">");
				String valueNovoPath = ((__helper.GET("novo_path") != null) ? 
							__helper.GET("novo_path") :
							valuePath);
				valueNovoPath = HelperFile.getNomeSemBarra(valueNovoPath);
				

				vBuilder.append("    		<table width=\"350\" align=\"top\" class=\"tabela_popup\">");
				
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td height=\"11\" align=\"left\" valign=\"middle\" class=\"textos5\" style=\"padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;\" >");
				
				vBuilder.append(HelperHttp.imprimirMensagem(context.getString(R.string.informacao_mover_arquivo), TYPE_MENSAGEM.MENSAGEM_INFO, null));
				
				
				vBuilder.append("    				</td>");
				vBuilder.append("    			</tr>");
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");

				vBuilder.append(HelperString.ucFirst(context.getString(R.string.path)) + ": <input type=\"text\" maxlength=\"200\" id=\"novo_path\" name=\"novo_path\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\"" + vStrPath+ "\"/>");
				vBuilder.append("    				</td>");
				vBuilder.append("    			</tr>");
				
				String vSufixURL = "";
				if(__helper.GET("is_multiple") == null){
					vBuilder.append("    			<tr>");
					vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
					
					String valueNovoNome = ((__helper.GET("novo_nome") != null) ? 
							__helper.GET("novo_nome") :
							valueNome);
					valueNovoNome = HelperFile.getNomeSemBarra(valueNovoNome);
					
					vBuilder.append(HelperString.ucFirst(context.getString(R.string.novo_nome)) + ": <input type=\"text\" maxlength=\"200\" id=\"novo_nome\" name=\"novo_nome\" style=\"width: 300px;\" class=\"input_text\" onfocus=\"this.className='focus_text';\" onblur=\"this.className='input_text';\" value=\"" +valueNovoNome + "\"/>");
					vBuilder.append("    				</td>");
					vBuilder.append("    			</tr>");
					vSufixURL = "&next_action=" + vStrAction + "&" + "novo_nome" + "='+document.getElementById('novo_nome').value+'" + "&" + EXTDAOArquivo.PATH +"=" + valuePath + "&" + EXTDAOArquivo.NOME + "=" + valueNome;
				} else{
					vSufixURL = "&next_action=" + vStrAction + "&" +"is_multiple" + "=" +  __helper.GET("is_multiple")  + "&" + EXTDAOArquivo.PATH +"=" + valuePath + "&" + EXTDAOArquivo.NOME + "=" + valueNome;
				}
				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td height=\"22\" align=\"left\" valign=\"middle\" style=\"padding-top: 5px; color: #333333; \" >");
				vBuilder.append("    					<input name=\"txtBotao\" type=\"submit\" value=\"" + context.getString(R.string.ok).toUpperCase() + "\" class=\"botoes_form\" style=\"cursor: pointer;\" />") ;
				
				vBuilder.append("    				</td>");
				
				vBuilder.append("    			</tr>");
				

				vBuilder.append("    			<tr>");
				vBuilder.append("    				<td height=\"11\" align=\"left\" valign=\"middle\" class=\"textos5\" style=\"padding-left: 5px; padding-top: 10px; padding-bottom: 10px; color: #333333;\" >");
				vBuilder.append(HelperHttp.getHTMLTreePath("popup.php5?tipo=pages&page=ListsArquivoCopy" + vSufixURL, "novo_path", vStrPath));
				
				vBuilder.append("    			</td>");
				vBuilder.append("    			</tr>");
				
				vBuilder.append("    		</table>");
				vBuilder.append("    	</form>");
				
				vBuilder.append(HelperHttp.getHTMLTabelaExplorerSimplificada(__helper, "novo_path", vStrPath, "tabela_popup","popup.php5?tipo=pages&page=ListsArquivoCopy" + vSufixURL));
				
			}
		}
		
		return new ContainerResponse( __helper, vBuilder);
	}

}
